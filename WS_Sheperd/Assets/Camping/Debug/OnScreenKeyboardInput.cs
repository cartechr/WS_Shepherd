using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnScreenKeyboardInput : MonoBehaviour
{
    public GameObject wKey;
    public GameObject aKey;
    public GameObject sKey;
    public GameObject dKey;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.W))
        {
            wKey.SetActive(true);
        }
        if (Input.GetKeyDown(KeyCode.A))
        {
            aKey.SetActive(true);
        }
        if (Input.GetKeyDown(KeyCode.S))
        {
            sKey.SetActive(true);
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            dKey.SetActive(true);
        }

        if (Input.GetKeyUp(KeyCode.W))
        {
            wKey.SetActive(false);
        }
        if (Input.GetKeyUp(KeyCode.A))
        {
            aKey.SetActive(false);
        }
        if (Input.GetKeyUp(KeyCode.S))
        {
            sKey.SetActive(false);
        }
        if (Input.GetKeyUp(KeyCode.D))
        {
            dKey.SetActive(false);
        }

    }
}
