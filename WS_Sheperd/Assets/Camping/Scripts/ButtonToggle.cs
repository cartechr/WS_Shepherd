using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ButtonToggle : MonoBehaviour
{
    // I named this script wrong, it is not a toggle... merely a timer.
    //This script is now defunct! all features are implemented into SheepZoomAndSwitch
    public Button TreatB;
    public Button BackB;
    public Button LeftB;
    public Button RightB;
    public SheepZoomAndSwitch SZAS;
    
    void Update()
    {
        
        if (SZAS.TreatButtonHit == true)
        {
            StartCoroutine(AllButtonsCooldown());
        }
        
    }

    public IEnumerator AllButtonsCooldown()
    {
        //Toggle interactable state of the Treat Button on and off
        TreatB.interactable = false;
        BackB.interactable = false;
        LeftB.interactable = false;
        RightB.interactable = false;

        yield return new WaitForSeconds(4);

        TreatB.interactable = true;
        BackB.interactable = true;
        LeftB.interactable = true;
        RightB.interactable = true;

        Debug.Log("Button cooldown is over");
    }




}
