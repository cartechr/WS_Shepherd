using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CampsitePlayerPoint : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnDrawGizmos()
    {
        Vector3 pos = transform.position;
        Gizmos.color = new Color(0, 1, 0, 1);
        Gizmos.DrawLine(pos - transform.right, pos + transform.right);
        Gizmos.DrawLine(pos + transform.up, pos - transform.up);
        Gizmos.DrawLine(pos - transform.forward, pos + transform.forward);
        Gizmos.DrawLine(pos + transform.forward, pos + transform.forward + transform.rotation * new Vector3(-0.2f, 0, -0.2f));
        Gizmos.DrawLine(pos + transform.forward, pos + transform.forward + transform.rotation * new Vector3(0.2f, 0, -0.2f));
    }
}
