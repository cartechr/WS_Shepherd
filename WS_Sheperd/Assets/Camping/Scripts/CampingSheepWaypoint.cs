using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CampingSheepWaypoint : MonoBehaviour
{

    public static List<CampingSheepWaypoint> CampingSheepWaypoints = new List<CampingSheepWaypoint>();

    public GameObject mySheep = null;

    public GameObject leftSheepPoint;
    public GameObject rightSheepPoint;
    public GameObject cinemachineCamera;
    
    // Start is called before the first frame update
    void Start()
    {
        GameEvents.ExitedCamp += BootOffSheep;
        CampingSheepWaypoints.Add(this);
    }

    private void OnDestroy()
    {
        GameEvents.ExitedCamp -= BootOffSheep;
        CampingSheepWaypoints.Remove(this);
    }

    public void ClaimSheep(GameObject sheep)
    {
        this.mySheep = sheep;
    }

    public void BootOffSheep(object sender, EventArgs args)
    {
        this.mySheep = null;
    }

    private void OnDrawGizmos()
    {
        Vector3 pos = transform.position;
        Gizmos.color = new Color(1, 0.5f, 0, 1);
        Gizmos.DrawLine(pos - transform.right, pos + transform.right);
        Gizmos.DrawLine(pos + transform.up, pos - transform.up);
        Gizmos.DrawLine(pos - transform.forward, pos + transform.forward);
        Gizmos.DrawLine(pos + transform.forward, pos + transform.forward + transform.rotation * new Vector3(-0.2f, 0, -0.2f));
        Gizmos.DrawLine(pos + transform.forward, pos + transform.forward + transform.rotation * new Vector3(0.2f, 0, -0.2f));
    }
}
