using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TentEnter : MonoBehaviour
{
    public CampingSceneSwitch parentScript;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (SceneManager.GetActiveScene().buildIndex == parentScript.nightSceneIndex && other.CompareTag("Player"))
        {
            parentScript.EnterTentAndSleep();
        }
        
    }
}
