using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using System;

public class CameraBasedInput : MonoBehaviour
{
    private Camera cinemachineCamera;

    Rigidbody rigidbody;

    public int playerSpeed;

    private ShepherdAnimationController sac;
    private GameObject cinmachineCamObject;

    private void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
        cinemachineCamera = Camera.main;

        sac = GetComponent<ShepherdAnimationController>();
        //cinmachineCamObject = GameObject.FindWithTag("MainCamera");
        cinmachineCamObject = GameObject.Find("First Camera");

        //Debug.Log("CBI: Initializing");
    }

    IEnumerator EnableOmniscentCameraAndDisableOtherScripts()
    {
        //Debug.Log("CBI: ENABLING");
        yield return new WaitUntil(() => (sac != null));
        sac.isOmniscentCamera = true;
        cinmachineCamObject.GetComponent<FirstPersonLook>().enabled = false;
        gameObject.GetComponent<FirstPersonMovement>().enabled = false;
        //Debug.Log("CBI: ENABLED");
    }

    private void OnEnable()
    {
        StartCoroutine(EnableOmniscentCameraAndDisableOtherScripts());
    }

    private void OnDisable()
    {
        //Debug.Log("CBI: DISABLING");
        sac.isOmniscentCamera = false;
        cinmachineCamObject.GetComponent<FirstPersonLook>().enabled = true;
        gameObject.GetComponent<FirstPersonMovement>().enabled = true;
        //Debug.Log("CBI: DISABLED");
    }

    // This used to be FixedUpdate, caused player to spin
    void Update()
    {
        CameraRelativeMovement();
    }

    void CameraRelativeMovement()
    {
        float pVerticalInput = Input.GetAxis("Vertical");
        float pHorizontalInput = Input.GetAxis("Horizontal");

        Vector3 forward = cinemachineCamera.transform.forward;
        forward.y = 0;
        forward = forward.normalized;
        Vector3 right = cinemachineCamera.transform.right;
        right.y = 0;
        right = right.normalized;

        Vector3 forwardRelativeVerticalInput = pVerticalInput * forward * playerSpeed;
        Vector3 rightRelativeVerticalInput = pHorizontalInput * right * playerSpeed;

        Vector3 cameraRelativeMovement = forwardRelativeVerticalInput + rightRelativeVerticalInput;

        //transform.Translate(cameraRelativeMovement, Space.World);
        rigidbody.velocity = new Vector3(cameraRelativeMovement.x, rigidbody.velocity.y, cameraRelativeMovement.z);

        if(cameraRelativeMovement != Vector3.zero)
        {
            transform.forward = cameraRelativeMovement;
        }
    }
}
