using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnlyEnableClosestCampsite : MonoBehaviour
{
    public GameObject[] nightTimeCampsites;
    public GameObject shepherdPlayer;

    void Start()
    {
        shepherdPlayer = GameObject.FindGameObjectWithTag("Player");
        nightTimeCampsites = GameObject.FindGameObjectsWithTag("Campsite");

        foreach(GameObject campsite in nightTimeCampsites)
        {
            campsite.gameObject.SetActive(false);
        }
        getClosestCampsite().SetActive(true);
    }

    void Update()
    {
        
    }

    private GameObject getClosestCampsite()
    {
        GameObject closestCampsite = null;
        float smallestDistance = Mathf.Infinity;
        foreach (GameObject cs in nightTimeCampsites)
        {
            float dist = Vector3.Distance(cs.gameObject.transform.position, shepherdPlayer.transform.position);
            if (dist < smallestDistance)
            {
                smallestDistance = dist;
                closestCampsite = cs.gameObject;
            }
        }
        return closestCampsite;
    }
}
