using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DisableObjectsOnCamping : MonoBehaviour
{
    public GameObject disableOnCampingParent;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if(SceneManager.GetActiveScene().buildIndex == 3) //night index, I'm going to refactor this later, hold me to it   -James
        {
            if (disableOnCampingParent.activeSelf == true)
            {
                disableOnCampingParent.SetActive(false);
            }
        } else
        {
            if (disableOnCampingParent.activeSelf == false)
            {
                disableOnCampingParent.SetActive(true);
            }
        }
    }
}
