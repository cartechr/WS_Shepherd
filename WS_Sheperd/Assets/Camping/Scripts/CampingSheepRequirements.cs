using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CampingSheepRequirements : MonoBehaviour
{
    public CampingSceneSwitch parentCampingSceneSwitch;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Sheep"))
        {
            parentCampingSceneSwitch.sheepCount++;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Sheep"))
        {
            parentCampingSceneSwitch.sheepCount--;
        }
    }
}
