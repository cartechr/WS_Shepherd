using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class SheepZoomAndSwitch : MonoBehaviour
{
    private List<CampingSheepWaypoint> sheepWaypoints;

    public GameObject currentlySelectedZoom;
    private GameObject previousSelectedZoom;

    public GameObject playerObject;

    public SheepHealth sheepHealth;
    public FakeShepherd fakeShepherd;

    public GameObject SheepHUDObject;
    public TextMeshProUGUI sheepInfoTextRight;
    public TextMeshProUGUI sheepInfoTextLeft;
    public TextMeshProUGUI sheepTreatButtonText;
    //public int statXCoordinatesRightSide;
    //public int statXCoordinatesLeftSide;

    public bool isZoomed = false;
    private bool isZooming = false;
    private bool enableMousePointerOnZoom = true;
    public bool TreatButtonHit = false;

 

    //public ButtonToggle buttonToggleTimerScript;
    public Button TreatB;
    public Button BackB;
    public Button LeftB;
    public Button RightB;

    void Start()
    {
        sheepWaypoints = new List<CampingSheepWaypoint>();
        foreach(Transform child in transform)
        {
            //Debug.Log("SHEEPZOOM: child found: " + child);
            sheepWaypoints.Add(child.GetComponent<CampingSheepWaypoint>());
        }
        //Debug.Log("SHEEPZOOM: sheep waypoints found: "+sheepWaypoints.Count);

        playerObject = GameObject.FindGameObjectWithTag("Player");
    }

    void Update()
    {
        //All controls here are placeholder to test how things look before I develop a way for the player to initiate this zoom with a specific sheep   -James // thank you James - Grayson
        if ((Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.Mouse0)) && !isZoomed)
        {
            currentlySelectedZoom = getClosestSheepWaypoint();
            if(currentlySelectedZoom != null)
            {
                currentlySelectedZoom.GetComponent<CampingSheepWaypoint>().cinemachineCamera.SetActive(true);
                Debug.Log("SHEEPZOOM: Closest Sheep Point: " + currentlySelectedZoom);
                isZoomed = true;
                sheepInfoTextRight.gameObject.SetActive(true);
                SheepHUDObject.SetActive(true);
                GameEvents.FakePlayerSwitchOn?.Invoke(this, playerObject);
                GameEvents.FakePlayerGoToPoint?.Invoke(this, currentlySelectedZoom);
                UpdateSheepInfoText(null);
                if (enableMousePointerOnZoom)
                {
                    //enable mouse
                    UnityEngine.Cursor.visible = true;
                    UnityEngine.Cursor.lockState = CursorLockMode.Confined;
                }
            }
        }
        if(Input.GetKeyDown(KeyCode.DownArrow) && isZoomed)
        {
            DisengageSheepZoom();
        }
        if (Input.GetKeyDown(KeyCode.RightArrow) && isZoomed)
        {
            ZoomToRightSheep();
        }
        if (Input.GetKeyDown(KeyCode.LeftArrow) && isZoomed)
        {
            ZoomToLeftSheep();
        }
    }

    public void DisengageSheepZoom()
    {
        UnityEngine.Cursor.visible = false;
        Debug.Log("SHEEPZOOM: Sheep Zoom disengaging");
        currentlySelectedZoom.GetComponent<CampingSheepWaypoint>().cinemachineCamera.SetActive(false);
        currentlySelectedZoom = null;
        isZoomed = false;
        toggleInfoTextSide("hide");
        SheepHUDObject.SetActive(false);
        //UpdateSheepInfoText(); 
        GameEvents.FakePlayerSwitchOff?.Invoke(this, playerObject);
    }

    public void ZoomToLeftSheep()
    {
        isZooming = true;
        previousSelectedZoom = currentlySelectedZoom;
        while (isZooming)
        {
            Debug.Log("SHEEPZOOM: Switching to Leftmost point: " + currentlySelectedZoom.GetComponent<CampingSheepWaypoint>().leftSheepPoint);
            if (currentlySelectedZoom.GetComponent<CampingSheepWaypoint>().leftSheepPoint.GetComponent<CampingSheepWaypoint>().mySheep != null)
            {
                previousSelectedZoom.GetComponent<CampingSheepWaypoint>().cinemachineCamera.SetActive(false);
                currentlySelectedZoom = currentlySelectedZoom.GetComponent<CampingSheepWaypoint>().leftSheepPoint;
                currentlySelectedZoom.GetComponent<CampingSheepWaypoint>().cinemachineCamera.SetActive(true);
                GameEvents.FakePlayerGoToPoint?.Invoke(this, currentlySelectedZoom);

                UpdateSheepInfoText("left");
                //toggleInfoTextSide("left"); //In FakeShepherd.cs now

                isZooming = false;
            }
            else
            {
                currentlySelectedZoom = currentlySelectedZoom.GetComponent<CampingSheepWaypoint>().leftSheepPoint;
            }
        }
    }

    public void ZoomToRightSheep()
    {
        isZooming = true;
        previousSelectedZoom = currentlySelectedZoom;
        while (isZooming)
        {
            Debug.Log("SHEEPZOOM: Switching to Rightmost point: " + currentlySelectedZoom.GetComponent<CampingSheepWaypoint>().rightSheepPoint);
            if (currentlySelectedZoom.GetComponent<CampingSheepWaypoint>().rightSheepPoint.GetComponent<CampingSheepWaypoint>().mySheep != null)
            {
                previousSelectedZoom.GetComponent<CampingSheepWaypoint>().cinemachineCamera.SetActive(false);
                currentlySelectedZoom = currentlySelectedZoom.GetComponent<CampingSheepWaypoint>().rightSheepPoint;
                currentlySelectedZoom.GetComponent<CampingSheepWaypoint>().cinemachineCamera.SetActive(true);
                GameEvents.FakePlayerGoToPoint?.Invoke(this, currentlySelectedZoom);

                UpdateSheepInfoText("right");
                //toggleInfoTextSide("right"); //In FakeShepherd.cs now

                isZooming = false;
            }
            else
            {
                currentlySelectedZoom = currentlySelectedZoom.GetComponent<CampingSheepWaypoint>().rightSheepPoint;
            }
        }
    }

    public void toggleInfoTextSide(String input)
    {
        if(input == "left")
        {
            sheepInfoTextRight.gameObject.SetActive(false);
            sheepInfoTextLeft.gameObject.SetActive(true);
        } else if (input == "right")
        {
            sheepInfoTextRight.gameObject.SetActive(true);
            sheepInfoTextLeft.gameObject.SetActive(false);
        } else if (input == "hide")
        {
            sheepInfoTextRight.gameObject.SetActive(false);
            sheepInfoTextLeft.gameObject.SetActive(false);
        }
    }

    public void UpdateSheepInfoText(String rightleft)
    {
        GameObject sheep = currentlySelectedZoom.GetComponent<CampingSheepWaypoint>().mySheep;
        String sheepName = sheep.name;
        float sheepFriendship = sheep.GetComponent<SheepFriendship>().friendship;
        int sheepHealth = sheep.GetComponent<SheepHealth>().currentHealth;
        int sheepFood = sheep.GetComponent<SheepHealth>().food;
        int sheepWater = sheep.GetComponent<SheepHealth>().water;

        sheepInfoTextRight.SetText("Name: " + sheepName + "\nHealth: " + sheepHealth + "\nFriendship: " + sheepFriendship + "\nFood Level: " + sheepFood + "\nWater Level: " + sheepWater);
        sheepInfoTextLeft.SetText("Name: " + sheepName + "\nHealth: " + sheepHealth + "\nFriendship: " + sheepFriendship + "\nFood Level: " + sheepFood + "\nWater Level: " + sheepWater);
        sheepTreatButtonText.SetText("Feed Treat x (" + playerObject.GetComponent<Player>().getPlayerTreats() + ")");
        /*
        if (rightleft == "right")
        {
            sheepInfoTextRight.SetText("Name: " + sheepName + "\nHealth: " + sheepHealth + "\nFriendship: " + sheepFriendship + "\nFood Level: " + sheepFood + "\nWater Level: " + sheepWater);
        } else if (rightleft == "left")
        {
            sheepInfoTextLeft.SetText("Name: " + sheepName + "\nHealth: " + sheepHealth + "\nFriendship: " + sheepFriendship + "\nFood Level: " + sheepFood + "\nWater Level: " + sheepWater);
        } else
        {
            sheepInfoTextRight.SetText("Name: " + sheepName + "\nHealth: " + sheepHealth + "\nFriendship: " + sheepFriendship + "\nFood Level: " + sheepFood + "\nWater Level: " + sheepWater);
            sheepInfoTextLeft.SetText("Name: " + sheepName + "\nHealth: " + sheepHealth + "\nFriendship: " + sheepFriendship + "\nFood Level: " + sheepFood + "\nWater Level: " + sheepWater);
        }
        */
    }

    //enum thingy, don worry bout it
   
    public void SheepTreatButtonPressed()
    {
        StartCoroutine(AllButtonsCooldown());
        GameObject sheepCurrent = currentlySelectedZoom.GetComponent<CampingSheepWaypoint>().mySheep;
        playerObject.GetComponent<Player>().TreatFeed(sheepCurrent);
        SheepHealth myCurrentSheepHealth = sheepCurrent.GetComponent<SheepHealth>();
        UpdateSheepInfoText("both");
        Debug.Log("coolDown for treats started");

        if (myCurrentSheepHealth.mySheepType == SheepHealth.SheepType.Large)
        {
            Debug.Log("Large sheep detected");
            StartCoroutine(PettingLargeAnim());
        }
        if (myCurrentSheepHealth.mySheepType == SheepHealth.SheepType.Small)
        {
            Debug.Log("Small sheep detected");
            StartCoroutine(PettingSmallAnim());
        }
    }

    public IEnumerator PettingLargeAnim()
    {
        fakeShepherd.animator.SetBool("IsPetLarge", true);

        yield return new WaitForSeconds(0.001f);
        fakeShepherd.animator.SetBool("IsPetLarge", false);
    }
    public IEnumerator PettingSmallAnim()
    {
        fakeShepherd.animator.SetBool("IsPetSmall", true);

        yield return new WaitForSeconds(0.001f);
        fakeShepherd.animator.SetBool("IsPetSmall", false);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "FakeShepherd")
        {
            other.GetComponent<FakeShepherd>().activeCampsitePoints = this;
        }
    }

    private GameObject getClosestSheepWaypoint()
    {
        GameObject closestPoint = null;
        float smallestDistance = Mathf.Infinity;
        foreach(CampingSheepWaypoint gsw in sheepWaypoints)
        {
            float dist = Vector3.Distance(gsw.gameObject.transform.position, playerObject.transform.position);
            if(dist < smallestDistance && gsw.mySheep != null)
            {
                smallestDistance = dist;
                closestPoint = gsw.gameObject;
            }
        }
        return closestPoint;
    }

    public IEnumerator AllButtonsCooldown()
    {
        //Toggle interactable state of the Treat Button on and off
        TreatB.interactable = false;
        BackB.interactable = false;
        LeftB.interactable = false;
        RightB.interactable = false;

        yield return new WaitForSeconds(10);

        TreatB.interactable = true;
        BackB.interactable = true;
        LeftB.interactable = true;
        RightB.interactable = true;

        Debug.Log("Button cooldown is over");
    }
}
