using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FaderScript : MonoBehaviour
{
    public GameObject darkness;
    public void Update()
    {
        if(Input.GetKeyDown(KeyCode.M))
        {
            StartCoroutine(fadeDarkness());
        }
        if(Input.GetKeyDown(KeyCode.N))
        {
            StartCoroutine(fadeDarkness(false));
        }
    }

    public IEnumerator fadeDarkness(bool fadetoBlack = true, int fadeSpeed = 5)
    {   
        Color objectColor = darkness.GetComponent<Image>().color;
        float fadeAmount;

        if (fadetoBlack)
        {
            while (darkness.GetComponent<Image>().color.a < 1)
            {
                fadeAmount = objectColor.a + (fadeSpeed * Time.deltaTime);

                objectColor = new Color(objectColor.r, objectColor.g, objectColor.b, fadeAmount);
                darkness.GetComponent<Image>().color = objectColor;
                yield return null;
            }
        } else
        {
            while (darkness.GetComponent<Image>().color.a > 0)
            {
                fadeAmount = objectColor.a - (fadeSpeed * Time.deltaTime);
                objectColor = new Color(objectColor.r, objectColor.g, objectColor.b, fadeAmount);
                darkness.GetComponent<Image>().color = objectColor;
                yield return null;
            }
        }
    }
}