using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CampingSceneSwitch : MonoBehaviour
{
    public bool CampingPossible = false;
    public int levelToLoad;
    private int levelIndex;
    private bool isPlayerSetUp;
    private bool playerFullySetUp = false;

    public GameObject playerObject;
    public GameObject sheepObject;
    public GameObject crookObject;
    public GameObject gameController;
    public GameObject campsiteCamera;
    public GameObject playerPoint;

    public GameObject CSP1;
    public GameObject CSP2;
    public GameObject CSP3;
    public GameObject CSP4;
    public GameObject CSP5;
    public GameObject CSP6;
    public GameObject CSP7;

    public int nightSceneIndex = 3;
    public int mainSceneIndex = 1;

    public Animator animator;

    public int sheepCount = 0;
    public int aliveSheep = 7;

    private Controls controls;

    void Start() {
        gameController = GameObject.Find("GameController");
        controls = gameController.GetComponent<GameController>().controls;
        isPlayerSetUp = false;
        StartCoroutine(ReassignPlayerSheepObjectsIfNecessary());
    }

    void Update()
    {
        if(Input.GetKeyDown(controls.camp) && CampingPossible == true)
        {
            FadeSceneSwitch(levelToLoad);
        }

        if (isPlayerSetUp && !playerFullySetUp)
        {
            EnableLocalCamera();
            playerFullySetUp = true;
        }
    }

    public void CheckIfSheepCampingRequirementsMet()
    {
        if(sheepCount == aliveSheep)
        {
            FadeSceneSwitch(nightSceneIndex);
        }
    }

    public void EnterTentAndSleep()
    {
        FadeSceneSwitch(mainSceneIndex);
    }

    private void FadeSceneSwitch(int levelToLoad)
    {
        levelIndex = levelToLoad;
        animator.SetTrigger("FadeOutTrigger");
        DontDestroyOnLoad(playerObject);
        DontDestroyOnLoad(sheepObject);
        DontDestroyOnLoad(gameController);
        tutorialGameEvents.Friendship(this, EventArgs.Empty);
    }

    public void OnFadeComplete()
    {
        SceneManager.LoadScene(levelIndex);
    }

    public void FadeInSummonSheep()
    {
        Debug.Log("Active Scene = "+SceneManager.GetActiveScene().buildIndex);
        if (SceneManager.GetActiveScene().buildIndex == nightSceneIndex)
        {
            Debug.Log("Calling Upon the Sheep to their Camping Positions!");
            GameEvents.EnteredCamp?.Invoke(this, EventArgs.Empty);
            
        }
        if (SceneManager.GetActiveScene().buildIndex == mainSceneIndex)
        {
            //playerObject.GetComponentInChildren<Crook>().gameObject.SetActive(true);
            Debug.Log("Dispersing Sheep From their Camping Positions!");
            GameEvents.ExitedCamp?.Invoke(this, EventArgs.Empty);
            
        }
        
        StartCoroutine(ReassignPlayerSheepObjectsIfNecessary());
    }

    private void OnTriggerEnter(Collider other)
    {
        //Detects if the player enters the collider
        if (other.CompareTag("Player"))
        {
            CampingPossible = true;
            StartCoroutine(ReassignPlayerSheepObjectsIfNecessary());
        }
        if (other.CompareTag("Sheep"))
        {
            
        }
    }


    private void OnTriggerExit(Collider other)
    {
        //Detects if the player exits the collider
        if (other.CompareTag("Player"))
        {
            CampingPossible = false;
        }

        if (other.CompareTag("Sheep"))
        {
            
        }
    }
    IEnumerator ReassignPlayerSheepObjectsIfNecessary()
    {
        yield return new WaitUntil(() => (GameObject.FindGameObjectsWithTag("Player").Length == 1));
        if (playerObject == null)
        {
            playerObject = GameObject.FindGameObjectWithTag("Player");
        }
        if (sheepObject == null)
        {
            sheepObject = GameObject.FindGameObjectWithTag("Sheep_Group");
        }
        if (gameController == null)
        {
            gameController = GameObject.Find("GameController");
        }
        if (crookObject == null)
        {
            crookObject = playerObject.GetComponentInChildren<Crook>(true).gameObject;
        }
        Debug.Log("Duplicate Player Checks performed!");
        setUpPlayer();
    }

    private void setUpPlayer()
    {
        if (SceneManager.GetActiveScene().buildIndex == nightSceneIndex)
        {
            if (Vector3.Distance(playerObject.transform.position, transform.position) < 20)
            {
                //Teleports player to designated Player Point in campsite when they are found in the NightScene and within range
                playerObject.transform.position = playerPoint.transform.position;
                playerObject.transform.rotation = playerPoint.transform.rotation;
            }
        }

        StartCoroutine(toggleCrook());
        isPlayerSetUp = true;
    }

    private void EnableLocalCamera() //Enables local camera and Omniscent Camera
    {
        if (SceneManager.GetActiveScene().buildIndex == nightSceneIndex)
        {
            if (Vector3.Distance(playerObject.transform.position, transform.position) < 20)
            {
                campsiteCamera.SetActive(true);
                playerObject.GetComponent<CameraBasedInput>().enabled = true;
                //playerObject.GetComponentInChildren<Crook>().gameObject.SetActive(false);
            }
        } else if (SceneManager.GetActiveScene().buildIndex == mainSceneIndex)
        {
            if (playerObject != null)
            {
                playerObject.GetComponent<CameraBasedInput>().enabled = false;
                
            }
        }
        
    }

    IEnumerator toggleCrook()
    {
        yield return new WaitUntil(() => (crookObject != null));
        if (SceneManager.GetActiveScene().buildIndex == nightSceneIndex && crookObject.activeSelf)
        {
            Debug.Log("CROOK: set to false");
            crookObject.SetActive(false);
        }
        else if (SceneManager.GetActiveScene().buildIndex == mainSceneIndex && !crookObject.activeSelf)
        {
            Debug.Log("CROOK: set to true");
            crookObject.SetActive(true);
        }
    }

}