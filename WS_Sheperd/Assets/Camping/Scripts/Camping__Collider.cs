using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Camping__Collider : MonoBehaviour
{
    static public int ExistingSheep = 0;
    public int SheepInside = 0;
    public bool CampingPossible = false;
    public GameObject Barrier;

    public GameObject fire;

    public GameObject darkness;

    public GameObject tent;

    public int timer = -1;


    static public bool Camping = false;


    void Start()
    {
        StartCoroutine(fadeDarkness(false));
        Barrier.SetActive(false);
        fire.SetActive(false);
        tent.SetActive(false);

    }

    void Update()
    {
        if (timer == 0)
        {
            StartCoroutine(fadeDarkness(false));
            Debug.Log("Timer == 0");
        }
        if (timer >= 0)
        {
            timer = timer - 1;
        }
    
        if (Input.GetKeyDown(KeyCode.Space) && CampingPossible == true && Camping == false && SheepHealth.sheepList.Count == SheepInside && timer <= 0)
        {
            ///Activates the barrier
            Barrier.SetActive(true);
            Camping = true;
            StartCoroutine(fadeDarkness());
            fire.SetActive(true);
            tent.SetActive(true);
            timer = 200;
            
            Debug.Log("Camping Successful");
            
        }
        if (Input.GetKeyDown(KeyCode.Space) && CampingPossible == true && Camping == false && SheepHealth.sheepList.Count != SheepInside)
        {
            ///Checks if there is enough sheep
            Debug.Log("NOT ENOUGH SHEEP!");
        }
        if (Input.GetKeyDown(KeyCode.X) && CampingPossible == true && Camping == true  && timer <= 0)
        {
            ///Removes the barrier
            Barrier.SetActive(false);
            Camping = false;
            StartCoroutine(fadeDarkness());
            timer = 200;
            fire.SetActive(false);
            tent.SetActive(false);
            Debug.Log("Camping Done!");
            
        }
        if (Input.GetKeyDown(KeyCode.L))
        {
            ///Adds a theoretical sheep
            ExistingSheep = ExistingSheep + 1;
        }
        if (Input.GetKeyDown(KeyCode.K))
        {
            ///Removes a theoretical sheep
            ExistingSheep = ExistingSheep - 1;
        }
        if (Input.GetKeyDown(KeyCode.C))
        {
            ///Returns the Camping variable
            Debug.Log(Camping);
        }
        if(Input.GetKeyDown(KeyCode.M))
        {
            StartCoroutine(fadeDarkness());
        }
        if(Input.GetKeyDown(KeyCode.N))
        {
            StartCoroutine(fadeDarkness(false));
        }

    

    }


    private void OnTriggerEnter(Collider other)
    {
        //Detects if the player enters the collider
        if (other.CompareTag("Player"))
        {
            CampingPossible = true;
        }   
        if (other.CompareTag("Sheep"))
        {
            SheepInside = SheepInside + 1;
        }   
    }

    private void OnTriggerExit(Collider other)
    {
        //Detects if the player exits the collider
        if (other.CompareTag("Player"))
        {
            CampingPossible = false;
        }

        if (other.CompareTag("Sheep"))
        {
            SheepInside = SheepInside - 1;
        }   
    }

    public IEnumerator fadeDarkness(bool fadetoBlack = true, int fadeSpeed = 5)
    {   
        Color objectColor = darkness.GetComponent<Image>().color;
        float fadeAmount;

        if (fadetoBlack)
        {
            while (darkness.GetComponent<Image>().color.a < 1)
            {
                fadeAmount = objectColor.a + (fadeSpeed * Time.deltaTime);

                objectColor = new Color(objectColor.r, objectColor.g, objectColor.b, fadeAmount);
                darkness.GetComponent<Image>().color = objectColor;
                yield return null;
            }
        } else
        {
            while (darkness.GetComponent<Image>().color.a > 0)
            {
                fadeAmount = objectColor.a - (fadeSpeed * Time.deltaTime);
                objectColor = new Color(objectColor.r, objectColor.g, objectColor.b, fadeAmount);
                darkness.GetComponent<Image>().color = objectColor;
                yield return null;
            }
        }
    }
}