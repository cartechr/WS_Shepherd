using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainTheme : MonoBehaviour
{


    FMOD.Studio.EventInstance mainTheme;
    
    void Start()
    {
        FMODUnity.RuntimeManager.StudioSystem.setParameterByName("GameState", 2 );
        mainTheme = FMODUnity.RuntimeManager.CreateInstance("event:/MUSIC/Main Menu");
        mainTheme.start();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void StopMusic()
    {
        mainTheme.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
    }
}
