
using UnityEngine;

public class MoveStorm : MonoBehaviour

{
    public WindZone windZone;

    private new ParticleSystem particleSystem;

    private void Start()
    {
        particleSystem = GetComponent<ParticleSystem>();
    }

    private void Update()
    {
        particleSystem.transform.rotation = Quaternion.Euler(0f, windZone.transform.eulerAngles.y, 0f);
    }
}
