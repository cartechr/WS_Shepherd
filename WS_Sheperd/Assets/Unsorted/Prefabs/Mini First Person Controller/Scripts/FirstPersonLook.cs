﻿using System;
using UnityEngine;

public class FirstPersonLook : MonoBehaviour
{
    [SerializeField]
    Transform character;
    public float sensitivity = 2;
    public float smoothing = 1.5f;

    [SerializeField] Vector2 rotation;
    Vector2 frameVelocity;


    void Reset()
    {
        // Get the character from the FirstPersonMovement in parents.
        //character = GetComponentInParent<FirstPersonMovement>().transform;
        character = transform.parent;
    }

    private void Awake()
    {
        character = transform.parent;
        rotation = new Vector2(character.eulerAngles.y, 0);
        //frameVelocity = rotation;
        Debug.Log(rotation);
    }

    void Start()
    {
        // Lock the mouse cursor to the game screen.
        Cursor.lockState = CursorLockMode.Locked;
        
        
    }

    void Update()
    {
        // Get smooth velocity.
        Vector2 mouseDelta = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));
        Vector2 rawFrameVelocity = Vector2.Scale(mouseDelta, Vector2.one * sensitivity);
        frameVelocity = Vector2.Lerp(frameVelocity, rawFrameVelocity, 1 / smoothing);
        rotation += frameVelocity;
        //rotation.y = Mathf.Clamp(rotation.y, -90, 90);

        // Rotate camera up-down and controller left-right from velocity.
        //transform.localRotation = Quaternion.AngleAxis(-velocity.y, Vector3.right);
        character.localRotation = Quaternion.AngleAxis(rotation.x, Vector3.up);
    }
}
