﻿using System.Collections.Generic;
using UnityEngine;

public class FirstPersonMovement : MonoBehaviour
{
    public float speed = 5;

    [Header("Running")]
    public bool canRun = true;
    public bool IsRunning { get; private set; }
    public float runSpeed = 9;
    public KeyCode runningKey = KeyCode.LeftShift;
    public GameObject firstPersonAudio;
    public GameObject Shepherd;

    public Camera_Swap_Shep css;

    Animator animator;
    Rigidbody rigidbody;
    /// <summary> Functions to override movement speed. Will use the last added override. </summary>
    public List<System.Func<float>> speedOverrides = new List<System.Func<float>>();


    //FMOD STUFF
    public bool IsWalking = false;
    private float horizontalInput;
    private float forwardInput;
    private Controls controls;

    void Awake()
    {
        // Get the rigidbody on this.
        rigidbody = GetComponent<Rigidbody>();
        animator = GetComponent<Animator>();
    }

    void Start() {
        controls = Shepherd.GetComponent<Player>().gamecontroller.controls;
    }
    
    void FixedUpdate()
    {

        PlayerMovement();
    }

    public void PlayerMovement()
    {
        //NO FOOTSTEP IN CUTSCENE
        var emitter = firstPersonAudio.GetComponent<FMODUnity.StudioEventEmitter>();
        Player player = Shepherd.GetComponent<Player>();
        if (css.CanMove == false)
            {
                emitter.SetParameter("walking", 0);
                //Debug.Log("cutscene means no noise");
            }

        if (player.FMOD_is_on_land == 1)
        {
            emitter.SetParameter("Water", 1);
        }

        if (player.FMOD_is_on_land == 0)
        {
            emitter.SetParameter("Water", 0);
        }

        //WALKING STUFF
        if (css.CanMove == true)
        {
            // Update IsRunning from input.
            IsRunning = canRun && Input.GetKey(runningKey);

            // Get targetMovingSpeed.
            float targetMovingSpeed = IsRunning ? runSpeed : speed;
            if (speedOverrides.Count > 0)
            {
                targetMovingSpeed = speedOverrides[speedOverrides.Count - 1]();
            }

            // Get targetVelocity from input.
            Vector2 targetVelocity = new Vector2(controls.getHorizontal(), controls.getVertical());
            targetVelocity.Normalize();
            targetVelocity = targetVelocity * targetMovingSpeed;

            //used for footsteps-ben
            //horizontalInput = Input.GetAxis("Horizontal");
            //forwardInput = Input.GetAxis("Vertical");

            //animator.SetFloat("Walking", forwardInput);
            // Apply movement.
            rigidbody.velocity = transform.rotation * new Vector3(targetVelocity.x, rigidbody.velocity.y, targetVelocity.y);

            /*footsteps noise
            if ((horizontalInput != 0 || forwardInput != 0) && IsWalking == false)
            {
                IsWalking = true;
                emitter.SetParameter("walking", 1);
            }
            else if ((horizontalInput == 0 && forwardInput == 0) && IsWalking == true)
            {
                IsWalking = false;
                emitter.SetParameter("walking", 0);
            }*/
        }
    }
    
}