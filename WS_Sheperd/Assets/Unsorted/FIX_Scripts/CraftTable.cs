using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CraftTable : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Within Crafting Range");
        if (other.gameObject.CompareTag("Craft_Range") && Input.GetKeyDown(KeyCode.C))
        {
            Debug.Log("Crafting");
        }
    }

}
