using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneSelect : MonoBehaviour
{
    public MainTheme menuMusic;
    public void SelectScene(string currentButton)
    {
        switch(currentButton)
        {
            case "start":
                SceneManager.LoadScene("Ricky'sMain");
                FMODUnity.RuntimeManager.StudioSystem.setParameterByName("GameState", 0 );
                menuMusic.StopMusic();
                break;
            case "credits":
                SceneManager.LoadScene("InfoScene");
                break;
            case "exit":
                Application.Quit();
                Debug.Log("quit");
                break;
            case "menu":
                // Probably change this to 
                SceneManager.LoadScene("Main Menu");
                break;
        }
    }
}
