using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace Shepherd
{
    public class SheepController : MonoBehaviour
    {
        [Header("Components")]
        [SerializeField] private NavMeshAgent navMeshAgent = null;
        [SerializeField] private Transform shepherd = null;



        [Header("Attributes")]
        [SerializeField] private string sheepName = "";
        [SerializeField] private float distanceBetweenShepherd = 0.5f;
        [SerializeField] private int health = 100;
        public int Health { get => health; }
        [SerializeField] private int thirst = 100;
        public int Thirst { get => thirst; }
        [SerializeField] private int hunger = 100;
        public int Hunger { get => hunger; }



        /* States */
        private SheepState sheepState = SheepState.Normal;
        public SheepState SheepState { get => sheepState; }



        private void Update()
        {
            if (sheepState == SheepState.Normal)
            {
                if (Vector3.Distance(transform.position, shepherd.position) > distanceBetweenShepherd)
                {
                    navMeshAgent.isStopped = false;
                    navMeshAgent.SetDestination(shepherd.position);
                }
                else
                {
                    navMeshAgent.isStopped = true;

                    // Look at the shepherd
                    Vector3 dir = (shepherd.position - transform.position).normalized;
                    Vector3 rot = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(dir), Time.deltaTime).eulerAngles;
                    Vector3 ogRot = transform.rotation.eulerAngles;
                    transform.rotation = Quaternion.Euler(ogRot.x, rot.y, ogRot.z);
                }
            } 
        }

        public void GetTrapped()
        {
            sheepState = SheepState.Trapped;
            navMeshAgent.enabled = false;
        }

        public void GetRescued()
        {
            sheepState = SheepState.Normal;
            navMeshAgent.enabled = true;
        }

        public void GetAttacked(int damage)
        {
            health -= damage;
        }

        private void OnTriggerEnter(Collider collider)
        {
            if (collider.CompareTag("Traps"))
            {
                GetTrapped();
            }
        }
    }
}