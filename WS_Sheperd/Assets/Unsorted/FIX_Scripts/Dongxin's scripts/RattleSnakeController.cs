using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace Shepherd
{
    public class RattleSnakeController : MonoBehaviour
    {
        /* Components */
        [Header("Components")]
        [SerializeField] private NavMeshAgent navMeshAgent = null;
        [SerializeField] private Transform rock = null;



        /* Parameters */
        [Header("Parameters")]
        [SerializeField] private float alertRadius = 2.5f;
        [SerializeField] private float attackingRange = 3f;
        [SerializeField] private LayerMask sheepLayer = default(LayerMask);
        [SerializeField] private float bitingInterval = 0.5f;
        [SerializeField] private int bitingDamage = 1;



        private List<GameObject> sheepInRangeList = new List<GameObject>();
        private Transform target = null;
        private SheepController targetSheep = null;
        private bool isBiting = false;



        private void Update()
        {
            if (target != null)
            {
                navMeshAgent.destination = target.position;

                float distance = Vector3.Distance(transform.position, target.position);
                if (distance > attackingRange)
                {
                    target = null;
                    targetSheep = null;
                }
                else
                {
                    if (distance < attackingRange)
                    {
                        if (isBiting == false)
                            StartCoroutine(Bite());
                    }
                }
            }
            else
            {
                bool hasTarget = false;
                for (int i = 0; i < sheepInRangeList.Count; i++)
                {
                    GameObject sheepGo = sheepInRangeList[i];
                    if (Vector3.Distance(transform.position, sheepGo.transform.position) <= attackingRange)
                    {
                        target = sheepGo.transform;
                        targetSheep = sheepGo.GetComponent<SheepController>();
                        navMeshAgent.destination = target.position;
                        hasTarget = true;
                        break;
                    }
                }

                if (!hasTarget)
                {
                    navMeshAgent.destination = rock.position;
                }
            }
        }

        private void OnTriggerEnter(Collider collider)
        {
            if (collider.CompareTag("Sheep"))
            {
                sheepInRangeList.Add(collider.gameObject);
            }
        }

        private IEnumerator Bite()
        {
            isBiting = true;

            targetSheep.GetAttacked(bitingDamage);

            yield return new WaitForSeconds(bitingInterval);
        
            isBiting = false;
        }
    }
}