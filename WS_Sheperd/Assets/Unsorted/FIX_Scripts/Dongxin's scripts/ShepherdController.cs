using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shepherd
{
    public class ShepherdController : MonoBehaviour
    {
        /* Components */
        [Header("Components")]
        [SerializeField] private CharacterController characterController = null;
        [SerializeField] private new Transform camera = null;
        [SerializeField] private Transform facingTransform = null;
        [SerializeField] private Transform groundCheck = null;
        [SerializeField] private LayerMask groundLayer = default(LayerMask);



        /* Movement */
        [Header("Movement")]
        [SerializeField] private float walkingSpeed = 10f;
        [SerializeField] private float runningSpeed = 20f;
        [SerializeField] private float turnSmoothTime = 0.1f;
        private float turnSmoothVelocity;
        private const float turningSpeed = 10f;
        private Vector3 velocity = Vector3.zero;
        private const float diagonalMovementParameter = 0.70710678118f;
        [SerializeField] private float gravity = 9.81f;
        [SerializeField] private float minJumpVelocity = 1f;
        [SerializeField] private float maxJumpVelocity = 3f;
        private const float groundCheckHeight = 0.15f;
        private bool isJumping = false;

        [SerializeField] private bool onGround = false;

        

        /* Sheep */
        [Header("Sheep")]
        [Tooltip("Reference to all current sheep")]
        [SerializeField] private List<SheepController> sheepList = new List<SheepController>();
        [SerializeField] private LayerMask sheepLayer = default(LayerMask);
        private const float rescuingDistance = 5f;



        private void Update()
        {
            /* Movement */
            // Ground check
            onGround = Physics.CheckSphere(groundCheck.position, 0.25f, groundLayer);
            if (onGround && velocity.y < 0f)
                velocity.y = -2f;
            
            if (onGround)
            {
                velocity.y = 0f;
                gravity = 9.81f;
            }
            else
            {
                if (!isJumping)
                {
                    gravity += Time.deltaTime * 50f;
                    velocity.y -= gravity * Time.deltaTime;
                }
            }

            // Make sure the facing transform is in the same direction as the camera
            Vector3 rot = facingTransform.rotation.eulerAngles;
            rot.y = camera.rotation.eulerAngles.y;
            facingTransform.rotation = Quaternion.Euler(rot);

            // Get input
            float hor = Input.GetAxisRaw("Horizontal");
            float ver = Input.GetAxisRaw("Vertical");

            // Jump
            if (Input.GetButtonDown("Jump") && onGround)
            {
                isJumping = true;
            }

            if (isJumping)
            {
                if (!Input.GetButton("Jump") && velocity.y >= minJumpVelocity)
                {
                    isJumping = false;
                }
                else
                {
                    velocity.y += 30f * Time.deltaTime;
                    if (velocity.y >= maxJumpVelocity)
                    {
                        velocity.y = maxJumpVelocity;
                        isJumping = false;
                        gravity = velocity.y;
                    }
                }
            }

            /* 
             * Make sure diagonal movement has the same speed as directional movement
             * This is only for keyboard or D-pad movement
             * For analog movement, don't use this this code block
             */
            if (hor != 0f && ver != 0f)
            {
                hor *= diagonalMovementParameter;
                ver *= diagonalMovementParameter;
            }

            Vector3 dir = facingTransform.TransformVector(new Vector3(hor, 0f, ver));
            if (dir != Vector3.zero)
            {
                float angle = Mathf.Atan2(dir.x, dir.z) * Mathf.Rad2Deg;
                rot = transform.rotation.eulerAngles;
                transform.rotation = Quaternion.Euler(new Vector3(rot.x, Mathf.LerpAngle(rot.y, angle, turningSpeed * Time.deltaTime), rot.z));
            }

            velocity = Vector3.Lerp(velocity, dir, 10f * Time.deltaTime);

            if (velocity != Vector3.zero)
                characterController.Move(velocity * walkingSpeed * Time.deltaTime);
            


            /* Actions */
            // Rescue
            if (Input.GetButtonDown("Action"))
            {
                // Potential optimization: use heap or tree to store sheep references if there will be large number of sheep
                for (int i = 0; i < sheepList.Count; i++)
                {
                    SheepController sheep = sheepList[i];
                    // Check if the sheep is trapped
                    if (sheep.SheepState == SheepState.Trapped)
                    {
                        // Check if in range
                        if (Vector3.Distance(transform.position, sheep.transform.position) <= rescuingDistance)
                        {
                            // Check if the mouse is pointing at this sheep
                            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                            if (Physics.Raycast(ray, out RaycastHit hit, Mathf.Infinity, sheepLayer))
                            {
                                sheep.GetRescued();
                                break;
                            }
                        }
                    }
                }
            }
        }
    }
}