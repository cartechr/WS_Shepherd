using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.Rendering;

public class Poison_Plant : MonoBehaviour
{
    public GameObject volume;
    [SerializeField] float StartTimer;
    [SerializeField] float CurrentTimer;
    public GameObject particles;
    public int Plant_Damage = 5;
    public bool enabletimer = false;
    public List<GameObject> sheepList = new List<GameObject>();

    // Start is called before the first frame update
    void Start()
    {
        CurrentTimer = StartTimer;
    }
 private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            Debug.Log("Player nearby poison plant - POISON_PLANT");
            volume.gameObject.SetActive(true);
            particles.gameObject.SetActive(true);
        }
        if (other.gameObject.CompareTag("Sheep"))
        {
            sheepList.Add(other.gameObject);
            //Debug.Log(sheepList.Count);

            //var sheep = other.gameObject;
            //sheep.GetComponent<SheepHealth>().PoisonPlantDamage();

            //Debug.Log(other.gameObject.name);
        }
    }
    private void Update()
    {
        CurrentTimer -= Time.deltaTime;
        if (CurrentTimer <= 0)
        {
            CurrentTimer = StartTimer;
            for (int i = 0; sheepList.Count > i; i++)
            {
                sheepList[i].GetComponent<SheepHealth>().PoisonPlantDamage(4);
            }
        }

    }


    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            volume.gameObject.SetActive(false);
            particles.gameObject.SetActive(false);
        }
        if (other.gameObject.CompareTag("Sheep"))
        {
            sheepList.Remove(other.gameObject);
        }
    }
}
