using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SheepTreats : MonoBehaviour
{
    [SerializeField] bool GetTreat = false;
    [SerializeField] private int NumOfTreats;
    //Fruit object on top of cactus.
    [SerializeField] public GameObject Fruit;
    [SerializeField] public ParticleSystem Picked;


    private Player player;

    public TextMeshProUGUI TreatUIText;
    public Canvas canvas;

    private void Start()
    {
        GameEvents.LoadFromSave += LoadFromGameController;
        player = GetComponent<Player>();
        canvas.gameObject.SetActive(false);
        LoadFromGameController(this, EventArgs.Empty);
    }

    private void OnDestroy()
    {
        GameEvents.LoadFromSave -= LoadFromGameController;
    }

    // Update is called once per frame
    void Update()
    {
        if(GetTreat == true && Input.GetButtonDown("Follow Command"))
            {
                player.StartCoroutine(player.TreatEndAnim());
                if(NumOfTreats >= 1)
                {
                    NumOfTreats -= 3;
                    Debug.Log("Grabbed treat" + NumOfTreats + " left");
                    player.PlayerTreat += 3;
                    Debug.Log("Player is holding " + player.PlayerTreat + " treats");
                    TreatUIText.SetText("Holding " + player.PlayerTreat + " treat(s)");
                   
                    //Debug.Log(player.PlayerTreat + " treats collected");
                }
                if(NumOfTreats == 0){
                    //yield return new WaitForSeconds(1.0f);
                    //Debug.Log("No More Treats");
                    //Fruit.SetActive(false);
                    this.StartCoroutine(this.FruitDeath());
                    GetTreat = false;
                }

                GameController.Instance().savedSheepTreatFruits[transform.position.GetHashCode()] = NumOfTreats;
            }
    }
    public IEnumerator FruitDeath()
    {
        yield return new WaitForSeconds(1.0f);
        FMODUnity.RuntimeManager.PlayOneShot("event:/Audio/FruitPick");
        Fruit.SetActive(false);
        Picked.Play();
    }
    private void OnTriggerEnter(Collider other) 
    {
        if(other.gameObject.CompareTag("Player"))
        {
           canvas.gameObject.SetActive(true);
            player = other.gameObject.GetComponent<Player>();
            if(NumOfTreats > 0){
                GetTreat = true;
                Debug.Log("Can grab treats");
                //treat.Active();
            }
        }
    }
    private void OnTriggerExit(Collider other) 
    {
       canvas.gameObject.SetActive(false);
        if(other.gameObject.CompareTag("Player"))
        {
            GetTreat = false;
            Debug.Log("Can't grab treats");
            //treat.NotActive();
        }
    }

    void LoadFromGameController(object sender, EventArgs args)
    {
        GameController gc = GameController.Instance();
        int key = transform.position.GetHashCode();
        if (gc.savedSheepTreatFruits.ContainsKey(key)) {
            NumOfTreats = gc.savedSheepTreatFruits[key];
            Fruit.SetActive(true);
            if (NumOfTreats <= 0) {
                Fruit.SetActive(false);
            }
        } else {
            gc.savedSheepTreatFruits[key] = NumOfTreats;
        }
    }
}
