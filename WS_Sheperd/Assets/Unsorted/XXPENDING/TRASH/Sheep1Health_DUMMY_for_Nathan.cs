using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Sheep1Health_DUMMY_for_Nathan : NPC_Follow
{
    Animator animator;

    public int maxHealth;
    public int currentHealth;
    public GameController gamecontroller;
    public Coyote_Combat coyote;
    public int water_drinking_rate;
    public int food_eating_Rate;

    NPC_Follow npc_follow;
    public float invulnerabilityTimer;

    public float slowedTimer;

    public bool Slowed = false;
    Transform SheepPosition;

    private int Damage;
    public ParticleSystem stress;

    //FMOD
    FMOD.Studio.EventInstance distress;
    private float bleatTimer;
    public int bleatClass;

    // MAIN PROPERTIES
    public float sheepStamina;
    public float maxSheepStamina;
    [HideInInspector] public bool hasRegenerated = true;
    [HideInInspector] public bool moving = false;

    // REGEN PROPERTIES
    [Range(0, 50)] [SerializeField] private float staminaDrain;
    [Range(0, 50)] [SerializeField] private float staminaRegen;

    // SPEED PROPERTIES
    [SerializeField] private float slowedSpeed;
    [SerializeField] private float normalSpeed;

    

    public static List<Sheep1Health_DUMMY_for_Nathan> sheepList = new List<Sheep1Health_DUMMY_for_Nathan>();
    public static List<Sheep1Health_DUMMY_for_Nathan> GetSheepList()

    {
        return sheepList;
    }

    private void Awake()
    {
        sheepList.Add(this);
    }
    void Start()
    {
        npc_follow = GetComponent<NPC_Follow>();
        SheepPosition = GetComponent<Transform>();
        initialize();
        animator = GetComponent<Animator>();
        currentHealth = maxHealth;
        agent.Speed(normalSpeed);

        //FMOD
        distress = FMODUnity.RuntimeManager.CreateInstance("event:/Audio/Distress");
        bleatTimer = Random.Range(0.0f, 30.0f);
    }


    protected void SheepAnimations()
    {
      /*  horizontalInput = Input.GetAxis("Horizontal");
        //forwardInput = Input.GetAxis("Vertical");



        if ((horizontalInput != 0 || forwardInput != 0) && IsWalking == false)
        {
            IsWalking = true;
            // print("moving");
            animator.Play("Walk");
        }
        if ((horizontalInput == 0 && forwardInput == 0) && IsWalking == true)
        {
            IsWalking = false;
            // print("notMoving");
            animator.Play("Idle");
        } */

    }
    //returns true if sheep is killed
    public bool TakeDamage()
    {
        currentHealth -= coyote.CoyoteDamage;
        if (currentHealth <= 0)
        {
            sheepList.Remove(this);
            Destroy(gameObject);
            Debug.Log(gameObject.name + " is dead ");
            currentHealth = 0;
            return true;
        }
        else
        {
            Debug.Log(gameObject.name + "s health is " + currentHealth);
            return false;
        }
    }


    //public void Heal(int amount)
    //{
    //currentHealth += amount;

    //if (currentHealth > maxHealth)
    //{
    //currentHealth = maxHealth;
    //}

    //}

    private void Update()
    {
        FollowPlayer();
        SheepAnimations();
        if (moving == false)
        {
            if (sheepStamina <= maxSheepStamina - 0.01)
            {
                sheepStamina += staminaRegen * Time.deltaTime;
                //UpdateStamina

                if (sheepStamina >= maxSheepStamina)
                {
                    //set to normal speed
                    hasRegenerated = true;
                }
            }
        }
        IsMoving();
        Moving();
        DetermineSpeed();
        if (invulnerabilityTimer > 0)
            {
                invulnerabilityTimer -= Time.deltaTime;
            }
        if (slowedTimer > 0)
            {
                slowedTimer -= Time.deltaTime;
                Slowed = true;
            }
        if (slowedTimer <= 0)
            {
                Slowed = false;
            }


        //SHEEP BLEATING
        if (bleatTimer > 0)
            {
            bleatTimer -= Time.deltaTime;
        }

        if (bleatTimer <= 0)
        {
            Bleat();
            Debug.Log("bleat");
        }
    }

    public void Moving()
    {
        if (hasRegenerated && moving && Camping__Collider.Camping == false)
        {
            sheepStamina -= staminaDrain * Time.deltaTime;
        }
        if (Camping__Collider.Camping == true)
        {
            sheepStamina -= maxSheepStamina;
        }
    }

    public void IsMoving()
    {
        if (agent.IsStopped())
        {
            moving = false;
        }
        else moving = true;
       // stress.Pause();
    }

    public void DetermineSpeed()
    {
        if (sheepStamina <= 0)
        {
            agent.Speed(slowedSpeed);
        }
        if (Slowed == true)
        {
            agent.Speed(slowedSpeed);
            Debug.Log("Slowed!");
        }
        else if (sheepStamina >= maxSheepStamina && hasRegenerated && Slowed == false)
        {
            agent.Speed(normalSpeed);
        }
    }
    public void Bleat()
    {
        if(bleatClass == 1)
        {
            FMODUnity.RuntimeManager.PlayOneShot("event:/Audio/SheepBleatLightVariant", transform.position);
        }
        if (bleatClass == 2)
        {
            FMODUnity.RuntimeManager.PlayOneShot("event:/Audio/SheepBleatMediumVariant", transform.position);
        }
        if (bleatClass == 3)
        {
            FMODUnity.RuntimeManager.PlayOneShot("event:/Audio/SheepBleatHeavyVariant", transform.position);
        }
        bleatTimer = Random.Range(0.0f, 30.0f);
    }

    private void OnTriggerEnter(Collider other)
    {
        
        //Detects if the player enters the collider
        if (other.CompareTag("Snake"))
        {
            if (invulnerabilityTimer <= 0)
            {
                currentHealth -= 5;
                Debug.Log("Danger! Tsuchinoko!");
                invulnerabilityTimer = 6;

                //DISTRESS SOUND HERE
                distress.start();

                //PARTICLE EFFECT HERE
                stress.Play();
                //TEXTURE CHANGE HERE
            }
        }
        if (other.CompareTag("Brambles"))
        {
            if (invulnerabilityTimer <= 0)
            {
                currentHealth -= 1;
                Debug.Log("Danger! Bush!");
                invulnerabilityTimer = 10;
                slowedTimer = 10;

                //DISTRESS SOUND HERE
                distress.start();

                //PARTICLE EFFECT HERE
                stress.Play();
                //TEXTURE CHANGE HERE
            }
        }      
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("Water"))
        {
            Water_Sys water_resource = other.GetComponent<Water_Sys>();
            if (water < npc_follow.WaterAtStart)
            {
              //  animator.Play("Eat");
                water += water_drinking_rate;
                water_resource.waterLeft -= water_drinking_rate;
            }
            Debug.Log("water");
            Debug.Log(water_resource.waterLeft);
            if (water_resource.waterLeft <= 0)
            {
                gamecontroller.WaterList.Remove(other.gameObject);
                //Destroy(water_resource.gameObject);
                water_resource.waterLeft = 0;
               // agent.CallAgent(player.position);
                //water = 500;
            }
        }
        if (other.gameObject.CompareTag("Food"))
        {
            Eat_Sys food_resource = other.GetComponent<Eat_Sys>();
            if (food < npc_follow.FoodAtStart)
            {
               // animator.Play("Eat");
                food += food_eating_Rate;
                food_resource.foodLeft -= food_eating_Rate;
            }
            else
            {
                food = npc_follow.FoodAtStart;
            }
            //npc_follow.food += food_eating_Rate;
            Debug.Log(food_resource.foodLeft);
            if (food_resource.foodLeft <= 0)
            {
                gamecontroller.FoodList.Remove(other.gameObject);
                //Destroy(food_resource.gameObject);
                food_resource.foodLeft = 0;
               // agent.CallAgent(player.position);
                //food = 500;
            }
        }
    }
}

// https://www.youtube.com/watch?v=vNL4WYgvwd8&t=0s
// https://www.youtube.com/watch?v=Fs2YCoamO_U&ab_channel=SpeedTutor