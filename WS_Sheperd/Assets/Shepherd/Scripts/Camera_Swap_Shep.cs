using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Cinemachine;
using UnityEngine.SceneManagement;

public class Camera_Swap_Shep : MonoBehaviour
{
    public GameObject CM_vcam1;
    public GameObject CM_vcam2;
    public GameObject CM_vcam3;
    public GameObject zoom_vcam4;
    public GameObject CM_vcam5;

    //cam 6 quick dirty non-permanant solution
    public GameObject CM_vcam6;


    public GameObject CM_freecam;
    //public  GameObject Cm_vcam5;
    //public GameObject craftControl;
    public GameObject Player;


    //quick dirty non-permanant gate close solution.
    public GameObject Gate1;
    public GameObject Gate2;

    public FirstPersonMovement fpm;

    //public StateMachine sm;

   

    //public bool touching = false;
    public bool toggle = false;
    public bool CanMove = true;
    public bool ControlCamera = true;
    public bool iszoomedin = false;
    //public bool R = false;
    //public bool C = false;
    public bool experimentalCameraEnabled;

    public GameObject firstPersonLookObject;
    public GameObject camShepControllerObject;

    public bool freeLookCamToggle = false;

    public bool seenIntroCutscene = false;
    public bool seenCoyoteWaterfallCutscene = false;
    

    void Start()
    {
        //Starting cutscene at start of level
        if (seenIntroCutscene) return;
        
        //CineBarsCON.Instance.HideBars();
        CineBarsCON.Instance?.ShowBars();
        firstPersonLookObject.GetComponent<FirstPersonLook>().enabled = false;
        camShepControllerObject.GetComponent<Cam_Shep_Controller>().enabled = false;
        ControlCamera = false;

        
        CM_vcam5.SetActive(true);
        CM_vcam1.SetActive(false);
        CM_vcam2.SetActive(false);
        CanMove = false;

        StartCoroutine(FinishCut());
        
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P)) {
            //Cm_vcam5.SetActive(true);
        }
        if (Input.GetKeyDown(KeyCode.V) && experimentalCameraEnabled && ControlCamera && SceneManager.GetActiveScene().buildIndex != 3)
        {
            freeLookCamToggle = !freeLookCamToggle;
            if (freeLookCamToggle)
            {
                CM_vcam1.SetActive(false);
                CM_vcam2.SetActive(false);
                CM_freecam.SetActive(true);
                GetComponent<CameraBasedInput>().enabled = true;
            } else
            {
                CM_vcam1.SetActive(true);
                CM_freecam.SetActive(false);
                GetComponent<CameraBasedInput>().enabled = false;
            }
        }

        //Debug.Log(ControlCamera);
        //Debug.Log(SceneManager.GetActiveScene().buildIndex);
        if (ControlCamera && SceneManager.GetActiveScene().buildIndex != 3)
        {
            //Debug.Log("camera check");
            if(!freeLookCamToggle && !iszoomedin) //James' Free Look Camera is NOT enabled, use default camera
            {
                if (Input.GetKey(KeyCode.Q))
                {
                    //Debug.Log("Sheep CAM toggled");
                    //Debug.Log("Q pressed");
                    CM_vcam1.SetActive(false);
                    CM_vcam2.SetActive(true);
                }
                else
                {
                    CM_vcam2.SetActive(false);
                    CM_vcam1.SetActive(true);
                }
            }
        }
        Zoom();
    }

    public void Zoom()
    {
        //camera zoom swaps to camera 4, this camera will have a tighter fov
        if (ControlCamera && SceneManager.GetActiveScene().buildIndex != 3)
        {
            //Debug.Log("camera check");
            if (!freeLookCamToggle) //James' Free Look Camera is NOT enabled, use default camera
            {
                if (Input.mouseScrollDelta.y > 0)
                {
                    iszoomedin = true;
                    Debug.Log("Zoomed in");
                    CM_vcam1.SetActive(false);
                    zoom_vcam4.SetActive(true);
                }
                else if (Input.mouseScrollDelta.y < 0)
                {
                    iszoomedin = false;
                    Debug.Log("Zoomed out");
                    zoom_vcam4.SetActive(false);
                    CM_vcam1.SetActive(true);
                }
            }
        }
    }

    //public void PhysicsUpdate()
    //{
    //craftControl.SetActive(false);
    //}
    /*public void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("Craft_Range"))
        {
            Debug.Log("Within Crafting Range");
            craftControl.SetActive(true);

        }
    }*/


    /*public void Crafting()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {
            if (craftControl.activeSelf)
            {
                Debug.Log("Crafting");
                CM_vcam3.SetActive(true);
                CM_vcam1.SetActive(false);
            }
        }
            
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            CM_vcam1.SetActive(true);
            CM_vcam3.SetActive(false);
        }
    }*/





    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Move")) {
            if (seenCoyoteWaterfallCutscene) return;
            
            firstPersonLookObject.GetComponent<FirstPersonLook>().enabled = false;
            camShepControllerObject.GetComponent<Cam_Shep_Controller>().enabled = false;
            ControlCamera = false;
            GameObject cutsceneObject = other.gameObject.transform.parent.gameObject;
            CM_vcam3.GetComponent<CinemachineVirtualCamera>().LookAt = cutsceneObject.transform.Find("Cinematic_Coyote");
            CM_vcam3.GetComponent<CinemachineVirtualCamera>().Follow = cutsceneObject.transform.Find("Cine_Follow_DEV");
            
            CineBarsCON.Instance.ShowBars();
            CM_vcam3.SetActive(true);
            CM_vcam2.SetActive(false);
            CM_vcam1.SetActive(false);
            CanMove = false;

            StartCoroutine(FinishCut2());
        }

    }

    IEnumerator FinishCut()
    {
        seenIntroCutscene = true;
        yield return new WaitForSeconds(9);
        CineBarsCON.Instance.HideBars();
        
        CanMove = true;


        // this breaks after exiting camping!
        if (Gate1 && Gate2) {
            //dirty solution
            Gate1.transform.eulerAngles = new Vector3(0, 0, 0);
            Gate2.transform.eulerAngles = new Vector3(0, -218, 0);
        }



        CM_vcam6.SetActive(true);
        CM_vcam5.SetActive(false);
        yield return new WaitForSeconds(2);
        CM_vcam1.SetActive(true);
        CM_vcam6.SetActive(false);
        ControlCamera = true;
        firstPersonLookObject.GetComponent<FirstPersonLook>().enabled = true;
        camShepControllerObject.GetComponent<Cam_Shep_Controller>().enabled = true;
    }
    IEnumerator FinishCut2()
    {
        seenCoyoteWaterfallCutscene = true;
        yield return new WaitForSeconds(15);
        CineBarsCON.Instance.HideBars();
        //Swaps the sheep into panic state when hearing the Coyote Howl, placed here to allow player to experience full state
        foreach (var sheep in SheepHealth.sheepList)
        {
            sheep.GetComponent<StateMachine>().SetState("Panic");
        }
        CanMove = true;
        CM_vcam1.SetActive(true);
        CM_vcam3.SetActive(false);
        ControlCamera = true;
        firstPersonLookObject.GetComponent<FirstPersonLook>().enabled = true;
        camShepControllerObject.GetComponent<Cam_Shep_Controller>().enabled = true;
    }

    public void ResetToDefaultCamera()
    {
        Debug.Log("Resetting to default camera mode");
        CM_vcam3.SetActive(false);
        CM_vcam5.SetActive(false);
        CM_vcam6.SetActive(false);
        CanMove = true;
        CM_vcam1.SetActive(true);
        ControlCamera = true;
        firstPersonLookObject.GetComponent<FirstPersonLook>().enabled = true;
        camShepControllerObject.GetComponent<Cam_Shep_Controller>().enabled = true;
        CineBarsCON.Instance.HideBarsImmediate();
    }
}
