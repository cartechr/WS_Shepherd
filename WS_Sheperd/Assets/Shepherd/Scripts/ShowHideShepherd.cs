using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowHideShepherd : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        GameEvents.HidePlayer += OnHidePlayer;
        GameEvents.ShowPlayer += OnShowPlayer;
    }

    private void OnDestroy()
    {
        GameEvents.HidePlayer -= OnHidePlayer;
        GameEvents.ShowPlayer -= OnShowPlayer;
    }

    void OnHidePlayer(object sender, EventArgs args)
    {
        gameObject.SetActive(false);
    }
    
    void OnShowPlayer(object sender, EventArgs args)
    {
        gameObject.SetActive(true);
    }
    
}
