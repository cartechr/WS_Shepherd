using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crook : MonoBehaviour
{

    private int magic_count = 0;
    [SerializeField] private MeshRenderer magicMeshRenderer;
    [SerializeField] private int magicMaterialIndex;

    [SerializeField] private Material normalMaterial;
    [SerializeField] private Material magicMaterial;

    [SerializeField] private Material currentMaterial;

    [SerializeField] private float fadeTime;
    [SerializeField] private ParticleSystem magicParticles;

    // Start is called before the first frame update
    void Start()
    {
        currentMaterial = magicMeshRenderer.materials[1];

        GameEvents.AddStaffMagic += OnAddMagic;
        GameEvents.RemoveStaffMagic += OnRemoveMagic;
        StartCoroutine(DisableMagic());
        magicParticles.Stop();
    }

    private void OnEnable()
    {
        Start();
    }

    private void Update()
    {
        magicMeshRenderer.materials.SetValue(currentMaterial, magicMaterialIndex);
    }

    private void OnDestroy()
    {
        GameEvents.AddStaffMagic -= OnAddMagic;
        GameEvents.RemoveStaffMagic -= OnRemoveMagic;
    }

    void OnAddMagic(object sender, EventArgs args)
    {
        // if we're about to add the first magic
        if (magic_count == 0) {
            StartCoroutine(EnableMagic());
            magicParticles.Play();
             FMODUnity.RuntimeManager.PlayOneShot("event:/Audio/MagicChimes");
        }
        magic_count += 1;
    }
    
    void OnRemoveMagic(object sender, EventArgs args)
    {
        // if we're about to remove the last magic
        if (magic_count == 1) {
            StartCoroutine(DisableMagic());
            magicParticles.Stop();
        }
        magic_count -= 1;
    }
    
    IEnumerator EnableMagic()
    {
        for (float timer = 0; timer < fadeTime; timer += Time.deltaTime) {
            float t = timer / fadeTime;
            currentMaterial.Lerp(normalMaterial, magicMaterial, t);
            yield return null;
        }
        currentMaterial.Lerp(normalMaterial, magicMaterial, 1);
    }
    
    IEnumerator DisableMagic()
    {
        for (float timer = 0; timer < fadeTime; timer += Time.deltaTime) {
            float t = timer / fadeTime;
            currentMaterial.Lerp(normalMaterial, magicMaterial, 1 - t);
            yield return null;
        }
        currentMaterial.Lerp(normalMaterial, magicMaterial, 0);
    }
}
