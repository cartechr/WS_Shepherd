using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShepherdAnimationController : MonoBehaviour {
    //Edit: h
    public bool canWalk = true;
    public bool canRepel;
    public bool isOmniscentCamera;
    private Animator animator;
    private int isWalkingHash;
    private int isWalkingBHash;
    private int isStrafeRHash;
    private int isStrafeLHash;
    private int isRepelHash;
    private int isRunningHash;
    private bool isWalking;
    private bool isWalkingB;
    private bool isStrafeL;
    private bool isStrafeR;
    private bool isRepelling;
    private bool isRunning;
    private Controls controls;
    public GameObject firstPersonAudio;
    public GameObject firstPersonMoveObject;

    public FirstPersonMovement fpm;
    void Start()
    {
        firstPersonMoveObject.GetComponent<FirstPersonMovement>().enabled = true;
        animator = GetComponent<Animator>();
        isWalkingHash = Animator.StringToHash("IsWalking");
        isWalkingBHash = Animator.StringToHash("IsWalkingB");
        isStrafeRHash = Animator.StringToHash("IsStrafeR");
        isStrafeLHash = Animator.StringToHash("IsStrafeL");
        isRepelHash = Animator.StringToHash("IsRepel");
        isRunningHash = Animator.StringToHash("IsRunning");
        isWalking = false;
        isWalkingB = false;
        isStrafeR = false;
        isStrafeL = false;
        isRepelling = false;
        isRunning = false;
        controls = GetComponent<Player>().gamecontroller.controls;
        
        // Change later
        canWalk = true;
        canRepel = true;
    }
    void PlayFootstep(string path)
    {
        FMODUnity.RuntimeManager.PlayOneShot(path, GetComponent<Transform>().position);
        //Debug.Log("Sound is playing right now");
    }
    
    void Update() {
        if (!isOmniscentCamera)
        {
            DefaultMovementInput();
        } else
        {
            OmniscentCameraInput();
        }
        
        //Repel started
        if(Input.GetKeyDown(controls.repel) && canRepel) {
            animator.SetBool(isRepelHash, true);
            Debug.Log("cannot move/ fpm is locked");
            isRepelling = true;
            canRepel = false;
            firstPersonMoveObject.GetComponent<FirstPersonMovement>().enabled = false;
            StartCoroutine(MovementTimer());
            ParticleSystem ps = GameObject.Find("Ripple").GetComponent<ParticleSystem>();
            ps.Play();
            FMODUnity.RuntimeManager.PlayOneShot("event:/Audio/CoyoteRepel");
            
        }
        //Repel ended
        if(Input.GetKeyUp(controls.repel) && isRepelling) {
            animator.SetBool(isRepelHash, false);
            isRepelling = false;
            
        }
    }

    public IEnumerator MovementTimer()
    {
        yield return new WaitForSeconds(4f);
        Debug.Log("can move again/ fpm is unlocked");
        firstPersonMoveObject.GetComponent<FirstPersonMovement>().enabled = true;
    }



    private void DefaultMovementInput()
    {
        if (Input.GetKeyDown(controls.forward) && canWalk)
        {
            animator.SetBool(isWalkingHash, true);
            isWalking = true;
        }
        if (Input.GetKeyUp(controls.forward) && isWalking)
        {
            animator.SetBool(isWalkingHash, false);
            isWalking = false;
        }

        // Walking Backwards
        if (Input.GetKeyDown(controls.backward) && canWalk)
        {
            animator.SetBool(isWalkingBHash, true);
            isWalkingB = true;
        }
        if (Input.GetKeyUp(controls.backward) && isWalkingB)
        {
            animator.SetBool(isWalkingBHash, false);
            isWalkingB = false;
        }

        //Strafing
        if (Input.GetKeyDown(controls.right) && canWalk)
        {
            animator.SetBool(isStrafeRHash, true);
            isStrafeR = true;
        }
        if (Input.GetKeyUp(controls.right) && isStrafeR)
        {
            animator.SetBool(isStrafeRHash, false);
            isStrafeR = false;
        }

        if (Input.GetKeyDown(controls.left) && canWalk)
        {
            animator.SetBool(isStrafeLHash, true);
            isStrafeL = true;
        }
        if (Input.GetKeyUp(controls.left) && isStrafeL)
        {
            animator.SetBool(isStrafeLHash, false);
            isStrafeL = false;
        }

        if (Input.GetKey(controls.sprint) && Input.GetKey(controls.forward) && canWalk)
        {
            animator.SetBool(isRunningHash, true);
            isRunning = true;
        }
        if (Input.GetKeyUp(controls.sprint) && isRunning)
        {
            animator.SetBool(isRunningHash, false);
            isRunning = false;
        }
    }

    private void OmniscentCameraInput() //Very Hacky method, will need to clean up before controller implementation - James
    {
        //Debug.Log("CAMERA OMNISCENCE = TRUE");
        if ((Input.GetKey(controls.forward) || Input.GetKey(controls.left) || Input.GetKey(controls.backward) || Input.GetKey(controls.right)) && canWalk)
        {
            animator.SetBool(isWalkingHash, true);
            isWalking = true;
        } else
        {
            animator.SetBool(isWalkingHash, false);
            isWalking = false;
            //Debug.Log("ALL KEYS LIFTED");
        }
    }
}