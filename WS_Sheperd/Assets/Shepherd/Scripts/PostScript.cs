using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PostScript : MonoBehaviour
{
    
    private GameObject ghostPost;
    [SerializeField] private Color ghostColor;
    private bool placed = false;

    [SerializeField] private Material[] ghostMats;
    [SerializeField] private Material[] originalMats;

    // Start is called before the first frame update
    void Start()
    {
        GameEvents.PlayerFollowCommandEvent += OnPlayerFollowCommand;
        // this assumes all materials are on a single MeshRenderer
    }

    private void OnDestroy()
    {
        GameEvents.PlayerFollowCommandEvent -= OnPlayerFollowCommand;
    }

    private void OnPlayerFollowCommand(object sender, SphereArgs args)
    {
        if (Vector3.Distance(transform.position, args.center) < args.radius) {
            Destroy(gameObject);
        }
    }

    public void GoingGhost()
    {
        foreach (var meshRenderer in GetComponentsInChildren<MeshRenderer>()) {
            meshRenderer.materials = ghostMats;
            foreach (var mat in meshRenderer.materials) {
                mat.color = ghostColor;
            }
        }
    }

    public void Place()
    {
        foreach (var meshRenderer in GetComponentsInChildren<MeshRenderer>()) {
            meshRenderer.materials = originalMats;
        }
    }

    public void Move(Vector3 position, Vector3 facing)
    {
        transform.position = position;
        transform.LookAt(facing, Vector3.up);
        if (Physics.Raycast(position, Vector3.down, out RaycastHit hit)) {
            transform.position = hit.point;
        }
    }

}
