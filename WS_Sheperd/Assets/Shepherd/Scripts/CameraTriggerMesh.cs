using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraTriggerMesh : MonoBehaviour
{

    private MeshCollider _meshCollider;
    private MeshFilter _meshFilter;
    private Camera _camera;

    [SerializeField] public float depth = 30;
    
    Vector3[] points = new Vector3[8];
    private Mesh mesh;
    
    // Start is called before the first frame update
    void Awake()
    {
        _meshCollider = GetComponent<MeshCollider>();
        _meshFilter = GetComponent<MeshFilter>();
        _camera = GetComponent<Camera>();
        mesh = new Mesh();
        UpdateMesh();
    }
    
    void UpdateMesh(){
        points[0] = GetCorner(new Vector3(0, 0, depth));
        points[1] = GetCorner(new Vector3(0, 1, depth));
        points[2] = GetCorner(new Vector3(1, 0, depth));
        points[3] = GetCorner(new Vector3(1, 1, depth));
        points[4] = GetCorner(new Vector3(0, 0, _camera.nearClipPlane));
        points[5] = GetCorner(new Vector3(0, 1, _camera.nearClipPlane));
        points[6] = GetCorner(new Vector3(1, 0, _camera.nearClipPlane));
        points[7] = GetCorner(new Vector3(1, 1, _camera.nearClipPlane));
        mesh.vertices = points;
        mesh.triangles = new int[]
        {
            0, 1, 2, 
            1, 3, 2, 
            1, 5, 7, 
            7, 3, 1, 
            4, 6, 5, 
            6, 7, 5, 
            0, 6, 4,
            0, 2, 6,
            2, 3, 6,
            6, 3, 7,
            0, 4, 1,
            1, 4, 5
        };
        _meshCollider.sharedMesh = mesh;
        _meshFilter.mesh = mesh;
    }

    // Update is called once per frame
    void Update()
    {
        UpdateMesh();
        mesh.RecalculateNormals();
    }

    Vector3 GetCorner(Vector3 viewport_point)
    {
        Vector3 world_point = _camera.ViewportToWorldPoint(viewport_point);
        //Vector3 world_point = viewport_point;
        world_point = transform.InverseTransformPoint(world_point);
        //Debug.DrawLine(transform.position, world_point, Color.green);
        return world_point;
    }
}
