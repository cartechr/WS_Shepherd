using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

//[RequireComponent(typeof(PlayerInput))]
public class Cam_Shep_Controller : MonoBehaviour
{
    public Vector2 _look;
    public Vector2 _move;
    public float rotationPower = 0.2f;
    public float rotationLerp = 0.5f;

    public Vector3 nextPosition;
    public Quaternion nextRotation;
    public float speed = 1f;
   // public Camera camera;

    //public GameObject followTransform; 
    
    
    // Update is called once per frame
    //private void Update()
    //{
        //Vector3 angles = transform.eulerAngles;
       // angles.x += Input.GetAxis("Tilt_V") * rotationPower;
        //Debug.Log(angles.x);
        //transform.eulerAngles = angles;
    //}




    public void Update()
    {
        float angle = -Input.GetAxis("Tilt_V") * rotationPower * Time.deltaTime;
        angle = Mathf.Clamp(angle, -10, 10);
        transform.Rotate(Vector3.right, angle);

        Vector3 angles = transform.localEulerAngles;
        angles.x = Mathf.Clamp((angles.x + 180) % 360f - 180, -30, 30);
        angles.z = 0;
        transform.localEulerAngles = angles;
    }

    

}
