using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices.ComTypes;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    public GameObject treatCanvas;
    public TextMeshProUGUI TreatUIText;
    public bool Repelling = false;
    public bool followplayer = false;
    public GameController gamecontroller;
    public GameObject sheepwaypoint;
    public float coolDown = 5;
    public float coolDownTimer;
    public float staycoolDownTimer;
    public Cinematic_AI cinematic_ai;
    public Coyote_Walking coyote_walking;
    public Animator animator;
    public Camera_Swap_Shep CameraSwapShep;
    public SheepFriendship SheepFriendship;
    GameObject currentsheep;

    public FirstPersonMovement fpm;
    public GameObject firstPersonMoveObject;

    public bool IsCrafting = false;
    //public bool IsPicking = false;
    public bool CanPressF = false;
    public float sheepCommandRadius = 8f;
    public float repelRadius = 10f;

    //FMOD-talk to ben
    public float FMODProgressValue = 0;
    public float FMODEnvironmentValue = 1;
    public float FMOD_is_on_land = 0;

    private ShepherdAnimationController sac;

    [SerializeField] private GameObject postPrefab; 
    private GameObject ghostPostPrefab;
    private PostScript my_post = null;
    public bool placing_post = false;

    [Header("Sheep/Treats")]
    public int PlayerTreat = 0;
    [SerializeField] private float treatDistance;

    private bool is_camping = false;
    private Controls controls;


    void Start() {
       sac = GetComponent<ShepherdAnimationController>();
       controls = gamecontroller.controls;
    }

    private void Awake()
    {
        animator = GetComponent<Animator>();

        //Upon Awaking, if there is an existing Player from DontDestroyOnLoad, delete existing player
        if (GameObject.FindGameObjectsWithTag(gameObject.tag).Length > 1)
        {
            Destroy(gameObject);
        }
        else
        {
            //Normal Awake Function:
        }
        
        GameEvents.EnteredCamp += OnEnterCamping;
        GameEvents.ExitedCamp += OnExitCamping;
    }

    private void OnDestroy()
    {
        GameEvents.EnteredCamp -= OnEnterCamping;
        GameEvents.ExitedCamp -= OnExitCamping;
    }

    //public NPC_Follow check;
    // Update is called once per frame
    public void Update()
    {
        if (Input.GetButtonDown("Follow Command")) {
            placing_post = false;
            //  Following the Shepherd
            //Debug.Log("Pressed F");
            // Emit the signal to all sheep
            GameEvents.PlayerFollowCommandEvent?.Invoke(this, new SphereArgs(transform.position, sheepCommandRadius));
        }

        if (Input.GetButtonDown("Stay Command") && !is_camping) {

            staycoolDownTimer = coolDown;
            if (my_post) {
                if (placing_post) {
                    my_post.Place();
                    placing_post = false;
                    GameEvents.UpdatePlacePostEvent?.Invoke(this, placing_post);
                    GameEvents.PlayerStayCommandEvent?.Invoke(this,
                        new SphereGameObjectArgs(transform.position, sheepCommandRadius, my_post.gameObject));    
                } else {
                    // perhaps play a sound that implies a failed action, such as pressing E when not near anything in a source game
                }
            } else if (!placing_post) {
                placing_post = true;
                GameEvents.UpdatePlacePostEvent?.Invoke(this, placing_post);
                GameObject post = Instantiate(postPrefab, transform.position, Quaternion.identity);
                my_post = post.GetComponent<PostScript>();
                my_post.GoingGhost();
            }
        }

        if (my_post && placing_post) {
            my_post.Move(transform.position + transform.forward * 1.5f, transform.position);
        }

        if (staycoolDownTimer > 0)
        {
            staycoolDownTimer -= Time.deltaTime;
        }

        if (staycoolDownTimer < 0)
        {
            
            staycoolDownTimer = 0;
        }

        if (coolDownTimer > 0)
        {
            coolDownTimer -= Time.deltaTime;
        }

        if (coolDownTimer < 0)
        {
            sac.canRepel = true;
            coolDownTimer = 0;
        }

        R_Pressed();

        if (Input.GetKeyDown(controls.repel) && coolDownTimer != 0)
        {
            Debug.Log("Recharging!");
        }

        /*
        //CRAFTING
        if (Input.GetKeyDown(KeyCode.C) && )
        {
            Debug.Log("Craft Mode");
            sac.IsCrafting = true;
            coolDownTimer = coolDown;
        }
        */
        
        //dist = Vector3.Distance(transform.possi)

    
    }

    public IEnumerator TreatEndAnim()
    {
        Debug.Log("Treat Picking anim started");
        firstPersonMoveObject.GetComponent<FirstPersonMovement>().enabled = false;
        animator.SetBool("IsPicking", true);
        //TreatFeed(null);

        yield return new WaitForSeconds(0.001f);
        animator.SetBool("IsPicking", false);

        yield return new WaitForSeconds(2f);
        Debug.Log("Treat Picking anim ended");
        firstPersonMoveObject.GetComponent<FirstPersonMovement>().enabled = true;
        
    }



    public void TreatFeed(GameObject sheepOverride)
    {
        /*
            Debug.Log("There are " + sheepList.Count + " sheep.");
            for (int i = 0; i < sheepList.Count; i++)
            {
                Player sheep = sheepList[i];
                if (Vector3.Distance(transform.position, sheep.transform.position) <= TreatDistance)
                {
                    Debug.Log("Give treat to sheep?");
                    PlayerTreat -= 1;
                }
            }
            */
        //treatCanvas.SetActive(true);
        GameObject closestSheep;
        if (sheepOverride == null)
        {
            closestSheep = GetClosestSheep();
        } else
        {
            closestSheep = sheepOverride;
        }
      
        Debug.Log("TREAT: Closest Sheep: " + closestSheep.name + ", Sheep Distance: " + Vector3.Distance(closestSheep.transform.position, transform.position));
        if (Vector3.Distance(closestSheep.transform.position, transform.position) < treatDistance || sheepOverride) //sheepOverride != null implies that the player is using fake shepherd and can not properly track the player --James
        {
            if (PlayerTreat > 0)
            {
                //currentsheep = closestSheep;
                StartCoroutine(Treats(closestSheep));
                //Give Sheep a Treat - sheep receiving treat should be referenced by closestSheep           -James
                //closestSheep.GetComponent<SheepHealth>().friendshipStats.PermanentBoost(5);   //This is adding friendship boost to the relevant sheep after feeding them a treat    -James
                Debug.Log("TREAT: Treat Given to: " + closestSheep.name);
                PlayerTreat -= 1;
                //TreatUIText.SetText("Holding " + PlayerTreat + " treat(s)");
            }
        }
    }

    private IEnumerator Treats(GameObject closestSheep)
    {
        Debug.Log("coroutine started");
        closestSheep.GetComponent<SheepHealth>().animator.Play("Eat");
        yield return new WaitForSeconds(1.5f);

        closestSheep.GetComponent<MoodController>().StartEating();
        yield return new WaitForSeconds(4);

        Debug.Log("Wait for seconds over");
        closestSheep.GetComponent<MoodController>().StopEating();
        closestSheep.GetComponent<SheepHealth>().friendshipStats.PermanentBoost(5);
        
        UpdateSheepInfoGarageText(); //Update Treat UI here - James TODO
        tutorialGameEvents.End(this, EventArgs.Empty);

    }

    private void UpdateSheepInfoGarageText()
    {
        FakeShepherd fakeShep = GameObject.FindGameObjectWithTag("FakeShepherd").GetComponent<FakeShepherd>();
        fakeShep.activeCampsitePoints.UpdateSheepInfoText(null);
    }

    private GameObject GetClosestSheep()
    {
        GameObject closestSheep = null;
        float smallestDist = Mathf.Infinity;
        foreach(SheepHealth sh in SheepHealth.GetSheepList())
        {
            float dist = Vector3.Distance(sh.gameObject.transform.position, transform.position);
            if(dist < smallestDist)
            {
                smallestDist = dist;
                closestSheep = sh.gameObject;
            }
        }
        return closestSheep;
    }

    public void OnTriggerEnter(Collider other)
    {
        /*if (Vector3.Distance(Sheep.transform.position, transform.position) < 10 && Input.GetKeyDown(KeyCode.F))
        {
           followplayer = true;
        }*/

        //GAME PROGRESS MODULATION
        if (other.gameObject.CompareTag("ProgressPoint"))
        {
            FMODProgressValue += 1;
            Debug.Log(FMODProgressValue);
            other.gameObject.SetActive(false);
        }

        /*GAME ENVIRONMENT CHANGE
        if (other.gameObject.CompareTag("Area_Lush"))
        {
            FMODEnvironmentValue = 0;
        }*/

        if (other.gameObject.CompareTag("Water"))
        {
           FMOD_is_on_land = 1;
        }
        
    }

   public void OnTriggerExit(Collider other)
    {
        /*if (other.gameObject.CompareTag("Area_Lush"))
        {
            FMODEnvironmentValue = 1;
        }*/

        if (other.gameObject.CompareTag("Water"))
        {
           FMOD_is_on_land = 0;
        }
    }
        
    


    private void R_Pressed()
    {
        if (Input.GetKeyDown(controls.repel) && coolDownTimer == 0)
        {
            Debug.Log("YOU SHALL NOT PASS!");
            coolDownTimer = coolDown;


            float repel_duration = 4f;
            GameEvents.PlayerRepelEvent?.Invoke(this, new TimeSphereArgs(transform.position, repelRadius, Time.time + repel_duration));
            // Kep mapping for repelling the combative coyote
            // Re-enable later
            /*Coyote_Controller coyote_controller = GameController.Instance().ActiveCoyote();
            if (coyote_controller == null)
            {
                Debug.Log("active coyote is not set (null)");
                return;
            }
            if (Vector3.Distance(coyote_controller.transform.position, transform.position) > 10)
            {
                Debug.Log("too far");
                return;
            }*/

            Debug.Log("currently repelling");
            //Re-enable later
            //coyote_controller.EnterRepelState();
        }
    }

    public void GetRammed()
    {
        Debug.Log("I HAVE BEEN RAMMMMMMED!!!");
        // feel free to replace
        //StartCoroutine(RammingFrame());
        StartCoroutine(RammedAnim());
        FMODUnity.RuntimeManager.PlayOneShot("event:/Audio/Ram");
    }
    
    // temporary - feel free to replace
    IEnumerator RammingFrame()
    {
        for (int i = 0; i < 360; i++) {
            yield return null;
            transform.localScale = new Vector3(1, Mathf.Cos(i * Mathf.Deg2Rad), 1);
        }
    }

    public IEnumerator RammedAnim()
    {
        Debug.Log("Rammed anim started");
        firstPersonMoveObject.GetComponent<FirstPersonMovement>().enabled = false;
        animator.SetBool("IsRammed", true);

        yield return new WaitForSeconds(0.001f);
        Debug.Log("Standing anim started");
        animator.SetBool("IsRammed", false);

        yield return new WaitForSeconds(1f);
        firstPersonMoveObject.GetComponent<FirstPersonMovement>().enabled = true;
    }

    void OnEnterCamping(object sender, EventArgs args)
    {
        is_camping = true;
    }
    
    void OnExitCamping(object sender, EventArgs args)
    {
        is_camping = false;
    }

    public int getPlayerTreats()
    {
        return PlayerTreat;
    }
}
