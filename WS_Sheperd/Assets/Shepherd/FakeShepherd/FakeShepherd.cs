using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FakeShepherd : MonoBehaviour
{

    public Vector3 target;
    public Vector3 lookAt;
    public float speed;
    public float backwardOffset;
    public float lateralOffset;

    public Animator animator;

    public SheepZoomAndSwitch activeCampsitePoints;
    
    // Start is called before the first frame update
    void Start()
    {
        GameEvents.FakePlayerGoToPoint += OnFakePlayerGoToPoint;
        GameEvents.FakePlayerSwitchOn += OnFakePlayerSwitchOn;
        GameEvents.FakePlayerSwitchOff += OnFakePlayerSwitchOff;
    }

    private void OnDestroy()
    {
        GameEvents.FakePlayerGoToPoint -= OnFakePlayerGoToPoint;
        GameEvents.FakePlayerSwitchOn -= OnFakePlayerSwitchOn;
        GameEvents.FakePlayerSwitchOff -= OnFakePlayerSwitchOff;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, target, speed * Time.deltaTime);
        transform.LookAt(lookAt);
        if (transform.position == target) {
            animator.SetBool("IsWalking", false);
        } else {
            animator.SetBool("IsWalking", true);
        }

        if (activeCampsitePoints == null)
        {
            activeCampsitePoints = GameObject.Find("SheepPoints").GetComponent<SheepZoomAndSwitch>();
        }
    }

    void Go(Vector3 p_target, Vector3 p_lookAt)
    {
        target = p_target;
        lookAt = p_lookAt;
    }

    void OnFakePlayerGoToPoint(object sender, GameObject point)
    {
        Vector3 point_pos = point.transform.position;
        Vector3 right_pos = point_pos + point.transform.forward * backwardOffset + point.transform.right * lateralOffset;
        Vector3 left_pos = point_pos + point.transform.forward * backwardOffset + point.transform.right * -lateralOffset;
        Vector3 dest_pos;
        if (Vector3.Distance(transform.position, right_pos) < Vector3.Distance(transform.position, left_pos)) {
            //snaps to Left side of Screen
            //Debug.Log("Snaps to Point-Right, Camera-Left");
            StartCoroutine(ActivateInfoTextRight());
            dest_pos = right_pos;
        } else {
            //snaps to Right side of Screen
            //Debug.Log("Snaps to Point-Left, Camera-Right");
            StartCoroutine(ActivateInfoTextLeft());
            dest_pos = left_pos;
        }
        Debug.Log("Right Pos: " + right_pos + "\nLeft Pos: " + left_pos + "\nPoint Pos: " + point_pos + "\nDestination Pos: " + dest_pos);
        Go(dest_pos, point_pos);
    }

    void OnFakePlayerSwitchOn(object sender, GameObject realPlayer)
    {
        gameObject.SetActive(true);
        GameEvents.HidePlayer?.Invoke(this, EventArgs.Empty);
        transform.position = realPlayer.transform.position;
        transform.rotation = realPlayer.transform.rotation;
        transform.localScale = realPlayer.transform.localScale;
    }
    
    void OnFakePlayerSwitchOff(object sender, GameObject realPlayer)
    {
        gameObject.SetActive(false);
        GameEvents.ShowPlayer?.Invoke(this, EventArgs.Empty);
        realPlayer.transform.position = transform.position;
        realPlayer.transform.rotation = transform.rotation;
        realPlayer.transform.localScale = transform.localScale;
    }

    private IEnumerator ActivateInfoTextRight()
    {
        yield return new WaitUntil(() => (activeCampsitePoints != null));
        activeCampsitePoints.toggleInfoTextSide("right");
    }

    private IEnumerator ActivateInfoTextLeft()
    {
        yield return new WaitUntil(() => (activeCampsitePoints != null));
        activeCampsitePoints.toggleInfoTextSide("left");
    }
}
