using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudsRandomizer : MonoBehaviour

{ 
 public Material _CloudMAT;
public float minAlphaCip = 0.4f;
public float maxAlphaCip = 0.6f;
public float lerpSpeed = 0.1f;
public float timerInterval = 10.0f;

private float timer;
private float startValue;
private float targetValue;

void Start()
{
    timer = timerInterval;
    targetValue = Random.Range(minAlphaCip, maxAlphaCip);
        startValue = 0.5f;
}

void Update()
{
    timer -= Time.deltaTime;
    if (timer <= 0)
    {
        startValue = _CloudMAT.GetFloat("_AlphaCip");
        targetValue = Random.Range(minAlphaCip, maxAlphaCip);
        timer = timerInterval;
    }

    float newAlphaCip = Mathf.Lerp(startValue, targetValue, lerpSpeed * Time.deltaTime);
    _CloudMAT.SetFloat("_AlphaCip", newAlphaCip);
}
}
