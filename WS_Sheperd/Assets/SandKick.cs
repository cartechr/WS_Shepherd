using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SandKick : MonoBehaviour
{
    public ParticleSystem particleSystemPrefab;

    private Animator animator;
    private AnimationClip animationClip;

    private void Start()
    {
        animator = GetComponent<Animator>();
        animationClip = animator.runtimeAnimatorController.animationClips[0];
    }

    void PlayParticleSystem()
    {
        Vector3 position = transform.position;
        ParticleSystem particleSystem = Instantiate(particleSystemPrefab, position, Quaternion.identity);
        particleSystem.Play();

        // Get the duration of the particle system
        float duration = particleSystem.main.duration + particleSystem.main.startLifetime.constantMax;

        // Destroy the particle system game object after the duration has passed
        Destroy(particleSystem.gameObject, duration);
    }

    private void Update()
    {
        if (animator.GetCurrentAnimatorStateInfo(0).IsName(animationClip.name))
        {
            foreach (AnimationEvent animationEvent in animationClip.events)
            {
                if (animationEvent.functionName == "PlayParticleSystem" && animationEvent.time > 0 && animationEvent.time <= animator.GetCurrentAnimatorStateInfo(0).normalizedTime)
                {
                    animator.Update(animationEvent.time);
                    return;
                }
            }
        }
    }
}