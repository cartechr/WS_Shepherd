using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.AI;

public class CoolNavMeshPathDebugger : MonoBehaviour
{
    private NavMeshAgent agent;
    
    // Start is called before the first frame update
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
    }

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        if (agent)
            Handles.Label(transform.position + Vector3.up * 2.5f, agent.pathStatus.ToString());
    }
#endif
    
}
