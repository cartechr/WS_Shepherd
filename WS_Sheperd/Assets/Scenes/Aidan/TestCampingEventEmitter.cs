using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestCampingEventEmitter : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("h")) {
            Debug.Log("GO TO THE CAMPING WAYPOINTS!");
            GameEvents.EnteredCamp?.Invoke(this, EventArgs.Empty);
        }
    }
}
