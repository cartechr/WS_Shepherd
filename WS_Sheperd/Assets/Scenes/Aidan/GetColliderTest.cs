using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetColliderTest : MonoBehaviour
{
    
    [SerializeField] private Collider myCollider;
    
    // Start is called before the first frame update
    void Start()
    {
        myCollider = GetComponent<Collider>();
    }
}
