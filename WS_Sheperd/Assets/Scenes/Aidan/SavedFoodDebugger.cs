using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class SavedFoodDebugger : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI text;

    void Update()
    {
        string msg = "";
        foreach (var key in GameController.Instance().savedFoodHealths) {
            msg += key.Key + ": " + key.Value + "\n";
        }

        text.text = msg;
    }
}
