using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LoadingScreen : MonoBehaviour
{
    public GameObject LoadingScreeen;
    public Image LoadingBarFill;
    public float speed;

    private float targetLoadAmount = 0f;
    private float currentLoadAmount = 0f;
    private bool isLoading = false;
    private AsyncOperation operation;
    private float t;
    private bool past90;


    public void LoadScene(int sceneId) {
        past90 = false;
        LoadingScreeen.SetActive(true);
        operation = SceneManager.LoadSceneAsync(sceneId);
        operation.allowSceneActivation = false;
        isLoading = true;
        t = 0f;
    }

    void Update() {
        if(isLoading) {
            if(!past90) {
                targetLoadAmount = operation.progress;
            }
            if(FloatCheck(currentLoadAmount, targetLoadAmount, 0.01f, true)) {
                currentLoadAmount = Mathf.Lerp(currentLoadAmount, targetLoadAmount, t);
                LoadingBarFill.fillAmount = currentLoadAmount;
                t += speed * Time.deltaTime;
            } else {
                t = 0;
            }

            if(FloatCheck(currentLoadAmount, 0.9f, 0.01f, false)) {
                past90 = true;
                targetLoadAmount = 1f;
            }

            if(FloatCheck(currentLoadAmount, 1f, 0.01f, false)) {
                operation.allowSceneActivation = true;
            }
        }
    }

    private bool FloatCheck(float a, float b, float e, bool isGreater) {
        if(isGreater) {
            return Mathf.Abs(a - b) > e;
        }
        return Mathf.Abs(a - b) < e;
    }
}