using Borodar.FarlandSkies.LowPoly;
using UnityEngine;

public class ChangeSandColor : MonoBehaviour



{
    [SerializeField] private Color dayColor1;
    [SerializeField] private Color dayColor2;
    [SerializeField] private Color duskColor1;
    [SerializeField] private Color duskColor2;
    [SerializeField] private Color dawnColor1;
    [SerializeField] private Color dawnColor2;
    [SerializeField] private Color nightColor1;
    [SerializeField] private Color nightColor2;

    public ParticleSystem _particleSystem;

    private void Start()
    {
        _particleSystem = GetComponent<ParticleSystem>();
    }

    private void Update()
    {
        SkyboxCycleManager skyboxCycleManager = SkyboxCycleManager.Instance;

        if (skyboxCycleManager.CycleProgress >= 30 && skyboxCycleManager.CycleProgress < 55)
        {
            _particleSystem.startColor = Color.Lerp(dayColor1, dayColor2, skyboxCycleManager.CycleProgress / 100f);
        }
        if (skyboxCycleManager.CycleProgress >= 5 && skyboxCycleManager.CycleProgress < 30)
        {
            _particleSystem.startColor = Color.Lerp(dawnColor1, dawnColor2, skyboxCycleManager.CycleProgress / 100f);
        }
        if  (skyboxCycleManager.CycleProgress >= 55 && skyboxCycleManager.CycleProgress < 80)
        {
            _particleSystem.startColor = Color.Lerp(duskColor1, duskColor2, skyboxCycleManager.CycleProgress / 100f);
        }
        if (skyboxCycleManager.CycleProgress >= 80 || skyboxCycleManager.CycleProgress < 5)
        {
            _particleSystem.startColor = Color.Lerp(nightColor1, nightColor2, skyboxCycleManager.CycleProgress / 100f);
        }
    }

}

