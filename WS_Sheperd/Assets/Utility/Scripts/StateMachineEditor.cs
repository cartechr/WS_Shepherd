using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;


#if UNITY_EDITOR 

    [CustomEditor(typeof(StateMachine))]
    public class StateMachineEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            StateMachine myStateMachine = (StateMachine)target;
            if (GUILayout.Button("Set State")) {
               myStateMachine.DebugSetState();
            }
            if (GUILayout.Button("Set State With Object")) {
                myStateMachine.DebugSetStateWithParameter();
            }
        }
    }
#endif
