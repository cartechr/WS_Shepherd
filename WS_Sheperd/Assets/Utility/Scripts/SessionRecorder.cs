using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class SessionRecorder : MonoBehaviour
{
    
    public static GameObject instance;

    [SerializeField] private bool isRecording = true;
    [SerializeField] private float recordEverySeconds = 2f;
    private float recordTimer = 0;
    private string session_timestamp;

    [SerializeField] private GameObject player;
    [SerializeField] private GameObject sheepLamb;
    [SerializeField] private GameObject sheepBeefy;
    [SerializeField] private GameObject sheepStubborn;
    [SerializeField] private GameObject sheepLoud;
    [SerializeField] private GameObject sheepReliable;
    [SerializeField] private GameObject sheepFlighty;
    [SerializeField] private GameObject sheepLeader;
    
    private SheepHealth sheepLambSheepHealth;
    private SheepHealth sheepBeefySheepHealth;
    private SheepHealth sheepStubbornSheepHealth;
    private SheepHealth sheepLoudSheepHealth;
    private SheepHealth sheepReliableSheepHealth;
    private SheepHealth sheepFlightySheepHealth;
    private SheepHealth sheepLeaderSheepHealth;
    
    private StateMachine sheepLambStateMachine;
    private StateMachine sheepBeefyStateMachine;
    private StateMachine sheepStubbornStateMachine;
    private StateMachine sheepLoudStateMachine;
    private StateMachine sheepReliableStateMachine;
    private StateMachine sheepFlightyStateMachine;
    private StateMachine sheepLeaderStateMachine;

    private void Awake()
    {
        if (instance) {
            Destroy(gameObject);
        } else {
            instance = gameObject;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        session_timestamp = System.DateTime.Now.ToString().Replace('/','-').Replace(':','-');
        CreateHeaders();
        
        DontDestroyOnLoad(this.gameObject);
        
        sheepLambSheepHealth = sheepLamb.GetComponent<SheepHealth>();
        sheepBeefySheepHealth = sheepLamb.GetComponent<SheepHealth>();
        sheepStubbornSheepHealth = sheepLamb.GetComponent<SheepHealth>();
        sheepLoudSheepHealth = sheepLamb.GetComponent<SheepHealth>();
        sheepReliableSheepHealth = sheepLamb.GetComponent<SheepHealth>();
        sheepFlightySheepHealth = sheepLamb.GetComponent<SheepHealth>();
        sheepLeaderSheepHealth = sheepLamb.GetComponent<SheepHealth>();
        
        sheepLambStateMachine = sheepLamb.GetComponent<StateMachine>();
        sheepBeefyStateMachine = sheepBeefy.GetComponent<StateMachine>();
        sheepStubbornStateMachine = sheepStubborn.GetComponent<StateMachine>();
        sheepLoudStateMachine = sheepLoud.GetComponent<StateMachine>();
        sheepReliableStateMachine = sheepReliable.GetComponent<StateMachine>();
        sheepFlightyStateMachine = sheepFlighty.GetComponent<StateMachine>();
        sheepLeaderStateMachine = sheepLeader.GetComponent<StateMachine>();
    }

    // Update is called once per frame
    void Update()
    {
        recordTimer -= Time.deltaTime;
        if (recordTimer <= 0) {
            RecordData();
            recordTimer = recordEverySeconds;
        }
    }

    public void CreateHeaders()
    {
        Directory.CreateDirectory(Application.dataPath + "/TelemetryData");
        string path = Application.dataPath + "/TelemetryData/" + session_timestamp + ".csv";
        StreamWriter writer = new StreamWriter(path, true);
        string headers = PosNameXYZ("Player") +
                         SheepHeader("SheepLamb") +
                         SheepHeader("SheepBeefy") +
                         SheepHeader("SheepStubborn") +
                         SheepHeader("SheepLoud") +
                         SheepHeader("SheepReliable") +
                         SheepHeader("SheepFlighty") +
                         SheepHeader("SheepLeader");
        writer.WriteLine(headers);
        writer.Close();
    }

    public void RecordData()
    {
        Directory.CreateDirectory(Application.dataPath + "/TelemetryData");
        string path = Application.dataPath + "/TelemetryData/" + session_timestamp + ".csv";
        StreamWriter writer = new StreamWriter(path, true);
        string data = TransformToXYZ(player.transform) +
                      SheepData(sheepLamb, sheepLambSheepHealth, sheepLambStateMachine) +
                      SheepData(sheepBeefy, sheepBeefySheepHealth, sheepBeefyStateMachine) +
                      SheepData(sheepStubborn, sheepStubbornSheepHealth, sheepStubbornStateMachine) +
                      SheepData(sheepLoud, sheepLoudSheepHealth, sheepLoudStateMachine) +
                      SheepData(sheepReliable, sheepReliableSheepHealth, sheepReliableStateMachine) +
                      SheepData(sheepFlighty, sheepFlightySheepHealth, sheepFlightyStateMachine) +
                      SheepData(sheepLeader, sheepLeaderSheepHealth, sheepLeaderStateMachine);
        writer.WriteLine(data);
        writer.Close();
    }
    
    string PosNameXYZ(string _name)
    {
        return _name + "X," + _name + "Y," + _name + "Z,";
    }

    string SheepHeader(string _name)
    {
        return PosNameXYZ(_name) +
               _name + "Food," +
               _name + "Water," +
               _name + "Stamina," +
               _name + "MaxStamina," +
               _name + "Health," +
               _name + "MaxHealth," +
               _name + "IsEating," +
               _name + "IsDrinking," +
               _name + "Friendship," +
               _name + "State,";
    }

    string TransformToXYZ(Transform p_transform)
    {
        Vector3 pos = p_transform.position;
        return pos.x + "," + pos.y + "," + pos.z + ",";
    }

    string SheepData(GameObject p_sheep, SheepHealth sheepHealth, StateMachine stateMachine)
    {
        return TransformToXYZ(p_sheep.transform) +
               sheepHealth.food + "," +
               sheepHealth.water + "," +
               sheepHealth.sheepStamina + "," +
               sheepHealth.maxSheepStamina + "," +
               sheepHealth.currentHealth + "," +
               sheepHealth.maxHealth + "," +
               sheepHealth.is_eating + "," +
               sheepHealth.is_drinking + "," +
               sheepHealth.friendshipStats.friendship + "," +
               stateMachine.currentStateName + ",";
    }
}
