

#if (UNITY_EDITOR)
    using System.Collections.Generic;
    using System.Linq;
    using UnityEditor;
    using UnityEditor.AI;
    using UnityEngine;
    using UnityEngine.SceneManagement;
    public class TerrainNavMeshBaker 
    {
        private static Dictionary<Terrain, TreeInstance[]> _terrainTrees = new Dictionary<Terrain, TreeInstance[]>();

        [MenuItem("Tools/Bake NavMesh")]
        public static void BakeNavMesh()
        {
            try
            {
                RemoveTrees();
                NavMeshBuilder.BuildNavMesh();
            }
            finally
            {
                RestoreTrees();
            }
        }

        private static void RestoreTrees()
        {
            foreach (var terrain in _terrainTrees)
            {
                terrain.Key.terrainData.treeInstances = terrain.Value;
            }
        }

        private static void RemoveTrees()
        {
            var terrains = new List<Terrain>();

            for (int i = 0; i < SceneManager.sceneCount; i++)
            {
                var scene = SceneManager.GetSceneAt(i);

                if (!scene.isLoaded)
                    continue;

                terrains.AddRange(scene.GetRootGameObjects().SelectMany(x => x.GetComponentsInChildren<Terrain>()));
            }

            _terrainTrees.Clear();
            foreach (var terrain in terrains)
            {
                // save old trees
                _terrainTrees[terrain] = terrain.terrainData.treeInstances;
                
                // create a new list of empty trees
                var solidTrees = new List<TreeInstance>();

                foreach (var treeInstance in _terrainTrees[terrain]) {
                    GameObject treeObject = terrain.terrainData.treePrototypes[treeInstance.prototypeIndex].prefab.gameObject;
                    Collider treeCollider = treeObject.GetComponent<Collider>();
                    if (treeObject.name == "Cactus_l_BT_02") {
                        Debug.Log("HIYAH!");
                    }
                    if (treeCollider && !treeCollider.isTrigger) {
                        solidTrees.Add(treeInstance);
                        continue;
                    }

                    for (int i = 0; i < treeObject.transform.childCount; i++) {
                        Collider childCollider = treeObject.transform.GetChild(i).GetComponent<Collider>();
                        if (childCollider && !childCollider.isTrigger) {
                            solidTrees.Add(treeInstance);
                            break;
                        }
                    }
                    /*
                    foreach (var childCollider in treeObject.GetComponentsInChildren<Collider>()) {
                        if (!childCollider.isTrigger) {
                            solidTrees.Add(treeInstance);
                            break;
                        }
                    }
                    */
                }

                // convert list to array and attach only the solid trees to terrain
                terrain.terrainData.treeInstances = solidTrees.ToArray();
            }
        }
    }
#endif

// original script by unity-marcus from this thread: 
// https://answers.unity.com/questions/365280/ignoring-trees-when-baking-navmesh-on-terrain.html