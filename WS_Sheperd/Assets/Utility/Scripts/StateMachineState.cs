using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
    A State for a State Machine. RunState is called from the State Machine
      when it is in this state. RunState is meant to be overridden in a subclass.
      You may want to include a reference to the component the state machine acts on
      in the subclass so you may call its functions within RunState. 
*/
public class StateMachineState : MonoBehaviour
{

    // override the stateName in subclasses in Awake, do not change it during runtime
    [NonSerialized] public string stateName;
    // an object that behaviours may interpret in their specific ways
    [NonSerialized] public GameObject parameterObject;

    // you will have to hook up to the StateMachine manually
    [SerializeField] protected StateMachine myStateMachine;
    // don't write to this, just read it
    [NonSerialized] public bool is_active;
    // a list of state names that this state is not permitted to switch to
    [NonSerialized] public List<string> disallowSwitchingToStates = new List<string>();
    // can this state switch into itself?
    [NonSerialized] public bool can_switch_to_itself = false;
    // if this state tries to switch into itself, will its timeInState reset?
    [NonSerialized] public bool can_reset_timer = false;

    // if you override Start, please call base.Start()
    protected virtual void Start()
    {
        if (myStateMachine == null) {
            myStateMachine = GetComponent<StateMachine>();
        }
        myStateMachine.AddState(this, stateName);
    }

    // Don't change the state within EnterState! Use SetStateDeferred instead
    public virtual void EnterState()
    {
        // override with state logic
    }

    public virtual void RunState()
    {
        // override with state logic
    }

    // Don't change the state within ExitState! Use SetStateDeferred instead
    public virtual void ExitState()
    {
        // override with state logic
    }
    
    public virtual bool CanEnterState()
    {
        return true;
    }
    
}
