using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using Random = UnityEngine.Random;

public class StateMachine : MonoBehaviour
{

    [SerializeField] private bool automaticallyCallState;
    
    private Dictionary<string, StateMachineState> myStates = new Dictionary<string, StateMachineState>();
    public StateMachineState currentState = null;
    public float timeInState = 0f;
    public StateMachineState previousState = null;
    private string deferredState = "";

    public string currentStateName
    {
        get { return currentState.stateName; }
    }

    // Start is called before the first frame update
    void Start()
    {
        currentState.is_active = true;
    }

    /// I chose to do this only on updates since this doesn't really
    // directly interact with the physics system; it's more akin to 
    // a player giving commands, and doesn't need to be super fast.
    // If you want it to run on FixedUpdate, make a subclass and override
    // Update to do nothing, and then copy this into FixedUpdate
    void Update()
    {
        if (automaticallyCallState) {
            if (currentState) {
                timeInState += Time.deltaTime;
                currentState.RunState();
                ProcessDeferredSetState();
            }
        }
    }

    public void AddState(StateMachineState new_state, string state_name)
    {
        if (String.IsNullOrEmpty(state_name)) {
            throw new ArgumentException(
                "State" + new_state + ", " + state_name + " was added to " + gameObject + "'s StateMachine with a null or empty name");
        }
        myStates.Add(state_name, new_state);
    }

    public void SetState(string state_name)
    {
        if (myStates[state_name] == currentState && !currentState.can_switch_to_itself) {
            if (currentState.can_reset_timer) {
                timeInState = 0;
            }
            return;
        }
        if (currentState.disallowSwitchingToStates.Contains(state_name)) {
            Debug.LogWarning($"A sheep tried to switch to a disallowed state: {currentStateName} -> {state_name}");
            return;
        }
        
        currentState.is_active = false;
        currentState.ExitState();
        previousState = currentState;
        
        currentState = myStates[state_name];
        timeInState = 0f;
        currentState.is_active = true;
        currentState.EnterState();
    }

    public void SetStateWithObject(string state_name, GameObject arg)
    {
        if (myStates[state_name] == currentState) {
            return;
        }
        if (currentState.disallowSwitchingToStates.Contains(state_name)) {
            return;
        }
        
        currentState.is_active = false;
        currentState.ExitState();
        previousState = currentState;
        
        currentState = myStates[state_name];
        timeInState = 0f;
        currentState.is_active = true;
        currentState.parameterObject = arg;
        currentState.EnterState();
    }

    public bool SetStateWithProbability(string state_name, float probability)
    {
        if (Random.value <= probability) {
            SetState(state_name);
            return true;
        }
        return false;
    }

    public void SetStateDeferred(string state_name)
    {
        deferredState = state_name;
    }

    public void ProcessDeferredSetState()
    {
        if (deferredState != "") {
            SetState(deferredState);
            deferredState = "";
        }
    }
    
    // SetStatesWithProbabilities({stateA, stateB}, {0.2, 0.4})
    // that would give stateA a probability of 20% and stateB a probability of 40%, with a 40% chance of doing nothing
    public bool SetStatesWithProbabilities(string[] state_names, float[] probabilities)
    {
        if (state_names.Length != probabilities.Length) {
            throw new ArgumentException("SetStatesWithProbabilities has been called with an invalid state_name and probability format");
        }

        float lower_probability = 0;
        float random_value = Random.value;
        for (int i = 0; i < probabilities.Length; i++) {
            float probability = lower_probability + probabilities[i];
            if (random_value <= probability) {
                SetState(state_names[i]);
                return true;
            }
            lower_probability += probabilities[i];
        }

        return false;
    }

    public bool CanEnterState(string state_name)
    {
        return myStates[state_name].CanEnterState();
    }

    // use sparingly
    public StateMachineState GetState(string state_name)
    {
        return myStates[state_name];
    }

    [SerializeField] private string Debug_SetStateTo;
    
    public void DebugSetState()
    {
        SetState(Debug_SetStateTo);
    }

    [SerializeField] private GameObject Debug_ParameterObject;

    public void DebugSetStateWithParameter()
    {
        SetStateWithObject(Debug_SetStateTo, Debug_ParameterObject);
    }

#if UNITY_EDITOR
    public void OnDrawGizmos()
    {
        Handles.Label(transform.position + Vector3.up * 2, currentState.stateName);
    }
#endif
    
}
