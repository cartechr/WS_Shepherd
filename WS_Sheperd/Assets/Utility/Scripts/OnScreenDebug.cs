using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class OnScreenDebug : MonoBehaviour
{

    public static OnScreenDebug instance;

    public bool disabled = false;

    public static Dictionary<string, string> Category = new Dictionary<string, string>();

    private TextMeshProUGUI textbox;
    [SerializeField] private TMP_InputField inputField;

    private bool is_searching = false;
    private string current_category = null;

    // Start is called before the first frame update
    void Awake()
    {
        if (instance) {
            Debug.LogError("ARRRRGH THERE IS MORE THAN ONE ONSCREENDEBUG");
            Destroy(this);
        } else {
            instance = this;
        }
    }

    private void Start()
    {
        textbox = GetComponent<TextMeshProUGUI>();
        inputField.gameObject.SetActive(false);
        inputField.onSubmit.AddListener(OnSubmit);
        SetCategoryShortcut("Help", "F3 to Search Debug, F4/F5 to decrease/increase font size, F6 to toggle heavy data collection, hold F7 to fast-forward at 4x speed. Search Categories for a list of searchable debugs");
        SetCategoryShortcut("Categories", _categories_list);
    }

    private void OnDestroy()
    {
        inputField.onSubmit.RemoveListener(OnSubmit);
    }

    public void SetText(string text)
    {
        textbox.text = text;
    }

    private string _categories_list = "";
    public void SetCategory(string category, string text)
    {
        // the check needs to happen before assignment;
        // assignment needs to happen before recursion
        if (!Category.ContainsKey(category)) {
            Category[category] = text;
            _categories_list += $"{category}, ";
            SetCategory("Categories", _categories_list);
        } else {
            Category[category] = text;
        }
    }

    // D for direct, idk
    public static void SetTextShortcut(string text)
    {
        instance.SetText(text);
    }
    
    public static void SetCategoryShortcut(string category, string text)
    {
        instance.SetCategory(category, text);
    }

    public static bool IsDisabled()
    {
        return instance.disabled;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.F3)) {
            is_searching = !is_searching;
            inputField.gameObject.SetActive(is_searching);
            if (is_searching) {
                inputField.ActivateInputField();
            }
        }
        if (Input.GetKeyDown(KeyCode.F4)) {
            textbox.fontSize -= 1f;
        }
        if (Input.GetKeyDown(KeyCode.F5)) {
            textbox.fontSize += 1f;
        }
        if (Input.GetKeyDown(KeyCode.F6)) {
            disabled = !disabled;
        }
        
        if (Input.GetKeyDown(KeyCode.F7)) {
            Time.timeScale = 4;
        }
        if (Input.GetKeyUp(KeyCode.F7)) {
            Time.timeScale = 1;
        }

        if (current_category != null) {
            SetText(Category[current_category]);
        }
    }

    private void OnSubmit(string debug_search)
    {
        inputField.text = "";
        inputField.DeactivateInputField();
        is_searching = false;
        inputField.gameObject.SetActive(false);
        current_category = debug_search;
        if (!Category.ContainsKey(current_category)) {
            current_category = null;
        }
    }
}
