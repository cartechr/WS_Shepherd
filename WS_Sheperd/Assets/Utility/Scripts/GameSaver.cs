using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;

public class GameSaver : MonoBehaviour
{
    
    public static GameObject instance;

    private string session_timestamp;
    private string save_slot;
    private string path;

    private List<(string, Exception)> load_failures = new List<(string, Exception)>();

    [SerializeField] private GameObject player;
    [SerializeField] private GameController gameController;
    [SerializeField] private GameObject sheepLamb;
    [SerializeField] private GameObject sheepBeefy;
    [SerializeField] private GameObject sheepStubborn;
    [SerializeField] private GameObject sheepLoud;
    [SerializeField] private GameObject sheepReliable;
    [SerializeField] private GameObject sheepFlighty;
    [SerializeField] private GameObject sheepLeader;
    
    private SheepHealth sheepLambSheepHealth;
    private SheepHealth sheepBeefySheepHealth;
    private SheepHealth sheepStubbornSheepHealth;
    private SheepHealth sheepLoudSheepHealth;
    private SheepHealth sheepReliableSheepHealth;
    private SheepHealth sheepFlightySheepHealth;
    private SheepHealth sheepLeaderSheepHealth;
    
    private StateMachine sheepLambStateMachine;
    private StateMachine sheepBeefyStateMachine;
    private StateMachine sheepStubbornStateMachine;
    private StateMachine sheepLoudStateMachine;
    private StateMachine sheepReliableStateMachine;
    private StateMachine sheepFlightyStateMachine;
    private StateMachine sheepLeaderStateMachine;

    private void Awake()
    {
        if (instance) {
            Destroy(gameObject);
        } else {
            instance = gameObject;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        session_timestamp = System.DateTime.Now.ToString().Replace('/','-').Replace(':','-');
        save_slot = "SaveFile1";
        path = Application.persistentDataPath + "/SaveData/" + save_slot + ".json";
        //CreateHeaders();
        
        DontDestroyOnLoad(this.gameObject);
        
        sheepLambSheepHealth = sheepLamb.GetComponent<SheepHealth>();
        sheepBeefySheepHealth = sheepLamb.GetComponent<SheepHealth>();
        sheepStubbornSheepHealth = sheepLamb.GetComponent<SheepHealth>();
        sheepLoudSheepHealth = sheepLamb.GetComponent<SheepHealth>();
        sheepReliableSheepHealth = sheepLamb.GetComponent<SheepHealth>();
        sheepFlightySheepHealth = sheepLamb.GetComponent<SheepHealth>();
        sheepLeaderSheepHealth = sheepLamb.GetComponent<SheepHealth>();
        
        sheepLambStateMachine = sheepLamb.GetComponent<StateMachine>();
        sheepBeefyStateMachine = sheepBeefy.GetComponent<StateMachine>();
        sheepStubbornStateMachine = sheepStubborn.GetComponent<StateMachine>();
        sheepLoudStateMachine = sheepLoud.GetComponent<StateMachine>();
        sheepReliableStateMachine = sheepReliable.GetComponent<StateMachine>();
        sheepFlightyStateMachine = sheepFlighty.GetComponent<StateMachine>();
        sheepLeaderStateMachine = sheepLeader.GetComponent<StateMachine>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Save")) {
            RecordData();
        }
        if (Input.GetButtonDown("Load")) {
            StartCoroutine(RestoreData());
        }
    }

    public void CreateHeaders()
    {
        Directory.CreateDirectory(Application.dataPath + "/TelemetryData");
        string path = Application.dataPath + "/TelemetryData/" + session_timestamp + ".csv";
        StreamWriter writer = new StreamWriter(path, true);
        string headers = PosNameXYZ("Player") +
                         SheepHeader("SheepLamb") +
                         SheepHeader("SheepBeefy") +
                         SheepHeader("SheepStubborn") +
                         SheepHeader("SheepLoud") +
                         SheepHeader("SheepReliable") +
                         SheepHeader("SheepFlighty") +
                         SheepHeader("SheepLeader");
        writer.WriteLine(headers);
        writer.Close();
    }

    public void RecordData()
    {
        Directory.CreateDirectory(Application.persistentDataPath + "/SaveData");
        StreamWriter writer = new StreamWriter(path, false);
        
        // event abuse
        FloatArgs time_of_day_vessel = new FloatArgs(0);
        GameEvents.GetTimeOfDay(this, time_of_day_vessel);
        float time_of_day = time_of_day_vessel.number;
        
        var foodList = new List<object>();
        foreach (var pair in gameController.savedFoodHealths) {
            foodList.Add(new Dictionary<string, object>{
                ["Key"] = pair.Key,
                ["Value"] = pair.Value
            });
        }
        var waterList = new List<object>();
        foreach (var pair in gameController.savedWaterHealths) {
            waterList.Add(new Dictionary<string, object>{ 
                ["Key"] = pair.Key,
                ["Value"] = pair.Value
            });
        }
        var sheepTreatFruitList = new List<object>();
        foreach (var pair in gameController.savedSheepTreatFruits) {
            sheepTreatFruitList.Add(new Dictionary<string, object>{ 
                ["Key"] = pair.Key,
                ["Value"] = pair.Value
            });
        }
        var brambleList = new List<object>();
        foreach (var pair in gameController.savedBrambleStates) {
            brambleList.Add(new Dictionary<string, object>{ 
                ["Key"] = pair.Key,
                ["Value"] = new Dictionary<string, object>()
                {
                    ["State"] = pair.Value.Item1,
                    ["CaughtSheep"] = pair.Value.Item2
                }
            });
        }
        var coyoteList = new List<object>();
        foreach (var pair in gameController.savedCoyotes) {
            coyoteList.Add(new Dictionary<string, object>{ 
                ["Key"] = pair.Key,
                ["Value"] = new Dictionary<string, object>()
                {
                    ["Activated"] = pair.Value.Item1,
                    ["State"] = pair.Value.Item2,
                    ["RepelHealth"] = pair.Value.Item3,
                    ["Position"] = Vector3ToXYZ(pair.Value.Item4),
                    ["Rotation"] = Vector3ToXYZ(pair.Value.Item5)
                    
                }
            });
        }
        var playerRB = player.GetComponent<Rigidbody>();
        var playerPlayer = player.GetComponent<Player>();
        var data = new Dictionary<string, object>
        {
            ["SceneIndex"] = SceneManager.GetActiveScene().buildIndex,
            ["Player"] = new Dictionary<string, object>
            {
                ["Position"] = Vector3ToXYZ(player.transform.position),
                ["Rotation"] = Vector3ToXYZ(player.transform.rotation.eulerAngles),
                ["Velocity"] = Vector3ToXYZ(playerRB.velocity),
                ["CooldownTimer"] = playerPlayer.coolDownTimer,
                ["PlayerTreat"] = playerPlayer.PlayerTreat,
                ["SeenIntroCutscene"] = playerPlayer.CameraSwapShep.seenIntroCutscene,
                ["SeenCoyoteWaterfallCutscene"] = playerPlayer.CameraSwapShep.seenCoyoteWaterfallCutscene,
                ["PlayedBaseballMinigame"] = false, // https://www.youtube.com/watch?v=G1JNhQO79IE
            },
            ["GameController"] = new Dictionary<string, object>
            {
                ["SavedFood"] = foodList,
                ["SavedWater"] = waterList,
                ["SavedSheepTreatFruits"] = sheepTreatFruitList,
                ["SavedBrambles"] = brambleList,
                ["SavedCoyotes"] = coyoteList,
            },
            ["TimeOfDay"] = time_of_day,
            ["Tutorial"] = new Dictionary<string, object>
            {
                ["Objectives"] = PrepareDict(Tutorial.TutorialDict),
                ["CurrentObjective"] = gameController.tutorial.GetCurrentObjectiveCanvasName(),
                ["ObjectiveCompleted"] = gameController.tutorial.IsCurrentObjectiveCompleted(),
                ["FadingOut"] = gameController.tutorial.IsFadingOut(),
            },
            ["SheepLamb"] = SheepData(sheepLamb),
            ["SheepBeefy"] = SheepData(sheepBeefy),
            ["SheepStubborn"] = SheepData(sheepStubborn),
            ["SheepLoud"] = SheepData(sheepLoud),
            ["SheepReliable"] = SheepData(sheepReliable),
            ["SheepFlighty"] = SheepData(sheepFlighty),
            ["SheepLeader"] = SheepData(sheepLeader),
        };
        /*
        string data = TransformToXYZ(player.transform) +
                      SheepData(sheepLamb, sheepLambSheepHealth, sheepLambStateMachine) +
                      SheepData(sheepBeefy, sheepBeefySheepHealth, sheepBeefyStateMachine) +
                      SheepData(sheepStubborn, sheepStubbornSheepHealth, sheepStubbornStateMachine) +
                      SheepData(sheepLoud, sheepLoudSheepHealth, sheepLoudStateMachine) +
                      SheepData(sheepReliable, sheepReliableSheepHealth, sheepReliableStateMachine) +
                      SheepData(sheepFlighty, sheepFlightySheepHealth, sheepFlightyStateMachine) +
                      SheepData(sheepLeader, sheepLeaderSheepHealth, sheepLeaderStateMachine);
        */
        string in_json = DictToJSON(data, 0);
        writer.WriteLine(in_json);
        writer.Close();
        Debug.Log("Saved game at " + path);
    }

    IEnumerator RestoreData()
    {
        load_failures.Clear();

        StreamReader reader = new StreamReader(path);
        string in_json = reader.ReadToEnd();
        Dictionary<string, object> data = JSONToDict(in_json.Substring(1, in_json.Length - 2));

        int scene_index = LoadInt(1, data, "SceneIndex");
        if (SceneManager.GetActiveScene().buildIndex != scene_index) {
            SceneManager.LoadScene(scene_index);
            yield return null;
        }
        
        player.transform.position = LoadVector3(player.transform.position, data, "Player", "Position");
        player.transform.eulerAngles = LoadVector3(player.transform.eulerAngles, data, "Player", "Rotation");
        var playerRB = player.GetComponent<Rigidbody>();
        playerRB.velocity = LoadVector3(playerRB.velocity, data, "Player", "Velocity");
        var playerPlayer = player.GetComponent<Player>();
        TryLoadFloat(ref playerPlayer.coolDownTimer, data, "Player", "CooldownTimer");
        TryLoadInt(ref playerPlayer.PlayerTreat, data, "Player", "PlayerTreat");
        playerPlayer.CameraSwapShep.ResetToDefaultCamera();
        TryLoadBool(ref playerPlayer.CameraSwapShep.seenIntroCutscene, data, "Player", "SeenIntroCutscene");
        TryLoadBool(ref playerPlayer.CameraSwapShep.seenCoyoteWaterfallCutscene, data, "Player", "SeenCoyoteWaterfallCutscene");

        gameController.savedFoodHealths.Clear();
        List<object> savedFoodList = LoadList(data, "GameController", "SavedFood");
        for (int i = 0; i < savedFoodList.Count; i++) {
            int key = LoadInt(0, savedFoodList, i, "Key");
            int value = LoadInt(0, savedFoodList, i, "Value");
            gameController.savedFoodHealths[key] = value;
        }

        gameController.savedWaterHealths.Clear();
        List<object> savedWaterList = LoadList(data, "GameController", "SavedWater");
        for (int i = 0; i < savedWaterList.Count; i++) {
            int key = LoadInt(0, savedWaterList, i, "Key");
            int value = LoadInt(0, savedWaterList, i, "Value");
            gameController.savedWaterHealths[key] = value;
        }

        gameController.savedSheepTreatFruits.Clear();
        List<object> savedSheepTreatFruitsList = LoadList(data, "GameController", "SavedSheepTreatFruits");
        for (int i = 0; i < savedSheepTreatFruitsList.Count; i++) {
            int key = LoadInt(0, savedSheepTreatFruitsList, i, "Key");
            int value = LoadInt(0, savedSheepTreatFruitsList, i, "Value");
            gameController.savedSheepTreatFruits[key] = value;
        }

        gameController.savedBrambleStates.Clear();
        List<object> savedBrambleList = LoadList(data, "GameController", "SavedBrambles");
        for (int i = 0; i < savedBrambleList.Count; i++) {
            int key = LoadInt(0, savedBrambleList, i, "Key");
            bool disarmed = LoadBool(false, savedBrambleList, i, "Value", "State");
            int caughtSheepRole =
                LoadInt((int) SheepHealth.SheepRole.None, savedBrambleList, i, "Value", "CaughtSheep");
            gameController.savedBrambleStates[key] = (disarmed, caughtSheepRole);
        }

        gameController.savedCoyotes.Clear();
        List<object> savedCoyoteList = LoadList(data, "GameController", "SavedCoyotes");
        for (int i = 0; i < savedCoyoteList.Count; i++) {
            int key = LoadInt(0, savedCoyoteList, i, "Key");
            bool activated = LoadBool(false, savedCoyoteList, i, "Value", "Activated");
            int coyoteState = LoadInt((int) CoyoteState.Combat, savedCoyoteList, i, "Value", "State");
            int repelHealth = LoadInt(3, savedCoyoteList, i, "Value", "RepelHealth");
            Vector3 coyotePos = LoadVector3(Vector3.negativeInfinity, savedCoyoteList, i, "Value", "Position");
            Vector3 coyoteRot = LoadVector3(Vector3.negativeInfinity, savedCoyoteList, i, "Value", "Rotation");
            gameController.savedCoyotes[key] = (activated, coyoteState, repelHealth, coyotePos, coyoteRot);
        }

        GameEvents.LoadFromSave?.Invoke(this, EventArgs.Empty);

        GameEvents.SetTimeOfDay(this, new FloatArgs(LoadFloat(0, data, "TimeOfDay")));
        Time.timeScale = 1f;

        foreach (var pair in new Dictionary<string, bool>(Tutorial.TutorialDict)) {
            Tutorial.TutorialDict[pair.Key] = LoadBool(pair.Value, data, "Tutorial", "Objectives", pair.Key);
        }

        gameController.tutorial.SetCurrentObjectiveByName(
            LoadString(gameController.tutorial.GetCurrentObjectiveCanvasName(), data, "Tutorial", "CurrentObjective")
        );
        gameController.tutorial.SetCurrentObjectiveCompleted(
            LoadBool(gameController.tutorial.objective_completed, data, "Tutorial", "ObjectiveCompleted")
        );
        gameController.tutorial.SetFadingOut(
            LoadBool(gameController.tutorial.IsFadingOut(), data, "Tutorial", "FadingOut")
        );
        gameController.tutorial.RecheckVisible();

        RestoreSheep(data, "SheepLamb", sheepLamb);
        RestoreSheep(data, "SheepBeefy", sheepBeefy);
        RestoreSheep(data, "SheepStubborn", sheepStubborn);
        RestoreSheep(data, "SheepLoud", sheepLoud);
        RestoreSheep(data, "SheepReliable", sheepReliable);
        RestoreSheep(data, "SheepFlighty", sheepFlighty);
        RestoreSheep(data, "SheepLeader", sheepLeader);
        reader.Close();

        try {
            string msg = "Load Failures: ";
            foreach (var failure in load_failures) {
                msg += $"{failure.Item1}, ";
                Debug.LogException(failure.Item2);
            }
            OnScreenDebug.SetCategoryShortcut("LoadFailures", msg);
        } catch {} // suppressing all errors is intentional. this is debug.
    }

    string PosNameXYZ(string _name)
    {
        return _name + "X," + _name + "Y," + _name + "Z,";
    }

    string SheepHeader(string _name)
    {
        return PosNameXYZ(_name) +
               _name + "Food," +
               _name + "Water," +
               _name + "Stamina," +
               _name + "MaxStamina," +
               _name + "Health," +
               _name + "MaxHealth," +
               _name + "IsEating," +
               _name + "IsDrinking," +
               _name + "Friendship," +
               _name + "State,";
    }

    Dictionary<string, object> Vector3ToXYZ(Vector3 vec)
    {
        return new Dictionary<string, object>
        {
            ["X"] = vec.x,
            ["Y"] = vec.y,
            ["Z"] = vec.z
        };
    }

    Vector3 XYZToVector3(Dictionary<string, object> xyz)
    {
        float x = (float) Convert.ToSingle(xyz["X"]);
        float y = (float) Convert.ToSingle(xyz["Y"]);
        float z = (float) Convert.ToSingle(xyz["Z"]);
        return new Vector3(x, y, z);
        //return new Vector3((float) xyz["X"], (float) xyz["Y"], (float) xyz["Z"]);
    }

    Dictionary<string, object> SheepData(GameObject p_sheep)
    {
        SheepHealth sheepHealth = p_sheep.GetComponent<SheepHealth>();
        StateMachine stateMachine = p_sheep.GetComponent<StateMachine>();
        SheepFriendship friendshipStats = p_sheep.GetComponent<SheepFriendship>();
        return new Dictionary<string, object>
        {
            ["Position"] = Vector3ToXYZ(p_sheep.transform.position),
            ["Rotation"] = Vector3ToXYZ(p_sheep.transform.eulerAngles),
            ["Food"] = sheepHealth.food,
            ["Water"] = sheepHealth.water,
            ["Stamina"] = sheepHealth.sheepStamina,
            ["MaxStamina"] = sheepHealth.maxSheepStamina,
            ["Health"] = sheepHealth.currentHealth,
            ["MaxHealth"] = sheepHealth.maxHealth,
            ["IsEating"] = sheepHealth.is_eating,
            ["IsDrinking"] = sheepHealth.is_drinking,
            ["Friendship"] = friendshipStats.friendship,
            ["MaxFriendship"] = friendshipStats.maximumFriendship,
            ["MinFriendship"] = friendshipStats.minimumFriendship,
            ["State"] = stateMachine.currentStateName,
            ["TimeInState"] = stateMachine.timeInState,
            ["EasyMode"] = sheepHealth.easy_mode,
            // mood controller?
        };
    }

    void RestoreSheep(Dictionary<string, object> data, string keyName, GameObject sheep)
    {
        SheepHealth sheepHealth = sheep.GetComponent<SheepHealth>();
        StateMachine stateMachine = sheep.GetComponent<StateMachine>();
        SheepFriendship friendshipStats = sheep.GetComponent<SheepFriendship>();
        NavMeshAgent sheepNaveMeshAgent = sheep.GetComponent<NavMeshAgent>();

        Vector3 loaded_position = LoadVector3(sheep.transform.position, data, keyName, "Position");
        sheep.transform.position = loaded_position;
        sheepNaveMeshAgent.Warp(loaded_position);
        sheep.transform.eulerAngles = LoadVector3(sheep.transform.eulerAngles, data, keyName, "Rotation");
        sheepHealth.food = LoadInt(sheepHealth.food, data, keyName, "Food");
        sheepHealth.water = LoadInt(sheepHealth.water, data, keyName, "Water");
        sheepHealth.sheepStamina = LoadFloat(sheepHealth.sheepStamina, data, keyName, "Stamina");
        sheepHealth.maxSheepStamina = LoadFloat(sheepHealth.maxSheepStamina, data, keyName, "MaxStamina");
        sheepHealth.currentHealth = LoadInt(sheepHealth.currentHealth, data, keyName, "Health");
        sheepHealth.maxHealth = LoadInt(sheepHealth.maxHealth, data, keyName, "MaxHealth");
        sheepHealth.is_eating = LoadBool(sheepHealth.is_eating, data, keyName, "IsEating");
        sheepHealth.is_drinking = LoadBool(sheepHealth.is_drinking, data, keyName, "IsDrinking");
        friendshipStats.friendship = LoadFloat(friendshipStats.friendship, data, keyName, "Friendship");
        friendshipStats.maximumFriendship = LoadFloat(friendshipStats.maximumFriendship, data, keyName, "MaxFriendship");
        friendshipStats.minimumFriendship = LoadFloat(friendshipStats.minimumFriendship, data, keyName, "MinFriendship");
        stateMachine.SetState( LoadString(stateMachine.currentStateName, data, keyName, "State"));
        stateMachine.timeInState = LoadFloat(stateMachine.timeInState, data, keyName, "TimeInState");
        sheepHealth.easy_mode = LoadBool(sheepHealth.easy_mode, data, keyName, "EasyMode");
    }

    // I am incredibly annoyed that Unity doesn't do this for me
    // that's like the entire point of JSON
    string DictToJSON(Dictionary<string, object> dict, int num_indent)
    {
        string json = "";
        string indent = "";
        for (int i = 0; i < num_indent; i++) {
            indent += "\t";
        }
        
        json += "{\n";
        bool is_first = true;
        foreach (var pair in dict) {
            if (!is_first) {json += ", \n";}
            json += indent + "\t" + "\"" + pair.Key + "\" : " + ThingToJSON(pair.Value, num_indent + 1);
            is_first = false;
        }
        json += "\n" + indent + "}";
        return json;
    }

    string ListToJSON(List<object> list, int num_indent)
    {
        string json = "";
        string indent = "";
        for (int i = 0; i < num_indent; i++) {
            indent += "\t";
        }
        
        json += "[\n";
        bool is_first = true;
        foreach (var thing in list) {
            if (!is_first) {json += ", \n";}
            json += indent + "\t" + ThingToJSON(thing, num_indent + 1);
            is_first = false;
        }
        json += "\n" + indent + "]";
        return json;
    }

    string ThingToJSON(object thing, int num_indent)
    {
        if (thing is string) {
            return "\"" + thing + "\"";
        }
        if (thing is int) {
            return thing.ToString();
        }
        if (thing is float) {
            return thing.ToString();
        }
        if (thing is double) {
            return thing.ToString();
        }
        if (thing is bool) {
            return thing.ToString().ToLower();
        }
        if (thing is null) {
            return "null";
        }
        if (thing is List<object>) {
            return ListToJSON(thing as List<object>, num_indent);
        }
        if (thing is Dictionary<string, object>) {
            return DictToJSON(thing as Dictionary<string, object>, num_indent);
        }
        Debug.LogError("JSON Encoder: " + thing.ToString() + " failed to encode properly");
        return thing.ToString();
    }

    List<string> JSONSplitter(string json)
    {
        List<string> json_entities = new List<string>();
        int braces = 0;
        int brackets = 0;
        bool quotes = false;
        bool other = false;
        bool escape = false;

        int entity_start = 0;
        void StartOfEntity(int start)
        {
            entity_start = start;
        }
        void EndOfEntity(int end)
        {
            json_entities.Add(json.Substring(entity_start, end + 1 - entity_start));
        }

        for (int i = 0; i < json.Length; i++) {
            if (escape) {
                continue;
            }
            if (json[i] == '\\') {
                escape = true;
            }
            if (braces > 0) {
                if (json[i] == '{') {
                    braces += 1;
                }
                if (json[i] == '}') {
                    braces -= 1;
                    if (braces == 0) {
                        EndOfEntity(i);
                    }
                }
            }
            else if (brackets > 0) {
                if (json[i] == '[') {
                    brackets -= 1;
                }
                if (json[i] == ']') {
                    brackets -= 1;
                    if (brackets == 0) {
                        EndOfEntity(i);
                    }
                }
            }
            else if (quotes) {
                if (json[i] == '\"') {
                    quotes = false;
                    EndOfEntity(i);
                }
            }
            else if (other) {
                if (json[i] == '\n' || json[i] == ' ' || json[i] == '\t' || json[i] == ',') {
                    other = false;
                    EndOfEntity(i - 1);
                }
            } else {
                if (json[i] == '{') {
                    braces += 1;
                    StartOfEntity(i);
                }
                else if (json[i] == '[') {
                    brackets += 1;
                    StartOfEntity(i);
                }
                else if (json[i] == '\"') {
                    quotes = true;
                    StartOfEntity(i);
                }
                else if (json[i] == ':') {
                    StartOfEntity(i);
                    EndOfEntity(i);
                }
                else if (json[i] != '\n' && json[i] != ' ' && json[i] != '\t' && json[i] != ',') {
                    other = true;
                    StartOfEntity(i);
                }
            }
        }
        return json_entities;
    }

    object JSONEntityToThing(string json_entity)
    {
        if (json_entity[0] == '{') {
            return JSONToDict(json_entity.Substring(1, json_entity.Length - 2));
        }
        if (json_entity[0] == '[') {
            return JSONToList(json_entity.Substring(1, json_entity.Length - 2));
        }
        if (json_entity[0] == '\"') {
            return json_entity.Substring(1, json_entity.Length - 2);
        }
        if (json_entity[0] == 't') {
            return true;
        }
        if (json_entity[0] == 'f') {
            return false;
        }
        if (json_entity[0] == 'n') {
            return null;
        }
        if (new []{'1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '-', 'E'}.Contains(json_entity[0])) {
            if (json_entity.Contains('.')) {
                return double.Parse(json_entity);
            } else {
                // would be an int but casting up to a float is too much to worry about
                // and doubles always downcast to floats and ints
                return double.Parse(json_entity);
            }
        }
        return null;
    }

    Dictionary<string, object> JSONToDict(string json)
    {
        Dictionary<string, object> dict = new Dictionary<string, object>();

        List<string> json_entities = JSONSplitter(json);

        string current_key = "";
        bool looking_for_key = true;
        foreach (var entity in json_entities) {
            object thing = JSONEntityToThing(entity);
            if (looking_for_key && thing is string) {
                current_key = thing as string;
                looking_for_key = false;
                continue;
            }
            if (entity.Equals(":")) {
                looking_for_key = false;
                continue;
            }
            if (!looking_for_key) {
                dict.Add(current_key, thing);
                looking_for_key = true;
                continue;
            }
        }
        
        return dict;
    }
    
    List<object> JSONToList(string json)
    {
        List<object> list = new List<object>();

        List<string> json_entities = JSONSplitter(json);
        
        foreach (var entity in json_entities) {
            object thing = JSONEntityToThing(entity); 
            list.Add(thing);
        }
        
        return list;
    }

    Dictionary<string, object> GetDict(Dictionary<string, object> dict, string key)
    {
        return dict[key] as Dictionary<string, object>;
    }

    List<object> GetList(Dictionary<string, object> dict, string key)
    {
        return dict[key] as List<object>;
    }

    /// get dict from a list
    Dictionary<string, object> GetDictL(List<object> list, int index)
    {
        return list[index] as Dictionary<string, object>;
    }

    Dictionary<string, object> PrepareDict<T, V>(Dictionary<T, V> dict)
    {
        Dictionary<string, object> result = new Dictionary<string, object>();
        foreach (var pair in dict) {
            result[pair.Key.ToString()] = pair.Value;
        }
        return result;
    }

    // THIS IS HIDEOUS CODE I AM SO SORRY
    object TraverseSaveData(object data, object[] accessors, int depth = 0)
    {
        var accessor = accessors[depth];
        object next_data = null;
        if (accessor is string) { // data must be a dict<string, object>
            next_data = (data as Dictionary<string, object>)[accessor as string];
        }
        if (accessor is int) { // data must be a list<object>
            next_data = (data as List<object>)[(int) accessor];
        }

        if (next_data is null) {
            throw new JsonException("LOAD ERROR: NULL?");
        }

        if (depth == accessors.Length - 1) {
            return next_data;
        }

        if (accessor is string || accessor is int) {
            return TraverseSaveData(next_data, accessors, depth + 1);
        }

        throw new JsonException("THERE WAS A JSON LOAD ERROR");
    }

    int LoadInt(int default_value, object data, params object[] accessors)
    {
        try {
            object thing = TraverseSaveData(data, accessors);
            if (thing is double || thing is float) {
                return Convert.ToInt32(thing);
            }
            return (int) thing;
        } catch (Exception e) {
            load_failures.Add((String.Join("-", accessors), e));
            return default_value;
        }
    }

    float LoadFloat(float default_value, object data, params object[] accessors)
    {
        try {
            object thing = TraverseSaveData(data, accessors);
            if (thing is double || thing is int) {
                return Convert.ToSingle(thing);
            }
            return (float) thing;
        } catch (Exception e) {
            load_failures.Add((String.Join("-", accessors), e));
            return default_value;
        }
    }
    
    bool LoadBool(bool default_value, object data, params object[] accessors)
    {
        try {
            object thing = TraverseSaveData(data, accessors);
            return (bool) thing;
        } catch (Exception e) {
            load_failures.Add((String.Join("-", accessors), e));
            return default_value;
        }
    }
    
    string LoadString(string default_value, object data, params object[] accessors)
    {
        try {
            object thing = TraverseSaveData(data, accessors);
            return (string) thing;
        } catch (Exception e) {
            load_failures.Add((String.Join("-", accessors), e));
            return default_value;
        }
    }
    
    double LoadDouble(double default_value, object data, params object[] accessors)
    {
        try {
            object thing = TraverseSaveData(data, accessors);
            if (thing is float || thing is int) {
                return Convert.ToDouble(thing);
            }
            return (double) thing;
        } catch (Exception e) {
            load_failures.Add((String.Join("-", accessors), e));
            return default_value;
        }
    }
    
    Vector3 LoadVector3(Vector3 default_value, object data, params object[] accessors)
    {
        try {
            object thing = TraverseSaveData(data, accessors);
            return XYZToVector3(thing as Dictionary<string, object>);
        } catch (Exception e) {
            load_failures.Add((String.Join("-", accessors), e));
            return default_value;
        }
    }

    Dictionary<string, object> LoadDictionary(object data, params object[] accessors)
    {
        try {
            Dictionary<string, object> thing = TraverseSaveData(data, accessors) as Dictionary<string, object>;
            return thing;
        } catch (Exception e) {
            load_failures.Add((String.Join("-", accessors), e));
            return new Dictionary<string, object>();
        }
    }

    List<object> LoadList(object data, params object[] accessors)
    {
        try {
            List<object> thing = TraverseSaveData(data, accessors) as List<object>;
            return thing;
        } catch (Exception e) {
            load_failures.Add((String.Join("-", accessors), e));
            return new List<object>();
        }
    }

    // Try section
    // If load fails, leave unchanged
    
    void TryLoadInt(ref int into, object data, params object[] accessors)
    {
        into = LoadInt(into, data, accessors);
    }
    
    void TryLoadFloat(ref float into, object data, params object[] accessors)
    {
        into = LoadFloat(into, data, accessors);
    }
    
    void TryLoadBool(ref bool into, object data, params object[] accessors)
    {
        into = LoadBool(into, data, accessors);
    }
    
    void TryLoadString(ref string into, object data, params object[] accessors)
    {
        into = LoadString(into, data, accessors);
    }
    
    void TryLoadDouble(ref double into, object data, params object[] accessors)
    {
        into = LoadDouble(into, data, accessors);
    }
    
    void TryLoadVector3(ref Vector3 into, object data, params object[] accessors)
    {
        into = LoadVector3(into, data, accessors);
    }
    
    void TryLoadDictionary(ref Dictionary<string, object> into, object data, params object[] accessors)
    {
        into = LoadDictionary(into, data, accessors);
    }
    
    void TryLoadList(ref List<object> into, object data, params object[] accessors)
    {
        into = LoadList(into, data, accessors);
    }
}
