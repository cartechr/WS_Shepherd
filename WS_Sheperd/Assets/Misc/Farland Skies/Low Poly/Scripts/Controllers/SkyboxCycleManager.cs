﻿using System;
using Borodar.FarlandSkies.Core.Helpers;
using UnityEngine;

namespace Borodar.FarlandSkies.LowPoly
{
    [ExecuteInEditMode]
    [HelpURL("http://www.borodar.com/stuff/farlandskies/lowpoly/docs/QuickStart_v2.5.1.pdf")]
    public class SkyboxCycleManager : Singleton<SkyboxCycleManager>
    {
        [Tooltip("Day-night cycle duration from 0% to 100% (in seconds)")]
        public float CycleDuration = 10f;

        [Tooltip("Current time of day (in percents)")]
        public float CycleProgress;

        public bool Paused;
        public bool DayTime;

        private SkyboxDayNightCycle _dayNightCycle;

        //---------------------------------------------------------------------
        // Messages
        //---------------------------------------------------------------------

        protected void Start()
        {
            GameEvents.GetTimeOfDay += OnGetTimeOfDay;
            GameEvents.SetTimeOfDay += OnSetTimeOfDay;
            _dayNightCycle = SkyboxDayNightCycle.Instance;
            UpdateTimeOfDay();
        }

        private void OnDestroy()
        {
            GameEvents.GetTimeOfDay -= OnGetTimeOfDay;
            GameEvents.SetTimeOfDay -= OnSetTimeOfDay;
        }

        protected void Update()
        {
            if (Application.isPlaying && !Paused)
            {
                CycleProgress += (Time.deltaTime / CycleDuration) * 100f;
                CycleProgress %= 100f;
            }
            if(CycleProgress >= 80 || CycleProgress <= 30)
            {
                DayTime = false;
            }
            if(CycleProgress <= 80 && CycleProgress >= 30)
            {
                DayTime = true;
                
            }
            UpdateTimeOfDay();
        }

        protected void OnValidate()
        {
            UpdateTimeOfDay();
        }

        //---------------------------------------------------------------------
        // Helpers
        //---------------------------------------------------------------------

        private void UpdateTimeOfDay()
        {
            if (_dayNightCycle != null)
                _dayNightCycle.TimeOfDay = CycleProgress;
        }

        // event abuse
        public void OnGetTimeOfDay(object sender, FloatArgs args)
        {
            args.number = CycleProgress;
        }
        
        public void OnSetTimeOfDay(object sender, FloatArgs args)
        {
            CycleProgress = args.number;
        }
    }
}