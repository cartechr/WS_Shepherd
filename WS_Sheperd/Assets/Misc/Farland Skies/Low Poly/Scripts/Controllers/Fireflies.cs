using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fireflies : MonoBehaviour
{
    Borodar.FarlandSkies.LowPoly.SkyboxCycleManager skyman;
    public GameObject fireflies;
    public GameObject skyboxhaver;
    
    void Start()
    {
        skyman = skyboxhaver.GetComponent<Borodar.FarlandSkies.LowPoly.SkyboxCycleManager>();
        if (!skyman) {
            Destroy(gameObject);
        }
        fireflies.SetActive(false);
    }

    
    void Update()
    {
        // 0.8 and 0.3 should be the correct values for night time
        if (skyman.DayTime == false) /*skyman.CycleProgress >= 0.8 || skyman.CycleProgress <= 0.3*/
        {
            Debug.Log("Fireflies are flying");
            fireflies.SetActive(true);
        }

        if (skyman.DayTime == true) /*skyman.CycleProgress <= 0.8 && skyman.CycleProgress >= 0.3*/
        {
            Debug.Log("Fireflies are napping");
            fireflies.SetActive(false);
        }
    }
}
