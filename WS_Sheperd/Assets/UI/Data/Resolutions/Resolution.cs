using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObject/Resolution")]
public class Resolution : UIOption {
    public int x;
    public int y;
}
