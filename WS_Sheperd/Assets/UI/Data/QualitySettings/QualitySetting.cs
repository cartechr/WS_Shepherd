using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName="ScriptableObject/QualitySetting")]
public class QualitySetting : UIOption {
    public int qualityIndex;
}
