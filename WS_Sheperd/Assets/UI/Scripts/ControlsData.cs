using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ControlsData {

	public KeyCode forward;
	public KeyCode backward;
	public KeyCode left;
	public KeyCode right;
	public KeyCode sprint;
	public KeyCode camp;
	public KeyCode pause;
	public KeyCode repel;
	public KeyCode interact;
	public KeyCode place;
	public int sensitivity;
	public bool invertLook;

	public ControlsData(Controls controls) {
		forward = controls.forward;
		backward = controls.backward;
		left = controls.left;
		right = controls.right;
		sprint = controls.sprint;
		camp = controls.camp;
		pause = controls.pause;
		repel = controls.repel;
		interact = controls.interact;
		place = controls.place;
		sensitivity = controls.sensitivity;
		invertLook = controls.invertLook;
	}
	
}
