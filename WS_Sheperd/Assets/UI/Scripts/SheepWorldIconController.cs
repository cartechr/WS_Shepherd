using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SheepWorldIconController : MonoBehaviour {

    private Image sheepIcon;
    private bool isActive;

    // Start is called before the first frame update
    void Start() {
        sheepIcon = GetComponentInChildren<Image>();
    }

    // Update is called once per frame
    void Update() {
        transform.LookAt(Camera.main.transform);
    }

    public void ToggleActive(bool active) {
        isActive = active;
        if(isActive) {
            sheepIcon.color = new Color(1, 1, 1, 1);
        } else {
            sheepIcon.color = new Color(0, 0, 0, 0);
        }
    }
}
