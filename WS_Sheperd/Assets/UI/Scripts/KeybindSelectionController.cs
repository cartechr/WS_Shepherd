using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.XR;

public class KeybindSelectionController : MonoBehaviour, ISelection {

	public TMP_Text text;
	public KeyCode defaultKey;

	private KeyCode selection;
	private bool settingKey = false;
	private IEnumerable<KeyCode> allKeys;

	void Start() {
		allKeys = System.Enum.GetValues(typeof(KeyCode)).Cast<KeyCode>();
	}

	void Awake() {
		selection = defaultKey;
		UpdateSelection();
	}

	void Update() {
		if(settingKey) {
			if(Input.anyKeyDown) {
				foreach(KeyCode key in allKeys) {
					if(Input.GetKeyDown(key)) {
						settingKey = false;
						selection = key;
						UpdateSelection();
						break;
					}
				}
			}
		}
	}

	private void UpdateSelection() {
		text.text = selection.ToString();
	}

	public void SetKey() {
		text.text = "";
		settingKey = true;
	}

	public object GetSelection() {
		return selection;
	}

	public void ResetSelection() {
		selection = defaultKey;
		UpdateSelection();
	}

	public void SubmitSelection() {
		defaultKey = selection;
		UpdateSelection();
	}

	public void UpdateKey(KeyCode key) {
		selection = key;
		UpdateSelection();
	}
}
