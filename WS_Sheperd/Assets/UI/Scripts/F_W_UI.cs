using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class F_W_UI : MonoBehaviour
{
    //public GameObject hungerbar;
   // public int WaitSeconds;

    public Animator hungerAnimator;
    public Animator thirstAnimator;
    //public GameObject Empty;

    public NPC_Follow sheep;

    public Transform canvasTransform;

    public CanvasGroup hungerUIGroup;
    private bool hungerFadeIn = false;
    private bool hungerFadeOut = false;

    public CanvasGroup thirstUIGroup;
    private bool thirstFadeIn = false;
    private bool thirstFadeOut = false;
    

    private void Start() {
        sheep.WaterChangedStages += SetThirstLevel;
        sheep.FoodChangedStages += SetHungerLevel;
       // F_animator = GetComponent<Animator>();
       // W_animator = GetComponent<Animator>();
        StartCoroutine(FadeAtStart());
    }
    
    void Update() {
        if (hungerFadeIn) {
            if (hungerUIGroup.alpha < 1) {
                hungerUIGroup.alpha += Time.deltaTime;
                if (hungerUIGroup.alpha >= 1) {
                    hungerFadeIn = false;
                }
            }
        }

        if(hungerFadeOut) {
            if(hungerUIGroup.alpha > 0) {
                hungerUIGroup.alpha -= Time.deltaTime;
                if(hungerUIGroup.alpha <= 0) {
                    hungerFadeOut = false;
                }
            }
        }


        if(thirstFadeIn) {
            if(thirstUIGroup.alpha < 1) {
                thirstUIGroup.alpha += Time.deltaTime;
                if(thirstUIGroup.alpha >= 1) {
                    thirstFadeIn = false;
                }
            }
        }

        if(thirstFadeOut) {
            if(thirstUIGroup.alpha > 0) {
                thirstUIGroup.alpha -= Time.deltaTime;
                if(thirstUIGroup.alpha <= 0) {
                    thirstFadeOut = false;
                }
            }
        }
    }
    IEnumerator FadeAtStart()
    {
        yield return new WaitForSecondsRealtime(5f);
        hungerFadeOut = true;
        thirstFadeOut = true;
    }

    IEnumerator HungerFading()
    {
        hungerFadeIn = true;
        yield return new WaitForSecondsRealtime(5f);
        hungerFadeOut = true;

    }

    IEnumerator ThirstFading()
    {
        thirstFadeIn = true;
        yield return new WaitForSecondsRealtime(5f);
        thirstFadeOut = true;
    }
   /* IEnumerator HungarBarMethod(bool fading = true, int fadeSpeed = 5)
    {
        Debug.Log(WaitSeconds + " seconds before fade out");
        yield return new WaitForSeconds(WaitSeconds);
        Debug.Log("Start Fading Out");
        Color objectColor = hungerbar.GetComponent<Image>().color;
        int fadeAmount;

        if (fading)
        {
            while (hungerbar.GetComponent<Image>().color.a < 1)
            {
                fadeAmount = objectColor.a + (fadeSpeed * Time.deltaTime);

                objectColor = new Color(objectColor.r, objectColor.g, objectColor.b, fadeAmount);
                hungerbar.GetComponent<Image>().color = objectColor;
                yield return null;
            }
        }
        else
        {
            while (hungerbar.GetComponent<Image>().color.a > 0)
            {
                fadeAmount = objectColor.a - (fadeSpeed * Time.deltaTime);
                objectColor = new Color(objectColor.r, objectColor.g, objectColor.b, fadeAmount);
                hungerbar.GetComponent<Image>().color = objectColor;
                yield return null;
            }
        }
    }
    */
    private void LateUpdate()
    {
        canvasTransform.LookAt(Camera.main.transform);
    }

    private void SetThirstLevel(object o, string state) {
        thirstAnimator.Play(state);
        StartCoroutine(ThirstFading());
    }

    private void SetHungerLevel(object o, string state) {
        hungerAnimator.Play(state);
        StartCoroutine(HungerFading());
    }

    //https://www.youtube.com/watch?v=lvsHgjPD8qA&ab_channel=MetalStormGames
    //https://www.youtube.com/watch?v=tF9RMjF9wDc&ab_channel=SpeedTutor
}
