using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class SheepIconParent : MonoBehaviour {
    public abstract void SetWander(SheepIconController sheepIcon, bool isWandering);
}
