using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;
using UnityEngine.Animations;
#if UNITY_EDITOR
using UnityEngine.WSA;
#endif

public class SheepInfoBarController : SheepIconParent {

    public float recenterSpeed = 20f;

    private List<(SheepIconController, int)> activeIcons;
    private List<(SheepIconController, int)> inactiveIcons;
    private List<SheepIconController> removing;
    private List<SheepIconController> adding;
    private SheepIconController[] ordered;
    private Vector3[] positions;
    private bool isRecentering;

    void Start() {
        isRecentering = false;
        removing = new List<SheepIconController>();
        adding = new List<SheepIconController>();
        activeIcons = new List<(SheepIconController, int)>();
        inactiveIcons = new List<(SheepIconController, int)>();
        SheepIconController[] controllers = GetComponentsInChildren<SheepIconController>();
        for(int i = 0; i < controllers.Length; i++) {
            controllers[i].isActive = true;
            activeIcons.Add((controllers[i], i));
        }
    }

    void Update() {

        foreach (var icon in new List<SheepIconController>(adding)) {
            if (!icon.iconFadeIn) {
                adding.Remove(icon);
            }
        }
        foreach (var icon in new List<SheepIconController>(removing)) {
            if (!icon.iconFadeOut) {
                removing.Remove(icon);
            }
        }
        
        if(isRecentering) {
            int x = 0;
            int y = 0;
            for(int i = 0; i < ordered.Length; i++) {
                if(ordered[i] != null) {
                    x += 1;
                    if(Vector3.Distance(ordered[i].rect.anchoredPosition3D, positions[i]) > 0.05f) {
                        ordered[i].rect.anchoredPosition3D = Vector3.MoveTowards(ordered[i].rect.anchoredPosition3D, positions[i], recenterSpeed * Time.deltaTime);
                    } else {
                        y += 1;
                    }
                }
            }

            if(x == y) {
                isRecentering = false;
            }
        }

        List<SheepIconController> toBeRemoved = new List<SheepIconController>(removing);
        foreach(SheepIconController icon in toBeRemoved) {
            if(icon.iconSprite.color.a > 0) {
                icon.rect.anchoredPosition3D += Vector3.down * (recenterSpeed * Time.deltaTime);
            } else {
                removing.Remove(icon);
            }
        }

        List<SheepIconController> toBeAdded = new List<SheepIconController>(adding);
        foreach(SheepIconController icon in toBeAdded) {
            if(icon.iconSprite.color.a < 1) {
                icon.rect.anchoredPosition3D += Vector3.up * (recenterSpeed * Time.deltaTime);
            } else {
                adding.Remove(icon);
            }
        }
    }

    private void RemoveFromBar(SheepIconController sheepIcon) {
        removing.Add(sheepIcon);
        sheepIcon.FadeIcon(false);
        foreach((SheepIconController, int) icon in activeIcons) {
            if(icon.Item1 == sheepIcon) {
                activeIcons.Remove(icon);
                inactiveIcons.Add(icon);
                break;
            }
        }
        
    }

    private void AddToBar(SheepIconController sheepIcon) {
        adding.Add(sheepIcon);
        sheepIcon.FadeIcon(true);
        foreach((SheepIconController, int) icon in inactiveIcons) {
            if(icon.Item1 == sheepIcon) {
                inactiveIcons.Remove(icon);
                activeIcons.Add(icon);
                break;
            }
        }
    }

    private void RecenterBar() {
        RecalculatePositions();
        isRecentering = true;
    }
    
    public override void SetWander(SheepIconController sheepIcon, bool isWandering) {
        if(isWandering) {
            RemoveFromBar(sheepIcon);
            sheepIcon.connectedSheep.worldIcon.ToggleActive(true);
            RecenterBar();
        } else {
            AddToBar(sheepIcon);
            sheepIcon.connectedSheep.worldIcon.ToggleActive(false);
            RecenterBar();
        }
    }
	
	private Vector3[] RecalculatePositions() {
		float startX = 0f - ((activeIcons.Count - 1) * 100f / 2f);
        ordered = new SheepIconController[activeIcons.Count + inactiveIcons.Count];
        positions = new Vector3[ordered.Length];
        foreach((SheepIconController, int) icon in activeIcons) {
            ordered[icon.Item2] = icon.Item1;
        }
        int x = 0;
        for(int i = 0; i < ordered.Length; i++) {
            if(ordered[i] != null) {
                positions[i] = new Vector3(startX + (100f * x), 0f, 0f);
                x += 1;
            }
            
        }
		return positions;
	}
    
}
