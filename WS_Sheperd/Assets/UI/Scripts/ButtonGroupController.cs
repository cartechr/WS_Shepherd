using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonGroupController : MonoBehaviour {
    public List<Button> buttons;
    public int defaultIndex;

    private int previousIndex;

    void Awake() {
        ToggleOn(buttons[defaultIndex]);
    }

    public void Select(Button button) {
        int index = buttons.IndexOf(button);
        if(index == -1) {
            return;
        }
        ToggleOff(buttons[previousIndex]);
        ToggleOn(buttons[index]);
    }

    private void ToggleOn(Button button) {
        ColorBlock temp = button.colors;
        temp.normalColor += new Color(0f, 0f, 0f, 255f);
        button.colors = temp;
    }

    private void ToggleOff(Button button) {
        ColorBlock temp = button.colors;
        temp.normalColor -= new Color(0f, 0f, 0f, 255f);
        button.colors = temp;
    }
}
