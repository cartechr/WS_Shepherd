using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ArrowSelectionController : MonoBehaviour, ISelection {
    public List<UIOption> options;
    public TMP_Text selection;
    public int defaultIndex;
    
    private int selectedIndex = 0;

    void Awake() {
        ResetSelection();
    }

    public void ResetSelection() {
        selectedIndex = defaultIndex;
        UpdateSelection();
    }

    public void SubmitSelection() {
        defaultIndex = selectedIndex;
        UpdateSelection();
    }

    public void DecrementSelection() {
        if(selectedIndex == 0) {
            selectedIndex = options.Count-1;
        } else {
            selectedIndex -= 1;
        }
        UpdateSelection();
    }

    public void IncrementSelection() {
        if(selectedIndex == options.Count-1) {
            selectedIndex = 0;
        } else {
            selectedIndex += 1;
        }
        UpdateSelection();
    }

    public object GetSelection() {
        return options[selectedIndex];
    }

    private void UpdateSelection() {
        selection.text = options[selectedIndex].text;
    }
    
}
