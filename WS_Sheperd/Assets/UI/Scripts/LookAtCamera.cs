﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtCamera : MonoBehaviour
{
    // Update is called once per frame
    public Transform screen;
    void Update()
    {
        transform.LookAt(screen);
    }
}
