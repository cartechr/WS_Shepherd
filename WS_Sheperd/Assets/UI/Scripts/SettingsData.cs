using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SettingsData {

	public int resolutionIndex;
	public int qualityIndex;
	public int masterVolume;
	public int musicVolume;
	public int sfxVolume;
	public int ambienceVolume;

	public SettingsData(SettingsMenuController settings) {
		resolutionIndex = settings.resolution.defaultIndex;
		qualityIndex = settings.quality.defaultIndex;
		masterVolume = settings.masterVolume.defaultValue;
		musicVolume = settings.musicVolume.defaultValue;
		sfxVolume = settings.sfxVolume.defaultValue;
		ambienceVolume = settings.ambienceVolume.defaultValue;
	}
	
}
