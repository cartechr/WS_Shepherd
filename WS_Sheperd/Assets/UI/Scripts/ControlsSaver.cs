using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public static class ControlsSaver {
    
    public static void SaveControls(Controls controls) {
        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "/controls.controlsFile";
        FileStream stream = new FileStream(path, FileMode.Create);
        ControlsData data = new ControlsData(controls);
        formatter.Serialize(stream, data);
        stream.Close();
    }

    public static ControlsData LoadControls() {
        string path = Application.persistentDataPath + "/controls.controlsFile";
        if(File.Exists(path)) {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);
            ControlsData data = formatter.Deserialize(stream) as ControlsData;
            stream.Close();
            return data;
        } else {
            Debug.LogError("Save file not found");
            return null;
        }
    }
}
