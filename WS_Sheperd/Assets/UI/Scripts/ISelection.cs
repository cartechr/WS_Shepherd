using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISelection {
    public object GetSelection();

    public void SubmitSelection();

    public void ResetSelection();
}
