using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadingUI : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject hungerBar;
    public void Update()
    {
        if(Input.GetKeyDown(KeyCode.M))
        {
            StartCoroutine(fade());
        }
        if(Input.GetKeyDown(KeyCode.N))
        {
            StartCoroutine(fade(false));
        }
    }

    // Update is called once per frame
    public IEnumerator fade(bool fading = true, int fadeSpeed = 5)
    {   
        Color objectColor = hungerBar.GetComponent<Image>().color;
        float fadeAmount;

        if (fading)
        {
            while (hungerBar.GetComponent<Image>().color.a < 1)
            {
                fadeAmount = objectColor.a + (fadeSpeed * Time.deltaTime);

                objectColor = new Color(objectColor.r, objectColor.g, objectColor.b, fadeAmount);
                hungerBar.GetComponent<Image>().color = objectColor;
                yield return null;
            }
        } else
        {
            while (hungerBar.GetComponent<Image>().color.a > 0)
            {
                fadeAmount = objectColor.a - (fadeSpeed * Time.deltaTime);
                objectColor = new Color(objectColor.r, objectColor.g, objectColor.b, fadeAmount);
                hungerBar.GetComponent<Image>().color = objectColor;
                yield return null;
            }
        }
    }
}
