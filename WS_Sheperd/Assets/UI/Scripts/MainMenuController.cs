using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuController : Menu {

    public MainTheme menuMusic;
    
    public void SelectScene(string currentButton) {
        switch(currentButton) {
            case "start":
                SceneManager.LoadScene("ReleaseMainScene");
                FMODUnity.RuntimeManager.StudioSystem.setParameterByName("GameState", 0 );
                menuMusic.StopMusic();
                break;
            case "credits":
                SceneManager.LoadScene("InfoScene");
                break;
            case "exit":
                // Probably change this to Application.quit
                SceneManager.LoadScene("Main Menu");
                break;
        }
    }
}
