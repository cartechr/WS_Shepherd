using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trail_Spawner : MonoBehaviour
{
    public GameObject TrailObject;
    public Transform target;


    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("CreateTrail", 1f, 1f);
    }
    void CreateTrail()
    {
        if (target != null)
        {
            var spawnPos = transform.position + (transform.forward * 2);
            var newObj = Instantiate(TrailObject, spawnPos, transform.rotation);
            newObj.GetComponent<Trail_Path>().target = target;
        }
    }
}

//https://www.youtube.com/watch?v=szY8KGG1dOg&ab_channel=JohnP.Doran