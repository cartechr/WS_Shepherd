using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreatUI : MonoBehaviour
{
    [SerializeField] Transform canvasTransform;

    // Start is called before the first frame update
    void Start()
    {
        gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void LateUpdate() {
        canvasTransform.LookAt(Camera.main.transform);
    }

    public void Active(){
        gameObject.SetActive(true);
    }

    public void NotActive(){
        gameObject.SetActive(false);
    }
}
