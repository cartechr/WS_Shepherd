using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class MenuSystemController : MonoBehaviour {

    public GameController gameController;
    public PlayTestPauseRestart pauseObject;
    public GameObject[] otherUIElements;
    public Menu pauseMenu;

    private Menu[] menus;
    private bool isPaused;

    void Start() {
        isPaused = false;
        menus = GetComponentsInChildren<Menu>();
    }

    void Update() {
        if(Input.GetKeyDown(gameController.controls.pause)) {
            if(isPaused) {
                Resume();
            } else {
                Pause();
            }
            
        }
    }

    public void QuitGame() {
        Application.Quit();
    }

    public void TurnOffMenu(Menu menu) {
        menu.container.SetActive(false);
    }

    public void TurnOnPrevious(Menu menu) {
       TurnOnMenu(menu.GetPreviousMenu());
    }

    public void TurnOnMenu(Menu menu) {
        menu.container.SetActive(true);
    }

    public void DisableOther() {
        foreach(GameObject go in otherUIElements) {
            go.SetActive(false);
        }
    }

    public void EnableOther() {
        foreach(GameObject go in otherUIElements) {
            go.SetActive(true);
        }
    }

    public void Pause() {
        DisableOther();
        TurnOnMenu(pauseMenu);
        pauseObject.Pause();
        isPaused = true;
    }

    public void Resume() {
        foreach(Menu m in menus) {
            TurnOffMenu(m);
        }
        EnableOther();
        pauseObject.Resume();
        isPaused = false;
    }

}
