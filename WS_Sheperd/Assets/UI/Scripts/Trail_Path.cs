using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Trail_Path : MonoBehaviour
{
    private NavMeshAgent agent;
    //Sheep will be target
    [SerializeField] public Transform target;
    private float stopDistance = 2;

    // Start is called before the first frame update
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        agent.SetDestination(target.position);
    }

    // Update is called once per frame
    void Update()
    {
        if(Vector3.Distance(transform.position, target.position) < stopDistance)
        {
            Destroy(gameObject);
        }
    }
}

    //https://www.youtube.com/watch?v=szY8KGG1dOg&ab_channel=JohnP.Doran