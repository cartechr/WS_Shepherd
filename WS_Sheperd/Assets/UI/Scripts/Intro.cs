using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Intro : MonoBehaviour {
    public GameController gameController;

    public static bool shownYet = false;
    
    public Canvas introBlurb;

    // Start is called before the first frame update
    void Start()
    {
        introBlurb = GetComponent<Canvas>();
        if (!gameController) {gameController = GameController.Instance();}
        Time.timeScale = 0f;

        if (shownYet) {
            introBlurb.enabled = false;
            Time.timeScale = 1f;
        }

        shownYet = true;
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(gameController.controls.camp))
        {
           introBlurb.enabled = false;
           Time.timeScale = 1f;
        }
    }
}
