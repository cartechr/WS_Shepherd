using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingsMenuController : Menu {

	public ArrowSelectionController resolution;
	public ArrowSelectionController quality;
	public SliderSelectionController masterVolume;
	public SliderSelectionController musicVolume;
	public SliderSelectionController sfxVolume;
	public SliderSelectionController ambienceVolume;
	
	public void TurnOffTab(GameObject tab) {
		tab.SetActive(false);
	}

	public void TurnOnTab(GameObject tab) {
		tab.SetActive(true);
	}

	public void ApplySettings() {
		Resolution res = (Resolution) resolution.GetSelection();
		QualitySetting qual = (QualitySetting) quality.GetSelection();
		int master = (int)masterVolume.GetSelection();
		int music = (int)musicVolume.GetSelection();
		int sfx = (int)sfxVolume.GetSelection();
		int ambience = (int)ambienceVolume.GetSelection();
		string[] names = QualitySettings.names;
		for(int i = 0; i < names.Length; i++) {
			if(names[i] == qual.name) {
				QualitySettings.SetQualityLevel(i, true);
			}
		}
		Screen.SetResolution(res.x, res.y, true);
		FMODUnity.RuntimeManager.StudioSystem.setParameterByName("Master Volume", master);
		FMODUnity.RuntimeManager.StudioSystem.setParameterByName("Music Volume", music);
		FMODUnity.RuntimeManager.StudioSystem.setParameterByName("SFX Volume", sfx);
		FMODUnity.RuntimeManager.StudioSystem.setParameterByName("Environment Volume", ambience);
		resolution.SubmitSelection();
		quality.SubmitSelection();
		masterVolume.SubmitSelection();
		musicVolume.SubmitSelection();
		sfxVolume.SubmitSelection();
		ambienceVolume.SubmitSelection();
		SettingsSaver.SaveSettings(this);
	}

	void Awake() {
		SettingsData loaded = SettingsSaver.LoadSettings();
		if(loaded != null) {
			resolution.defaultIndex = loaded.resolutionIndex;
			resolution.ResetSelection();
			quality.defaultIndex = loaded.qualityIndex;
			quality.ResetSelection();
			masterVolume.defaultValue = loaded.masterVolume;
			masterVolume.ResetSelection();
			musicVolume.defaultValue = loaded.musicVolume;
			musicVolume.ResetSelection();
			sfxVolume.defaultValue = loaded.sfxVolume;
			sfxVolume.ResetSelection();
			ambienceVolume.defaultValue = loaded.ambienceVolume;
			ambienceVolume.ResetSelection();
		}
	}
	
	
}
