using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.AccessControl;
using TMPro;
using UnityEngine;
using UnityEngine.ProBuilder;

public class SliderSelectionController : MonoBehaviour, ISelection {

    public Vector2 range;
    public int defaultValue;
    public float width;
    public RectTransform LeftArrow;
    public RectTransform RightArrow;
    public RectTransform Bar;
    public RectTransform Knob;
    public TMP_Text valueText;

    private int selectedValue;
    private float leftPos;
    private float rightPos;
    private float step;
    private RectTransform valueTextRect;
    private Vector2 initialMousePos;
    private int initialValue;

    void Awake() {
        Rect temp = Bar.rect;
        temp.width = width;
        Bar.rect.Set(temp.x, temp.y, temp.width, temp.height);
        leftPos = Bar.position.x - (width / 2f);
        rightPos = Bar.position.x + (width / 2f);
        LeftArrow.position = new Vector3(leftPos - 40f, LeftArrow.position.y, LeftArrow.position.z);
        RightArrow.position = new Vector3(rightPos + 40f, RightArrow.position.y, RightArrow.position.z);
        step = width / (range.y - range.x + 1);
        selectedValue = defaultValue;
        valueTextRect = valueText.GetComponent<RectTransform>();
        valueTextRect.position = new Vector3(rightPos + 175, valueTextRect.position.y, valueTextRect.position.z);
        UpdateSelection();
    }

    public void IncrementSlider() {
        if(selectedValue < range.y) {
            selectedValue += 1;
        }
        UpdateSelection();
    }

    public void DecrementSlider() {
        if(selectedValue > range.x) {
            selectedValue -= 1;
        }
        UpdateSelection();
    }

    public void BarClick() {
        float normalized = (Input.mousePosition.x - leftPos) / (rightPos - leftPos);
        selectedValue = (int) (normalized * (range.y - range.x) + range.x);
        UpdateSelection();
    }

    public void BeginDrag() {
        initialMousePos = Input.mousePosition;
        initialValue = selectedValue;
    }

    public void Drag() {
        Vector2 mouseDelta = (Vector2)Input.mousePosition - initialMousePos;
        int result = (int) (initialValue + (mouseDelta.x / step));
        if(result < range.x) {
            selectedValue = (int) range.x;
        } else if(result > range.y) {
            selectedValue = (int) range.y;
        } else {
            selectedValue = result;
        }
        
        UpdateSelection();
    }

    public object GetSelection() {
        return selectedValue;
    }

    private void UpdateSelection() {
        float normalizedValue = (selectedValue - range.x) / (range.y - range.x);
        float pos = normalizedValue * (rightPos - leftPos) + leftPos;
        Knob.position = new Vector3(pos, Knob.position.y, Knob.position.z);
        valueText.text = selectedValue.ToString();
    }

    public void SubmitSelection() {
        defaultValue = selectedValue;
        UpdateSelection();
    }

    public void ResetSelection() {
        selectedValue = defaultValue;
        UpdateSelection();
    }
}
