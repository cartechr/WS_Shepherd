using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SheepIconController : MonoBehaviour {
    public Image backgroundSprite;
    public Image background2Sprite;
    public Image healthSprite;
    public Image hungerSprite;
    public Image thirstSprite;
    public Image iconSprite;
    public float healthBarStartAngle;
    public float healthBarEndAngle;
    public float hungerBarStartAngle;
    public float hungerBarEndAngle;
    public float thirstBarStartAngle;
    public float thirstBarEndAngle;
    public SheepHealth connectedSheep;
    public RectTransform rect;
    public bool startVisible = false;
    public bool isActive;
    public bool isVisible;

    private float hungerCap;
    private float thirstCap;
    private float healthCap;
    public bool iconFadeIn;
    public bool iconFadeOut;

    private SheepIconParent parentController;

    // Start is called before the first frame update
    void Start() {
        parentController = GetComponentInParent<SheepIconParent>();
        connectedSheep.LoseSheep += ActivateWander;
        connectedSheep.SwitchedToFollowState += DeactivateWander;
        connectedSheep.WaterChanged += SetThirstLevel;
        connectedSheep.FoodChanged += SetHungerLevel;
        connectedSheep.HealthChanged += SetHealthLevel;
        healthSprite.transform.localEulerAngles = new Vector3(0f, 0f, -healthBarStartAngle);
        thirstSprite.transform.localEulerAngles = new Vector3(0f, 0f, -thirstBarStartAngle);
        hungerSprite.transform.localEulerAngles = new Vector3(0f, 0f, -hungerBarStartAngle);
        hungerCap = AngleDifference(hungerBarStartAngle, hungerBarEndAngle) / 360f;
        thirstCap = AngleDifference(thirstBarStartAngle, thirstBarEndAngle) / 360f;
        healthCap = AngleDifference(healthBarStartAngle, healthBarEndAngle) / 360f;
        healthSprite.fillAmount = healthCap;
        thirstSprite.fillAmount = thirstCap;
        hungerSprite.fillAmount = hungerCap;
        rect = GetComponent<RectTransform>();
        iconFadeIn = false;
        iconFadeOut = false;
        SetVisible(startVisible);
        isVisible = startVisible;
    }

    // Update is called once per frame
    void Update() {
        if(iconFadeIn) {
            if(iconSprite.color.a < 1) {
                iconSprite.color += new Color(0f, 0f, 0f, Time.deltaTime);
                hungerSprite.color += new Color(0f, 0f, 0f, Time.deltaTime);
                thirstSprite.color += new Color(0f, 0f, 0f, Time.deltaTime);
                healthSprite.color += new Color(0f, 0f, 0f, Time.deltaTime);
                backgroundSprite.color += new Color(0f, 0f, 0f, Time.deltaTime);
                background2Sprite.color += new Color(0f, 0f, 0f, Time.deltaTime);
                if (iconSprite.color.a >= 1) {
                    iconFadeIn = false;
                }
            }
        }

        if (iconFadeOut) {
            if (iconSprite.color.a > 0) {
                iconSprite.color -= new Color(0f, 0f, 0f, Time.deltaTime);
                hungerSprite.color -= new Color(0f, 0f, 0f, Time.deltaTime);
                thirstSprite.color -= new Color(0f, 0f, 0f, Time.deltaTime);
                healthSprite.color -= new Color(0f, 0f, 0f, Time.deltaTime);
                backgroundSprite.color -= new Color(0f, 0f, 0f, Time.deltaTime);
                background2Sprite.color -= new Color(0f, 0f, 0f, Time.deltaTime);
                if (iconSprite.color.a <= 0) {
                    iconFadeOut = false;
                }
            }
        }
    }

    public void FadeIcon(bool isFadingIn) {
        if (isFadingIn) {
            iconFadeOut = false;
            iconFadeIn = true;
            isActive = true;
        } else {
            isActive = false;
            iconFadeIn = false;
            iconFadeOut = true;
        }
    }

    public void SetVisible(bool visible) {
        if(visible) {
            iconSprite.color = SetAlpha(iconSprite.color, 1);
            hungerSprite.color = SetAlpha(hungerSprite.color, 1);
            thirstSprite.color = SetAlpha(thirstSprite.color, 1);
            healthSprite.color = SetAlpha(healthSprite.color, 1);
            backgroundSprite.color = SetAlpha(backgroundSprite.color, 1);
            background2Sprite.color = SetAlpha(backgroundSprite.color, 1);
        } else {
            iconSprite.color = SetAlpha(iconSprite.color, 0);
            hungerSprite.color = SetAlpha(hungerSprite.color, 0);
            thirstSprite.color = SetAlpha(thirstSprite.color, 0);
            healthSprite.color = SetAlpha(healthSprite.color, 0);
            backgroundSprite.color = SetAlpha(backgroundSprite.color, 0);
            background2Sprite.color = SetAlpha(backgroundSprite.color, 0);
        }

        isVisible = visible;
    }

    private void SetHungerLevel(object o, float state) {
        float hungerAmount = state;
        /*if(state == "Full") {
            hungerAmount = 1f;
        } else if(state == "ThreeFourth") {
            hungerAmount = 0.75f;
        } else if(state == "OneHalf") {
            hungerAmount = 0.5f;
        } else if(state == "OneFourth") {
            hungerAmount = 0.25f;
        } else {
            hungerAmount = 0f;
        }*/
        hungerSprite.fillAmount = hungerCap * hungerAmount;
    }

    private void SetThirstLevel(object o, float state) {
        float thirstAmount = state;
        /*if(state == "Water_Full") {
            thirstAmount = 1f;
        } else if(state == "Water_Three_Fourth") {
            thirstAmount = 0.75f;
        } else if(state == "Water_One_Half") {
            thirstAmount = 0.5f;
        } else if(state == "Water_One_Fourth") {
            thirstAmount = 0.25f;
        } else {
            thirstAmount = 0f;
        }*/
        thirstSprite.fillAmount = thirstCap * thirstAmount;
    }

    private void SetHealthLevel(object o, float healthAmount) {
        healthSprite.fillAmount = healthCap * healthAmount;
    }

    private void ActivateWander(object o, SheepHealth sheep) {
        parentController.SetWander(this, true);
    }

    private void DeactivateWander(object o, SheepHealth sheep) {
        parentController.SetWander(this, false);
    }

    private Color SetAlpha(Color c, float alpha) {
        Color result = new Color(c.r, c.g, c.b, alpha);
        return result;
    }

    private float AngleDifference(float angle1, float angle2) {
        if(angle2 < angle1) {
            float angle1DistTo360 = 360f - angle1;
            return angle1DistTo360 + angle2;
        } else {
            return angle2 - angle1;
        }
    }

}
