using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.Rendering;
#endif
using UnityEngine;

public static class SettingsSaver {
    public static void SaveSettings(SettingsMenuController settings) {
        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "/settings.settingsFile";
        FileStream stream = new FileStream(path, FileMode.Create);
        SettingsData data = new SettingsData(settings);
        formatter.Serialize(stream, data);
        stream.Close();
    }

    public static SettingsData LoadSettings() {
        string path = Application.persistentDataPath + "/settings.settingsFile";
        if(File.Exists(path)) {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);
            SettingsData data = formatter.Deserialize(stream) as SettingsData;
            stream.Close();
            return data;
        } else {
            Debug.LogError("Save file not found");
            return null;
        }
    }
}
