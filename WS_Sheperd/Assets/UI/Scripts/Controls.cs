using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controls : MonoBehaviour {
	public KeyCode forward;
	public KeyCode backward;
	public KeyCode left;
	public KeyCode right;
	public KeyCode sprint;
	public KeyCode camp;
	public KeyCode pause;
	public KeyCode repel;
	public KeyCode interact;
	public KeyCode place;
	public int sensitivity;
	public bool invertLook = false;

	private KeyCode defaultForward;
	private KeyCode defaultBackward;
	private KeyCode defaultLeft;
	private KeyCode defaultRight;
	private KeyCode defaultSprint;
	private KeyCode defaultCamp;
	private KeyCode defaultPause;
	private KeyCode defaultRepel;
	private KeyCode defaultInteract;
	private KeyCode defaultPlace;

	void Start() {
		defaultForward = forward;
		defaultBackward = backward;
		defaultLeft = left;
		defaultRight = right;
		defaultSprint = sprint;
		defaultCamp = camp;
		defaultPause = pause;
		defaultRepel = repel;
		defaultInteract = interact;
		defaultPlace = place;
		Load();
	}

	public void Save() {
		ControlsSaver.SaveControls(this);
	}

	private void Load() {
		ControlsData loaded = ControlsSaver.LoadControls();
		if(loaded != null) {
			forward = loaded.forward;
			backward = loaded.backward;
			left = loaded.left;
			right = loaded.right;
			sprint = loaded.sprint;
			camp = loaded.camp;
			pause = loaded.pause;
			repel = loaded.repel;
			interact = loaded.interact;
			place = loaded.place;
			sensitivity = loaded.sensitivity;
			invertLook = loaded.invertLook;
		}
	}

	public void ResetKeybinds() {
		forward = defaultForward;
		backward = defaultBackward;
		left = defaultLeft;
		right = defaultRight;
		sprint = defaultSprint;
		camp = defaultCamp;
		pause = defaultPause;
		repel = defaultRepel;
		interact = defaultInteract;
		place = defaultPlace;
	}
	
	public float getVertical() {
		float result = 0f;
		if(Input.GetKey(forward)) {
			result += 1f;
		}

		if(Input.GetKey(backward)) {
			result -= 1f;
		}

		return result;
	}

	public float getHorizontal() {
		float result = 0f;
		if(Input.GetKey(right)) {
			result += 1f;
		}

		if(Input.GetKey(left)) {
			result -= 1f;
		}

		return result;
	}
}
