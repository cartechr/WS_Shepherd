using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Menu : MonoBehaviour {
    public MenuSystemController menuSystemController;
    public GameObject container;
    
    private Menu previousMenu;

    public Menu GetPreviousMenu() {
        return previousMenu;
    }

    public void SetPreviousMenu(Menu menu) {
        this.previousMenu = menu;
    }
    
}
