using FMODUnity;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Remoting.Channels;
#if UNITY_EDITOR
using UnityEditorInternal;
#endif
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using EventHandler = System.EventHandler;
using Object = System.Object;

public class Tutorial : MonoBehaviour
{

    public Toggle toggle;
    public Tutorial_UI tutorialUI;

    [Header("Food_Water")]
    public GameObject Food_water;
    public GameObject FW_Barrier1;
    public GameObject FW_Barrier2;

    [Header("Friendship")]
    public GameObject Friendship;

    [Header("Wander")]
    public GameObject Wander;
    public GameObject W_Barrier;
    public GameObject W_Barrier2;

    [Header("Camping")]
    public GameObject Camping;
    public GameObject C_Barrier1;
    public GameObject C_Barrier2;

    [Header("Coyote_Repel")]
    public GameObject Coyote_repel;

    [Header("Obstacles")]
    public GameObject Obstacles;
    public GameObject O_Barrier1;
    public GameObject O_Barrier2;

    [Header("Stay Command")]
    public GameObject Stay_command;

    [Header("WASD")]
    public GameObject WASD;
    //public GameObject Barrier1

    [Header("Sheep_Treat")]
    public GameObject sheepTreat;

    GameObject storevar = null;
    CanvasGroup canvas;
    public bool objective_completed = false;


    //dictionary to keep already called tutorial UI;
    [SerializeField]
    public static Dictionary<string, bool> TutorialDict = new Dictionary<string, bool>()
    {
        {"FW", false},
        {"Friendship", false},
        {"Wander", false},
        {"Camping", false},
        {"Coyote_Repel", false},
        {"Obstacles", false},
        {"Stay", false},
        { "WASD", false },
        { "Sheep_Treat", false }
    };

    private bool T_FadeOut = false;

    //for repel UI
    public static bool isRepelled = false;
    public bool CoyoteUI = false;

    //for wandering UI
    public static bool NotWandering = false;

    const string FOOD_WATER = "FW";
    const string FRIENDSHIP = "Friendship";
    const string WANDER = "Wander";
    const string CAMPING = "Camping";
    const string COYOTE_REPEL = "Coyote_Repel";
    const string OBSTACLES = "Obstacles";
    const string STAY = "Stay";
    const string Wasd = "WASD";
    const string SHEEP_TREAT = "Sheep_Treat";
    FMOD.Studio.EventInstance tutStartSound;

    private static readonly object DoItAgain = new object();

    private void Start()
    {
        tutorialGameEvents.FW += StartFWUI;
        tutorialGameEvents.Friendship += StartFriendUI;
        tutorialGameEvents.Wander += StartWanderUI;
        tutorialGameEvents.Camping += StartCampingUI;
        tutorialGameEvents.Coyote += StartCoyote_RepelUI;
        tutorialGameEvents.Obstacles += StartObstacleUI;
        tutorialGameEvents.End += End_All_UI;
        tutorialGameEvents.Stay += StartStayCommandUI;
        tutorialGameEvents.WASD += WASDUI;
        tutorialGameEvents.Treat += Treat_UI;

        tutStartSound = FMODUnity.RuntimeManager.CreateInstance("event:/Audio/TutorialPopup");
    }

    private void OnDestroy()
    {
        tutorialGameEvents.FW -= StartFWUI;
        tutorialGameEvents.Friendship -= StartFriendUI;
        tutorialGameEvents.Wander -= StartWanderUI;
        tutorialGameEvents.Camping -= StartCampingUI;
        tutorialGameEvents.Coyote -= StartCoyote_RepelUI;
        tutorialGameEvents.Obstacles -= StartObstacleUI;
        tutorialGameEvents.End -= End_All_UI;
        tutorialGameEvents.Stay -= StartStayCommandUI;
        tutorialGameEvents.WASD -= WASDUI;
        tutorialGameEvents.Treat -= Treat_UI;
    }

    void Update()
    {

        //fades UI out
        if (T_FadeOut == true)
        {
            if (canvas.alpha > 0)
            {
                Debug.Log("Fading Out Now");
                canvas.alpha -= Time.deltaTime;
                toggle.GetComponent<CanvasGroup>().alpha = canvas.alpha;
            }
            if (canvas.alpha <= 0)
            {
                T_FadeOut = false;
                //storevar.SetActive(false);
                //toggle.gameObject.SetActive(false);
                //toggle.GetComponent<CanvasGroup>().alpha = 1;
                toggle.isOn = false;
                storevar = null;
            }

            if (SceneManager.GetActiveScene().buildIndex == 3)
            {
                bool check = false;
                if (check == false)
                {
                    tutorialGameEvents.Camping(this, EventArgs.Empty);
                }
            }

        }

        if (CoyoteUI && isRepelled)
        {
            tutorialGameEvents.End(this, EventArgs.Empty);
            CoyoteUI = false;
        }

        try {
            OnScreenDebug.instance.SetCategory("Tutorial",
                "Food-Water: " + TutorialDict[FOOD_WATER] + "\n" +
                "Friendship: " + TutorialDict[FRIENDSHIP] + "\n" +
                "Wander: " + TutorialDict[WANDER] + "\n" +
                "Camping: " + TutorialDict[CAMPING] + "\n" +
                "Coyote Repel: " + TutorialDict[COYOTE_REPEL] + "\n" +
                "Obstacles: " + TutorialDict[OBSTACLES] + "\n" +
                "Stay Command: " + TutorialDict[STAY] + "\n" +
                "WASD: " + TutorialDict[Wasd] + "\n" +
                "Sheep_Treat: " + TutorialDict[SHEEP_TREAT]
            );
        } catch (Exception e)
        {
            Debug.LogException(e);
        }

    }
    public void StartFWUI(object sender, EventArgs args)
    {
            if (TutorialDict["FW"] == false || sender == DoItAgain)
            {
                Food_water.GetComponent<CanvasGroup>().alpha = 1;
                toggle.GetComponent<CanvasGroup>().alpha = 1;
                tutStartSound.start();
                FW_Barrier1.gameObject.SetActive(true);
                FW_Barrier2.gameObject.SetActive(true);

                storevar = Food_water;
                TutorialDict["FW"] = true;
                objective_completed = false;
            }
            if (TutorialDict["FW"] == true)
            {
                Debug.Log("Food/Water UI already called");
            }
 
    }

    public void StartFriendUI(object sender, EventArgs args)
    {
            if (TutorialDict["Friendship"] == false || sender == DoItAgain)
            {
                Friendship.GetComponent<CanvasGroup>().alpha = 1;
                toggle.GetComponent<CanvasGroup>().alpha = 1;
                tutStartSound.start();
                storevar = Friendship;
                TutorialDict["Friendship"] = true;
                objective_completed = false;
            }
            if (TutorialDict["Friendship"] == true)
            {
                Debug.Log("Friendship UI already called");
            }

    }
    public void StartWanderUI(object sender, EventArgs args)
    {
            if (TutorialDict["Wander"] == false || sender == DoItAgain)
            {
                Wander.GetComponent<CanvasGroup>().alpha = 1;
                toggle.GetComponent<CanvasGroup>().alpha = 1;
                tutStartSound.start();
                W_Barrier.SetActive(true);
                W_Barrier2.SetActive(true);

                storevar = Wander;
                TutorialDict["Wander"] = true;
                objective_completed = false;
            }
            if (TutorialDict["Wander"] == true)
            {
                Debug.Log("Wander UI already called");
            }
   
    }

    public void StartCampingUI(object sender, EventArgs args)
    {
            if (TutorialDict["Camping"] == false || sender == DoItAgain)
            {
                Camping.GetComponent<CanvasGroup>().alpha = 1;
                toggle.GetComponent<CanvasGroup>().alpha = 1;
                storevar = Camping;
                TutorialDict["Camping"] = true;
                objective_completed = false;
                tutStartSound.start();
            }
            if (TutorialDict["Camping"] == true)
            {
                Debug.Log("Camping UI already called");
            }
   
    }
    public void StartCoyote_RepelUI(object sender, EventArgs args)
    {
            if (TutorialDict["Coyote_Repel"] == false || sender == DoItAgain)
            {

                Coyote_repel.GetComponent<CanvasGroup>().alpha = 1;
                toggle.GetComponent<CanvasGroup>().alpha = 1;

                //repel barriers?

                storevar = Coyote_repel;
                TutorialDict["Coyote_Repel"] = true;
                objective_completed = false;
                CoyoteUI = true;
                tutStartSound.start();
            }
            if (TutorialDict["Coyote_Repel"] == true)
            {
                Debug.Log("Coyote_Repel UI already called");
            }

    }

    public void StartObstacleUI(object sender, EventArgs args)
    {
            if (TutorialDict["Obstacles"] == false || sender == DoItAgain)
            {
                Obstacles.GetComponent<CanvasGroup>().alpha = 1;
                toggle.GetComponent<CanvasGroup>().alpha = 1;

                storevar = Obstacles;
                TutorialDict["Obstacles"] = true;
                objective_completed = false;
                tutStartSound.start();
            }
            if (TutorialDict["Obstacles"] == true)
            {
                Debug.Log("Obstacle UI already called");
            }

    }


    public void End_All_UI(object sender, EventArgs args)
    {
        toggle.isOn = true;
        objective_completed = true;

        StartCoroutine(Checkmark());
    }

    public void StartStayCommandUI(object sender, EventArgs args)
    {
        if (TutorialDict["Stay"] == false || sender == DoItAgain)
        {
            Stay_command.GetComponent<CanvasGroup>().alpha = 1;
            toggle.GetComponent<CanvasGroup>().alpha = 1;

            storevar = Stay_command;
            TutorialDict["Stay"] = true;
        }
        if (TutorialDict["Stay"] == true)
        {
            Debug.Log("Stay Command UI already called");
        }
    }

    public void WASDUI(object sender, EventArgs args)
    {
        if (TutorialDict["WASD"] == false || sender == DoItAgain)
        {
            WASD.GetComponent<CanvasGroup>().alpha = 1;
            toggle.GetComponent<CanvasGroup>().alpha = 1;

            FW_Barrier1.gameObject.SetActive(true);
            FW_Barrier2.gameObject.SetActive(true);

            storevar = WASD;
            TutorialDict["WASD"] = true;
            objective_completed = false;
        }
        if (TutorialDict["WASD"] == true)
        {
            Debug.Log("WASD UI already called");
        }

    }

    public void Treat_UI(object sender, EventArgs args)
    {
        if (TutorialDict["Sheep_Treat"] == false || sender == DoItAgain)
        {
            sheepTreat.GetComponent<CanvasGroup>().alpha = 1;
            toggle.GetComponent<CanvasGroup>().alpha = 1;

            FW_Barrier1.gameObject.SetActive(true);
            FW_Barrier2.gameObject.SetActive(true);

            storevar = sheepTreat;
            TutorialDict["Sheep_Treat"] = true;
            objective_completed = false;
        }
        if (TutorialDict["Sheep_Treat"] == true)
        {
            Debug.Log("Sheep_Treat UI already called");
        }

    }


    IEnumerator Checkmark()
    {
        canvas = storevar.GetComponent<CanvasGroup>();
        yield return new WaitForSeconds(2f);
        //Debug.Log("Fade Out");
        /* Food_water.gameObject.SetActive(false);
         Friendship.gameObject.SetActive(false);
         Wander.gameObject.SetActive(false);
         Camping.gameObject.SetActive(false);
         Coyote_repel.gameObject.SetActive(false);
         Obstacles.gameObject.SetActive(false);
         toggle.gameObject.SetActive(false);
        */

        T_FadeOut = true;

        if (TutorialDict["FW"] == true)
        {
            FW_Barrier1.SetActive(false);
            FW_Barrier2.SetActive(false);
        }
        if (TutorialDict["Obstacles"] == true)
        {
            O_Barrier1.SetActive(false);
            O_Barrier2.SetActive(false);
        }
        if (TutorialDict["Wander"] == true)
        {
            W_Barrier.SetActive(false);
            W_Barrier2.SetActive(false);
        }
        if (TutorialDict["Camping"] == true)
        {
            C_Barrier1.SetActive(false);
            C_Barrier2.SetActive(false);
        }
    }

    public string GetCurrentObjectiveCanvasName()
    {
        return storevar?.name;
    }

    public void SetCurrentObjectiveByName(string name)
    {
        storevar = GameObject.Find(name);
        if (storevar) {
            canvas = storevar.GetComponent<CanvasGroup>();
        }
    }

    public bool IsCurrentObjectiveCompleted()
    {
        return objective_completed;
    }

    public void SetCurrentObjectiveCompleted(bool value)
    {
        objective_completed = value;
    }

    public bool IsFadingOut()
    {
        return T_FadeOut;
    }
    public void SetFadingOut(bool value)
    {
        T_FadeOut = value;
    }
    public void RecheckVisible()
    {
        StopAllCoroutines();

        if (TutorialDict["FW"] == true)
        {
            FW_Barrier1.SetActive(false);
            FW_Barrier2.SetActive(false);
        }
        if (TutorialDict["Obstacles"] == true)
        {
            O_Barrier1.SetActive(false);
            O_Barrier2.SetActive(false);
        }
        if (TutorialDict["Wander"] == true)
        {
            W_Barrier.SetActive(false);
            W_Barrier2.SetActive(false);
        }
        if (TutorialDict["Camping"] == true)
        {
            C_Barrier1.SetActive(false);
            C_Barrier2.SetActive(false);
        }

        Food_water.GetComponent<CanvasGroup>().alpha = 0;
        Friendship.GetComponent<CanvasGroup>().alpha = 0;
        Wander.GetComponent<CanvasGroup>().alpha = 0;
        Camping.GetComponent<CanvasGroup>().alpha = 0;
        Coyote_repel.GetComponent<CanvasGroup>().alpha = 0;
        Obstacles.GetComponent<CanvasGroup>().alpha = 0;
        Stay_command.GetComponent<CanvasGroup>().alpha = 0;

        toggle.GetComponent<CanvasGroup>().alpha = 0;

        toggle.isOn = objective_completed;

        if (!objective_completed) {
            if (storevar == Food_water) {
                StartFWUI(DoItAgain, EventArgs.Empty);
            } else if (storevar == Friendship) {
                StartFriendUI(DoItAgain, EventArgs.Empty);
            } else if (storevar == Wander) {
                StartWanderUI(DoItAgain, EventArgs.Empty);
            } else if (storevar == Camping) {
                StartCampingUI(DoItAgain, EventArgs.Empty);
            } else if (storevar == Coyote_repel) {
                StartCoyote_RepelUI(DoItAgain, EventArgs.Empty);
            } else if (storevar == Obstacles) {
                StartObstacleUI(DoItAgain, EventArgs.Empty);
            } else if (storevar == Stay_command) {
                StartStayCommandUI(DoItAgain, EventArgs.Empty);
            } else if (storevar == WASD) {
                WASDUI(DoItAgain, EventArgs.Empty);
            } else if (storevar = sheepTreat) {
                Treat_UI(DoItAgain, EventArgs.Empty);
            }
        }
    } 
}

    public static class tutorialGameEvents
    {
        public static EventHandler FW;
        public static EventHandler Friendship;
        public static EventHandler Wander;
        public static EventHandler Camping;
        public static EventHandler Coyote;
        public static EventHandler Obstacles;
        public static EventHandler End;
        public static EventHandler Stay;
        public static EventHandler WASD;
        public static EventHandler Treat;
    } 


