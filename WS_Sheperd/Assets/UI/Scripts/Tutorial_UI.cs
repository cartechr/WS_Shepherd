using JetBrains.Annotations;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Remoting;
#if UNITY_EDITOR
using UnityEditorInternal;
#endif
using UnityEngine;
using UnityEngine.SceneManagement;

public class Tutorial_UI : MonoBehaviour
{
    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("StartUI") && other.gameObject.name == "Start_FW")
        {
            tutorialGameEvents.FW(this, EventArgs.Empty);
        }
        if (other.gameObject.CompareTag("StartUI") && other.gameObject.name == "Start_Wander")
        {
            tutorialGameEvents.Wander(this, EventArgs.Empty);
        }
        if (other.gameObject.CompareTag("StartUI") && other.gameObject.name == "Start_Camping")
        {
            tutorialGameEvents.Camping(this, EventArgs.Empty);
        }
        if (other.gameObject.CompareTag("StartUI") && other.gameObject.name == "Start_Treats_UI")
        {
            tutorialGameEvents.Treat(this, EventArgs.Empty);
        }
        if (other.gameObject.CompareTag("StartUI") && other.gameObject.name == "Start_WASD")
        {
            tutorialGameEvents.Treat(this, EventArgs.Empty);
        }
        if (other.gameObject.CompareTag("Completed"))
        {
            tutorialGameEvents.End(this, EventArgs.Empty);
        }
    }
}

