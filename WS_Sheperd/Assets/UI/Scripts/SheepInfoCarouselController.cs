using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;
using UnityEngine.UI;

public class SheepInfoCarouselController : SheepIconParent {

    public Transform center;
    public Transform cam;
    public Vector3 offset;

    private SheepIconController[] sheepIcons;
    private RectTransform thisRectP;
    private Rect thisRect;
    private RectTransform canvasRect;
    
    private float angleTR;
    private float angleBR;
    private float angleBL;
    private float angleTL;

    // Start is called before the first frame update
    void Start() {
        sheepIcons = GetComponentsInChildren<SheepIconController>();
        foreach(SheepIconController icon in sheepIcons) {
            icon.isActive = false;
        }
        thisRectP = GetComponent<RectTransform>();
        canvasRect = GetComponentInParent<RectTransform>();
        thisRectP.sizeDelta = canvasRect.sizeDelta;
        thisRect = thisRectP.rect;
        float hDistance = Mathf.Sqrt(Mathf.Pow(thisRect.width / 2f, 2) + Mathf.Pow(thisRect.height / 2f, 2));
        float generalAngle = (Mathf.Asin((thisRect.width / 2f) / hDistance) * 180f) / Mathf.PI;
        float oppositeAngle = 90f - generalAngle;
        angleTR = generalAngle;
        angleBR = angleTR + oppositeAngle + oppositeAngle;
        angleBL = angleBR + generalAngle + generalAngle;
        angleTL = angleBL + oppositeAngle + oppositeAngle;
    }

    // Update is called once per frame
    void Update() {
        Vector3 centerPosition = center.position;
        // Vector3 forwardDirection = new Vector3(cam.forward.x, 0, cam.forward.z).normalized;
        Vector3 forwardDirection = Vector3.ProjectOnPlane(cam.transform.forward, Vector3.up).normalized;
        Debug.DrawRay(centerPosition, forwardDirection, Color.green);
        foreach(SheepIconController sic in sheepIcons) {
            Vector3 dirToSheep = (sic.connectedSheep.transform.position - centerPosition).normalized;
            //Vector3 sheepDirection = new Vector3(dirToSheep.x, 0, dirToSheep.z).normalized;
            Vector3 sheepDirection = Vector3.ProjectOnPlane(dirToSheep, Vector3.up).normalized;
            Debug.DrawRay(centerPosition, sheepDirection, Color.red);
            float sheepAngle = Vector3.SignedAngle(forwardDirection, sheepDirection, Vector3.up);
            if(sheepAngle < 0) {
                sheepAngle = (180f - Mathf.Abs(sheepAngle)) + 180f;
            }
            //Debug.Log(sic.connectedSheep.gameObject.name + ": " + sheepAngle);
            /*
             * Calculate normalized screen position based on angle
             * Set x and y to be between 0 and 1
             */
            Vector2 normalizedPosition = Vector2.zero;
            if(sheepAngle <= angleTR) { // TOP RIGHT
                normalizedPosition.x = ((sheepAngle / angleTR) / 2f) + 0.5f;
                normalizedPosition.y = 1f;
            } else if(sheepAngle >= angleTL) { // TOP LEFT
                normalizedPosition.x = ((sheepAngle - angleTL) / (360f - angleTL) / 2f);
                normalizedPosition.y = 1f;
            } else if(sheepAngle >= angleBR && sheepAngle <= angleBL) { // BOTTOM
                normalizedPosition.x = 1f - ((sheepAngle - angleBR) / (angleBL - angleBR));
                normalizedPosition.y = 0f;
            } else if(sheepAngle > angleTR && sheepAngle < angleBR) { // RIGHT
                normalizedPosition.x = 1f;
                normalizedPosition.y = 1f - ((sheepAngle - angleTR) / (angleBR - angleTR));
            } else if(sheepAngle > angleBL && sheepAngle < angleTL) { // LEFT
                normalizedPosition.x = 0f;
                normalizedPosition.y = (sheepAngle - angleBL) / (angleTL - angleBL);
            }
            // Convert normalized screen position to actual coordinates on the canvas
            float screenPositionX = thisRect.width * normalizedPosition.x;
            float screenPositionY = thisRect.height * normalizedPosition.y;
            float iconWidth = sic.rect.rect.width * sic.rect.localScale.x;
            float iconHeight = sic.rect.rect.height * sic.rect.localScale.y;
            screenPositionX = Mathf.Clamp(screenPositionX, iconWidth / 2f, thisRect.width - iconWidth / 2f);
            screenPositionY = Mathf.Clamp(screenPositionY, iconHeight / 2f, thisRect.height - iconHeight / 2f);
            Vector3 screenPosition = new Vector3(screenPositionX, screenPositionY, 0f);
            sic.rect.position = screenPosition;
        }

        foreach(SheepIconController icon in sheepIcons) {
            if(icon.isActive) {
                Vector3 pos = Camera.main.WorldToViewportPoint(icon.connectedSheep.worldIcon.transform.position);
                if((pos.x > 0 && pos.x < 1) && (pos.y > 0 && pos.y < 1) && pos.z > 0) {
                    icon.SetVisible(false);
                } else {
                    icon.SetVisible(true);
                }
            }
        }
    }

    public override void SetWander(SheepIconController sheepIcon, bool isWandering) {
       sheepIcon.FadeIcon(isWandering);
    }
}
