using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CheckboxController : MonoBehaviour, ISelection {
    public bool active;
    public Sprite off;
    public Sprite on;
    public bool defaultSelection;

    private Image image;
    // Start is called before the first frame update
    void Start() {
        image = GetComponentInChildren<Image>();
        ResetSelection();
    }

    public object GetSelection() {
        return active;
    }

    public void Toggle() {
        active = !active;
        UpdateSprite();
    }

    private void UpdateSprite() {
        if(active) {
            image.sprite = on;
        } else {
            image.sprite = off;
        }
    }

    public void ResetSelection() {
        active = defaultSelection;
        UpdateSprite();
    }

    public void SubmitSelection() {
        defaultSelection = active;
        UpdateSprite();
    }
}
