using System.Collections;
using System.Collections.Generic;
using System.Xml.Xsl;
using UnityEngine;

public class KeybindMenuController : Menu {

	public KeybindSelectionController forward;
	public KeybindSelectionController backward;
	public KeybindSelectionController left;
	public KeybindSelectionController right;
	public KeybindSelectionController sprint;
	public KeybindSelectionController camp;
	public KeybindSelectionController pause;
	public KeybindSelectionController repel;
	public KeybindSelectionController interact;
	public KeybindSelectionController place;

	public Controls mainMenuControls;
	
	public void ApplyKeybinds() {
		if(menuSystemController.gameController != null) {
			menuSystemController.gameController.controls.forward = (KeyCode)forward.GetSelection();
			menuSystemController.gameController.controls.backward = (KeyCode)backward.GetSelection();
			menuSystemController.gameController.controls.left = (KeyCode)left.GetSelection();
			menuSystemController.gameController.controls.right = (KeyCode)right.GetSelection();
			menuSystemController.gameController.controls.sprint = (KeyCode)sprint.GetSelection();
			menuSystemController.gameController.controls.camp = (KeyCode)camp.GetSelection();
			menuSystemController.gameController.controls.pause = (KeyCode)pause.GetSelection();
			menuSystemController.gameController.controls.repel = (KeyCode)repel.GetSelection();
			menuSystemController.gameController.controls.interact = (KeyCode)interact.GetSelection();
			menuSystemController.gameController.controls.place = (KeyCode)place.GetSelection();
			menuSystemController.gameController.controls.Save();
		} else {
			mainMenuControls.forward = (KeyCode)forward.GetSelection();
			mainMenuControls.backward = (KeyCode)backward.GetSelection();
			mainMenuControls.left = (KeyCode)left.GetSelection();
			mainMenuControls.right = (KeyCode)right.GetSelection();
			mainMenuControls.sprint = (KeyCode)sprint.GetSelection();
			mainMenuControls.camp = (KeyCode)camp.GetSelection();
			mainMenuControls.pause = (KeyCode)pause.GetSelection();
			mainMenuControls.repel = (KeyCode)repel.GetSelection();
			mainMenuControls.interact = (KeyCode)interact.GetSelection();
			mainMenuControls.place = (KeyCode)place.GetSelection();
			mainMenuControls.Save();
		}
		
	}

	void Awake() {
		if(menuSystemController.gameController != null) {
			forward.defaultKey = menuSystemController.gameController.controls.forward;
			backward.defaultKey = menuSystemController.gameController.controls.backward;
			left.defaultKey = menuSystemController.gameController.controls.left;
			right.defaultKey = menuSystemController.gameController.controls.right;
			sprint.defaultKey = menuSystemController.gameController.controls.sprint;
			camp.defaultKey = menuSystemController.gameController.controls.camp;
			pause.defaultKey = menuSystemController.gameController.controls.pause;
			repel.defaultKey = menuSystemController.gameController.controls.repel;
			interact.defaultKey = menuSystemController.gameController.controls.interact;
			place.defaultKey = menuSystemController.gameController.controls.place;
		} else {
			forward.defaultKey = mainMenuControls.forward;
			backward.defaultKey = mainMenuControls.backward;
			left.defaultKey = mainMenuControls.left;
			right.defaultKey = mainMenuControls.right;
			sprint.defaultKey = mainMenuControls.sprint;
			camp.defaultKey = mainMenuControls.camp;
			pause.defaultKey = mainMenuControls.pause;
			repel.defaultKey = mainMenuControls.repel;
			interact.defaultKey = mainMenuControls.interact;
			place.defaultKey = mainMenuControls.place;
		}
	}

	public void ResetKeys() {
		if(menuSystemController.gameController != null) {
			menuSystemController.gameController.controls.ResetKeybinds();
			forward.UpdateKey(menuSystemController.gameController.controls.forward);
			backward.UpdateKey(menuSystemController.gameController.controls.backward);
			left.UpdateKey(menuSystemController.gameController.controls.left);
			right.UpdateKey(menuSystemController.gameController.controls.right);
			sprint.UpdateKey(menuSystemController.gameController.controls.sprint);
			camp.UpdateKey(menuSystemController.gameController.controls.camp);
			pause.UpdateKey(menuSystemController.gameController.controls.pause);
			repel.UpdateKey(menuSystemController.gameController.controls.repel);
			interact.UpdateKey(menuSystemController.gameController.controls.interact);
			place.UpdateKey(menuSystemController.gameController.controls.place);
		} else {
			mainMenuControls.ResetKeybinds();
			forward.UpdateKey(mainMenuControls.forward);
			backward.UpdateKey(mainMenuControls.backward);
			left.UpdateKey(mainMenuControls.left);
			right.UpdateKey(mainMenuControls.right);
			sprint.UpdateKey(mainMenuControls.sprint);
			camp.UpdateKey(mainMenuControls.camp);
			pause.UpdateKey(mainMenuControls.pause);
			repel.UpdateKey(mainMenuControls.repel);
			interact.UpdateKey(mainMenuControls.interact);
			place.UpdateKey(mainMenuControls.place);
		}
	}
	
}
