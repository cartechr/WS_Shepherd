using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DebugTimer : MonoBehaviour
{
    public Text timerText;
    private int timeElapsedInPlaytest;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        timerText.text = "Playtest Runtime" + Time.realtimeSinceStartup;
    }
}
