using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class jamesCampingDebug : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(DebugRoutine());
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator DebugRoutine()
    {
        yield return new WaitForSeconds(3);
        Debug.Log("Calling Upon the Sheep to their Camping Positions!");
        GameEvents.EnteredCamp?.Invoke(this, EventArgs.Empty);
    }
}
