using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayTestPauseRestart : MonoBehaviour
{
    public static bool isGamePaused = false;
    public GameObject musicManagerObject;

    private float delayBeforeLoading = 5f;
    private float timeElapsed;
    private bool restartStarted = false;
    private Controls controls;

    void Start() {
        controls = GetComponentInParent<Controls>();
    }

    // Update is called once per frame
    void Update()
    {
        if (restartStarted)
        {
            loadSceneOnDelay();
        }
        if (Input.GetKeyDown(KeyCode.LeftBracket))
        {
            if (isGamePaused)
            {
                ResetEntireGameplay();
            }
        }
    }

    private void loadSceneOnDelay()
    {
        timeElapsed += Time.deltaTime;
        if (timeElapsed > delayBeforeLoading)
        {
            SceneManager.LoadScene(0);
        }
    }

    public void Pause()
    {
        MusicScript musicScript = musicManagerObject.GetComponent<MusicScript>();
        //musicScript.PauseMusic();
        //menuSystemController.Pause();
        Time.timeScale = 0f;
        isGamePaused = true;
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.Confined;
    }

    public void Resume()
    {
        MusicScript musicScript = musicManagerObject.GetComponent<MusicScript>();
        //musicScript.ResumeMusic();
        //menuSystemController.Resume();
        Time.timeScale = 1f;
        isGamePaused = false;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Confined;
    }

    public void ResetEntireGameplay()
    {
        Time.timeScale = 1f;
        Debug.Log("EXIT");
        Destroy(GameObject.FindGameObjectWithTag("Player"));
        Destroy(GameObject.FindGameObjectWithTag("Sheep_Group"));
        Destroy(GameObject.FindGameObjectWithTag("GameController"));
        restartStarted = true;
    }


}
