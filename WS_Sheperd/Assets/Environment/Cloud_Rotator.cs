using UnityEngine;

namespace Borodar.FarlandSkies.LowPoly
{
    public class Cloud_Rotator : MonoBehaviour
    {
        public float RotationPerSecond = 1;

        protected void Update()
        {
            SkyboxController.Instance.CloudsRotation = Time.time * RotationPerSecond;
        }
    }
}
