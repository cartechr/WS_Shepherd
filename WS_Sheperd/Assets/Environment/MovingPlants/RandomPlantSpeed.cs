using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomPlantSpeed : MonoBehaviour
{
    public float minSpeed = 0.8f;
    public float maxSpeed = 1.0f;
    private Animator animator;

    void Start()
    {
        animator = GetComponent<Animator>();
        animator.speed = Random.Range(minSpeed, maxSpeed);
    }
}