using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindmillSpin : MonoBehaviour
{

    [SerializeField] private float rotationSpeed = 10f;
    [SerializeField] private Vector3 my_axis = new Vector3(0, 0, 10);

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(my_axis, rotationSpeed * Time.deltaTime);
    }

    private void OnDrawGizmosSelected()
    {
        Debug.DrawRay(transform.position, transform.rotation * my_axis, Color.yellow);
    }
}
