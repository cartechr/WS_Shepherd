using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TumbleweedMovement : MonoBehaviour
{
    // Reference to the WindZone component in the scene
    [SerializeField] private WindZone windZone;

    // Variables for the wind force, direction and magnitude
    private float windForce;
    private Vector3 windDirection;
    private float windMagnitude;

    // Variables for the object's speed, rotation and position
    private float speed;
    private Vector3 direction;
    private Vector3 position;

    // Variables for the object's rigidbody and collider
    private Rigidbody rb;
   

    // Variables for the object's bounce force and direction
    private float bounceForce;
    private Vector3 bounceDirection;

    void Start()
    {
        if (!windZone) {
            windZone = FindObjectOfType<WindChange>()?.GetComponent<WindZone>();
        }
        
        
        // Get the object's rigidbody and collider components
        rb = GetComponent<Rigidbody>();
       

        // Set the initial speed and direction of the object
        speed = 0.1f;
        direction = Vector3.forward;

        // Set the initial bounce force and direction
        bounceForce = 0.5f;
        bounceDirection = Vector3.up;
    }

    void Update()
    {
        // Get the wind force, direction and magnitude
        windForce = windZone.windMain;
        windDirection = windZone.transform.forward;
        windMagnitude = windForce * Time.deltaTime;

        // Move the object according to the wind direction and magnitude
        rb.AddForce(windDirection * windMagnitude); 

        // Rotate the object according to the wind direction and magnitude
        transform.Rotate(Vector3.up * windMagnitude * 10.0f);

       

        
    }


    public void OnCollisionEnter(Collision other)
    {
        // Get the normal of the surface the object collides with
        Vector3 normal = other.contacts[0].normal;

        // Calculate the bounce force and direction
        Vector3 bounceForce = Vector3.Reflect(direction, normal);
        bounceForce = bounceForce * speed;

        // Apply the bounce force to the object
        rb.AddForce(bounceForce);
    }
}

