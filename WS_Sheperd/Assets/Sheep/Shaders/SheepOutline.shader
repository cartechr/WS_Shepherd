Shader "Unlit/SheepOutline"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _Color ("Color", Color) = (1, 1, 1, 1) 
        _thickness ("Thickness", float) = 0.1
        //[Enum(UnityEngine.Rendering.CullMode)] _CullMode ("Cull Mode", Float) = 0.0 // default to back face culling
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100
        Cull Off

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
                float3 normal : NORMAL;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            float _thickness;
            float4 _Color; 

            v2f vert (appdata v)
            {
                v2f o;
                v.vertex = mul(unity_ObjectToWorld, v.vertex);
                v.normal = mul(unity_ObjectToWorld, v.normal);
                v.vertex += float4(normalize(v.normal) * _thickness, 0);
                //o.vertex = UnityObjectToClipPos(v.vertex);
                o.vertex = UnityWorldToClipPos(v.vertex);
                o.normal = -1 * normalize(v.normal);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 frag (v2f i, fixed facing : VFACE) : SV_Target
            {
                clip(-facing);
                // sample the texture
                //fixed4 col = tex2D(_MainTex, i.uv);
                fixed4 col = _Color;
                // apply fog
                UNITY_APPLY_FOG(i.fogCoord, col);
                return col;
            }
            ENDCG
        }
    }
}
