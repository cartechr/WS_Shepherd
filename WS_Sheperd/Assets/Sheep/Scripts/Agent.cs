using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Agent : MonoBehaviour
{
    public NavMeshAgent agent;
    [NonSerialized] private float _normalStoppingDistance;
    [NonSerialized] private float _normalAngularSpeed;
    [NonSerialized] private float _normalAcceleration;
    [NonSerialized] private float _normalSpeed;
    [NonSerialized] private ObstacleAvoidanceType _normalObstacleAvoidanceType;

    private void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        _normalStoppingDistance = agent.stoppingDistance;
        _normalAngularSpeed = agent.angularSpeed;
        _normalAcceleration = agent.acceleration;
        _normalObstacleAvoidanceType = agent.obstacleAvoidanceType;
    }
    public void CallAgent(Vector3 position)
    {
        //Debug.Log("Destination is changing");
      agent.destination = position;
    }
    public void SetStop(bool stopped)
    {
        agent.isStopped = stopped;
    }
    public void Speed(float sheepspeed)
    {
        agent.speed = sheepspeed;
    }
    public bool IsStopped()
    {
        return agent.isStopped;
    }

    public bool CallIfReachable(Vector3 position)
    {
        NavMeshPath path = new NavMeshPath();
        agent.CalculatePath(position, path);
        bool reachable = (path.status != NavMeshPathStatus.PathPartial);
        if (reachable) {
            agent.SetDestination(position);
        }
        return reachable;
    }

    public void WarpTo(Vector3 position)
    {
        agent.Warp(position);
    }

    public void SetStoppingDistance(float newStoppingDistance)
    {
        agent.stoppingDistance = newStoppingDistance;
    }
    
    public void ResetStoppingDistance()
    {
        agent.stoppingDistance = _normalStoppingDistance;
    }

    public bool HasReachedDestination()
    {
        return Vector3.Distance(agent.destination, transform.position) <= agent.stoppingDistance;
    }

    public void SetAngularSpeed(float newAngularSpeed)
    {
        agent.angularSpeed = newAngularSpeed;
    }
    
    public void ResetAngularSpeed()
    {
        agent.angularSpeed = _normalAngularSpeed;
    }
    
    public void SetAcceleration(float newAcceleration)
    {
        agent.acceleration = newAcceleration;
    }
    
    public void ResetAcceleration()
    {
        agent.acceleration = _normalAcceleration;
    }

    public void SetObstacleAvoidanceType(ObstacleAvoidanceType newObstacleAvoidanceType)
    {
        agent.obstacleAvoidanceType = newObstacleAvoidanceType;
    }

    public void ResetObstacleAvoidanceType()
    {
        agent.obstacleAvoidanceType = _normalObstacleAvoidanceType;
    }
}
