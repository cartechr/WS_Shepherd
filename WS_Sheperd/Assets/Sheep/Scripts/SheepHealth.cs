using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

public class SheepHealth : NPC_Follow
{
    [Header("")]
    public Animator animator;

    [Header("Health")]
    public int maxHealth;
    public int currentHealth;
    [Header("")]
    public GameController gamecontroller;
    public New_Rattlesnake rattlesnake;
    public SheepFriendship friendshipStats;
    public int water_drinking_rate;
    public int food_eating_Rate;
    public SheepWorldIconController worldIcon;
    public StateMachine myStateMachine;

    NPC_Follow npc_follow;

    public float slowedTimer;

    public bool Slowed = false;
    Transform SheepPosition;

    private int Damage;
    [Header("Particle Systems")]
    public ParticleSystem stress;
    public ParticleSystem dead;


    //FMOD
    FMOD.Studio.EventInstance distress;
    private float bleatTimer;
    [Header("")]
    public int bleatClass;

    [Header("Stamina (Deprecated)")]
    // MAIN PROPERTIES
    [SerializeField] private float _sheepStamina;
    public float sheepStamina
    {
        get
        {
            return _sheepStamina;
        }
        set
        {
            _sheepStamina = value;
            StaminaChanged?.Invoke(this, this);
        }
    }
    public float maxSheepStamina;
    [HideInInspector] public bool hasRegenerated = true;
    [HideInInspector] public bool moving = false;
    [Header("Statuses")]
    public bool panicking = false;
    [NonSerialized] public bool is_drinking;
    [NonSerialized] public bool is_eating;

    [Header("Stamina properties (Deprecated)")]
    // REGEN PROPERTIES
    [Range(0, 50)] [SerializeField] private float staminaDrain;
    [Range(0, 50)] [SerializeField] private float staminaRegen;

    [Header("Speed")]
    // SPEED PROPERTIES
    [SerializeField] public float slowedSpeed;
    [SerializeField] public float normalSpeed;
    [SerializeField] public float panicSpeed = 12f;
    [SerializeField] public float wanderSpeed;
    
    [Header("Collision Tracking")]
    public List<Eat_Sys> touchingFoods = new List<Eat_Sys>();
    public List<Water_Sys> touchingWaters = new List<Water_Sys>();

    // TODO: delete if we scrap promises
    public object promise = null;
    public static readonly Object PROMISE_FOOD = new Object(); // symbol pattern!
    public static readonly Object PROMISE_WATER = new Object();
    public static readonly Object PROMISE_CAMPING = new Object();

    
    public enum SheepRole {None, Lamb, Beefy, Stubborn, Loud, Reliable, Flighty, Leader}
    [Header("Sheep Types")]
    public SheepRole mySheepRole;

    public enum SheepType { None, Small, Large}
    public SheepType mySheepType;

    public EventHandler<SheepHealth> SwitchedToFollowState;
    public EventHandler<SheepHealth> SwitchedToWanderStateImmediate;
    public EventHandler<SheepHealth> SwitchedToWanderState;
    public EventHandler<SheepHealth> SwitchedToHungryState;
    public EventHandler<SheepHealth> SwitchedToThirstyState;
    public EventHandler<SheepHealth> SwitchedToPanicState;
    public EventHandler<SheepHealth> SwitchedToCampingState;
    public EventHandler<SheepHealth> SwitchedToEatingState;
    public EventHandler<SheepHealth> SwitchedToDrinkingState;
    public EventHandler<SheepHealth> SwitchedToEnsnaredState;
    public EventHandler<SheepHealth> SwitchedToDistractedState;
    public EventHandler<SheepHealth> SwitchedToFrightenedState;
    public EventHandler<SheepHealth> SwitchedToStayState;
    public EventHandler<SheepHealth> SwitchedToDeadState;
    
    public EventHandler<SheepHealth> SwitchedOutOfFollowState;
    public EventHandler<SheepHealth> SwitchedOutOfWanderState;
    public EventHandler<SheepHealth> SwitchedOutOfHungryState;
    public EventHandler<SheepHealth> SwitchedOutOfThirstyState;
    public EventHandler<SheepHealth> SwitchedOutOfPanicState;
    public EventHandler<SheepHealth> SwitchedOutOfCampingState;
    public EventHandler<SheepHealth> SwitchedOutOfEatingState;
    public EventHandler<SheepHealth> SwitchedOutOfDrinkingState;
    public EventHandler<SheepHealth> SwitchedOutOfEnsnaredState;
    public EventHandler<SheepHealth> SwitchedOutOfDistractedState;
    public EventHandler<SheepHealth> SwitchedOutOfFrightenedState;
    public EventHandler<SheepHealth> SwitchedOutOfStayState;
    public EventHandler<SheepHealth> SwitchedOutOfDeadState;
    
    // FoodChanged is defined in NPC_Follow
    // WaterChanged is defined in NPC_Follow
    public EventHandler<SheepHealth> StaminaChanged;
    public EventHandler<(SheepHealth, float)> FriendshipChangedToValue;
    public EventHandler<(SheepHealth, float)> FriendshipIncreasedBy;
    public EventHandler<(SheepHealth, float)> FriendshipDecreasedBy;



    public EventHandler<SheepHealth> LoseSheep;
    public EventHandler<SheepHealth> AteFood;
    public EventHandler<SheepHealth> DrankWater;
    
    public EventHandler<SheepHealth> TookDamage;
    public EventHandler<SheepHealth> DeathEvent;


    public static List<SheepHealth> sheepList = new List<SheepHealth>();
    public static List<SheepHealth> GetSheepList()

    {
        return sheepList;
    }

    public static SheepHealth FindSheepByRole(SheepRole role, bool defaultNull = false)
    {
        foreach (var sheep in sheepList) {
            if (sheep.mySheepRole == role) {
                return sheep;
            }
        }
        if (defaultNull) {
            return null;
        }
        throw new KeyNotFoundException("There was no sheep with role " + role);
    }

    private void Awake()
    {
        sheepList.Add(this);
        friendshipStats = GetComponent<SheepFriendship>();
        myStateMachine = GetComponent<StateMachine>();
        GameEvents.DeactivateEasyMode += OnDeactivateEasyMode;
    }

    private void OnDestroy()
    {
        sheepList.Remove(this);
        GameEvents.DeactivateEasyMode -= OnDeactivateEasyMode;
        try {
            TearDownDebug();
        } catch (Exception e) {
            Debug.LogException(e);
        }
    }

    void Start() {
        worldIcon = GetComponentInChildren<SheepWorldIconController>();
        //Debug.Log(sheepList);
        npc_follow = GetComponent<NPC_Follow>();
        SheepPosition = GetComponent<Transform>();
        initialize();
        animator = GetComponent<Animator>();
        currentHealth = maxHealth;
        agent.Speed(normalSpeed);

        //FMOD
        //distress = FMODUnity.RuntimeManager.CreateInstance("event:/Audio/Distress");
        bleatTimer = Random.Range(0.0f, 30.0f);
        food = 40;
        water = 60;

        try {
            SetupDebug();
        }
        catch (Exception e) {
            Debug.LogException(e);
        }
    }


    protected void SheepAnimations()
    {
      /*  horizontalInput = Input.GetAxis("Horizontal");
        //forwardInput = Input.GetAxis("Vertical");



        if ((horizontalInput != 0 || forwardInput != 0) && IsWalking == false)
        {
            IsWalking = true;
            // print("moving");
            animator.Play("Walk");
        }
        if ((horizontalInput == 0 && forwardInput == 0) && IsWalking == true)
        {
            IsWalking = false;
            // print("notMoving");
            animator.Play("Idle");
        } */

    }
    //returns true if sheep is killed
    public bool TakeDamage(int amount)
    {
        currentHealth -= amount;
        CheckHealth();
        if (currentHealth <= 0)
        {
            sheepList.Remove(this);
            this.GetComponent<StateMachine>().SetState("Dead");
            Debug.Log(gameObject.name + " is dead ");
            currentHealth = 0;
            dead.Play();
            TookDamage?.Invoke(this, this);
            DeathEvent?.Invoke(this, this);
            return true;
        }
        else
        {
            Debug.Log(gameObject.name + "s health is " + currentHealth);
            TookDamage?.Invoke(this, this);
            return false;
        }
    }

    public EventHandler<float> HealthChanged;

    private void CheckHealth() {
        float healthValue = (float) currentHealth / maxHealth;
        if(healthValue > 0.75f) {
            HealthChanged?.Invoke(this, 1f);
        } else if(healthValue > 0.5f) {
            HealthChanged?.Invoke(this, 0.75f);
        } else if(healthValue > 0.25f) {
            HealthChanged?.Invoke(this, 0.5f);
        } else if(healthValue > 0f) {
            HealthChanged?.Invoke(this, 0.25f);
        } else {
            HealthChanged?.Invoke(this, 0f);
        }

    }

    public bool SnakeDamage()
    {
        this.GetComponent<StateMachine>().SetState("Panic");
        return TakeDamage(rattlesnake.DamageToSheep);
    }

    public bool PoisonPlantDamage(int damage)
    {
        return TakeDamage(damage);
    }
    public bool JumpingChollaDamage(int damage)
    {
        return TakeDamage(damage);
    }



    //public void Heal(int amount)
    //{
    //currentHealth += amount;

    //if (currentHealth > maxHealth)
    //{
    //currentHealth = maxHealth;
    //}

    //}

    protected override void Update()
    {
        //FollowPlayer();
        SheepAnimations();
        if (moving == false)
        {
            if (sheepStamina <= maxSheepStamina - 0.01)
            {
                sheepStamina += staminaRegen * Time.deltaTime;
                //UpdateStamina

                if (sheepStamina >= maxSheepStamina)
                {
                    //set to normal speed
                    hasRegenerated = true;
                }
            }
        }
        IsMoving();
        Moving();
        DetermineSpeed();
        if (slowedTimer > 0)
            {
                slowedTimer -= Time.deltaTime;
                Slowed = true;
            }
        if (slowedTimer <= 0)
            {
                Slowed = false;
            }


        //SHEEP BLEATING
        if (bleatTimer > 0)
            {
            bleatTimer -= Time.deltaTime;
        }

        if (bleatTimer <= 0)
        {
            Bleat();
            Debug.Log("bleat");
        }
        base.Update();

        try { DoMyDebug(); }
        catch (Exception e) { Debug.LogException(e); }
    }

    public void Moving()
    {
        if (hasRegenerated && moving && Camping__Collider.Camping == false)
        {
            sheepStamina -= staminaDrain * Time.deltaTime;
        }
    }

    public void IsMoving()
    {
        if (agent.IsStopped())
        {
            moving = false;
        }
        else moving = true;
       // stress.Pause();
    }

    public void DetermineSpeed()
    {
        return; // sheep speed is managed elsewhere
        if (panicking) {
            agent.Speed(panicSpeed);
            return;
        }
        if (sheepStamina <= 0)
        {
            agent.Speed(slowedSpeed);
        }
        if (Slowed == true)
        {
            agent.Speed(slowedSpeed);
            Debug.Log("Slowed!");
        }
        else if (sheepStamina >= maxSheepStamina && hasRegenerated && Slowed == false)
        {
            agent.Speed(normalSpeed);
        }
    }
    public void Bleat()
    {
        if(bleatClass == 1)
        {
            FMODUnity.RuntimeManager.PlayOneShot("event:/Audio/SheepBleatLightVariant", transform.position);
        }
        if (bleatClass == 2)
        {
            FMODUnity.RuntimeManager.PlayOneShot("event:/Audio/SheepBleatMediumVariant", transform.position);
        }
        if (bleatClass == 3)
        {
            FMODUnity.RuntimeManager.PlayOneShot("event:/Audio/SheepBleatHeavyVariant", transform.position);
        }
        bleatTimer = Random.Range(0.0f, 30.0f);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Food")) {
            Eat_Sys eatSys = other.GetComponent<Eat_Sys>();
            if (eatSys != null) {
                touchingFoods.Add(eatSys);
            }
        }
        if (other.gameObject.CompareTag("Water")) {
            Water_Sys waterSys = other.GetComponent<Water_Sys>();
            if (waterSys != null) {
                touchingWaters.Add(waterSys);
            }
        }
        if (other.gameObject.CompareTag("Poison_Plant"))
        {
            Debug.Log("Poison plant detected - SHEEP HEALTH");
        }
        //Detects if the player enters the collider
        /*if (other.CompareTag("Snake"))
        {
                Debug.Log("Danger! Tsuchinoko!");

                //DISTRESS SOUND HERE
                //distress.start();

                //PARTICLE EFFECT HERE
                //stress.Play();
                //TEXTURE CHANGE HERE
        }
        if (other.CompareTag("Brambles"))
        {
            Debug.Log("Danger! Bush!");
            slowedTimer = 10;

            //DISTRESS SOUND HERE
            distress.start();

            //PARTICLE EFFECT HERE
            stress.Play();
            //TEXTURE CHANGE HERE
        }*/  
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Food")) {
            Eat_Sys eatSys = other.GetComponent<Eat_Sys>();
            if (eatSys != null) {
                touchingFoods.Remove(eatSys);
            }
        }
        if (other.gameObject.CompareTag("Water")) {
            Water_Sys waterSys = other.GetComponent<Water_Sys>();
            if (waterSys != null) {
                touchingWaters.Remove(waterSys);
            }
        }
    }

    private void OnTriggerStay(Collider other)
    {
       /* if (other.gameObject.CompareTag("Poison_Plant"))
        {
            Debug.Log("Sheep in radius");
        }*/
        return;
        is_drinking = false;
        is_eating = false;
        if (other.gameObject.CompareTag("Water"))
        {
            Water_Sys water_resource = other.GetComponent<Water_Sys>();
            if (water < npc_follow.WaterAtStart)
            {
              //  animator.Play("Eat");
                water += water_drinking_rate;
                water_resource.waterLeft -= water_drinking_rate;
                is_drinking = true;
                DrankWater?.Invoke(this, this);
            }
            Debug.Log(water_resource.waterLeft);
            if (water_resource.waterLeft <= 0)
            {
                gamecontroller.WaterList.Remove(other.gameObject);
                //Destroy(water_resource.gameObject);
                water_resource.waterLeft = 0;
               // agent.CallAgent(player.position);
                //water = 500;
            }
        }
        if (other.gameObject.CompareTag("Food"))
        {
           Eat_Sys food_resource = other.GetComponent<Eat_Sys>();
            if (food < npc_follow.FoodAtStart)
            {
               // animator.Play("Eat");
                food += food_eating_Rate;
                food_resource.foodLeft -= food_eating_Rate;
                is_eating = true;
                AteFood?.Invoke(this, this);
            }
            else
            {
                food = npc_follow.FoodAtStart;
            }
            //npc_follow.food += food_eating_Rate;
            Debug.Log(food_resource.foodLeft);
            if (food_resource.foodLeft <= 0)
            {
                gamecontroller.FoodList.Remove(other.gameObject);
                //Destroy(food_resource.gameObject);
                food_resource.foodLeft = 0;
               // agent.CallAgent(player.position);
                //food = 500;
            }
        }
    }

    private void OnDeactivateEasyMode(object sender, EventArgs args)
    {
        easy_mode = false;
    }

    // your ide should be able to collapse this region
    #region Debug

    public static string allSheepStateEventLog = "";
    public static string allSheepWanderlog = "";
    private string _sheepEventLog = "";
    private string _sheepStateEventLog = "";
    private const int _sheepEventLogLength = 10000;

    private void AddToLog(ref string log, string logString, bool is_state = false, int clamp_length = _sheepEventLogLength)
    {
        AddToMyLog(ref log, logString, is_state, clamp_length);
        if (is_state) {
            AddToMyLog(ref allSheepStateEventLog, $"{gameObject.name}: {logString}", false, clamp_length);
        }
    }
    
    private void AddToMyLog(ref string log, string logString, bool is_state = false, int clamp_length = _sheepEventLogLength)
    {
        log += logString + "\n";
        ClampStringStart(ref log, clamp_length);
        if (is_state) {
            AddToLog(ref _sheepStateEventLog, logString);
        }
    }

    private void ClampStringStart(ref string str, int target_length)
    {
        str = str.Substring(Mathf.Max(str.Length - target_length, 0), Mathf.Min(target_length, str.Length));
    }
    private void SetupDebug()
    {
        if (OnScreenDebug.IsDisabled()) { return;}
        SwitchedToFollowState += _Debug_OnSwitchedToFollowState;
        SwitchedToWanderStateImmediate += _Debug_OnSwitchedToWanderStateImmediate;
        SwitchedToWanderState += _Debug_OnSwitchedToWanderState;
        SwitchedToHungryState += _Debug_OnSwitchedToHungryState;
        SwitchedToThirstyState += _Debug_OnSwitchedToThirstyState;
        SwitchedToPanicState += _Debug_OnSwitchedToPanicState;
        SwitchedToCampingState += _Debug_OnSwitchedToCampingState;
        SwitchedToEatingState += _Debug_OnSwitchedToEatingState;
        SwitchedToDrinkingState += _Debug_OnSwitchedToDrinkingState;
        SwitchedToEnsnaredState += _Debug_OnSwitchedToEnsnaredState;
        SwitchedToDistractedState += _Debug_OnSwitchedToDistractedState;
        SwitchedToFrightenedState += _Debug_OnSwitchedToFrightenedState;
        SwitchedToStayState += _Debug_OnSwitchedToStayState;
        SwitchedToDeadState += _Debug_OnSwitchedToDeadState;
        SwitchedOutOfFollowState += _Debug_OnSwitchedOutOfFollowState;
        SwitchedOutOfWanderState += _Debug_OnSwitchedOutOfWanderState;
        SwitchedOutOfHungryState += _Debug_OnSwitchedOutOfHungryState;
        SwitchedOutOfThirstyState += _Debug_OnSwitchedOutOfThirstyState;
        SwitchedOutOfPanicState += _Debug_OnSwitchedOutOfPanicState;
        SwitchedOutOfCampingState += _Debug_OnSwitchedOutOfCampingState;
        SwitchedOutOfEatingState += _Debug_OnSwitchedOutOfEatingState;
        SwitchedOutOfDrinkingState += _Debug_OnSwitchedOutOfDrinkingState;
        SwitchedOutOfEnsnaredState += _Debug_OnSwitchedOutOfEnsnaredState;
        SwitchedOutOfDistractedState += _Debug_OnSwitchedOutOfDistractedState;
        SwitchedOutOfFrightenedState += _Debug_OnSwitchedOutOfFrightenedState;
        SwitchedOutOfStayState += _Debug_OnSwitchedOutOfStayState;
        SwitchedOutOfDeadState += _Debug_OnSwitchedOutOfDeadState;
        StaminaChanged += _Debug_OnStaminaChanged;
        FriendshipChangedToValue += _Debug_OnFriendshipChangedToValue;
        FriendshipIncreasedBy += _Debug_OnFriendshipIncreasedBy;
        FriendshipDecreasedBy += _Debug_OnFriendshipDecreasedBy;
        LoseSheep += _Debug_OnLoseSheep;
        AteFood += _Debug_OnAteFood;
        DrankWater += _Debug_OnDrankWater;
        DeathEvent += _Debug_OnDeathEvent;
        FoodChanged += _Debug_OnFoodChanged;
        WaterChanged += _Debug_OnWaterChanged;
    }

    private void TearDownDebug()
    {
        if (OnScreenDebug.IsDisabled()) { return;}
        SwitchedToFollowState -= _Debug_OnSwitchedToFollowState;
        SwitchedToWanderState -= _Debug_OnSwitchedToWanderState;
        SwitchedToHungryState -= _Debug_OnSwitchedToHungryState;
        SwitchedToThirstyState -= _Debug_OnSwitchedToThirstyState;
        SwitchedToPanicState -= _Debug_OnSwitchedToPanicState;
        SwitchedToCampingState -= _Debug_OnSwitchedToCampingState;
        SwitchedToEatingState -= _Debug_OnSwitchedToEatingState;
        SwitchedToDrinkingState -= _Debug_OnSwitchedToDrinkingState;
        SwitchedToEnsnaredState -= _Debug_OnSwitchedToEnsnaredState;
        SwitchedToDistractedState -= _Debug_OnSwitchedToDistractedState;
        SwitchedToFrightenedState -= _Debug_OnSwitchedToFrightenedState;
        SwitchedToStayState -= _Debug_OnSwitchedToStayState;
        SwitchedToDeadState -= _Debug_OnSwitchedToDeadState;
        SwitchedOutOfFollowState -= _Debug_OnSwitchedOutOfFollowState;
        SwitchedOutOfWanderState -= _Debug_OnSwitchedOutOfWanderState;
        SwitchedOutOfHungryState -= _Debug_OnSwitchedOutOfHungryState;
        SwitchedOutOfThirstyState -= _Debug_OnSwitchedOutOfThirstyState;
        SwitchedOutOfPanicState -= _Debug_OnSwitchedOutOfPanicState;
        SwitchedOutOfCampingState -= _Debug_OnSwitchedOutOfCampingState;
        SwitchedOutOfEatingState -= _Debug_OnSwitchedOutOfEatingState;
        SwitchedOutOfDrinkingState -= _Debug_OnSwitchedOutOfDrinkingState;
        SwitchedOutOfEnsnaredState -= _Debug_OnSwitchedOutOfEnsnaredState;
        SwitchedOutOfDistractedState -= _Debug_OnSwitchedOutOfDistractedState;
        SwitchedOutOfFrightenedState -= _Debug_OnSwitchedOutOfFrightenedState;
        SwitchedOutOfStayState -= _Debug_OnSwitchedOutOfStayState;
        SwitchedOutOfDeadState -= _Debug_OnSwitchedOutOfDeadState;
        StaminaChanged -= _Debug_OnStaminaChanged;
        FriendshipChangedToValue -= _Debug_OnFriendshipChangedToValue;
        FriendshipIncreasedBy -= _Debug_OnFriendshipIncreasedBy;
        FriendshipDecreasedBy -= _Debug_OnFriendshipDecreasedBy;
        LoseSheep -= _Debug_OnLoseSheep;
        AteFood -= _Debug_OnAteFood;
        DrankWater -= _Debug_OnDrankWater;
        DeathEvent -= _Debug_OnDeathEvent;
        FoodChanged -= _Debug_OnFoodChanged;
        WaterChanged -= _Debug_OnWaterChanged;
    }

    private void _Debug_OnSwitchedToFollowState(object sender, SheepHealth sheepHealth)
    { AddToLog(ref _sheepEventLog, "Switched To Follow State", true); }

    private void _Debug_OnSwitchedToWanderStateImmediate(object sender, SheepHealth sheepHealth)
    {
        AddToLog(ref _sheepEventLog, "Switched To Wander State (Internal)", true); 
        AddToMyLog(ref allSheepWanderlog, $"{gameObject.name}: Switched To Wander State (Internal)", false);
    }
    private void _Debug_OnSwitchedToWanderState(object sender, SheepHealth sheepHealth)
    {
        AddToLog(ref _sheepEventLog, "Switched To Wander State", true); 
        AddToMyLog(ref allSheepWanderlog, $"{gameObject.name}: Switched To Wander State", false);
    }
    private void _Debug_OnSwitchedToHungryState(object sender, SheepHealth sheepHealth)
    { AddToLog(ref _sheepEventLog, "Switched To Hungry State", true); }
    private void _Debug_OnSwitchedToThirstyState(object sender, SheepHealth sheepHealth)
    { AddToLog(ref _sheepEventLog, "Switched To Thirsty State", true); }
    private void _Debug_OnSwitchedToPanicState(object sender, SheepHealth sheepHealth)
    { AddToLog(ref _sheepEventLog, "Switched To Panic State", true); }
    private void _Debug_OnSwitchedToCampingState(object sender, SheepHealth sheepHealth)
    { AddToLog(ref _sheepEventLog, "Switched To Camping State", true); }
    private void _Debug_OnSwitchedToEatingState(object sender, SheepHealth sheepHealth)
    { AddToLog(ref _sheepEventLog, "Switched To Eating State", true); }
    private void _Debug_OnSwitchedToDrinkingState(object sender, SheepHealth sheepHealth)
    { AddToLog(ref _sheepEventLog, "Switched To Drinking State", true); }
    private void _Debug_OnSwitchedToEnsnaredState(object sender, SheepHealth sheepHealth)
    { AddToLog(ref _sheepEventLog, "Switched To Ensnared State", true); }
    private void _Debug_OnSwitchedToDistractedState(object sender, SheepHealth sheepHealth)
    { AddToLog(ref _sheepEventLog, "Switched To Distracted State", true); }
    private void _Debug_OnSwitchedToFrightenedState(object sender, SheepHealth sheepHealth)
    { AddToLog(ref _sheepEventLog, "Switched To Frightened State", true); }
    private void _Debug_OnSwitchedToStayState(object sender, SheepHealth sheepHealth)
    { AddToLog(ref _sheepEventLog, "Switched To Stay State", true); }
    private void _Debug_OnSwitchedToDeadState(object sender, SheepHealth sheepHealth)
    { AddToLog(ref _sheepEventLog, "Switched To Dead State", true); }
    
    private void _Debug_OnSwitchedOutOfFollowState(object sender, SheepHealth sheepHealth)
    { AddToLog(ref _sheepEventLog, "Switched Out Of Follow State", true); }
    private void _Debug_OnSwitchedOutOfWanderState(object sender, SheepHealth sheepHealth)
    { AddToLog(ref _sheepEventLog, "Switched Out Of Wander State", true); }
    private void _Debug_OnSwitchedOutOfHungryState(object sender, SheepHealth sheepHealth)
    { AddToLog(ref _sheepEventLog, "Switched Out Of Hungry State", true); }
    private void _Debug_OnSwitchedOutOfThirstyState(object sender, SheepHealth sheepHealth)
    { AddToLog(ref _sheepEventLog, "Switched Out Of Thirsty State", true); }
    private void _Debug_OnSwitchedOutOfPanicState(object sender, SheepHealth sheepHealth)
    { AddToLog(ref _sheepEventLog, "Switched Out Of Panic State", true); }
    private void _Debug_OnSwitchedOutOfCampingState(object sender, SheepHealth sheepHealth)
    { AddToLog(ref _sheepEventLog, "Switched Out Of Camping State", true); }
    private void _Debug_OnSwitchedOutOfEatingState(object sender, SheepHealth sheepHealth)
    { AddToLog(ref _sheepEventLog, "Switched Out Of Eating State", true); }
    private void _Debug_OnSwitchedOutOfDrinkingState(object sender, SheepHealth sheepHealth)
    { AddToLog(ref _sheepEventLog, "Switched Out Of Drinking State", true); }
    private void _Debug_OnSwitchedOutOfEnsnaredState(object sender, SheepHealth sheepHealth)
    { AddToLog(ref _sheepEventLog, "Switched Out Of Ensnared State", true); }
    private void _Debug_OnSwitchedOutOfDistractedState(object sender, SheepHealth sheepHealth)
    { AddToLog(ref _sheepEventLog, "Switched Out Of Distracted State", true); }
    private void _Debug_OnSwitchedOutOfFrightenedState(object sender, SheepHealth sheepHealth)
    { AddToLog(ref _sheepEventLog, "Switched Out Of Frightened State", true); }
    private void _Debug_OnSwitchedOutOfStayState(object sender, SheepHealth sheepHealth)
    { AddToLog(ref _sheepEventLog, "Switched Out Of Stay State", true); }
    private void _Debug_OnSwitchedOutOfDeadState(object sender, SheepHealth sheepHealth)
    { AddToLog(ref _sheepEventLog, "Switched Out Of Dead State", true); }
    
    private void _Debug_OnStaminaChanged(object sender, SheepHealth sheepHealth)
    { /*AddToLog(ref _sheepEventLog, "Stamina Changed");*/ }
    private void _Debug_OnFriendshipChangedToValue(object sender, (SheepHealth sheepHealth, float value) args)
    { AddToLog(ref _sheepEventLog, $"Friendship Changed To {args.value}"); }
    private void _Debug_OnFriendshipIncreasedBy(object sender, (SheepHealth sheepHealth, float value) args)
    { AddToLog(ref _sheepEventLog, $"Friendship Increased By {args.value}"); }
    private void _Debug_OnFriendshipDecreasedBy(object sender, (SheepHealth sheepHealth, float value) args)
    { AddToLog(ref _sheepEventLog, $"Friendship Decreased By {args.value}"); }
    private void _Debug_OnLoseSheep(object sender, SheepHealth sheepHealth)
    { AddToLog(ref _sheepEventLog, "Sheep Lost"); }
    private void _Debug_OnAteFood(object sender, SheepHealth sheepHealth)
    { AddToLog(ref _sheepEventLog, "Ate Food"); }
    private void _Debug_OnDrankWater(object sender, SheepHealth sheepHealth)
    { AddToLog(ref _sheepEventLog, "Drank Water"); }
    private void _Debug_OnDeathEvent(object sender, SheepHealth sheepHealth)
    { AddToLog(ref _sheepEventLog, "Death"); }
    private void _Debug_OnFoodChanged(object sender, float value)
    { AddToLog(ref _sheepEventLog, $"Food Changed to {value}"); }
    private void _Debug_OnWaterChanged(object sender, float value)
    { AddToLog(ref _sheepEventLog, $"Water Changed to {value}"); }

    private void DoMyDebug()
    {
        if (OnScreenDebug.IsDisabled()) { return;}
        OnScreenDebug.SetCategoryShortcut($"Sheep{mySheepRole}Events", _sheepEventLog);
        OnScreenDebug.SetCategoryShortcut($"Sheep{mySheepRole}StateEvents", _sheepStateEventLog);
    }
    
    #endregion
    // your ide should be able to collapse this region

}

