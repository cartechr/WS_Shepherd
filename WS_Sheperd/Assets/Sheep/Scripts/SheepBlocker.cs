using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SheepBlocker : MonoBehaviour
{
    public GameObject NoBacksies;


    public void Start()
    {
        NoBacksies.SetActive(false);
    }


    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Sheep"))
        {
            StartCoroutine(BlockerUp());
        }
    }


    public IEnumerator BlockerUp()
    {
        yield return new WaitForSeconds(8);
        NoBacksies.SetActive(true);
    }




}