using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SheepOutline : MonoBehaviour
{

    [SerializeField] private Material sheepOutlineMaterial;
    [SerializeField] private SkinnedMeshRenderer sheepRenderer;
    private SheepState_Wander myTrueWanderState;

    // this will change during gameplay
    [SerializeField] public float outlineDistance = 8f;

    private SheepHealth mySheepHealth;
    private Material[] originalMaterialArray;
    private Material[] altMaterialArray;

    private List<string> DontShowInStates = new List<string>
    {
        SheepState.FOLLOW_STATE_NAME,
        SheepState.CAMPING_STATE_NAME,
        SheepState.EATING_STATE_NAME,
        SheepState.DRINKING_STATE_NAME,
        SheepState.DEAD_STATE_NAME
    };
    
    // Start is called before the first frame update
    void Start()
    {
        mySheepHealth = GetComponent<SheepHealth>();
        myTrueWanderState = GetComponent<SheepState_Wander>();
        originalMaterialArray = sheepRenderer.materials;
        altMaterialArray = new Material[sheepRenderer.materials.Length + 1];
        for (int i = 0; i < sheepRenderer.materials.Length; i++) {
            altMaterialArray[i] = sheepRenderer.materials[i];
        }
        altMaterialArray[sheepRenderer.materials.Length] = sheepOutlineMaterial;
    }

    // Update is called once per frame
    void Update()
    {
        bool dist_req = Vector3.Distance(transform.position, mySheepHealth.player.transform.position) < outlineDistance;
        bool post_req = !DontShowInStates.Contains(mySheepHealth.myStateMachine.currentStateName) || 
                        mySheepHealth.playerScript.placing_post;
        bool wander_req = (mySheepHealth.myStateMachine.currentStateName != SheepState.WANDER_STATE_NAME) ||
                          (myTrueWanderState &&
                           myTrueWanderState.hasTelegraphedWandering);
        if (dist_req && post_req && wander_req) {
            sheepRenderer.materials = altMaterialArray;
        } else {
            sheepRenderer.materials = originalMaterialArray;
        }
    }
}
