using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Water_Sys : MonoBehaviour
{
    [SerializeField] private int waterhealth;

    public int waterLeft
    {
        get => waterhealth;
        set
        {
            waterhealth = value;
            GameController.Instance().savedWaterHealths[transform.position.GetHashCode()] = waterhealth;
        }
    }
    
    public void Start()
    {
        GameEvents.LoadFromSave += LoadFromGameController;
        GameController gc = GameController.Instance();
        gc.WaterList.Add(gameObject);
        LoadFromGameController(this, EventArgs.Empty);
    }

    public void OnDestroy()
    {
        GameEvents.LoadFromSave -= LoadFromGameController;
        GameController.Instance()?.WaterList.Remove(gameObject);
    }

    public void LoadFromGameController(object sender, EventArgs args)
    {
        GameController gc = GameController.Instance();
        int key = transform.position.GetHashCode();
        if (gc.savedWaterHealths.ContainsKey(key)) {
            waterhealth = gc.savedWaterHealths[key];
        } else {
            gc.savedWaterHealths[key] = waterhealth;
        }
    }
}