using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForceWanderTrigger : MonoBehaviour
{
    private bool activated = false;
    
    private void OnTriggerEnter(Collider other)
    {
        if (activated) {return;}
        if (other.CompareTag("Player")) {
            if (GameController.Instance().globalWanderingCauser.ForceAWander()) {
                activated = true;
                gameObject.SetActive(false);
                Debug.Log("Sheep should wander");
            }
        }
    }
}
