using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class MoodController : MonoBehaviour
{

    [NonSerialized] private SheepHealth mySheepHealth;
    [SerializeField] private MoodEffectController myMoodEffectController;
    private ParticleSystemRenderer myHoofprints;
    public Material normalPrint;
    public Material wanderPrint;
    private SheepOutline mySheepOutline;
    private SheepAniState mySheepAniState;
    private StateMachine mySheepStateMachine;
    public Animator animator;
    
    FMOD.Studio.EventInstance wanderingSound;
    FMOD.Studio.EventInstance friendSound;
    FMOD.Studio.EventInstance alertingSound;
    FMOD.Studio.EventInstance panickedSound;

    private bool sheepIsLost = false;

    private void Awake()
    {
        wanderingSound = FMODUnity.RuntimeManager.CreateInstance("event:/Audio/AlertWanderParent");
        friendSound = FMODUnity.RuntimeManager.CreateInstance("event:/Audio/AlertFollowParent");
        alertingSound = FMODUnity.RuntimeManager.CreateInstance("event:/Audio/AlertBasicParent");
        panickedSound = FMODUnity.RuntimeManager.CreateInstance("event:/Audio/AlertBasicParent");


    }

    // Start is called before the first frame update
    void Start()
    {
        if (!myMoodEffectController) {
            myMoodEffectController = this.transform.Find("Moods").gameObject.GetComponent<MoodEffectController>();
        }
        if (!myHoofprints) {
            myHoofprints = this.transform.Find("Hoofprints").gameObject.GetComponent<ParticleSystemRenderer>();
        }
        
        mySheepHealth = GetComponent<SheepHealth>();

        mySheepOutline = GetComponent<SheepOutline>();

        mySheepAniState = GetComponent<SheepAniState>();

        mySheepStateMachine = GetComponent<StateMachine>();

        animator = GetComponent<Animator>();

        mySheepHealth.SwitchedToWanderState += OnStartWander;
        mySheepHealth.SwitchedOutOfWanderState += OnStopWander;
        mySheepHealth.FriendshipChangedToValue += OnFriendshipChanged;
        mySheepHealth.FriendshipIncreasedBy += OnFriendshipIncreased;
        mySheepHealth.FriendshipDecreasedBy += OnFriendshipDecreased;
        mySheepHealth.SwitchedToFollowState += OnStartFollow;
        mySheepHealth.SwitchedOutOfFollowState += OnStopFollow;
        mySheepHealth.SwitchedToStayState += OnStartStay;
        mySheepHealth.SwitchedToHungryState += OnStartHungry;
        mySheepHealth.SwitchedOutOfHungryState += OnStopHungry;
        mySheepHealth.SwitchedToThirstyState += OnStartThirsty;
        mySheepHealth.SwitchedOutOfThirstyState += OnStopThirsty;
        mySheepHealth.SwitchedToEatingState += OnStartEatingState;
        mySheepHealth.SwitchedToDrinkingState += OnStartDrinkingState;
        mySheepHealth.SwitchedToCampingState += OnStartCamping;
        mySheepHealth.SwitchedOutOfCampingState += OnStopCamping;
        mySheepHealth.SwitchedToPanicState += OnStartPanic;
        mySheepHealth.SwitchedOutOfPanicState += OnStopPanic;
        mySheepHealth.SwitchedToFrightenedState += OnStartFrightened;
        mySheepHealth.SwitchedOutOfFrightenedState += OnStopFrightened;
        mySheepHealth.SwitchedToEnsnaredState += OnStartEnsnared;
        mySheepHealth.SwitchedOutOfEnsnaredState += OnStopEnsnared;
        mySheepHealth.SwitchedToDistractedState += OnStartDistracted;
        mySheepHealth.SwitchedOutOfDistractedState += OnStopDistracted;
        mySheepHealth.TookDamage += OnTakeDamage;
        mySheepHealth.SwitchedToDeadState += OnDie;
    }

    void OnStartWander(object sender, SheepHealth sheep)
    {
        LoseSheep();
        myMoodEffectController.StartAngerEffect();
        wanderingSound.start();
    }
    
    void OnStopWander(object sender, SheepHealth sheep)
    {
        if (Tutorial.TutorialDict["Wander"] == true)
        {
            tutorialGameEvents.End(this, EventArgs.Empty);
        }
    }
    
    // the float is the new value of the friendship stat (access w/ args.value)
    void OnFriendshipChanged(object sender, (SheepHealth sheepHealth, float value) args)
    {
        
    }
    
    // the float is how much the friendship increased by (access w/ args.value)
    void OnFriendshipIncreased(object sender, (SheepHealth sheepHealth, float value) args)
    {
        myMoodEffectController.TriggerLoveEffect();
    }
    
    // the float is how much the friendship decreased by (access w/ args.value)
    void OnFriendshipDecreased(object sender, (SheepHealth sheepHealth, float value) args)
    {
        
    }

    void OnStartFollow(object sender, SheepHealth sheep)
    {
        myMoodEffectController.EndAngerEffect();
        RecoverSheep();
    }

    void OnStopFollow(object sender, SheepHealth sheep)
    {

    }

    void OnStartStay(object sender, SheepHealth sheep)
    {
        
    }

    void OnStartHungry(object sender, SheepHealth sheep)
    {
        LoseSheep();
        myMoodEffectController.StartAngerEffect();
    }
    
    void OnStopHungry(object sender, SheepHealth sheep)
    {
        
    }
    
    void OnStartThirsty(object sender, SheepHealth sheep)
    {
        LoseSheep();
        myMoodEffectController.StartAngerEffect();
    }
    
    void OnStopThirsty(object sender, SheepHealth sheep)
    {
        
    }
    
    // eating state doesn't necessarily imply they're eating yet; see StartEating
    void OnStartEatingState(object sender, SheepHealth sheep)
    {
        
    }
    
    // eating state doesn't necessarily imply they're eating yet; see StartDrinking
    void OnStartDrinkingState(object sender, SheepHealth sheep)
    {
        
    }
    
    void OnStartCamping(object sender, SheepHealth sheep)
    {
        animator.SetBool("sitting", true);
    }
    void OnStopCamping(object sender, SheepHealth sheep)
    {
        animator.SetBool("sitting", false);
    }

    void OnStartPanic(object sender, SheepHealth sheep)
    {
        LoseSheep();
        myMoodEffectController.StartSweatEffect();
        panickedSound.start();
    }
    
    void OnStopPanic(object sender, SheepHealth sheep)
    {
        myMoodEffectController.EndSweatEffect();
    }
    
    void OnStartFrightened(object sender, SheepHealth sheep)
    {
        LoseSheep();
        myMoodEffectController.StartSweatEffect();
        panickedSound.start();
    }
    
    void OnStopFrightened(object sender, SheepHealth sheep)
    {
        myMoodEffectController.EndSweatEffect();
    }
    
    void OnStartEnsnared(object sender, SheepHealth sheep)
    {
        LoseSheep();
        myMoodEffectController.StartSweatEffect();
        alertingSound.start();
        if (mySheepOutline)
            mySheepOutline.outlineDistance = GetComponent<SheepState_Ensnared>().GetPlayerFreeDistance();

        tutorialGameEvents.Obstacles(this, EventArgs.Empty);
    }
    
    void OnStopEnsnared(object sender, SheepHealth sheep)
    {
        myMoodEffectController.EndSweatEffect();
        if (mySheepOutline)
            mySheepOutline.outlineDistance = mySheepHealth.playerScript.sheepCommandRadius;

        if (Tutorial.TutorialDict["Obstacles"] == true)
        {
            tutorialGameEvents.End(this, EventArgs.Empty);
        }
    }
    
    void OnStartDistracted(object sender, SheepHealth sheep)
    {
        LoseSheep();
        myMoodEffectController.StartCloudEffect();
        wanderingSound.start();
        if (mySheepOutline)
            mySheepOutline.outlineDistance = -1;

        tutorialGameEvents.Treat(this, EventArgs.Empty);
    }
    
    void OnStopDistracted(object sender, SheepHealth sheep)
    {
        myMoodEffectController.EndCloudEffect();
        if (mySheepOutline)
            mySheepOutline.outlineDistance = mySheepHealth.playerScript.sheepCommandRadius;

        if (Tutorial.TutorialDict["Sheep_Treat"] == true)
        {
            tutorialGameEvents.End(this, EventArgs.Empty);
        }
    }

    void OnTakeDamage(object sender, SheepHealth sheep)
    {
        alertingSound.start();
    }

    void OnDie(object sender, SheepHealth sheep)
    {
        // feel free to replace with something more appropriate
        // right now this just knocks the sheep 90 degrees sideways
        float[] dead_rots = new[] {-90f, 90f};
        Vector3 rot = transform.localEulerAngles; 
        rot.z = dead_rots[Random.Range(0, 1)];
        transform.localEulerAngles = rot;
        mySheepAniState.Stop();
    }

    public void FollowCommandResponse()
    {
        myMoodEffectController.TriggerStayEffect();
    }
    
    public void StayCommandResponse()
    {
        myMoodEffectController.TriggerStayEffect();
    }

    public void RecoverSheep()
    {
        if (sheepIsLost) {
            GameEvents.RemoveStaffMagic?.Invoke(this, EventArgs.Empty);
        }
        sheepIsLost = false;
        friendSound.start();
        myHoofprints.material = normalPrint;
    }

    public void LoseSheep()
    {
        // we don't want the wandering sound to trigger between two lost sheep states
        if (sheepIsLost) return;
        GameEvents.AddStaffMagic?.Invoke(this, EventArgs.Empty);
        mySheepHealth.LoseSheep?.Invoke(this, mySheepHealth);
        sheepIsLost = true;
       // wanderingSound.start();
       myHoofprints.material = wanderPrint;
    }

    public void StartEating()
    {
        myMoodEffectController.StartEatEffect();
    }

    public void StopEating()
    {
        myMoodEffectController.EndEatEffect();
    }
    
    public void StartDrinking()
    {
        myMoodEffectController.StartDrinkEffect();
    }

    public void StopDrinking()
    {
        myMoodEffectController.EndDrinkEffect();
    }

    public void StartAngry()
    {
        myMoodEffectController.StartAngerEffect();
    }

    public void StopAngry()
    {
        myMoodEffectController.EndAngerEffect();
    }
}
