using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SheepOverwrite : MonoBehaviour
{
    private void Awake()
    {
        //Upon Awaking, if there is an existing Player from DontDestroyOnLoad, delete existing Group of Sheep
        if (GameObject.FindGameObjectsWithTag(gameObject.tag).Length > 1)
        {
            Destroy(gameObject);
        }
        else
        {
            //Normal Awake Function:
        }
    }

}
