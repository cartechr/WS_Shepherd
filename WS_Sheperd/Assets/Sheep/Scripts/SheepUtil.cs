using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class SheepUtil
{
    
    // This pattern is so stupid but I love it so much
    public static bool AreAnySheepWandering()
    {
        bool result = false;
        void OnPositiveResponse(object sender, EventArgs args)
        {
            result = true;
        }
        GameEvents.CanASheepWanderAnswer += OnPositiveResponse;
        GameEvents.CanASheepWanderQuestion?.Invoke(null, EventArgs.Empty);
        GameEvents.CanASheepWanderAnswer -= OnPositiveResponse;
        return result;
    }
    
}
