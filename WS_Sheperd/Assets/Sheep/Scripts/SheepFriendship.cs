using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class SheepFriendship : MonoBehaviour
{

    [NonSerialized] private SheepHealth mySheepHealth;
    
    [SerializeField] private float _friendship = 50f;
    
    // these two may be moved to exclusively happen in camping
    // - sheep treat: increases minimum by +5 (and is a +5)
    // - shepherd song: +15
    
    // attacked by coyote: -15

    public float friendship
    {
        get
        {
            return _friendship;
        }
        set
        {
            _friendship = Mathf.Clamp(value, minimumFriendship, maximumFriendship);
            mySheepHealth.FriendshipChangedToValue?.Invoke(this, (mySheepHealth, _friendship));
        }
    }

    public float minimumFriendship = 0;
    public float maximumFriendship = 100;

    public float dwindle_interval = 1f;
    private float dwindle_timer;

    private void Awake()
    {
        mySheepHealth = GetComponent<SheepHealth>();
        GameEvents.AnySheepDeathEvent += OnAnySheepDeath;
    }

    private void Update()
    {
        if (mySheepHealth.stats_paused || mySheepHealth.easy_mode) {return;}
        
        dwindle_timer -= Time.deltaTime;
        if (dwindle_timer <= 0f) {
            ChangeFriendship(-1);
            dwindle_timer = dwindle_interval;
        }
    }

    void OnAnySheepDeath(object sender, EventArgs args)
    {
        ChangeFriendship(-20);
    }

    public void IncreaseFriendship(float amount){
        friendship += amount;
        if (amount > 0) {
            mySheepHealth.FriendshipIncreasedBy?.Invoke(this, (mySheepHealth, amount));
        }
        if (amount < 0) {
            mySheepHealth.FriendshipDecreasedBy?.Invoke(this, (mySheepHealth, -amount));
        }
    }
    
    public void DecreaseFriendship(float amount){
        friendship -= amount;
    }
    
    public void ChangeFriendship(float amount){
        IncreaseFriendship(amount);
    }

    public void SetFriendship(float amount)
    {
        friendship = amount;
    }

    public void PermanentBoost(float amount)
    {
        IncreaseFriendship(amount);
        minimumFriendship += amount;
    }

#if UNITY_EDITOR
    public void OnDrawGizmos()
    {
        Handles.Label(transform.position + Vector3.up * 2.5f, friendship.ToString());
    }
#endif
    
}
