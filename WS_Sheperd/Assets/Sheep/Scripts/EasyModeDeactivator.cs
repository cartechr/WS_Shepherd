using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EasyModeDeactivator : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player")) {
            GameEvents.DeactivateEasyMode?.Invoke(this, EventArgs.Empty);
        }
    }
}
