using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class SheepState_Wander_Loud : SheepState_Wander
{

    [SerializeField] private float soundInterval = 3f;
    private float soundTimer; 
    
    public override void RunState()
    {
        soundTimer -= Time.deltaTime;
        if (soundTimer <= 0) {
            // play sound here
            soundTimer = soundInterval;
        }
        base.RunState();
    }
}
