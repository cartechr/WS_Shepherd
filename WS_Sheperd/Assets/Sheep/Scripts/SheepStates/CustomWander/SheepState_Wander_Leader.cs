using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class SheepState_Wander_Leader : SheepState_Wander
{
    public override void EnterState()
    {
        foreach (var sheep in SheepHealth.GetSheepList()) {
            if (sheep != mySheepHealth) {
                sheep.GetComponent<SheepState_Follow>().target_transform = transform;
            }
        }
        base.EnterState();
    }

    public override void ExitState()
    {
        foreach (var sheep in SheepHealth.GetSheepList()) {
            if (sheep != mySheepHealth) {
                sheep.GetComponent<SheepState_Follow>().target_transform = mySheepHealth.player;
            }
        }
        base.ExitState();
    }
}
