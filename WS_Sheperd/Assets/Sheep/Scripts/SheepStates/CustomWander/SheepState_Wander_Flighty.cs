using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class SheepState_Wander_Flighty : SheepState_Panic
{
    
    [SerializeField] float timeUntilFreeze = 10f;
    
    protected override void Awake()
    {
        stateName = "Wander";
    }

    public override void EnterState()
    {
        StartCoroutine(FreezeAfterSeconds(timeUntilFreeze));
        NormalStateSetup();
        mySheepHealth.panicking = true;
        myAgent.Speed(mySheepHealth.panicSpeed);
        myAgent.SetAngularSpeed(panicAngularSpeed);
        myAgent.SetAcceleration(panicAcceleration);
        
        if (mySheepHealth.player) {
            panic_center = mySheepHealth.player.position;
        } else {
            panic_center = transform.position;
        }
        
        mySheepHealth.SwitchedToWanderState?.Invoke(this, mySheepHealth);
    }

    protected override void StateChangeBehaviour()
    {
        // pass - no state changes
    }
    
    public override void ExitState()
    {
        mySheepHealth.panicking = false;
        myAgent.Speed(mySheepHealth.normalSpeed);
        myAgent.ResetAngularSpeed();
        myAgent.ResetAcceleration();
        myAgent.SetStop(false);
        mySheepHealth.SwitchedOutOfWanderState?.Invoke(this, mySheepHealth);
    }

    IEnumerator FreezeAfterSeconds(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        if (is_active) {
            myAgent.SetStop(true);
        }
    }
    
    
}
