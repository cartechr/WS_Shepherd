using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;

public class SheepState_Wander_Ram : SheepState_Wander
{

    public bool should_back_up = true;
    public bool should_ram = true;
    [SerializeField] private float rammingSpeed = 16;
    [SerializeField] private float backUpDistance = 8;

    public override void EnterState()
    {
        should_ram = false;
        should_back_up = false;
        if (myStateMachine.previousState.stateName == SheepState.FOLLOW_STATE_NAME || 
            myStateMachine.previousState.stateName == SheepState.STAY_STATE_NAME) {
            should_ram = true;
            should_back_up = true;
        }
        myAgent.Speed(mySheepHealth.wanderSpeed);
        base.EnterState();
    }

    protected override void SelectAWander(int depth = 0)
    {
        if (should_back_up) {
            base.SelectAWander(depth);
        } else if (should_ram) {
            myAgent.CallAgent(mySheepHealth.player.position);
        } else {
            base.SelectAWander(depth);
        }
    }

    public override void RunState()
    {
        float distance = Vector3.Distance(mySheepHealth.player.position, transform.position);
        if (should_back_up) {
            myStateMachine.timeInState = 0; // rely on minimum wander time to guarantee a ram
            if (distance > backUpDistance) {
                should_back_up = false;
                myAgent.Speed(rammingSpeed);
                myAgent.SetAcceleration(100);
                myAgent.SetObstacleAvoidanceType(ObstacleAvoidanceType.NoObstacleAvoidance);
            }
        } else if (should_ram && distance < 1.0f) {
            mySheepHealth.playerScript.GetRammed();
            should_ram = false;
            myAgent.Speed(mySheepHealth.wanderSpeed);
            myAgent.ResetAcceleration();
            myAgent.ResetObstacleAvoidanceType();
        }
        base.RunState();
    }

    public override void ExitState()
    {
        myAgent.Speed(mySheepHealth.normalSpeed);
        myAgent.ResetAcceleration();
        myAgent.ResetObstacleAvoidanceType();
        should_back_up = true;
        should_ram = true;
        base.ExitState();
    }
}
