using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class SheepState_Wander_FollowSomethingElse : SheepState_AbstractTargeted
{

    [Tooltip("One of these is picked randomly to be followed.")]
    [SerializeField] private List<Transform> followCandidates;
    
    [Tooltip("This is how long this sheep is allowed to wander while preventing other sheep from getting bored and also wandering. After wandering for this amount of time, another sheep may also wander.")]
    [SerializeField] private float WanderingPreventionDuration = 180f;
    
    [Tooltip("Controls the easing for the chance to get hungry. Higher = steeper curve with lower odds when only slightly hungry.")]
    [SerializeField] private float HungryChanceExponent = 2f;
    [SerializeField] private float ThirstyChanceExponent = 2f;

    [SerializeField] private float minimumWanderTime = 4f;
    private Coroutine delayedTelegraphing;

    [SerializeField] private bool baadInfluence = false;
    
    private void Awake()
    {
        stateName = "Wander";
    }

    // Start is called before the first frame update
    protected override void Start()
    {
        target_transform = followCandidates[Random.Range(0, followCandidates.Count)];
        GameEvents.PlayerFollowCommandEvent += OnPlayerFollowCommand;
        GameEvents.CanASheepWanderQuestion += OnCanASheepWanderQuestion;
        base.Start();
    }
    
    private void OnDestroy()
    {
        GameEvents.PlayerFollowCommandEvent -= OnPlayerFollowCommand;
        GameEvents.CanASheepWanderQuestion -= OnCanASheepWanderQuestion;
    }
    
    public override void EnterState()
    {
        NormalStateSetup();
        myAgent.Speed(mySheepHealth.wanderSpeed);
        List<Transform> viableFollowCandidates = new List<Transform>();
        // don't follow dead sheep
        foreach (var candidate in followCandidates) {
            SheepHealth nullableSheephealth = candidate.GetComponent<SheepHealth>();
            if (nullableSheephealth){
                if (nullableSheephealth.currentHealth > 0) {
                    viableFollowCandidates.Add(candidate);
                }
            } else {
                // this code is used to follow non-sheep
                viableFollowCandidates.Add(candidate);
            }
        }

        if (viableFollowCandidates.Count > 0) {
            target_transform = viableFollowCandidates[Random.Range(0, viableFollowCandidates.Count)];
            if (baadInfluence) {
                SheepHealth nullableOtherSheep = target_transform.GetComponent<SheepHealth>();
                if (nullableOtherSheep) {
                    // if it's a sheep
                    // if it's a sheep that's not already lost
                    if (nullableOtherSheep.myStateMachine.currentStateName.Equals(SheepState.FOLLOW_STATE_NAME) ||
                        nullableOtherSheep.myStateMachine.currentStateName.Equals(SheepState.STAY_STATE_NAME)) {
                        // then become a bad influence >:)
                        nullableOtherSheep.myStateMachine.SetState("Wander");
                    }
                }
            }
        } else {
            // there are no other sheep but me
            target_transform = transform;
        }

        delayedTelegraphing = StartCoroutine(DelayedTelegraphing());
    }
    
    IEnumerator DelayedTelegraphing()
    {
        mySheepHealth.SwitchedToWanderStateImmediate?.Invoke(this, mySheepHealth);
        yield return new WaitForSeconds(minimumWanderTime);
        mySheepHealth.SwitchedToWanderState?.Invoke(this, mySheepHealth);
    }
    
    public override void ExitState()
    {
        StopCoroutine(delayedTelegraphing);
        mySheepHealth.SwitchedOutOfWanderState?.Invoke(this, mySheepHealth);
        myAgent.Speed(mySheepHealth.normalSpeed);
        base.ExitState();
    }
    
    protected override void StateChangeBehaviour()
    {
        StateChangeTimer -= Time.deltaTime;
        if (StateChangeTimer > 0) return;

        // bool only here to evaluate the things. the ORs make it stop at the first success
        bool r = myStateMachine.SetStateWithProbability("Thirsty", Mathf.Pow(CalculateDehydrationChance(), ThirstyChanceExponent)) || 
                 myStateMachine.SetStateWithProbability("Hungry", Mathf.Pow(CalculateStarvingChance(), HungryChanceExponent));
        
        StateChangeTimer = StateChangeTimerInterval;
    }

    protected override void OnPlayerFollowCommand(object sender, SphereArgs args)
    {
        if (myStateMachine.timeInState >= minimumWanderTime) {
            FollowCommandResponse(args.center, args.radius);
        }
    }

    void OnCanASheepWanderQuestion(object sender, EventArgs args)
    {
        if (is_active && myStateMachine.timeInState < WanderingPreventionDuration) {
            GameEvents.CanASheepWanderAnswer?.Invoke(this, EventArgs.Empty);
        }
    }
}
