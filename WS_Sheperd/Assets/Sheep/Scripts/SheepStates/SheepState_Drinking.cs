using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class SheepState_Drinking : SheepState_Thirsty
{
    
    Animator animator;
    private bool caused_by_player;
    [SerializeField] private float AntiSoftlockKickInTime = 25f;

    private void Awake()
    {
        stateName = "Drinking";
        disallowSwitchingToStates.Add("Hungry");
        disallowSwitchingToStates.Add("Thirsty");
        animator = GetComponent<Animator>();
    }
    
    protected  override void Start()
    {
        base.Start();
    }
    
    public override void EnterState()
    {
        NormalStateSetup();
        FindClosestWater();
        caused_by_player = true;
        myAgent.SetObstacleAvoidanceType(ObstacleAvoidanceType.NoObstacleAvoidance);
        if (myStateMachine.previousState.stateName == "Follow") {
            caused_by_player = true;
            //myMoodEffectController.TriggerLoveEffect();
            mySheepHealth.friendshipStats.ChangeFriendship(5);
        } else if (myStateMachine.previousState.stateName == "Thirsty") {
            caused_by_player = false;
            //myMoodEffectController.StartAngerEffect();
            myMoodController.StartAngry();
            mySheepHealth.friendshipStats.ChangeFriendship(-5);
        }
        mySheepHealth.SwitchedToDrinkingState?.Invoke(this, mySheepHealth);
    }
    
    public override void RunState()
    {
        if (!mySheepHealth.is_drinking)
            FollowBehaviour();
        StateChangeBehaviour();
        AntiSoftlockHack();
        DrinkBehaviour();
    }

    public override void ExitState()
    {
        //myMoodEffectController.EndDrinkEffect();
        myMoodController.StopDrinking();
        animator.SetBool("drinking", false);
        myAgent.ResetObstacleAvoidanceType();
        mySheepHealth.SwitchedOutOfDrinkingState?.Invoke(this, mySheepHealth);
    }

    protected override void StateChangeBehaviour()
    {
        StateChangeTimer -= Time.deltaTime;
        if (StateChangeTimer > 0) return;
        
        

        if (mySheepHealth.water >= mySheepHealth.WaterAtStart) {
            animator.SetBool("drinking", false);
            if (caused_by_player) {
                myStateMachine.SetState("Follow");
            } else {
                if (mySheepHealth.easy_mode) {
                    myStateMachine.SetState("Follow");
                } else {
                    myStateMachine.SetState("Wander");
                }
            }
        }
        
        StateChangeTimer = StateChangeTimerInterval;
    }
    
    void AntiSoftlockHack()
    {
        if (myStateMachine.timeInState >= AntiSoftlockKickInTime) {
            myAgent.SetStoppingDistance(0f);
        }
    }

    private void DrinkBehaviour()
    {
        if (!is_active) return;
        mySheepHealth.is_drinking = false;
        if (mySheepHealth.touchingWaters.Count > 0)
        {
            //myMoodEffectController.StartDrinkEffect();
            myMoodController.StartDrinking();
            Water_Sys water_resource = mySheepHealth.touchingWaters[0];
            if (mySheepHealth.water < mySheepHealth.WaterAtStart)
            {
                animator.SetBool("drinking", true);
                //  animator.Play("Eat");
                mySheepHealth.water += mySheepHealth.water_drinking_rate;
                water_resource.waterLeft -= mySheepHealth.water_drinking_rate;
                mySheepHealth.is_drinking = true;
                mySheepHealth.DrankWater?.Invoke(this, mySheepHealth);
            }
            //Debug.Log(water_resource.waterhealth);
            if (water_resource.waterLeft <= 0)
            {
                GameController.Instance().WaterList.Remove(water_resource.gameObject);
                //Destroy(water_resource.gameObject);
                water_resource.waterLeft = 0;
                // agent.CallAgent(player.position);
                //water = 500;
                mySheepHealth.touchingWaters.Remove(water_resource);
            }
        } else {
            //myMoodEffectController.EndDrinkEffect();
            myMoodController.StopDrinking();
            animator.SetBool("drinking", false);
        }
    }
    
    protected override void OnPlayerFollowCommand(object sender, SphereArgs args)
    {
        // pass
    }
}
