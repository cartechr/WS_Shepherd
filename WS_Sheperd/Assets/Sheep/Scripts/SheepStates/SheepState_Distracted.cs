using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SheepState_Distracted : SheepState_AbstractTargeted
{
    
    private void Awake()
    {
        stateName = "Distracted";
        disallowSwitchingToStates.Add("Camping");
    }
    
    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
    }

    public override void EnterState()
    {
        if (parameterObject) {
            target_transform = parameterObject.transform;
        }
        base.EnterState();
        mySheepHealth.SwitchedToDistractedState?.Invoke(this, mySheepHealth);
        //myMoodEffectController.StartCloudEffect();
    }
    
    public override void RunState()
    {
        FollowBehaviour();
        StateChangeBehaviour();
    }

    public override void ExitState()
    {
        mySheepHealth.SwitchedOutOfDistractedState?.Invoke(this, mySheepHealth);
        base.ExitState();
        //myMoodEffectController.EndCloudEffect();
    }

    protected override void StateChangeBehaviour()
    {
        StateChangeTimer -= Time.deltaTime;
        if (StateChangeTimer > 0) return;

        if (target_transform == null || !target_transform.gameObject.activeSelf) {
            myStateMachine.SetState("Follow");
            myMoodController.FollowCommandResponse();
            myMoodController.RecoverSheep();
        }
        
        StateChangeTimer = StateChangeTimerInterval;
    }
    
    protected override void OnPlayerFollowCommand(object sender, SphereArgs args)
    {
        return;
        // take treat instead now
        FollowCommandResponse(args.center, args.radius);
    }

}
