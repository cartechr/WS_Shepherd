using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SheepState_Hungry : SheepState_AbstractTargeted
{

    [SerializeField] private float maxFoodSearchDistance = 150f;
    private GameObject moodObject;
    
    private void Awake()
    {
        stateName = "Hungry";
        disallowSwitchingToStates.Add("Wander");
    }
    
    protected override void Start()
    {
        base.Start();
    }

    public override void EnterState()
    {
        NormalStateSetup();
        mySheepHealth.SwitchedToHungryState?.Invoke(this, mySheepHealth);
        // it would be easiest to put the food locating routine here
        FindClosestFood();
        FollowTimer = 0;
        StateChangeTimer = StateChangeTimerInterval;
        //myMoodEffectController.StartAngerEffect();
    }

    public override void RunState()
    {
        FollowBehaviour();
        StateChangeBehaviour();
    }

    public override void ExitState()
    {
        mySheepHealth.SwitchedOutOfHungryState?.Invoke(this, mySheepHealth);
        base.ExitState();
    }

    protected override void FollowBehaviour()
    {
        // make sure the food is still there
        FindClosestFood();
        base.FollowBehaviour();
    }

    override protected void StateChangeBehaviour()
    {
        StateChangeTimer -= Time.deltaTime;
        if (StateChangeTimer > 0) return;

        if (myAgent.HasReachedDestination() || mySheepHealth.is_eating) {
            myStateMachine.SetState("Eating");
        }
        
        StateChangeTimer = StateChangeTimerInterval;
    }
    
    protected override void OnPlayerFollowCommand(object sender, SphereArgs args)
    {
        FollowCommandResponse(args.center, args.radius);
    }

    protected void FindClosestFood()
    {
        float closest_distance = maxFoodSearchDistance;
        GameObject closest_food = this.gameObject;
        foreach (var foodObject in GameController.Instance().FoodList) {
            if (foodObject == null) {continue;}
            float dist = Vector3.Distance(foodObject.transform.position, transform.position);
            if (dist < closest_distance) {
                closest_distance = dist;
                closest_food = foodObject;
            }
        }
        // If the sheep fails to find food, ~~screw the player, imma go look for it~~ nvm, I guess I have to rely on the player
        if (closest_food == this.gameObject) {
            Debug.Log("Sheep " +gameObject+ " failed to find food");
            mySheepHealth.friendshipStats.DecreaseFriendship(0.5f);
            myStateMachine.SetStateDeferred("Follow"); // switched to follow since that makes more sense to me
        } else {
            target_transform = closest_food.transform;
        }
    }
    
}
