using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SheepState_Ensnared : SheepState
{
    
    [SerializeField, Tooltip("StateChangeTimerInterval - Controls how frequently the sheep checks to see when it should change states. CHANGING THIS WILL INVOLVE REBALANCING")]
    protected float StateChangeTimerInterval;
    protected float StateChangeTimer;
   


    [SerializeField] private float player_free_distance = 2f;
    public float GetPlayerFreeDistance() {return player_free_distance;}
    
    private void Awake()
    {
        stateName = "Ensnared";
        disallowSwitchingToStates.Add("Wander");
        disallowSwitchingToStates.Add("Hungry");
        disallowSwitchingToStates.Add("Thirsty");
        disallowSwitchingToStates.Add("Eating");
        disallowSwitchingToStates.Add("Drinking");
        disallowSwitchingToStates.Add("Camping");
        disallowSwitchingToStates.Add("Distracted");
        disallowSwitchingToStates.Add("Panic");
        disallowSwitchingToStates.Add("Frightened");
    }
    
    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
    }

    public override void EnterState()
    {
        myAgent.SetStop(true);
        myAgent.SetAcceleration(1000f);
        mySheepHealth.SwitchedToEnsnaredState?.Invoke(this, mySheepHealth);

        //myMoodEffectController.StartSweatEffect();
    }

    public override void RunState()
    {
        base.RunState();
    }

    protected override void OnPlayerFollowCommand(object sender, SphereArgs args)
    {
        FollowCommandResponse(args.center, player_free_distance);
        

    }
    
    public override void ExitState()
    {
        myAgent.SetStop(false);
        myAgent.ResetAcceleration();
        //myMoodEffectController.EndSweatEffect();
        mySheepHealth.friendshipStats.ChangeFriendship(15);
        mySheepHealth.SwitchedOutOfEnsnaredState?.Invoke(this, mySheepHealth);
    }
}
