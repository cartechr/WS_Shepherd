using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SheepState_AbstractTargeted : SheepState
{
    
    public Transform target_transform;
    [SerializeField, Tooltip("FollowTimerInterval - Controls how frequently the sheep updates its follow targeting")]
    protected float FollowTimerInterval;
    protected float FollowTimer;
    [SerializeField, Tooltip("StateChangeTimerInterval - Controls how frequently the sheep checks to see when it should change states. CHANGING THIS WILL INVOLVE REBALANCING")]
    protected float StateChangeTimerInterval;
    protected float StateChangeTimer;

    // debug variable
    private Vector3 last_dest;

    protected override void Start()
    {
        FollowTimer = FollowTimerInterval;
        StateChangeTimer = StateChangeTimerInterval;
        base.Start();
    }

    public override void EnterState()
    {
        NormalStateSetup();
        // we want it to immediately start following for telegraphing/clarity purposes
        FollowTimer = 0;
        StateChangeTimer = StateChangeTimerInterval;
    }

    public override void RunState()
    {
        FollowBehaviour();
        StateChangeBehaviour();
    }

    protected virtual void FollowBehaviour()
    {
        FollowTimer -= Time.deltaTime;
        if (FollowTimer > 0) return;
        
        Vector3 dest = transform.position;
        if (target_transform) {
            dest = target_transform.position;
        }
        myAgent.CallAgent(dest);
        last_dest = dest;
        FollowTimer = FollowTimerInterval;
    }

    protected virtual void StateChangeBehaviour()
    {
        StateChangeTimer -= Time.deltaTime;
        if (StateChangeTimer > 0) return;

        StateChangeTimer = StateChangeTimerInterval;
    }

    protected override void OnPlayerFollowCommand(object sender, SphereArgs args)
    {
        base.OnPlayerFollowCommand(sender, args);
    }

    protected override void OnPlayerStayCommand(object sender, SphereGameObjectArgs args)
    {
        base.OnPlayerStayCommand(sender, args);
    }

    private void OnDrawGizmos()
    {
        if (!is_active) return;
        Gizmos.color = Color.green;
        Gizmos.DrawWireCube(last_dest, Vector3.one);
    }
}
