using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SheepState_Follow : SheepState_AbstractTargeted
{

    //[SerializeField] private float WanderChance = 0.01f;
    [SerializeField] private float WanderThreshold = 0f;
    [Tooltip("Controls the easing for the chance to get hungry. Higher = steeper curve with lower odds when only slightly hungry.")]
    [SerializeField] private float HungryChanceExponent = 2f;
    [SerializeField] private float ThirstyChanceExponent = 2f;

    [Tooltip("If the sheep's food or water is under this percentage full, they will start eating or drinking when brought close to food or water by the player.")]
    [SerializeField] private float isFedUnderPercentThreshold = 0.8f;
    [Tooltip("How far a sheep looks for food to eat. How far away the food has to be from the sheep for it to factor into feeding")]
    [SerializeField] private float feedingRadius = 14f;

    [SerializeField] private float minimumFollowTime = 30f;

    private Transform player_transform;
    private Transform player_follow_transform;
    private Rigidbody player_rb;


    private void Awake()
    {
        stateName = "Follow";
    }
    
    protected override void Start()
    {
        player_transform = GameObject.FindWithTag("Player").transform;
        player_follow_transform = GameObject.FindWithTag("Player").transform.Find("FollowPoint");
        player_rb = player_transform.GetComponent<Rigidbody>();
        if (target_transform == null) {
            target_transform = player_follow_transform;
        }
        FollowTimer = FollowTimerInterval;
        StateChangeTimer = StateChangeTimerInterval;
        base.Start();
    }

    public override void EnterState()
    {
        NormalStateSetup();
        // we want it to immediately start following for telegraphing/clarity purposes
        FollowTimer = 0;
        StateChangeTimer = StateChangeTimerInterval;
        mySheepHealth.SwitchedToFollowState?.Invoke(this, mySheepHealth);
        //myMoodEffectController.EndAngerEffect();
        //myMoodController.StopAngry();
    }

    public override void RunState()
    {
        if (target_transform == player_transform || target_transform == player_follow_transform) {
            if (player_rb.velocity.magnitude > 0.1f) {
                target_transform = player_follow_transform;
            } else {
                target_transform = player_transform;
            }
        }
        FollowBehaviour();
        StateChangeBehaviour();
    }

    public override void ExitState()
    {
        mySheepHealth.SwitchedOutOfFollowState?.Invoke(this, mySheepHealth);
        base.ExitState();
    }

    protected override void StateChangeBehaviour()
    {
        StateChangeTimer -= Time.deltaTime;
        if (StateChangeTimer > 0) return;

        // r is only here to evaluate the things. the ORs make it stop at the first success
        bool r = TryFindClosestWater() ||
                 TryFindClosestFood() ||
                 mySheepHealth.easy_mode || // dont starve/dehydrate if easy mode
                 myStateMachine.timeInState < minimumFollowTime ||
                 myStateMachine.SetStateWithProbability("Thirsty", Mathf.Pow(CalculateDehydrationChance(), ThirstyChanceExponent)) ||
                 myStateMachine.SetStateWithProbability("Hungry", Mathf.Pow(CalculateStarvingChance(), HungryChanceExponent)) ||
                 SheepUtil.AreAnySheepWandering() ||
                 !(mySheepHealth.friendshipStats.friendship < WanderThreshold) //||
                 /*myStateMachine.SetStateWithProbability("Wander", 1f)*/;
                 //myStateMachine.SetStateWithProbability("Wander", WanderChance);

        StateChangeTimer = StateChangeTimerInterval;
    }
    
    bool TryFindClosestFood()
    {
        if (CalculateFoodPercentage() >= isFedUnderPercentThreshold) {return false;}
        float closest_distance = feedingRadius;
        GameObject closest_food = this.gameObject;
        foreach (var foodObject in GameController.Instance().FoodList) {
            if (foodObject == null) {continue;}
            float dist = Vector3.Distance(foodObject.transform.position, transform.position);
            if (dist < closest_distance) {
                closest_distance = dist;
                closest_food = foodObject;
            }
        }
        // If the sheep fails to find food, then keep following
        if (closest_food == this.gameObject) {
            return false;
        } else {
            myStateMachine.SetState("Eating");
            return true;
        }
    }
    
    bool TryFindClosestWater()
    {
        if (CalculateWaterPercentage() >= isFedUnderPercentThreshold) {return false;}
        float closest_distance = feedingRadius;
        GameObject closest_water = this.gameObject;
        foreach (var waterObject in GameController.Instance().WaterList) {
            if (waterObject == null) {continue;}
            float dist = Vector3.Distance(waterObject.transform.position, transform.position);
            if (dist < closest_distance) {
                closest_distance = dist;
                closest_water = waterObject;
            }
        }
        // If the sheep fails to find food, then keep following
        if (closest_water == this.gameObject) {
            return false;
        } else {
            myStateMachine.SetState("Drinking");
            return true;
        }
    }

    protected override void OnPlayerStayCommand(object sender, SphereGameObjectArgs args)
    {
        StayCommandResponse(args.center, args.radius, args.gameObject);
    }

    public void FollowPlayer()
    {
        target_transform = player_transform;
    }
    
    public void FollowInFrontOfPlayer()
    {
        target_transform = player_follow_transform;
    }

    public bool CanWander()
    {
        return (mySheepHealth.friendshipStats.friendship < WanderThreshold) && !mySheepHealth.easy_mode;
    }

}
