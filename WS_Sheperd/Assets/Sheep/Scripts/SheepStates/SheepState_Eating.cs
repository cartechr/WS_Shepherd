using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class SheepState_Eating : SheepState_Hungry
{
    public SheepAniState sas;
    Animator animator;
    private bool caused_by_player;
    [SerializeField] private float AntiSoftlockKickInTime = 25f;

    private void Awake()
    {
        stateName = "Eating";
        disallowSwitchingToStates.Add("Hungry");
        disallowSwitchingToStates.Add("Thirsty");
        animator = GetComponent<Animator>();
    }
    
    protected  override void Start()
    {
        base.Start();
    }
    
    public override void EnterState()
    {
        NormalStateSetup();
        FindClosestFood();
        caused_by_player = true;
        myAgent.SetObstacleAvoidanceType(ObstacleAvoidanceType.NoObstacleAvoidance);
        if (myStateMachine.previousState.stateName == "Follow") {
            caused_by_player = true;

            //myMoodEffectController.TriggerLoveEffect();
            mySheepHealth.friendshipStats.ChangeFriendship(5);
        } else if (myStateMachine.previousState.stateName == "Hungry") {
            caused_by_player = false;
            //myMoodEffectController.StartAngerEffect();
            myMoodController.StartAngry();
            mySheepHealth.friendshipStats.ChangeFriendship(-5);
        }
        mySheepHealth.SwitchedToEatingState?.Invoke(this, mySheepHealth);
    }
    
    public override void RunState()
    {
        if (!mySheepHealth.is_eating)
            FollowBehaviour();
        StateChangeBehaviour();
        AntiSoftlockHack();
        EatBehaviour();
    }

    public override void ExitState()
    {
        //myMoodEffectController.EndEatEffect();
        myMoodController.StopEating();
        myAgent.SetStop(false);
        animator.SetBool("eating", false);

        //trying to get sheep to eat then return to normal walking animation
        //sas.SheepMoveAni();
        myAgent.ResetObstacleAvoidanceType();
        mySheepHealth.SwitchedOutOfEatingState?.Invoke(this, mySheepHealth);
    }

    protected override void StateChangeBehaviour()
    {
        StateChangeTimer -= Time.deltaTime;
        if (StateChangeTimer > 0) return;
        
        if (animator.GetCurrentAnimatorStateInfo(0).IsName("Eat")) return;
        
        if (mySheepHealth.food >= mySheepHealth.FoodAtStart) {
            animator.SetBool("eating", false);
            myMoodController.StopEating();
            myAgent.SetStop(false);
            if (caused_by_player) {
                myStateMachine.SetState("Follow");
            } else {
                myStateMachine.SetState("Wander");
            }
        }
        
        StateChangeTimer = StateChangeTimerInterval;
    }

    void AntiSoftlockHack()
    {
        if (myStateMachine.timeInState >= AntiSoftlockKickInTime) {
            myAgent.SetStoppingDistance(0f);
        }
    }

    private void EatBehaviour()
    {
        if (!is_active) return;
        mySheepHealth.is_eating = false;
        if (mySheepHealth.touchingFoods.Count > 0)
        {
            //myMoodEffectController.StartEatEffect();
            myMoodController.StartEating();
            myAgent.SetStop(true);
            
            //StartCoroutine(ChangeAni());
            Eat_Sys food_resource = mySheepHealth.touchingFoods[0];
            if (mySheepHealth.food < mySheepHealth.FoodAtStart)
            {
                animator.SetBool("eating", true);
                mySheepHealth.food += mySheepHealth.food_eating_Rate;
                food_resource.foodLeft -= mySheepHealth.food_eating_Rate;
                mySheepHealth.is_eating = true;
                mySheepHealth.AteFood?.Invoke(this, mySheepHealth);
            }
            //Debug.Log(water_resource.waterhealth);
            if (food_resource.foodLeft <= 0)
            {
                GameController.Instance().FoodList.Remove(food_resource.gameObject);
                //Destroy(water_resource.gameObject);
                food_resource.foodLeft = 0;
                // agent.CallAgent(player.position);
                //water = 500;
                mySheepHealth.touchingFoods.Remove(food_resource);
            }
        } else {
            //myMoodEffectController.EndEatEffect();
            myMoodController.StopEating();
            myAgent.SetStop(false);
            animator.SetBool("eating", false);
        }
    }

    protected override void OnPlayerFollowCommand(object sender, SphereArgs args)
    {
        // pass
    }

    IEnumerator ChangeAni()
    {
        yield return new WaitForSeconds(5);
        Debug.Log("Eating anim should end");
        sas.EnterMoveAni();
    }

}
