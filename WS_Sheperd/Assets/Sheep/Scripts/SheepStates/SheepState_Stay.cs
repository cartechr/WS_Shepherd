using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SheepState_Stay : SheepState_AbstractTargeted
{
    
    [SerializeField] private float WanderChance = 0.01f;
    [Tooltip("Controls the easing for the chance to get hungry. Higher = steeper curve with lower odds when only slightly hungry.")]
    [SerializeField] private float HungryChanceExponent = 2f;
    [SerializeField] private float ThirstyChanceExponent = 2f;
    
    [SerializeField] private float minimumStayTime = 30f;
    [SerializeField] private float badStandingFriendshipDecayChance = 0.07f;
    [SerializeField] private float goodStandingFriendshipDecayChance = 0.07f;
    [SerializeField] private bool canBreakThePost = true;
    [SerializeField] private int friendshipPointsLostBeforeBreaking = 24;

    private int friendship_points_lost = 0;
    [SerializeField] private int okayWithBeingTiedToPostFriendshipThreshold = 50;
    private bool okay_with_being_tied_to_post = true;

    
    private void Awake()
    {
        stateName = "Stay";
    }
    
    protected override void Start()
    {
        base.Start();
    }

    public override bool CanEnterState()
    {
        return mySheepHealth.friendshipStats.friendship >= friendshipPointsLostBeforeBreaking;
    }

    // the player is close to the sheep
    public override void EnterState()
    {
        FollowTimer = 0;
        target_transform = parameterObject.transform;
        StateChangeTimer = StateChangeTimerInterval;
        friendship_points_lost = 0;
        okay_with_being_tied_to_post =
            mySheepHealth.friendshipStats.friendship >= okayWithBeingTiedToPostFriendshipThreshold;
        mySheepHealth.SwitchedToStayState?.Invoke(this, mySheepHealth);
    }

    public override void RunState()
    {
        FollowBehaviour();
        StateChangeBehaviour();
    }
    
    public override void ExitState()
    {
        mySheepHealth.SwitchedOutOfStayState?.Invoke(this, mySheepHealth);
    }

    protected override void FollowBehaviour()
    {
        if (target_transform) {
            base.FollowBehaviour();
        }
    }

    override protected void StateChangeBehaviour()
    {
        StateChangeTimer -= Time.deltaTime;
        if (StateChangeTimer > 0) return;

        float friendshipDecayChance = okay_with_being_tied_to_post
            ? goodStandingFriendshipDecayChance
            : badStandingFriendshipDecayChance;
        if (Random.value < friendshipDecayChance && target_transform != null) {
            mySheepHealth.friendshipStats.ChangeFriendship(-3);
            friendship_points_lost += 3;
        }

        // r is only here to evaluate the things. the ORs make it stop at the first success
        bool r = (friendship_points_lost >= friendshipPointsLostBeforeBreaking) ? BreakThePost() : false ||
                 target_transform == null ? myStateMachine.SetStateWithProbability("Follow", 1.0f) : false ||
                 myStateMachine.timeInState < minimumStayTime ||
                 myStateMachine.SetStateWithProbability("Thirsty", Mathf.Pow(CalculateDehydrationChance(), ThirstyChanceExponent)) ||
                 myStateMachine.SetStateWithProbability("Hungry", Mathf.Pow(CalculateStarvingChance(), HungryChanceExponent));
                 //SheepUtil.AreAnySheepWandering()
                 //myStateMachine.SetStateWithProbability("Wander", WanderChance);

        
        StateChangeTimer = StateChangeTimerInterval;
    }

    bool BreakThePost()
    {
        if (canBreakThePost) {
            // might need to change this later
            Destroy(target_transform.gameObject);
            myStateMachine.SetState("Wander");
            Debug.Log("POST BROKEN!!");
            return true;
        }
        return false;
    }
    
    protected override void OnPlayerFollowCommand(object sender, SphereArgs args)
    {
        FollowCommandResponse(args.center, args.radius);
    }
}
