using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;

public class SheepState_Panic : SheepState
{

    [SerializeField] private float PanicTimerInterval;
    private float PanicTimer;
    [SerializeField] private float PanicDistance;
    [SerializeField] private float FailPanicRange;
    [SerializeField] private float NormalTurnRange;
    [SerializeField] private float CornerTurnRange;

    [SerializeField, Tooltip("StateChangeTimerInterval - Controls how frequently the sheep checks to see when it should change states. CHANGING THIS WILL INVOLVE REBALANCING")]
    protected float StateChangeTimerInterval;
    protected float StateChangeTimer;
    
    [SerializeField] protected float panicAngularSpeed = 200f;
    [SerializeField] protected float panicAcceleration = 30f;
    [SerializeField] private float minimumPanicTime = 5f;

    protected Vector3 panic_center;
    private float panic_radius = 15f;

    // debug variables
    private Vector3 last_dest;
    private bool did_turn_around;
    private bool failed_to_wander = false;

    protected virtual void Awake()
    {
        stateName = "Panic";
        disallowSwitchingToStates.Add("Eating");
        disallowSwitchingToStates.Add("Drinking");
        disallowSwitchingToStates.Add("Camping");
        disallowSwitchingToStates.Add("Distracted");
    }

    protected override void Start()
    {
        PanicTimer = PanicTimerInterval;
        base.Start();
    }

    public override void EnterState()
    {
        NormalStateSetup();
        mySheepHealth.panicking = true;
        myAgent.Speed(mySheepHealth.panicSpeed);
        myAgent.SetAngularSpeed(panicAngularSpeed);
        myAgent.SetAcceleration(panicAcceleration);
        
        if (mySheepHealth.player) {
            panic_center = mySheepHealth.player.position;
        } else {
            panic_center = transform.position;
        }
        
        mySheepHealth.SwitchedToPanicState?.Invoke(this, mySheepHealth);
        //myMoodEffectController.StartSweatEffect();
    }

    public override void RunState()
    {
        PanicBehaviour();
        StateChangeBehaviour();
    }

    public override void ExitState()
    {
        mySheepHealth.panicking = false;
        myAgent.Speed(mySheepHealth.normalSpeed);
        myAgent.ResetAngularSpeed();
        myAgent.ResetAcceleration();
        //myMoodEffectController.EndSweatEffect();
        mySheepHealth.SwitchedOutOfPanicState?.Invoke(this, mySheepHealth);
    }

    protected virtual void PanicBehaviour()
    {
        PanicTimer -= Time.deltaTime;
        if (PanicTimer <= 0) {
            SelectAPanic(0);
            PanicTimer = PanicTimerInterval;
        }
    }

    protected virtual void StateChangeBehaviour()
    {
        StateChangeTimer -= Time.deltaTime;
        if (StateChangeTimer > 0) return;

        if (myStateMachine.timeInState > minimumPanicTime) {
            myStateMachine.SetStateWithProbability("Wander", 0.4f);
        }

        StateChangeTimer = StateChangeTimerInterval;
    }

    void SelectAPanic(int depth = 0)
    {
        if (depth > 5) {
            failed_to_wander = true;
            return;
        }
        failed_to_wander = false;

        Vector3 direction = transform.forward;
        float turn_range = NormalTurnRange;
        if (depth > 2) {
            turn_range = CornerTurnRange;
        }
        direction = Quaternion.Euler(0, Random.Range(-turn_range, turn_range), 0) * direction;
        Vector3 new_destination = transform.position + PanicDistance * (Vector3) direction;
        if (Vector3.Distance(panic_center, new_destination) > panic_radius) {
            SelectAPanic(depth + 1);
            return;
        }
        NavMeshHit hit;
        if (NavMesh.SamplePosition(new_destination, out hit, FailPanicRange, 1)) {
            myAgent.SetStop(false);
            if (myAgent.CallIfReachable(hit.position)) {
                did_turn_around = (depth >= 2);
                last_dest = new_destination;    
            } else {
                SelectAPanic(Mathf.Max(2, depth + 1)); // skip to CornerTurnRange if we're not there yet
            }
        } else {
            SelectAPanic(depth + 1);
        }
    }

    protected override void OnPlayerFollowCommand(object sender, SphereArgs args)
    {
        FollowCommandResponse(args.center, args.radius);
    }

    private void OnDrawGizmos()
    {
        if (!is_active) return;
        Gizmos.color = (did_turn_around) ? Color.magenta : Color.yellow;
        if (failed_to_wander) {Gizmos.color = Color.red;}
        Gizmos.DrawWireCube(last_dest, Vector3.one);
    }
}
