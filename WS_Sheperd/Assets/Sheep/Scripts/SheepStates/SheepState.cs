using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SheepState : StateMachineState
{
    // these might be unnecessary but I added them anyway
    public static string FOLLOW_STATE_NAME = "Follow";
    public static string WANDER_STATE_NAME = "Wander";
    public static string HUNGRY_STATE_NAME = "Hungry";
    public static string THIRSTY_STATE_NAME = "Thirsty";
    public static string EATING_STATE_NAME = "Eating";
    public static string DRINKING_STATE_NAME = "Drinking";
    public static string CAMPING_STATE_NAME = "Camping";
    public static string DISTRACTED_STATE_NAME = "Distracted";
    public static string ENSNARED_STATE_NAME = "Ensnared";
    public static string PANIC_STATE_NAME = "Panic";
    public static string FRIGHTENED_STATE_NAME = "Frightened";
    public static string STAY_STATE_NAME = "Stay";
    public static string DEAD_STATE_NAME = "Dead";


    protected Agent myAgent;
    protected SheepHealth mySheepHealth;
    protected GameObject myMoodObject;
    //protected MoodEffectController myMoodEffectController;
    protected MoodController myMoodController;

   // protected FMOD.Studio.EventInstance wanderingSound;
   // protected FMOD.Studio.EventInstance friendSound;
   // protected FMOD.Studio.EventInstance alertingSound;
   // protected FMOD.Studio.EventInstance panickedSound;
    
    [Tooltip("Set to NaN to use default value on NavMeshAgent")]
    [SerializeField] private float newStoppingDistance = float.NaN;
    
    // Start is called before the first frame update
    protected override void Start()
    {
        myAgent = GetComponent<Agent>();
        mySheepHealth = GetComponent<SheepHealth>();

       // wanderingSound = FMODUnity.RuntimeManager.CreateInstance("event:/Audio/Alerts/Alert_Wander_Parent");
        //friendSound = FMODUnity.RuntimeManager.CreateInstance("event:/Audio/Alerts/Alert_Follow_Parent");


        //myMoodObject = this.transform.Find("Moods").gameObject;
        //myMoodController = myMoodObject.GetComponent<MoodController>();
        //myMoodEffectController = myMoodObject.GetComponent<MoodEffectController>();
        myMoodController = GetComponent<MoodController>();
        
        GameEvents.PlayerFollowCommandEvent += _OnPlayerFollowCommand;
        GameEvents.PlayerStayCommandEvent += _OnPlayerStayCommand;
        GameEvents.EnteredCamp += OnEnteredCamp;
        GameEvents.ExitedCamp += OnExitedCamp;
        GameEvents.AnySheepAttackedEvent += OnAnySheepAttacked;
        GameEvents.AnySheepDeathEvent += OnAnySheepDeath;
        base.Start();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    protected void NormalStateSetup()
    {
        myAgent.ResetStoppingDistance();
        if (!float.IsNaN(newStoppingDistance)) {
            myAgent.SetStoppingDistance(newStoppingDistance);
        }
    }
    
    // layer of abstraction just so this can be overridden, since only the base function is actually subscribed
    private void _OnPlayerFollowCommand(object sender, SphereArgs args)
    {
        OnPlayerFollowCommand(sender, args);
    }
    
    protected virtual void OnPlayerFollowCommand(object sender, SphereArgs args)
    {
        // pass
    }

    protected void FollowCommandResponse(Vector3 player_pos, float radius)
    {
        if (!is_active) return;
        if (Vector3.Distance(transform.position, player_pos) <= radius) {
            if (myStateMachine.previousState.stateName == "Wander") {
                mySheepHealth.promise = SheepHealth.PROMISE_CAMPING;
            }
            else if (myStateMachine.previousState.stateName == "Hungry") {
                mySheepHealth.promise = SheepHealth.PROMISE_FOOD;
            }
            else if (myStateMachine.previousState.stateName == "Thirsty") {
                mySheepHealth.promise = SheepHealth.PROMISE_WATER;
            }
            myStateMachine.SetState("Follow");
            //myMoodEffectController.TriggerLoveEffect();
            myMoodController.FollowCommandResponse();
            if (myStateMachine.previousState.stateName != "Stay") {
                myMoodController.RecoverSheep();
            }
        }
    }

    // layer of abstraction just so this can be overridden, since only the base function is actually subscribed
    private void _OnPlayerStayCommand(object sender, SphereGameObjectArgs args)
    {
        OnPlayerStayCommand(sender, args);
    }

    protected virtual void OnPlayerStayCommand(object sender, SphereGameObjectArgs args)
    {
        // pass
    }

    protected void StayCommandResponse(Vector3 player_pos, float radius, GameObject post)
    {
        if (!is_active) return;
        if (Vector3.Distance(transform.position, player_pos) <= radius) {
            if (myStateMachine.CanEnterState("Stay")) {
                myStateMachine.SetStateWithObject("Stay", post);
                //myMoodEffectController.TriggerStayEffect();
                myMoodController.StayCommandResponse();
                // initial cost for tying the sheep to a post (they don't like it very much)
                //mySheepHealth.friendshipStats.ChangeFriendship(-10);            
            } else {
                // sheep refused to stay
            }
        }
    }

    public void OnEnteredCamp(object sender, EventArgs args)
    {
        if (!is_active || stateName == "Camping") return;
        myStateMachine.SetState("Camping");
    }
    
    public void OnExitedCamp(object sender, EventArgs args)
    {
        if (!is_active) return;
        myStateMachine.SetState("Follow");
    }

    public void OnAnySheepAttacked(object sender, EventArgs args)
    {
        if (!is_active) return;
        myStateMachine.SetState("Panic");
    }
    
    public void OnAnySheepDeath(object sender, EventArgs args)
    {
        if (!is_active) return;
        // pass?
    }

    public float CalculateStarvingChance()
    {
        return Mathf.Clamp01(1f - ((float)mySheepHealth.food - (float)mySheepHealth.WhenTheyStarve) /
            ((float)mySheepHealth.WhenTheyGetHungry - (float)mySheepHealth.WhenTheyStarve));
    }
    
    public float CalculateDehydrationChance()
    {
        return Mathf.Clamp01(1f - ((float)mySheepHealth.water - (float)mySheepHealth.WhenTheyDehydrate) /
            ((float)mySheepHealth.WhenTheyGetThirsty - (float)mySheepHealth.WhenTheyDehydrate));
    }

    public float CalculateFoodPercentage()
    {
        return (float)mySheepHealth.food / mySheepHealth.FoodAtStart;
    }
    
    public float CalculateWaterPercentage()
    {
        return (float)mySheepHealth.water / mySheepHealth.WaterAtStart;
    }
}
