using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;

public class SheepState_Frightened : SheepState
{
    
    [SerializeField] private float FrightenedTimerInterval = 0.5f;
    private float FrightenedTimer;
    
    [SerializeField] protected float StateChangeTimerInterval = 1f;
    protected float StateChangeTimer;

    [SerializeField] private float RunAwayDistance = 10f;
    [SerializeField] private float FailRunAwayRange = 5f;

    [SerializeField] private float scaredAngularSpeed = 200f;
    [SerializeField] private float scaredAcceleration = 30f;
    
    [SerializeField] private float stateChangeChance = 0.5f;
    
    private Vector3 last_dest;
    private bool is_cornered;
    
    private void Awake()
    {
        stateName = "Frightened";
        //disallowSwitchingToStates.Add("Eating");
        //disallowSwitchingToStates.Add("Drinking");
        //disallowSwitchingToStates.Add("Camping");
        //disallowSwitchingToStates.Add("Distracted");
    }
    
    // Start is called before the first frame update
    protected override void Start()
    {
        FrightenedTimer = FrightenedTimerInterval;
        StateChangeTimer = StateChangeTimerInterval;
        base.Start();
    }

    public override void EnterState()
    {
        NormalStateSetup();
        myAgent.SetAngularSpeed(scaredAngularSpeed);
        myAgent.SetAcceleration(scaredAcceleration);
        mySheepHealth.SwitchedToFrightenedState?.Invoke(this, mySheepHealth);
        // myMoodObject.StartScaredEffect();
    }
    
    public override void RunState()
    {
        FrightenedBehaviour();
        StateChangeBehaviour();
    }

    public override void ExitState()
    {
        myAgent.ResetAngularSpeed();
        myAgent.ResetAcceleration();
        mySheepHealth.SwitchedOutOfFrightenedState?.Invoke(this, mySheepHealth);
    }

    protected virtual void FrightenedBehaviour()
    {
        FrightenedTimer -= Time.deltaTime;
        if (FrightenedTimer <= 0) {
            RunAway();
            FrightenedTimer = FrightenedTimerInterval;
        }
    }

    protected virtual void StateChangeBehaviour()
    {
        StateChangeTimer -= Time.deltaTime;
        if (StateChangeTimer > 0) return;

        bool result = myStateMachine.SetStateWithProbability(myStateMachine.previousState.stateName, stateChangeChance);
        Debug.Log(myStateMachine.previousState.stateName + ": " + result.ToString());
        
        StateChangeTimer = StateChangeTimerInterval;
    }
    
    protected override void OnPlayerFollowCommand(object sender, SphereArgs args)
    {
        FollowCommandResponse(args.center, args.radius);
    }

    void RunAway(int depth = 0)
    {
        Vector3 direction = (transform.position - parameterObject.transform.position).normalized;
        direction = Quaternion.Euler(0, Random.Range(-40f, 40f), 0) * direction;
        Vector3 new_destination = transform.position + RunAwayDistance * (Vector3) direction;
        NavMeshHit hit;
        if (NavMesh.SamplePosition(new_destination, out hit, FailRunAwayRange, 1)) {
            myAgent.SetStop(false);
            if (myAgent.CallIfReachable(hit.position)) {
                last_dest = hit.position;
                is_cornered = false;
            }
        } else {
            if (depth > 5) {
                IAmCornered();
            } else {
                RunAway(depth + 1);
            }
        }
    }

    void IAmCornered(int depth = 0)
    {
        if (depth > 5) {return;}
        
        Vector3 direction = transform.forward;
        float turn_range = 180;
        direction = Quaternion.Euler(0, Random.Range(-turn_range, turn_range), 0) * direction;
        Vector3 new_destination = transform.position + RunAwayDistance * (Vector3) direction;
        NavMeshHit hit;
        if (NavMesh.SamplePosition(new_destination, out hit, RunAwayDistance, 1)) {
            myAgent.SetStop(false);
            if (myAgent.CallIfReachable(hit.position)) {
                last_dest = hit.position;
                is_cornered = true;
            }
        } else {
            IAmCornered(depth + 1);
        }
    }

    private void OnDrawGizmos()
    {
        if (!is_active) return;
        Gizmos.color = is_cornered ? new Color(0.5f, 0.9f, 0f) : new Color(0.5f, 0.1f, 0.5f);
        Gizmos.DrawWireCube(last_dest, Vector3.one);
    }
}
