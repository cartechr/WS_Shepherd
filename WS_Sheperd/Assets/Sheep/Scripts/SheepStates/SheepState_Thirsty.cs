using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SheepState_Thirsty : SheepState_AbstractTargeted
{

    [SerializeField] private float maxWaterSearchDistance = 150f;

    private void Awake()
    {
        stateName = "Thirsty";
        //disallowSwitchingToStates.Add("Wander");
        disallowSwitchingToStates.Add("Hungry");
    }
    
    protected override void Start()
    {
        base.Start();
    }

    public override void EnterState()
    {
        NormalStateSetup();
        mySheepHealth.SwitchedToThirstyState?.Invoke(this, mySheepHealth);
        // it would be easiest to put the water locating routine here
        FindClosestWater();
        FollowTimer = 0;
        StateChangeTimer = StateChangeTimerInterval;
        //myMoodEffectController.StartAngerEffect();
    }

    public override void RunState()
    {
        FollowBehaviour();
        StateChangeBehaviour();
    }

    public override void ExitState()
    {
        mySheepHealth.SwitchedOutOfThirstyState?.Invoke(this, mySheepHealth);
        base.ExitState();
    }

    protected override void FollowBehaviour()
    {
        // make sure the water is still there
        FindClosestWater();
        base.FollowBehaviour();
    }

    override protected void StateChangeBehaviour()
    {
        StateChangeTimer -= Time.deltaTime;
        if (StateChangeTimer > 0) return;

        if (myAgent.HasReachedDestination() || mySheepHealth.is_drinking) {
            myStateMachine.SetState("Drinking");
        }
        
        StateChangeTimer = StateChangeTimerInterval;
    }
    
    protected override void OnPlayerFollowCommand(object sender, SphereArgs args)
    {
        FollowCommandResponse(args.center, args.radius);
    }

    protected void FindClosestWater()
    {
        float closest_distance = maxWaterSearchDistance;
        GameObject closest_water = this.gameObject;
        foreach (var waterObject in GameController.Instance().WaterList) {
            if (waterObject == null) {continue;}
            float dist = Vector3.Distance(waterObject.transform.position, transform.position);
            if (dist < closest_distance) {
                closest_distance = dist;
                closest_water = waterObject;
            }
        }
        // If the sheep fails to find water, then I guess I'll stick with the player
        if (closest_water == this.gameObject) {
            Debug.Log("Sheep " +gameObject+ " failed to find water");
            mySheepHealth.friendshipStats.DecreaseFriendship(0.5f);
            myStateMachine.SetStateDeferred("Follow");
        } else {
            target_transform = closest_water.transform;
        }
    }
    
}
