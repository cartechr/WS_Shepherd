using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class SheepState_Dead : SheepState
{
    
    private void Awake()
    {
        stateName = "Dead";
        disallowSwitchingToStates.Add("Follow");
        disallowSwitchingToStates.Add("Stay");
        disallowSwitchingToStates.Add("Wander");
        disallowSwitchingToStates.Add("Hungry");
        disallowSwitchingToStates.Add("Thirsty");
        disallowSwitchingToStates.Add("Eating");
        disallowSwitchingToStates.Add("Drinking");
        disallowSwitchingToStates.Add("Distracted");
        disallowSwitchingToStates.Add("Ensnared");
        disallowSwitchingToStates.Add("Panic");
        disallowSwitchingToStates.Add("Frightened");
        disallowSwitchingToStates.Add("Camping");
    }
    
    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
    }

    public override void EnterState()
    {
        mySheepHealth.currentHealth = 0;
        mySheepHealth.enabled = false;
        NavMeshAgent navMeshAgent = GetComponent<NavMeshAgent>();
        if (navMeshAgent) {
            navMeshAgent.enabled = false;
        }
        SheepAniState anistate = GetComponent<SheepAniState>();
        if (anistate) {
            anistate.enabled = false;
        }
        mySheepHealth.SwitchedToDeadState?.Invoke(this, mySheepHealth);
        GameEvents.AnySheepDeathEvent?.Invoke(this, EventArgs.Empty);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override void ExitState()
    {
        base.ExitState();
        mySheepHealth.SwitchedOutOfDeadState?.Invoke(this, mySheepHealth);
        Debug.Log("A sheep was revived???!?");
        
        mySheepHealth.enabled = true;
        NavMeshAgent navMeshAgent = GetComponent<NavMeshAgent>();
        if (navMeshAgent) {
            navMeshAgent.enabled = true;
        }
        SheepAniState anistate = GetComponent<SheepAniState>();
        if (anistate) {
            anistate.enabled = true;
        }
    }
}
