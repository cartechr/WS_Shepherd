using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SheepState_Camping : SheepState_AbstractTargeted
{

    private void Awake()
    {
        stateName = "Camping";
        disallowSwitchingToStates.Add("Wander");
        disallowSwitchingToStates.Add("Hungry");
        disallowSwitchingToStates.Add("Thirsty");
        disallowSwitchingToStates.Add("Eating");
        disallowSwitchingToStates.Add("Drinking");
        disallowSwitchingToStates.Add("Distracted");
        disallowSwitchingToStates.Add("Ensnared");
        disallowSwitchingToStates.Add("Panic");
        disallowSwitchingToStates.Add("Frightened");
    }
    
    protected override void Start()
    {
        base.Start();
    }

    public override void EnterState()
    {
        FindCampingWaypoint();
        FollowTimer = 0;
        StateChangeTimer = StateChangeTimerInterval;
        mySheepHealth.PauseStats();
        mySheepHealth.SwitchedToCampingState?.Invoke(this, mySheepHealth);
    }

    public override void RunState()
    {
        FollowBehaviour();
        StateChangeBehaviour();
    }

    public override void ExitState()
    {
        mySheepHealth.UnpauseStats();
        mySheepHealth.SwitchedOutOfCampingState?.Invoke(this, mySheepHealth);
    }

    override protected void StateChangeBehaviour()
    {
        StateChangeTimer -= Time.deltaTime;
        if (StateChangeTimer > 0) return;

        // no documented random state changes
        StateChangeTimer = StateChangeTimerInterval;
    }

    void FindCampingWaypoint()
    {
        Vector3 position = transform.position;
        float closest_distance = float.MaxValue;
        CampingSheepWaypoint my_waypoint = null;
        foreach (var waypoint in CampingSheepWaypoint.CampingSheepWaypoints) {
            float distance = Vector3.Distance(position, waypoint.transform.position);
            if (waypoint.mySheep == null && distance < closest_distance) {
                closest_distance = distance;
                my_waypoint = waypoint;
                target_transform = waypoint.transform;
            }
        }
        if (my_waypoint) {
            my_waypoint.ClaimSheep(this.gameObject);
            myAgent.WarpTo(target_transform.position);
            transform.rotation = target_transform.rotation;
        }
    }
    
}
