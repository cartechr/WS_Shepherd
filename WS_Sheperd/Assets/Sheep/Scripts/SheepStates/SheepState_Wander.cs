using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;

public class SheepState_Wander : SheepState
{

    [SerializeField] private float WanderTimerInterval;
    private float WanderTimer;
    [SerializeField] private float WanderDistance;
    [SerializeField] private float FailWanderRange;
    [SerializeField] private float NormalTurnRange;
    [SerializeField] private float CornerTurnRange;

    [Tooltip("This is how long this sheep is allowed to wander while preventing other sheep from getting bored and also wandering. After wandering for this amount of time, another sheep may also wander.")]
    [SerializeField] private float WanderingPreventionDuration = 180f;
    
    [SerializeField, Tooltip("StateChangeTimerInterval - Controls how frequently the sheep checks to see when it should change states. CHANGING THIS WILL INVOLVE REBALANCING")]
    protected float StateChangeTimerInterval;
    protected float StateChangeTimer;
    
    [Tooltip("Controls the easing for the chance to get hungry. Higher = steeper curve with lower odds when only slightly hungry.")]
    [SerializeField] private float HungryChanceExponent = 2f;
    [SerializeField] private float ThirstyChanceExponent = 2f;

    [SerializeField] private float minimumWanderTime = 4f;
    [HideInInspector] public bool hasTelegraphedWandering = false;
    private Coroutine delayedTelegraphing;

    // debug variables
    private Vector3 last_dest;
    private bool did_turn_around;
    private bool failed_to_wander = false;

    
    private void Awake()
    {
        stateName = "Wander";
    }

    protected override void Start()
    {
        WanderTimer = WanderTimerInterval;
        GameEvents.PlayerFollowCommandEvent += OnPlayerFollowCommand;
        GameEvents.CanASheepWanderQuestion += OnCanASheepWanderQuestion;
        //FMOD TEST
        //wanderingSound = FMODUnity.RuntimeManager.CreateInstance("event:/Audio/Alerts/Alert_Wander_Parent");
       // friendSound = FMODUnity.RuntimeManager.CreateInstance("event:/Audio/Alerts/Alert_Follow_Parent");

        base.Start();
    }

    private void OnDestroy()
    {
        GameEvents.PlayerFollowCommandEvent -= OnPlayerFollowCommand;
        GameEvents.CanASheepWanderQuestion -= OnCanASheepWanderQuestion;
    }

    public override void EnterState()
    {
        NormalStateSetup();
        myAgent.Speed(mySheepHealth.wanderSpeed);
        delayedTelegraphing = StartCoroutine(DelayedTelegraphing());
        //myMoodEffectController.StartAngerEffect();
        //FMOD TEST
            //wanderingSound.start();
    }

    IEnumerator DelayedTelegraphing()
    {
        mySheepHealth.SwitchedToWanderStateImmediate?.Invoke(this, mySheepHealth);
        hasTelegraphedWandering = false;
        yield return new WaitForSeconds(minimumWanderTime);
        hasTelegraphedWandering = true;
        mySheepHealth.SwitchedToWanderState?.Invoke(this, mySheepHealth);
    }

    public override void RunState()
    {
        WanderBehaviour();
        StateChangeBehaviour();
    }

    public override void ExitState()
    {
        StopCoroutine(delayedTelegraphing);
        mySheepHealth.SwitchedOutOfWanderState?.Invoke(this, mySheepHealth);
        myAgent.Speed(mySheepHealth.normalSpeed);
        base.ExitState();
    }

    protected virtual void WanderBehaviour()
    {
        WanderTimer -= Time.deltaTime;
        if (WanderTimer <= 0) {
            SelectAWander(0);
            WanderTimer = WanderTimerInterval;
        }
    }

    protected virtual void StateChangeBehaviour()
    {
        StateChangeTimer -= Time.deltaTime;
        if (StateChangeTimer > 0) return;
        if (myStateMachine.timeInState <= minimumWanderTime) return;

        // bool only here to evaluate the things. the ORs make it stop at the first success
        bool r = myStateMachine.SetStateWithProbability("Thirsty", Mathf.Pow(CalculateDehydrationChance(), ThirstyChanceExponent)) || 
                 myStateMachine.SetStateWithProbability("Hungry", Mathf.Pow(CalculateStarvingChance(), HungryChanceExponent));
        
        StateChangeTimer = StateChangeTimerInterval;
    }

    protected virtual void SelectAWander(int depth = 0)
    {
        if (depth > 5) {
            failed_to_wander = true;
            return;
        }
        failed_to_wander = false;

        Vector3 direction = transform.forward;
        float turn_range = NormalTurnRange;
        if (depth > 2) {
            turn_range = CornerTurnRange;
        }
        direction = Quaternion.Euler(0, Random.Range(-turn_range, turn_range), 0) * direction;
        Vector3 new_destination = transform.position + WanderDistance * (Vector3) direction;
        NavMeshHit hit;
        if (NavMesh.SamplePosition(new_destination, out hit, FailWanderRange, 1)) {
            myAgent.SetStop(false);
            if (myAgent.CallIfReachable(hit.position)) {
                did_turn_around = (depth >= 2);
                last_dest = new_destination;    
            } else {
                SelectAWander(Mathf.Max(2, depth + 1)); // skip to CornerTurnRange if we're not there yet
            }
        } else {
            SelectAWander(depth + 1);
        }
    }

    protected override void OnPlayerFollowCommand(object sender, SphereArgs args)
    {
        if (!is_active) return;
        // minimumWanderTime does not apply if the previous state was something like eating
        if (myStateMachine.timeInState >= minimumWanderTime ||  
            !(myStateMachine.previousState.stateName == "Follow" || myStateMachine.previousState.stateName == "Stay")) {
            FollowCommandResponse(args.center, args.radius);
        }
    }

    void OnCanASheepWanderQuestion(object sender, EventArgs args)
    {
        if (is_active && myStateMachine.timeInState < WanderingPreventionDuration) {
            GameEvents.CanASheepWanderAnswer?.Invoke(this, EventArgs.Empty);
        }
    }

    private void OnDrawGizmos()
    {
        if (!is_active) return;
        Gizmos.color = (did_turn_around) ? Color.magenta : Color.yellow;
        if (failed_to_wander) {Gizmos.color = Color.red;}
        Gizmos.DrawWireCube(last_dest, Vector3.one);
    }
}
