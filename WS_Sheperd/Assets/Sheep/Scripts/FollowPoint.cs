using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPoint : MonoBehaviour
{
    
    [SerializeField] private Rigidbody playerRigidbody;
    [SerializeField] public float lengthAway;
    
    // Start is called before the first frame update
    void Start()
    {
    
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 vel = playerRigidbody.velocity;
        vel.y = 0;
        transform.position = playerRigidbody.position + vel.normalized * lengthAway;
    }
}
