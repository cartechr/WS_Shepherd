using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoodEffectController : MonoBehaviour {
    public ParticleSystem angerEffect;
    public ParticleSystem loveEffect;
    public ParticleSystem sweatEffect;
    public ParticleSystem eatEffect;
    public ParticleSystem drinkEffect;
    public ParticleSystem stayEffect;
    public ParticleSystem cloudEffect;
    public ParticleSystem betrayalEffectLeft;
    public ParticleSystem betrayalEffectRight;
    
    
    
    // Start is called before the first frame update
    void Start() {

    }

    // Update is called once per frame
    void Update() {
        
    }

    public void TriggerLoveEffect() {
        loveEffect.Play();
        StartCoroutine(StopParticleSystem(loveEffect.main.startLifetime.Evaluate(0), loveEffect));
    }

    public void StartAngerEffect() {
        angerEffect.Play();
    }

    public void EndAngerEffect() {
        angerEffect.Stop();
    }

    public void StartSweatEffect() {
        sweatEffect.Play();
    }

    public void EndSweatEffect() {
        sweatEffect.Stop();
    }

    public void StartEatEffect()
    {
        if (!eatEffect.isPlaying)
            eatEffect.Play();
    }

    public void EndEatEffect()
    {
        eatEffect.Stop();
        
    }public void StartDrinkEffect()
    {
        if (!drinkEffect.isPlaying)
            drinkEffect.Play();
    }

    public void EndDrinkEffect()
    {
        drinkEffect.Stop();
    }

    public void StartCloudEffect()
    {
        cloudEffect.Play();
    }

    public void EndCloudEffect()
    {
        cloudEffect.Stop();
    }

    public void TriggerStayEffect()
    {
        stayEffect.Play();
        StartCoroutine(StopParticleSystem(stayEffect.main.startLifetime.Evaluate(0), stayEffect));
    }

    public void TriggerBetrayalEffect()
    {
        betrayalEffectLeft.Play();
        betrayalEffectRight.Play();
    }

    private IEnumerator StopParticleSystem(float time, ParticleSystem system) {
        yield return new WaitForSecondsRealtime(time);
        system.Stop();
    }
}
