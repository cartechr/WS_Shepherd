using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class SheepAniState : MonoBehaviour
{
    [SerializeField]
    private Animator anime;
    [SerializeField]
    private NavMeshAgent agent;
    public Brambles brambles;
   
    private const string Speed = "Speed";
    //private RaycastHit[] Hits = new RaycastHit[1];

    public LayerMask groundMask;
    [SerializeField] private Transform model;

    private void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
        anime = GetComponent<Animator>();
    }


    private void Update()
    {
        //focus on getting walking in first 
        SheepMoveAni();

        // use this debug to see overall speed range
        //Debug.Log(agent.velocity.sqrMagnitude);

        RaycastHit ray = new RaycastHit();
        Physics.Raycast(transform.position, Vector3.down, out ray, 4f, groundMask, QueryTriggerInteraction.Ignore);
        
        
        
            if (ray.collider)
            {
                //Vector3 old_eulers = transform.localEulerAngles;*/
                Quaternion rotation = Quaternion.FromToRotation(transform.up, ray.normal);
                //Debug.Log((Vector3.up, ray.normal));
                /*
                Vector3 eulers = rotation.eulerAngles;
                eulers.y = old_eulers.y;
                rotation = Quaternion.Euler(eulers);*/
                //transform.rotation = rotation;
                rotation *= transform.rotation;
                //model.rotation = rotation;
                model.rotation = Quaternion.RotateTowards(model.rotation, rotation, 30 * Time.deltaTime);

                //float pitch = Vector3.Angle(Vector3.up, ray.normal);
                //float roll = Vector3.Angle()

                //Quaternion rotation = Quaternion.LookRotation(transform.forward, ray.normal);
                //transform.rotation = rotation;
            }

        
    }

    public void SheepMoveAni()
    {
        anime.SetFloat(Speed, agent.velocity.magnitude);
    }

    public void EnterMoveAni()
    {
        anime.Play("Walk");
        anime.SetFloat(Speed, agent.velocity.magnitude);
    }
    
    public void Stop(){
        anime.StopPlayback();
        anime.SetFloat(Speed, 0);
    }

    public void SheepFootstep(string path)
    {
        FMODUnity.RuntimeManager.PlayOneShot(path, GetComponent<Transform>().position);
    }

    


}
