using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.AI;

public class NPC_Follow : MonoBehaviour {
    Rigidbody rigidbody;
    protected float horizontalInput;
    protected float forwardInput;
    public bool IsWalking = false;

    //Transform that NPC has to follow
    public Transform player;
    //NavMesh Agent variable
    protected Agent agent;
    //bool to check if sheep should follow player or not
    public Player playerScript;
    //return to player if this equals to 1
    public int ReturnToPlayer = 0;
    bool ReturnToPlayer2 = true;
    //  Transform SheepPosition;

    [Header("Statuses (Unused)")]
    public bool Starving = false;
    public bool Thirsty = false;
    public bool Dead = false;
    public bool scattering = false;
    private float ScatterTime = 1f;

    [Header("Food")]
    [Tooltip("Health Capacity of Food")] public int FoodAtStart;

    [SerializeField] private int _food;

    public int food {
        get { return _food; }
        set {
            _food = value;
            CheckHunger();
        }
    }

    //each sheep has their own hunger level
    public int WhenTheyGetHungry;
    public int WhenTheyStarve;

    private float foodTimer = 0f;

    [Header("Water")]
    [Tooltip("Amount of thirst")] public int WaterAtStart;

    [SerializeField] private int _water;

    public int water {
        get { return _water; }
        set {
            _water = value;
            CheckThirst();
        }
    }

    //each sheep has their own thirst level;
    public int WhenTheyGetThirsty;
    public int WhenTheyDehydrate;

    private float waterTimer = 0f;

    [Header("")]
    public float DistToSheep;

    private F_W_UI foodWaterUIScript;
    public SheepInfoController sheepInfoController;

    public EventHandler<string> FoodChangedStages;
    public EventHandler<string> WaterChangedStages;
    public EventHandler<float> FoodChanged;
    public EventHandler<float> WaterChanged;

    [Header("Status Processing")]
    public bool stats_paused = false;
    public bool easy_mode = false;

    private int W_Full;
    private int W_ThreeFourth;
    private int W_OneHalf;
    private int W_OneFourth;
    private int W_Empty;

    private int F_Full;
    private int F_ThreeFourth;
    private int F_OneHalf;
    private int F_OneFourth;
    private int F_Empty;

    public void PauseStats() { stats_paused = true; }
    public void UnpauseStats() { stats_paused = false; }
   

    // Start is called before the first frame update
    protected void initialize()
    {
        foodWaterUIScript = GetComponentInChildren<F_W_UI>();
        agent = GetComponent<Agent>();
        food = FoodAtStart;
        water = WaterAtStart;
        foodTimer = GameController.Instance().HungerRate;
        waterTimer = GameController.Instance().WaterRate;

        W_Full = WaterAtStart;
        W_ThreeFourth = (int)(WaterAtStart * 0.75f);
        W_OneHalf = (int)(WaterAtStart * 0.5f);
        W_OneFourth = (int)(WaterAtStart * 0.25f);
        W_Empty = 0;

        F_Full = FoodAtStart;
        F_ThreeFourth = (int)(FoodAtStart * 0.75f);
        F_OneHalf = (int)(FoodAtStart * 0.5f);
        F_OneFourth = (int)(FoodAtStart * 0.25f);
        F_Empty = 0;
    }

    private void Start()
    {
        initialize();
        //SheepPosition = GetComponent<Transform>();
    }

    protected virtual void Update()
    {
        if (!stats_paused && !easy_mode) {
            foodTimer -= Time.deltaTime;
            waterTimer -= Time.deltaTime;
        }

        if (foodTimer < 0) {
            IncreaseHunger();
            foodTimer = GameController.Instance().HungerRate;
        }
        if (waterTimer < 0) {
            IncreaseWater();
            waterTimer = GameController.Instance().WaterRate;
        }
    }


    /// 
    /// VESTIGIAL! DO NOT USE!
    /// 
    protected void FollowPlayer()
    {
        return;
        if (ReturnToPlayer2 == true)
        {
            agent.CallAgent(player.position);
        }
        else
        {
            if (Vector3.Distance(transform.position, player.position) < DistToSheep)
            {  
                Debug.Log("detected sheep");
                if (water > WhenTheyGetThirsty && food > WhenTheyGetHungry)
                {
                    Debug.Log("dont return");
                    playerScript.CanPressF = true;
                    if(playerScript.followplayer == true)
                    {
                        Debug.Log("Do return");
                        ReturnToPlayer2 = true;
                        playerScript.followplayer = false;
                    }
                }
            }
            // RunScatter()
        }
    }

    /// 
    /// VESTIGIAL! DO NOT USE!
    /// 
    public void GetFood(GameObject FoundFood, float distance)
    {
        return;
        if (food <= WhenTheyGetHungry)
        {
            ReturnToPlayer2 = false;
            Debug.Log("They want food");
            agent.CallAgent(FoundFood.transform.position);
            Debug.Log(FoundFood.transform.position);
        }

        if (food == FoodAtStart)
        {
            ReturnToPlayer2 = true;
        }

    }

    private void CheckHunger() {
        FoodChanged?.Invoke(this, (float) food / FoodAtStart);
        if(food == F_Full) {
            FoodChangedStages?.Invoke(this, "Full");
        } else if(food == F_ThreeFourth) {
            FoodChangedStages?.Invoke(this, "ThreeFourth");
        } else if(food == F_OneHalf) {
            FoodChangedStages?.Invoke(this, "OneHalf");
        } else if(food == F_OneFourth) {
            FoodChangedStages?.Invoke(this, "OneFourth");
        } else if(food == F_Empty) {
            // Not Implemented;
        }
    }

    public void IncreaseHunger()
    {
        //if (eat.eating == false)
       // {
            food--;
            if(food < 0) {
                food = 0;
            }

            CheckHunger();

            //    if (food == 0)
            // {
            //    Starving = true;
            //}
            //if (sheep1health.currentHealth == 0)
            //{
            //    Dead = true;
            // }
       // }
    }

    /// 
    /// VESTIGIAL! DO NOT USE!
    /// 
    public void GetWater(GameObject FoundWater, float distance)
    {
        return;
        if (water <= WhenTheyGetThirsty)
        {
            ReturnToPlayer2 = false;
            Debug.Log("They want water");
            agent.CallAgent(FoundWater.transform.position);
        }
        if (water == WaterAtStart)
        {
            ReturnToPlayer2 = true;
        }
    }

    private void CheckThirst() {
        WaterChanged?.Invoke(this, (float) water / WaterAtStart);
        if(water == W_Full) {
            WaterChangedStages?.Invoke(this, "Water_Full");
        } else if(water == W_ThreeFourth) {
            WaterChangedStages?.Invoke(this, "Water_Three_Fourth");
        } else if(water == W_OneHalf) {
            WaterChangedStages?.Invoke(this, "Water_One_Half");
        } else if(water == W_OneFourth) {
            WaterChangedStages?.Invoke(this, "Water_One_Fourth");
        } else if(water == W_Empty) {
            // Not Implemented;
        }
    }

    public void IncreaseWater()
    {
       // if (drink.drinking == false)
      //  {
            water--;
            if(water < 0) {
                water = 0;
            }
            CheckThirst();
            //this will affect the sheep stamina
            //if (sheep1health.currentHealth == 0)
            // {
            //     Dead = true;
            // }
            // }
      //  }
    }
}


// https://sharpcoderblog.com/blog/npc-follow-player-in-unity-3d //