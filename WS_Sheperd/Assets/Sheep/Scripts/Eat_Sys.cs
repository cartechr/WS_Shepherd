using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Eat_Sys : MonoBehaviour
{
    
    [SerializeField] private int foodhealth;

    // this is NASTY but I can't rename foodhealth since it's used in prefabs
    public int foodLeft
    {
        get => foodhealth;
        set
        {
            foodhealth = value;
            GameController.Instance().savedFoodHealths[transform.position.GetHashCode()] = foodhealth;
        }
    }

    public void Start()
    {
        GameEvents.LoadFromSave += LoadFromGameController;
        GameController gc = GameController.Instance();
        gc.FoodList.Add(gameObject);
        LoadFromGameController(this, EventArgs.Empty);
    }

    public void OnDestroy()
    {
        GameEvents.LoadFromSave -= LoadFromGameController;
        GameController.Instance()?.FoodList.Remove(gameObject);
    }

    public void LoadFromGameController(object sender, EventArgs args)
    {
        GameController gc = GameController.Instance();
        if (gc.savedFoodHealths.ContainsKey(transform.position.GetHashCode())) {
            foodhealth = gc.savedFoodHealths[transform.position.GetHashCode()];
        } else {
            gc.savedFoodHealths[transform.position.GetHashCode()] = foodhealth;
        }
    }
}
