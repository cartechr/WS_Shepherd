using System.Collections;
using UnityEngine;

public class JumpingCholla : MonoBehaviour
{

    //Positions for the jumping cholla parts to shoot from

    ParticleSystem Particle_System;
    public bool activator = false;


    // Start is called before the first frame update
    void Start()
    {
        Particle_System = GetComponent<ParticleSystem>();
    }

    // Update is called once per frame
    void Update()
    {
      /*  if (Input.GetKeyDown(KeyCode.P)) 
        {
            if (!activator)
            {
                activator = true;
                Debug.Log("Yes Particles");
            }
            else
            {
                activator = false;
                Debug.Log("No Particles");
            }
        } */
        if (activator)
        {
            //Stops particle system from emitting new particles
            //Once the current particles are done, gameobject should disable
            ParticleSystem.EmissionModule em = Particle_System.emission;
            em.enabled = false;
            if (Particle_System.isEmitting == false)
            {
                Particle_System.gameObject.SetActive(true);
            }
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Sheep"))
        {
            if (!activator)
            {
                Debug.Log("JUMPING CHOLLA SHOOT!");
                activator = true;
            }
        }
    }

    private void OnParticleCollision(GameObject other)
    {
        Debug.Log("method is being called");
        if (other.gameObject.CompareTag("Sheep"))
        {
            //Do damage
            //Kill that particle?

            //damages current hit sheep
            Debug.Log(other + " was hit by particle");
            other.GetComponent<SheepHealth>().JumpingChollaDamage(2);
            //other.GetComponent<MoodController>().OnTakeDamage();
            //Sheep take damage sound
      
        }
    }
}
