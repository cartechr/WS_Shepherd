using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class NEWGeyser : MonoBehaviour
{
    private bool isPlaying;

    //change these float values
    [Header("Smoke")]
    public float SmokeInterval = 10f;
    public float SmokeTime = 3f;
    public float SmokeSpeed = 20f;
    public float SmokeLifetime = 1.2f;
    public float SmokeDensity = 40f;

    //change these float values
    [Header("Plume")]
    public float PlumeLifetime = 2f;
    public float PlumeDensity = 2.5f;
    public float PlumeSpeed = 0.1f;


    private List<StateMachine> sheepInRange;
    private float m_Waiting = 5.0f;
    [Header("References")]
    public VisualEffect SmokeEmitter;
    public VisualEffect PlumeEmitter;

    void Start()
    {
        sheepInRange = new List<StateMachine>();
    }

    void Update()
    {
        if (SmokeEmitter)
        {
            m_Waiting -= Time.deltaTime;
            if (m_Waiting < 0.0f)
            {
                m_Waiting = 5.0f;
                SmokeEmitter.SendEvent(VisualEffectAsset.PlayEventID);
                PlumeEmitter.SendEvent(VisualEffectAsset.PlayEventID);
                //FMODUnity.RuntimeManager.PlayOneShot("event:/Audio/Geyser");
            }
        }
    }


    private void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.CompareTag("Sheep"))
        {
            sheepInRange.Add(col.gameObject.GetComponent<StateMachine>());
        }
    }

    private void OnTriggerExit(Collider col)
    {
        if (col.gameObject.CompareTag("Sheep"))
        {
            StateMachine sheep = col.gameObject.GetComponent<StateMachine>();
            if (sheepInRange.Contains(sheep))
            {
                sheepInRange.Remove(sheep);
            }
        }

    }
}
