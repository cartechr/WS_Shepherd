using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeyserController : MonoBehaviour {
    [Header("Eruption")]
    public float eruptionInterval = 10f;
    public float eruptionTime = 3f;
    public float eruptionSpeed = 20f;
    public float eruptionLifetime = 1.2f;
    public float eruptionDensity = 40f;
    [Header("Steam")]
    public float steamLifetime = 2f;
    public float steamDensity = 2.5f;
    public float steamSpeed = 0.1f;
    [Tooltip("The multiplier applied to the speed value of steam during eruption")]
    public float steamSpeedMultiplier = 20f;
    [Tooltip("The multiplier applied to the lifetime value of steam during eruption")]
    public float steamLifetimeMultiplier = 1f;
    [Tooltip("The multiplier applied to the density value of steam during eruption")]
    public float steamDensityMultiplier = 5f;
    
    [Header("References")]
    public ParticleSystem[] steamEmitters;
    public ParticleSystem jetEmitter;

    private List<StateMachine> sheepInRange;
    private ParticleSystem.EmissionModule[] steamEmissionModules;
    private ParticleSystem.MainModule[] steamMainModules;
    private ParticleSystem.EmissionModule jetEmissionModule;
    private ParticleSystem.MainModule jetMainModule;

    private bool isErupting;
    

    // Start is called before the first frame update
    void Start() {
        sheepInRange = new List<StateMachine>();
        steamEmissionModules = new ParticleSystem.EmissionModule[steamEmitters.Length];
        steamMainModules = new ParticleSystem.MainModule[steamEmitters.Length];
        for(int i = 0; i < steamEmitters.Length; i++) {
            steamEmissionModules[i] = steamEmitters[i].emission;
            steamMainModules[i] = steamEmitters[i].main;
        }
        ResetSteamEmitters();

        jetEmissionModule = jetEmitter.emission;
        jetMainModule = jetEmitter.main;

        jetEmissionModule.rateOverTime = new ParticleSystem.MinMaxCurve(eruptionDensity);
        jetMainModule.startSpeed = new ParticleSystem.MinMaxCurve(eruptionSpeed);
        jetMainModule.startLifetime = new ParticleSystem.MinMaxCurve(eruptionLifetime);

        foreach(ParticleSystem steamEmitter in steamEmitters) {
            steamEmitter.Play();
        }

        isErupting = false;
        
        StartCoroutine(Cooldown());
    }

    // Update is called once per frame
    void Update() {

    }

    private void StartErupting() {
        jetEmitter.Play();
        UpdateSteamEmitters();
        isErupting = true;
        foreach(StateMachine sheep in sheepInRange) {
            sheep.SetStateWithObject("Panic", gameObject);
            //CHANGE TO FRIGHTENED ONCE CREATED
        }
        StartCoroutine(EruptionTime());
    }

    private void StopErupting() {
        jetEmitter.Stop();
        ResetSteamEmitters();
        isErupting = false;
        StartCoroutine(Cooldown());
    }

    private IEnumerator EruptionTime() {
        yield return new WaitForSecondsRealtime(eruptionTime);
        StopErupting();
    }

    private IEnumerator Cooldown() {
        yield return new WaitForSecondsRealtime(eruptionInterval);
        StartErupting();
    }

    private void ResetSteamEmitters() {
        for(int i = 0; i < steamEmitters.Length; i++) {
            steamEmissionModules[i].rateOverTime = new ParticleSystem.MinMaxCurve(steamDensity);
            steamMainModules[i].startSpeed = new ParticleSystem.MinMaxCurve(steamSpeed);
            steamMainModules[i].startLifetime = new ParticleSystem.MinMaxCurve(steamLifetime);
        }
    }

    private void UpdateSteamEmitters() {
        for(int i = 0; i < steamEmitters.Length; i++) {
            steamEmissionModules[i].rateOverTime = new ParticleSystem.MinMaxCurve(steamDensity * steamDensityMultiplier);
            steamMainModules[i].startSpeed = new ParticleSystem.MinMaxCurve(steamSpeed * steamSpeedMultiplier);
            steamMainModules[i].startLifetime = new ParticleSystem.MinMaxCurve(steamLifetime * steamLifetimeMultiplier);
        }
    }

    private void OnTriggerEnter(Collider col) {
        if(col.gameObject.CompareTag("Sheep")) {
            sheepInRange.Add(col.gameObject.GetComponent<StateMachine>());
        }
    }

    private void OnTriggerExit(Collider col) {
        if(col.gameObject.CompareTag("Sheep")) {
            StateMachine sheep = col.gameObject.GetComponent<StateMachine>();
            if(sheepInRange.Contains(sheep)) {
                sheepInRange.Remove(sheep);
            }
        }
        
    }
}
