using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DistractingCactus : MonoBehaviour {

    public bool isDistracting = true;
    public float cooldown = 5f;
    public int capacity = 1;

    private List<GameObject> sheepInRange;
    private List<StateMachine> distractedSheep;
    private List<StateMachine> focusedSheep;

    [SerializeField] private SheepTreats sheepTreats;

    // Start is called before the first frame update
    void Start() {
        distractedSheep = new List<StateMachine>();
        focusedSheep = new List<StateMachine>();
        sheepInRange = new List<GameObject>();
    }

    // Update is called once per frame
    void Update() {
        
    }

    void OnTriggerEnter(Collider col) {
        if(col.gameObject.CompareTag("Sheep")) {
            sheepInRange.Add(col.gameObject);
            if(distractedSheep.Count < capacity) {
                StateMachine sheep = col.gameObject.GetComponent<StateMachine>();
                if(!focusedSheep.Contains(sheep)) {
                    Distract(sheep);
                }
            }
        }
    }

    private void OnTriggerExit(Collider col) {
        if(col.gameObject.CompareTag("Sheep")) {
            sheepInRange.Remove(col.gameObject);
        }
    }

    private void Distract(StateMachine sheep) {
        distractedSheep.Add(sheep);
        sheep.SetStateWithObject("Distracted", sheepTreats.Fruit); // the fruit is the distraction (it gets removed)
        //CHANGE BACK TO DISTRACTED AFTER MILESTONE
    }

    private void CheckSheep(StateMachine sheep) {
        if(distractedSheep.Count < capacity) {
            if(sheepInRange.Contains(sheep.gameObject)) {
                Distract(sheep);
            }
        }
    }

    private IEnumerator Cooldown(StateMachine sheep) {
        yield return new WaitForSecondsRealtime(cooldown);
        focusedSheep.Remove(sheep);
        CheckSheep(sheep);
    }

    public void FocusSheep(StateMachine sheep) {
        focusedSheep.Add(sheep);
        StartCoroutine(Cooldown(sheep));
    }
}
