using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Rattlesnake : MonoBehaviour
{
    public float Patience = -1;
    ///The timer in between bites of the snake
    public GameObject Bite;
    public int SheepInRange = 0;

    public bool SheepIsIn;
    public float DamageTimer = -1;

    public int Damage = 10;

    //FMOD
    public bool Shenanigans = false;

 void Start()
    {
        
        Bite.SetActive(false);

    }

    void Update()
    {
        if (Patience >= -1)
        {
            Patience -= Time.deltaTime;
            Bite.SetActive(false);  
        }
        if (Patience <= 0)
        {
            Attack();
        }

        if (SheepInRange == 0)
        {
            SheepIsIn = false;
        }
        if (Patience <= -0.5)
        {
            Patience = 6;
        }
        if (SheepIsIn == false)
        {
            Patience = 6;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        //Detects if the player enters the collider
        if (other.CompareTag("Sheep") && SheepIsIn == false)
        {
            Patience = 10;
            SheepIsIn = true;
            SheepInRange = SheepInRange + 1;

            Shenanigans = true;//FMOD
        }   
    }

    private void OnTriggerExit(Collider other)
    {
        //Detects if the sheep exits the collider

        if (other.CompareTag("Sheep"))
        {
            SheepInRange = SheepInRange - 1;

            Shenanigans = false;//FMOD
        }   
    }
    void Attack()
    {
        Bite.SetActive(true);
    }
}
