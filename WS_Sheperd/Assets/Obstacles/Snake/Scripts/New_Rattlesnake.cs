using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class New_Rattlesnake : MonoBehaviour
{
    //target range for snake
    [SerializeField] public float strikingRange;
    //range for rattle
    [SerializeField] public float rattleRange;
    //How much the snake damages the sheep
    [SerializeField] public int DamageToSheep;
    
    [Header("Snake Timers")]
    //The total amount of the time the sheep is damage (damage over time)
    [SerializeField] private float TotalDamageTimer;
    [SerializeField] private float currentTotalDamageTimer;
    //The delay of each damage
    [SerializeField] private float DamageDelayTimer;
    [SerializeField] private float currentDamageDelayTimer;
    [SerializeField] private bool timer = false;

    [Header("How far back the snake hides")]
    [SerializeField] public float Hiding;

    [SerializeField] Vector3 hide;

    //FMOD
    public bool Shenanigans = false;

    public bool HasAttacked = false;

    SheepHealth target;

    private void Start() 
    {
        currentTotalDamageTimer = TotalDamageTimer;
        currentDamageDelayTimer = DamageDelayTimer;
        hide = transform.position - transform.forward * Hiding;
    }

    // Update is called once per frame


    //how to check if audio is currently playing (FMOD audio)
    void Update()
    {
        if (timer == true)
        {
            if (currentTotalDamageTimer > 0)
            {

                currentTotalDamageTimer -= Time.deltaTime;
                currentDamageDelayTimer -= Time.deltaTime;
                
                if (currentDamageDelayTimer <= 0)
                {
                    currentDamageDelayTimer = DamageDelayTimer;
                    Attack();
                }
            }
            if (currentTotalDamageTimer <= 0)
            {
                timer = false;
            }
        }
        if (HasAttacked == false)
        {
            foreach (SheepHealth sheep in SheepHealth.GetSheepList())
            {

                if (Vector3.Distance(transform.position, sheep.transform.position) < rattleRange)
                {
                    Debug.Log("Snake sees a sheep");
                    //target = sheep;


                    //Insert FMOD rattle sound here
                }

                if (Vector3.Distance(transform.position, sheep.transform.position) < strikingRange)
                {
                    target = sheep;
                    Attack();
                    timer = true;

                }
            }
           // else
            //{
              //  if (Vector3.Distance(transform.position, target.transform.position) > rattleRange)
               // {
                //    target = null;
                //    Debug.Log("Finding new target sheep (rattlesnake)");
               // }
           // }
        }

        if (HasAttacked == true)
        {
            transform.position = hide;
            //transform.position = Vector3.MoveTowards(transform.position, HidingPosition.position, slither);
        }
    }

    void Attack()
    {
        Debug.Log("SNAKE ATTACK!");
        target.SnakeDamage();
        HasAttacked = true;
    }
}
