using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Brambles : MonoBehaviour
{
    public Animator animator;
    [SerializeField] private Transform slurpPoint;
    public StateMachine caughtSheep;
    public bool disarmed = false;
    
    public float slurpSpeed = 1f;

    // Start is called before the first frame update
    void Start()
    {
        GameEvents.LoadFromSave += LoadFromGameController;
        LoadFromGameController(this, EventArgs.Empty);
    }

    private void OnDestroy()
    {
        GameEvents.LoadFromSave -= LoadFromGameController;
    }

    // Update is called once per frame
    void Update()
    {
        if (caughtSheep) {
            caughtSheep.transform.position = Vector3.MoveTowards(caughtSheep.transform.position, slurpPoint.position,
                slurpSpeed * Time.deltaTime);
        }
        if (caughtSheep && caughtSheep.currentStateName != SheepState.ENSNARED_STATE_NAME && disarmed == false) 
        {
            disarmed = true;
            Release();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        //Detects if the Sheep enters the collider
        if (other.gameObject.CompareTag("Sheep"))
        {
            Debug.Log(other.gameObject + " is ensnared?");
            if (other.gameObject.GetComponent<StateMachine>().timeInState > 3 && caughtSheep == null)
            {
                other.gameObject.GetComponent<StateMachine>().SetState("Ensnared");
                caughtSheep = other.GetComponent<StateMachine>();
                FMODUnity.RuntimeManager.PlayOneShot("event:/Audio/Bramble");
                tutorialGameEvents.Obstacles(this, EventArgs.Empty);
            }
            animator.SetBool("IsConstrict", true);
            animator.Play("Constrict");
            SaveToGameController();
        }
    }

    public void DisableColliders()
    {
        foreach (Collider c in GetComponents<Collider>())
        {
            c.enabled = false;
        }
    }

    //Use if needed
    public void EnableColliders()
    {
        foreach (Collider c in GetComponents<Collider>())
        {
            c.enabled = true;
        }
    }
        
    private void OnTriggerExit(Collider other)
    {
      

        /*if (other.gameObject.CompareTag("Sheep"))
        {
            //stuck = false;

            
            Release();
        }*/
    }

    void Release()
    {
        disarmed = true;
        animator.SetBool("IsRelease", true);
        DisableColliders();
        animator.Play("Unravel");
        StartCoroutine(diebramb());
        SaveToGameController();
        FMODUnity.RuntimeManager.PlayOneShot("event:/Audio/Bramble");
    }

    IEnumerator diebramb()
    {
        
        yield return new WaitForSeconds(8);
        gameObject.SetActive(false);
        
    }

    void SaveToGameController()
    {
        SheepHealth.SheepRole sheepRole = caughtSheep ? 
            caughtSheep.GetComponent<SheepHealth>().mySheepRole : 
            SheepHealth.SheepRole.None;
        var save_data = (disarmed, (int) sheepRole);
        GameController.Instance().savedBrambleStates[transform.position.GetHashCode()] = save_data;
    }

    void LoadFromGameController(object sender, EventArgs args)
    {
        GameController gc = GameController.Instance();
        if (gc.savedBrambleStates.ContainsKey(transform.position.GetHashCode())) {
            // load data is a tuple of (disarmed: bool, caughtSheepRole: int)
            var load_data = gc.savedBrambleStates[transform.position.GetHashCode()];
            disarmed = load_data.Item1;
            caughtSheep = SheepHealth.FindSheepByRole((SheepHealth.SheepRole) load_data.Item2, true)?.GetComponent<StateMachine>();
        } else {
            SaveToGameController();
        }
        
        // restore the state according to the updated values
        animator.SetBool("IsRelease", false);
        animator.SetBool("IsConstrict", false);
        if (!disarmed) {
             gameObject.SetActive(true);
             EnableColliders();
             StopAllCoroutines();
             if (caughtSheep == null) {
                 animator.Play("Idle");
             } else {
                 animator.SetBool("IsConstrict", true);
                 animator.Play("Constrict");
             }
        } else {
            gameObject.SetActive(false);
            DisableColliders();
            animator.SetBool("IsRelease", true);
        }
    }
}