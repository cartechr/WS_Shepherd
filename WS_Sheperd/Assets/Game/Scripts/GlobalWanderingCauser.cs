using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalWanderingCauser : MonoBehaviour
{

    [SerializeField] private float wanderTimeMin;
    [SerializeField] private float wanderTimeMax;

    private float triggerAWanderTimer = 0;
    
    // Start is called before the first frame update
    void Awake()
    {
        triggerAWanderTimer = Random.Range(wanderTimeMin, wanderTimeMax);
    }

    // Update is called once per frame
    void Update()
    {
        triggerAWanderTimer -= Time.deltaTime;
        if (triggerAWanderTimer <= 0) {
            TriggerAWander();
            triggerAWanderTimer = Random.Range(wanderTimeMin, wanderTimeMax);
        }
    }

    void TriggerAWander()
    {
        List<SheepHealth> wanderableSheep = new List<SheepHealth>();
        foreach (var sheep in SheepHealth.sheepList) {
            string current_state = sheep.GetComponent<StateMachine>().currentStateName;
            if (current_state.Equals(SheepState.FOLLOW_STATE_NAME) ||
                current_state.Equals(SheepState.STAY_STATE_NAME)) {
                if (sheep.GetComponent<SheepState_Follow>().CanWander()) {
                    wanderableSheep.Add(sheep);
                }
            }
        }

        if (wanderableSheep.Count > 0 && !SheepUtil.AreAnySheepWandering()) {
            wanderableSheep[Random.Range(0, wanderableSheep.Count)].GetComponent<StateMachine>().SetState("Wander");
        }
    }

    public bool ForceAWander()
    {
        List<SheepHealth> wanderableSheep = new List<SheepHealth>();
        foreach (var sheep in SheepHealth.sheepList) {
            string current_state = sheep.GetComponent<StateMachine>().currentStateName;
            if (current_state.Equals(SheepState.FOLLOW_STATE_NAME) || 
            current_state.Equals(SheepState.STAY_STATE_NAME)) {
                wanderableSheep.Add(sheep);
            }
        }

        if (wanderableSheep.Count > 0) {
            wanderableSheep[Random.Range(0, wanderableSheep.Count)].GetComponent<StateMachine>().SetState("Wander");
            return true;
        }
        return false;
    }
}
