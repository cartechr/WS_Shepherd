using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Borodar.FarlandSkies.Core.Helpers;
using Borodar.FarlandSkies.LowPoly;

public class MusicScript : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject skyboxhaver;
    public GameObject Shepherd;
    public Rattlesnake rattlesnake;
    FMOD.Studio.EventInstance gameMusic;
    FMOD.Studio.EventInstance ambienceControl;
    Borodar.FarlandSkies.LowPoly.SkyboxCycleManager skyman;

    public float GameState = 0;
    public float Progress = 0;
    public float MusicType = 0;
    public float TimeOfDay;


    private float targetTime = 150.0f;//10.0f
    private float debugTime = 1.0f;

    private bool MusicSwitch = false;
    public bool Paused = false;



    // Start is called before the first frame update
    void Start()
    {

        //Set Instance
        gameMusic = FMODUnity.RuntimeManager.CreateInstance("event:/MUSIC/Event Manager");
        // cue the music to play
        gameMusic.start();
        //Skybox Manager for Day/Night
        skyman = skyboxhaver.GetComponent<Borodar.FarlandSkies.LowPoly.SkyboxCycleManager>();

        ambienceControl = FMODUnity.RuntimeManager.CreateInstance("event:/Brock Environments/Environment Manager");
        ambienceControl.start();
        FMODUnity.RuntimeManager.AttachInstanceToGameObject(ambienceControl, GetComponent<Transform>(), GetComponent<Rigidbody>());
    }

    // Update is called once per frame
     void Update()
     {
        //TIME OF DAY
        FMODUpdateTimeOfDay();


        if (skyman.DayTime == false) /*skyman.CycleProgress >= 0.8 || skyman.CycleProgress <= 0.3*/
        {
            Debug.Log("ITS NIGHTTIME FMOD");
            TimeOfDay = 0;
        }

        if (skyman.DayTime == true) /*skyman.CycleProgress <= 0.8 && skyman.CycleProgress >= 0.3*/
        {
            Debug.Log("ITS DAYTIME FMOD");
            TimeOfDay = 1;
        }
        FMODUnity.RuntimeManager.StudioSystem.setParameterByName("TimeOfDay", TimeOfDay);





         //CHANGE MUSIC STATE
        UpdateGameState();

        UpdateProgressState(); // THIS WILL NOT REMAIN IN UPDATE FOREVER: FOR TESTING PURPOSES NOW

        //TIMER
        if (targetTime > 0 && Paused == false)
        {
            targetTime -= Time.deltaTime;
        }
        //IF TIMER RUNS OUT while in exploration, do silence for 40 seconds
        if(targetTime <= 0 && GameState == 0)
        {

            BeginSilence();  //silence+timer
            Debug.Log("TIMER OUT BEGIN SILENCE");
            UpdateMusicType(); //Switch to ambient if linear, switch to linear if ambient

         
        }
        //IF TIMER RUNS OUT while in silence, start playing next track
        if (targetTime <= 0 && GameState == 2)
        {
            targetTime = 150.0f;
            Debug.Log("TIMER OUT BEGIN MUSIC");
            
            GameState = 0;
        }
        //IF TIMER runs out while in shenanigans, do nothing.
    }


    //TIME OF DAY MODULATION
    void FMODUpdateTimeOfDay()
    {
        
    }


    //UPDATES 'GAME STATE'. exploring, shenanigans, combat?
    void UpdateGameState()
    {


        /*THIS RATTLESNAKE STUFF IS SUPER PLACEHOLDER. THERE WILL NEED TO BE
        if (rattlesnake.Shenanigans == true)
        {
            Debug.Log("SHENANIGANS START");
            GameState = 1;
        }
        if (rattlesnake.Shenanigans == false && GameState == 1)
        {
            BeginSilence();//silence+timer
            Debug.Log("SHENANIGANS END");
            UpdateMusicType();
        }*/
        FMODUnity.RuntimeManager.StudioSystem.setParameterByName("GameState", GameState);
    }


    //UPDATE GAME PROGRESS MODULATION
    void UpdateProgressState()
    {
        Player player = Shepherd.GetComponent<Player>();
        FMODUnity.RuntimeManager.StudioSystem.setParameterByName("Progress", player.FMODProgressValue);
//        Debug.Log(player.FMODProgressValue);

        //Update Environment
        //ambienceControl.setParameterByName("Environment", player.FMODEnvironmentValue);
    }

    //SWITCHES BETWEEN LINEAR AND AMBIENT.
    void UpdateMusicType()
    {
        MusicSwitch = false;
        if (MusicType == 0 && MusicSwitch == false)
        {
            MusicType = 1f;
            Debug.Log("LINEAR");
            MusicSwitch = true;
        }

        if (MusicType == 1 && MusicSwitch == false)
        {
            MusicType = 0f;
            Debug.Log("AMBIENT");
            MusicSwitch = true;
        }

        FMODUnity.RuntimeManager.StudioSystem.setParameterByName("MusicType", MusicType);
    }
    void BeginSilence()
    {
        GameState = 2;//silence
        targetTime = 30.0f;
    }

    public void PauseMusic()
    {
        gameMusic.setPaused(true);
        ambienceControl.setPaused(true);
        Paused = true;
    }

    public void ResumeMusic()
    {
        gameMusic.setPaused(false);
        ambienceControl.setPaused(false);
        Paused = false;
    }
}
