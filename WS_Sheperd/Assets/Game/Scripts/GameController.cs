using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class GameController : MonoBehaviour
{
    [SerializeField] List<Coyote_Controller> Combat_Coyote;
    public List<GameObject> FoodList;
    public List<GameObject> WaterList;
    public Controls controls;
    static GameController _instance;
    //Reference to npc_follow
    [Tooltip("Rate in seconds in which the hunger increases (seconds per tick)")]
    public float HungerRate = .5f;
    [Tooltip("Rate in seconds in which the thirst increases (seconds per tick)")]
    public float WaterRate = .5f;


    int Current_Coyote = 0;

    //Coyote Repelled speed
    [Header("CoyRepSpeed")]
    public float moveSpeed = 3f;
    public float rotSpeed = 100f;

    [Header("FoodList/WaterList Health")]

    //referencing for food and water hunger and thirst rates
    public NPC_Follow f_w;

    public int foodhealth;
    public int food_eating_rate;

    public GlobalWanderingCauser globalWanderingCauser;
    public Player player;

    public Tutorial tutorial;

    [Header("Saving")]
    // keys are hashes of the object's (starting) position
    public Dictionary<int, int> savedFoodHealths = new Dictionary<int, int>();
    public Dictionary<int, int> savedWaterHealths = new Dictionary<int, int>();
    public Dictionary<int, (bool, int)> savedBrambleStates = new Dictionary<int, (bool, int)>();
    public Dictionary<int, int> savedSheepTreatFruits = new Dictionary<int, int>();
    public Dictionary<int, (bool, int, int, Vector3, Vector3)> savedCoyotes = 
        new Dictionary<int, (bool, int, int, Vector3, Vector3)>();


    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    private void Start()
    {
        //InvokeRepeating("IncreaseHunger", 0, HungerRate);
        //InvokeRepeating("IncreaseWater", 0, WaterRate);
        globalWanderingCauser = GetComponent<GlobalWanderingCauser>();
        if (!player) {
            player = FindObjectOfType<Player>();
        }

        try {
            Application.logMessageReceived += OnLogDebug;
        }
        catch (Exception e) {
            Debug.LogException(e);
        }
        
    }

    private void OnDestroy()
    {
        try {
            Application.logMessageReceived -= OnLogDebug;
        }
        catch (Exception e) {
            Debug.LogException(e);
        }
    }

    public void IncreaseHunger()
    {
        for (int i = 0; i < SheepHealth.sheepList.Count; i++)
        {
            SheepHealth.sheepList[i].IncreaseHunger();
        }
    }
    public void IncreaseWater()
    {
        for (int i = 0; i < SheepHealth.sheepList.Count; i++)
        {
            SheepHealth.sheepList[i].IncreaseWater();
        }
    }

    void SheepFood()
    {
        return;
        for (int i = 0; i < SheepHealth.sheepList.Count; i++)
        {
            float distance = float.MaxValue;
            int closestFoodID = -1;

            for (int j = 0; j < FoodList.Count; j++)
            {
                float dist = Vector3.Distance(SheepHealth.sheepList[i].transform.position, FoodList[j].transform.position);
                if (dist < distance)
                {
                    distance = dist;
                    closestFoodID = j;
                }
            }

            if (closestFoodID != -1)
            {

                SheepHealth.sheepList[i].GetFood(FoodList[closestFoodID], distance);
            }
        }
    }

    /// 
    /// VESTIGIAL! DO NOT USE
    /// 
    void SheepWater()
    {
        return;
        for (int i = 0; i < SheepHealth.sheepList.Count; i++)
        {
            float distance = float.MaxValue;
            int closestWaterID = -1;

            for (int j = 0; j < WaterList.Count; j++)
            {
                float dist = Vector3.Distance(SheepHealth.sheepList[i].transform.position, WaterList[j].transform.position);
                if (dist < distance)
                {
                    distance = dist;
                    closestWaterID = j;
                }
            }

            if (closestWaterID != -1)
            {
                SheepHealth.sheepList[i].GetWater(WaterList[closestWaterID], distance);
            }
        }
    }
    private void Update()
    {
        SheepFood();
        SheepWater();
        foreach (var key in savedFoodHealths.Keys) {
            break;
        }

        try {
            DoMyDebug();
        } catch (Exception e) {
            Debug.LogException(e);
        }
    }


    public Coyote_Controller ActiveCoyote()
    {
        if (Current_Coyote < 0 || 0 >= Combat_Coyote.Count)
        {
            return null;
        }
        return Combat_Coyote[Current_Coyote];

    }

    public static GameController Instance()
    {
        return _instance;
    }

    public void DoMyDebug()
    {
        if (OnScreenDebug.IsDisabled()) { return; }
        
        string msg = "";

        float ToPercent(float n, float outOf)
        {
            return Mathf.Round(100f * n / outOf);
        }
        
        // SheepStats
        msg = "";
        foreach (var sheep in SheepHealth.sheepList) {
            msg += $"{sheep.name}: \n" +
                   $"Health: {sheep.currentHealth}/{sheep.maxHealth} ({ToPercent(sheep.currentHealth, sheep.maxHealth)}%), " +
                   $"Water: {sheep.water}/{sheep.WaterAtStart} ({ToPercent(sheep.water, sheep.WaterAtStart)}%), " +
                   $"Food: {sheep.food}/{sheep.FoodAtStart} ({ToPercent(sheep.food, sheep.FoodAtStart)}%), " +
                   $"Friendship [min-value-max]: {sheep.friendshipStats.minimumFriendship} < {sheep.friendshipStats.friendship} < " +
                    $"{sheep.friendshipStats.maximumFriendship} " +
                    $"({ToPercent(sheep.friendshipStats.friendship, sheep.friendshipStats.maximumFriendship)}%), " +
                   $"Stamina: {sheep.sheepStamina}/{sheep.maxSheepStamina} ({ToPercent(sheep.sheepStamina, sheep.maxSheepStamina)}%), " +
                   $"Is Drinking: {sheep.is_drinking}, " +
                   $"Is Eating: {sheep.is_eating}, " +
                   $"Is Moving: {sheep.moving}, " +
                   $"Is Panicking: {sheep.panicking}, " +
                   $"Is Slowed: {sheep.Slowed}, " +
                   $"Bleat Class: {sheep.bleatClass}, " +
                   $"Sheep Role: {sheep.mySheepRole.ToString()}, " +
                   $"State: {sheep.myStateMachine.currentStateName}, " +
                   $"Easy Mode: {sheep.easy_mode}" +
                   $"\n";
        }
        OnScreenDebug.SetCategoryShortcut("SheepStats", msg);
        
        // TouchingFoods
        msg = "";
        foreach (var sheep in SheepHealth.sheepList) {
            msg += sheep.name + ":";
            foreach (var food in sheep.touchingFoods) {
                msg += food.name + ", ";
            }
            msg += "\n";
        }
        OnScreenDebug.SetCategoryShortcut("TouchingFoods", msg);
        
        // TouchingWaters
        msg = "";
        foreach (var sheep in SheepHealth.sheepList) {
            msg += sheep.name + ":";
            foreach (var water in sheep.touchingWaters) {
                msg += water.name + ", ";
            }
            msg += "\n";
        }
        OnScreenDebug.SetCategoryShortcut("TouchingWaters", msg);
        
        // SheepStates
        msg = "";
        foreach (var sheep in SheepHealth.sheepList) {
            msg += sheep.name + ": " + sheep.myStateMachine.currentStateName + "\n";
        }
        OnScreenDebug.SetCategoryShortcut("SheepStates", msg);
        
        // Sheep
        msg = "";
        foreach (var sheep in SheepHealth.sheepList) {
            msg += $"{sheep.name}: \n" +
                   $"Health: {sheep.currentHealth}/{sheep.maxHealth} ({ToPercent(sheep.currentHealth, sheep.maxHealth)}%), " +
                   $"Water: {sheep.water}/{sheep.WaterAtStart} ({ToPercent(sheep.water, sheep.WaterAtStart)}%), " +
                   $"Food: {sheep.food}/{sheep.FoodAtStart} ({ToPercent(sheep.food, sheep.FoodAtStart)}%), " +
                   $"State: {sheep.myStateMachine.currentStateName}, " +
                   $"Easy Mode: {sheep.easy_mode}" +
                   $"\n";
        }
        OnScreenDebug.SetCategoryShortcut("Sheep", msg);
        
        // Console
        OnScreenDebug.SetCategoryShortcut("Console", _logCopy);
        
        // ConsoleLogs
        OnScreenDebug.SetCategoryShortcut("ConsoleLogs", _logCopyLogs);
        
        // ConsoleWarnings
        OnScreenDebug.SetCategoryShortcut("ConsoleWarnings", _logCopyWarnings);
        
        // ConsoleErrors
        OnScreenDebug.SetCategoryShortcut("ConsoleErrors", _logCopyErrors);
        
        // ConsoleAssertions
        OnScreenDebug.SetCategoryShortcut("ConsoleAssertions", _logCopyAssertions);
        
        // SavedFoodHealths
        msg = "";
        foreach (var pair in savedFoodHealths) {
            msg += $"({pair.Key}, {pair.Value}), ";
        }
        OnScreenDebug.SetCategoryShortcut("SavedFoodHealths", msg);
        
        // SavedWaterHealths
        msg = "";
        foreach (var pair in savedWaterHealths) {
            msg += $"({pair.Key}, {pair.Value}), ";
        }
        OnScreenDebug.SetCategoryShortcut("SavedWaterHealths", msg);
        
        // SavedBrambleStates
        msg = "";
        foreach (var pair in savedBrambleStates) {
            msg += $"({pair.Key}, {pair.Value}), ";
        }
        OnScreenDebug.SetCategoryShortcut("SavedBrambleStates", msg);
        
        // SavedSheepTreatFruits
        msg = "";
        foreach (var pair in savedSheepTreatFruits) {
            msg += $"({pair.Key}, {pair.Value}), ";
        }
        OnScreenDebug.SetCategoryShortcut("SavedSheepTreatFruits", msg);
        
        // SavedCoyotes
        msg = "";
        foreach (var pair in savedCoyotes) {
            msg += $"({pair.Key}, {pair.Value}), ";
        }
        OnScreenDebug.SetCategoryShortcut("SavedCoyotes", msg);
        
        // Time
        var time_vehicle = new FloatArgs(0f);
        GameEvents.GetTimeOfDay?.Invoke(this, time_vehicle);
        OnScreenDebug.SetCategoryShortcut("Time", $"{time_vehicle.number}");
        
        // Player
        OnScreenDebug.SetCategoryShortcut("Player",
            $"Placing Post: {player.placing_post}, " +
            $"Treats: {player.getPlayerTreats()}"
        );
        
        // SheepStateEvents
        OnScreenDebug.SetCategoryShortcut("SheepStateEvents", SheepHealth.allSheepStateEventLog);
        
        // WanderEvents
        OnScreenDebug.SetCategoryShortcut("WanderEvents", SheepHealth.allSheepWanderlog);
    }

    private string _logCopy = "";
    private int _logLength = 4000;
    private string _logCopyErrors = "";
    private string _logCopyWarnings = "";
    private string _logCopyLogs = "";
    private string _logCopyAssertions = "";
    private void OnLogDebug(string logString, string stackTrace, LogType logType)
    {
        AddToLog(ref _logCopy, logString, stackTrace, logType, _logLength, true);
        switch (logType) {
            case LogType.Log:
                AddToLog(ref _logCopyLogs, logString, stackTrace, logType, _logLength);
                break;
            case LogType.Warning:
                AddToLog(ref _logCopyWarnings, logString, stackTrace, logType, _logLength);
                break;
            case LogType.Error:
                AddToLog(ref _logCopyErrors, logString, stackTrace, logType, _logLength);
                break;
            case LogType.Exception:
                AddToLog(ref _logCopyErrors, logString, stackTrace, logType, _logLength);
                break;
            case LogType.Assert:
                AddToLog(ref _logCopyAssertions, logString, stackTrace, logType, _logLength);
                break;
        }
    }

    private void AddToLog(ref string log, string logString, string stackTrace, LogType logType, int clamp_length, bool includeType = false)
    {
        if (includeType) {
            log += logType + ": " + logString + "\n" + stackTrace + "\n\n";    
        } else {
            log += logString + "\n" + stackTrace + "\n\n";
        }
        ClampStringStart(ref log, _logLength);
    }

    private void ClampStringStart(ref string str, int target_length)
    {
        str = str.Substring(Mathf.Max(str.Length - target_length, 0), Mathf.Min(target_length, str.Length));
    }
}