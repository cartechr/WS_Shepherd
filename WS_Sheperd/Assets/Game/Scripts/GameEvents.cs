using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PositionArgs : EventArgs
{
    public Vector3 position;

    public PositionArgs(Vector3 p_position)
    {
        position = p_position;
    }
}

public class SphereArgs : EventArgs
{
    public Vector3 center;
    public float radius;

    public SphereArgs(Vector3 p_center, float p_radius)
    {
        center = p_center;
        radius = p_radius;
    }
}

public class TimeSphereArgs : EventArgs
{
    public Vector3 center;
    public float radius;
    public float time;

    public TimeSphereArgs(Vector3 p_center, float p_radius, float p_time)
    {
        center = p_center;
        radius = p_radius;
        time = p_time;
    }
}

public class GameObjectArgs : EventArgs
{
    public GameObject gameObject;

    public GameObjectArgs(GameObject p_gameObject)
    {
        gameObject = p_gameObject;
    }
}

public class SheepHealthArgs : EventArgs
{
    public SheepHealth sheepHealth;

    public SheepHealthArgs(SheepHealth p_sheepHealth)
    {
        sheepHealth = p_sheepHealth;
    }
}

public class SphereGameObjectArgs : EventArgs
{
    public Vector3 center;
    public float radius;
    public GameObject gameObject;

    public SphereGameObjectArgs(Vector3 p_center, float p_radius, GameObject p_gameObject)
    {
        center = p_center;
        radius = p_radius;
        gameObject = p_gameObject;
    }
}

public class IntegerArgs : EventArgs
{
    public int integer;

    public IntegerArgs(int p_integer)
    {
        integer = p_integer;
    }
}

public class FloatArgs : EventArgs
{
    public float number;

    public FloatArgs(float p_number)
    {
        number = p_number;
    }
}

public static class GameEvents
{

    public static EventHandler LoadFromSave;
    
    public static EventHandler<SphereArgs> PlayerFollowCommandEvent;
    public static EventHandler<SphereGameObjectArgs> PlayerStayCommandEvent;
    public static EventHandler<TimeSphereArgs> PlayerRepelEvent;

    public static EventHandler AnySheepDeathEvent;
    public static EventHandler AnySheepAttackedEvent;

    public static EventHandler CanASheepWanderQuestion;
    public static EventHandler CanASheepWanderAnswer;

    public static EventHandler EnteredCamp;
    public static EventHandler ExitedCamp;

    public static EventHandler AddStaffMagic;
    public static EventHandler RemoveStaffMagic;

    public static EventHandler<GameObject> FakePlayerGoToPoint;
    public static EventHandler<GameObject> FakePlayerSwitchOn;
    public static EventHandler<GameObject> FakePlayerSwitchOff;
    public static EventHandler HidePlayer;
    public static EventHandler ShowPlayer;

    public static EventHandler<FloatArgs> GetTimeOfDay;
    public static EventHandler<FloatArgs> SetTimeOfDay;

    public static EventHandler<bool> UpdatePlacePostEvent;

    public static EventHandler DeactivateEasyMode;

}
