using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CineCoyoteState {Howling, Walking};

public class Cinematic_AI : MonoBehaviour
{
    CineCoyoteState currentState = CineCoyoteState.Walking;

    private AudioSource howl;
    private bool hasHowled = false;

    Coyote_Howling coyote_howling;
    Coyote_Walking coyote_walking;

  

    void Start()
    {
        coyote_howling = GetComponent<Coyote_Howling>();
        coyote_walking = GetComponent<Coyote_Walking>();
    }


    void Update()
    {
        switch (currentState)
        {
            case (CineCoyoteState.Howling):coyote_howling.Howling_Update();
            Debug.Log("Howling_Update case has occured");
                break;
            default: coyote_walking.Walking_Update();
                break;
        }
    }

    public void SwapStateCine()
    {
        //call this to revert to walking state
       if (currentState == CineCoyoteState.Howling)
        {
            currentState = CineCoyoteState.Walking;
        }
        
    }

    public void SwapStateCineReversed()
    {
        //call this to revert to walking state
       if (currentState == CineCoyoteState.Walking)
        {
            currentState = CineCoyoteState.Howling;
        }
        
    }

    /*private void EnterWalkingState()
    {
        if (currentState == CineCoyoteState.Howling)
        {
            currentState = CineCoyoteState.Walking;
        }
    }*/


    // use a trigger where you want the howl to be and have the ontriggerenter swap the states calling EnterWalkingState





    /*private void Awake()
    {
        howl = GetComponent<AudioSource>();
    }*/


    //public void Howl()
    //{
    //if (!hasHowled)
    //{
    //hasHowled = true;
    //howl.Play();

    //foreach (var shp in sheep)
    //{
    //Debug.Log("Found a sheep");
    //current_sheep = shp;
    //shp.run_scatter = true;
    //}
    //}
    //}

}





