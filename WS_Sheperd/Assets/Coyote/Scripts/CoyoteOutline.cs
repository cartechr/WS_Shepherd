using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoyoteOutline : MonoBehaviour
{

    [SerializeField] private Material coyoteOutlineMaterial;
    [SerializeField] private SkinnedMeshRenderer coyoteRenderer;

    // this will change during gameplay
    [SerializeField] public float outlineDistance = 10f;

    private Coyote_Controller myCoyoteController;
    private Material[] originalMaterialArray;
    private Material[] altMaterialArray;
    
    // Start is called before the first frame update
    void Start()
    {
        myCoyoteController = GetComponent<Coyote_Controller>();
        originalMaterialArray = coyoteRenderer.materials;
        altMaterialArray = new Material[coyoteRenderer.materials.Length + 1];
        for (int i = 0; i < coyoteRenderer.materials.Length; i++) {
            altMaterialArray[i] = coyoteRenderer.materials[i];
        }
        altMaterialArray[coyoteRenderer.materials.Length] = coyoteOutlineMaterial;

        outlineDistance = myCoyoteController.Player.repelRadius;
    }

    // Update is called once per frame
    void Update()
    {
        if (myCoyoteController.currentState == CoyoteState.Combat && 
            Vector3.Distance(transform.position, myCoyoteController.Player.transform.position) < outlineDistance) {
            coyoteRenderer.materials = altMaterialArray;
        } else {
            coyoteRenderer.materials = originalMaterialArray;
        }
    }
}