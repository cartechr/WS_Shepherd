using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Coyote_Die : Coyote_State
{
    public Transform[] waypoints;
    public int speed;
    private int waypointIndex;
    private float dist;
    public GameObject Despawn;

    // we want the coyote to activate this script after being hit three times
    // and when three have occured, activate the waypointing system to have the coyote despawn 
    //in a certain area

    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
        waypointIndex = 0;
        transform.LookAt(waypoints[waypointIndex].position);
    }

    public override void EnterState()
    {
        _coyoteController.animator.ResetTrigger("Stun");
        _coyoteController.animator.SetTrigger("Stun");
        _coyoteController.PauseMoving(2);
    }

    public void Die_Update()
    {
        dist = Vector3.Distance(transform.position, waypoints[waypointIndex].position);
        if(dist < 20.0f)
        {
            IncreaseIndex();
        }
        Retreat();
        //Debug.Log(dist);
    }

    void Retreat()
    {
        GetComponent<NavMeshAgent>().destination = Despawn.transform.position;
        GetComponent<NavMeshAgent>().speed = 8;
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Despawn"))
        {
            //Destroy(this.gameObject);
            Debug.Log("Coyote has despawned");
        }
        if (other.gameObject.CompareTag("CoyoteCrawl"))
        {
            _coyoteController.animator.SetBool("GoingAway", true);
            _coyoteController.myAgent.speed = 2f;
        }
    }



    void IncreaseIndex()
    {
        waypointIndex++;
        if(waypointIndex >= waypoints.Length)
        {
            waypointIndex = 0;
        }
        transform.LookAt(waypoints[waypointIndex].position);
    }
}
