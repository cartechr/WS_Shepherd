using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.AI;

public enum CoyoteState {Combat, Repel, Die};

public class Coyote_Controller : MonoBehaviour
{

    public bool activated = false;
    public CoyoteState currentState = CoyoteState.Combat;

    Coyote_Combat fov_ai;
    Coyote_Repel coyote_repel;
    Coyote_Die coyote_die;
    int repelHealth = 3;

    private float originalStoppingDistance;
    private float originalAcceleration;

    public Animator animator;
    public NavMeshAgent myAgent;

    [SerializeField] public Player Player;

    private Vector3 initialPosition;
    private float loadingSickness = 0;

    // Timer for state swapped into repel and needs to recover before attacking again
    //public float repelTime = 10.0f;

    private void Awake()
    {
        // do not alter this variable, ever
        // it is used for save/load
        initialPosition = transform.position;
    }

    // Start is called before the first frame update
    void Start()
    {
        fov_ai = GetComponent<Coyote_Combat>();
        coyote_repel = GetComponent<Coyote_Repel>();
        coyote_die = GetComponent<Coyote_Die>();
        animator = GetComponent<Animator>();
        myAgent = GetComponent<NavMeshAgent>();
        originalStoppingDistance = myAgent.stoppingDistance;
        originalAcceleration = myAgent.acceleration;
        GameEvents.LoadFromSave += LoadFromGameController;
        LoadFromGameController(this, EventArgs.Empty);

        if (!Player) {
            Player = GameController.Instance().player;
        }
    }

    private void OnDestroy()
    {
        GameEvents.LoadFromSave -= LoadFromGameController;
    }

    private void OnTriggerEnter(Collider other)
    {
        // I need it to wait out a physics frame when loading
        if (other.CompareTag("MainCamera") && loadingSickness <= 0) {
            Activate();
        }
    }

    public void Activate()
    {
        activated = true;
        tutorialGameEvents.Coyote?.Invoke(this, EventArgs.Empty);
    }

    // Update is called once per frame
    void Update()
    {
        loadingSickness -= Time.deltaTime;
        if (!activated) {
            return;
        }
        animator.SetFloat("AngleToPlayer", Vector3.SignedAngle((Player.transform.position - transform.position), transform.forward, Vector3.up));
        animator.SetFloat("Speed", myAgent.velocity.magnitude);
        animator.SetFloat("TurnAngle", Vector3.SignedAngle((myAgent.destination - transform.position), transform.forward, Vector3.up));
        switch (currentState)
        {
            case (CoyoteState.Combat): 
                fov_ai.Combat_Update();
                break;
            case (CoyoteState.Repel):
                coyote_repel.Repel_Update();
                break;
            case (CoyoteState.Die):
                coyote_die.Die_Update();
                break;

        }
        
        SaveToGameController();
    }

    public void EnterRepelState()
    {
        currentState = CoyoteState.Repel;
        coyote_repel.EnterState();


    }
    
    public void RepelByPlayer()
    {
        Debug.Log("R is pressed");
        if (currentState == CoyoteState.Repel)
        {
            return;
        }

        repelHealth--;

        if(repelHealth <= 0)
        {
            currentState = CoyoteState.Die;
            coyote_die.EnterState();
        }
        else {
            animator.ResetTrigger("Repel");
            animator.SetTrigger("Repel");
            myAgent.destination = transform.position;
            myAgent.stoppingDistance = 1000000;
            Tutorial.isRepelled = true;
            Debug.Log("isRepelled = " + Tutorial.isRepelled);

            StartCoroutine(PauseForRepel(2));
        }

        // add timer that remembers how many times the coyote has been hit and when timer is
        // is over, revert back to attack script, when hit three times
        // swap state to despawn script

    }

    public IEnumerator PauseForRepel(float secs)
    {
        myAgent.acceleration = 50;
        yield return new WaitForSeconds(secs);
        EnterRepelState();
        myAgent.stoppingDistance = originalStoppingDistance;
        myAgent.acceleration = originalAcceleration;
    }

    public IEnumerator PauseMoving(float secs)
    {
        myAgent.stoppingDistance = 100000;
        myAgent.acceleration = 50;
        yield return new WaitForSeconds(secs);
        myAgent.stoppingDistance = originalStoppingDistance;
        myAgent.acceleration = originalAcceleration;
    }

  
    public void EnterAttackState()
    {
        if (currentState == CoyoteState.Die) {
            return;
        }
        FMODUnity.RuntimeManager.PlayOneShot("event:/Audio/Coyote Agression");
        currentState = CoyoteState.Combat;
        
    }

    public void EnterDieState()
    {
        currentState = CoyoteState.Die;
        coyote_die.EnterState();
    }

    public IEnumerator SwapStateTimer(float repel_duration)
    {
        //wait n seconds
        yield return new WaitForSeconds(repel_duration);
        EnterAttackState();
    }

    public void CoyoteStep(string path)
    {
        FMODUnity.RuntimeManager.PlayOneShot(path, GetComponent<Transform>().position);
    }

    public void CoyoteBite(string path)
    {
        FMODUnity.RuntimeManager.PlayOneShot(path, GetComponent<Transform>().position);
    }
    
    void LoadFromGameController(object sender, EventArgs args)
    {
        GameController gc = GameController.Instance();
        int key = initialPosition.GetHashCode();
        if (gc.savedCoyotes.ContainsKey(key)) {
            (bool loadActivated, int loadState, int loadRepelHealth, Vector3 loadPos, Vector3 loadRot) = gc.savedCoyotes[key];
            activated = loadActivated;
            currentState = (CoyoteState) loadState;
            repelHealth = loadRepelHealth;
            if (loadPos != Vector3.negativeInfinity) { //sentinel for invalid loads
                myAgent.Warp(loadPos);
                transform.position = loadPos;
                myAgent.Warp(loadPos);
            }
            if (loadRot != Vector3.negativeInfinity) { //sentinel for invalid loads
                transform.eulerAngles = loadRot;
            }
            // this CAN cause some bugs but I'll leave that to the speedrunners
            StopAllCoroutines();
            fov_ai.StopAllCoroutines();
            coyote_repel.StopAllCoroutines();
            coyote_die.StopAllCoroutines();
            myAgent.stoppingDistance = originalStoppingDistance;
            loadingSickness = 0.2f;
        } else {
            SaveToGameController();
        }
    }

    void SaveToGameController()
    {
        GameController gc = GameController.Instance();
        int key = initialPosition.GetHashCode();
        gc.savedCoyotes[key] = (activated, (int) currentState, repelHealth, transform.position, transform.eulerAngles);
    }
    
        
#if UNITY_EDITOR
    public void OnDrawGizmos()
    {
        Handles.Label(transform.position + Vector3.up * 2, currentState.ToString());
    }
#endif
    
}
