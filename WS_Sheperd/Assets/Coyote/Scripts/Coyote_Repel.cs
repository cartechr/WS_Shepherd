 using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;

public class Coyote_Repel : Coyote_State
{
    public GameController gameController;

    private bool isWandering = false;
    private bool isWalking = false;
    private string repel_pattern = ""; // debug
    [SerializeField] private float repel_time = 6.0f;
    [SerializeField] private float repel_scale = 10f;

    private NavMeshAgent agent;

    // Start is called before the first frame update
    protected override void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        base.Start();

        if (!gameController) {
            gameController = GameController.Instance();
        }
    }

    public override void EnterState()
    {
        isWandering = false;
    }

    // Update is called once per frame
    public void Repel_Update()
    {
        if (!isWandering)
        {
            Debug.Log("start wandering");
            StartCoroutine(Wander());
        }
      
    }
    //FIX this with Will
    IEnumerator Wander()
    {
        isWandering = true;
    
        Vector3 player_pos = _coyoteController.Player.transform.position;
        Vector3 towardsPlayer = (player_pos - transform.position);
        towardsPlayer.y = 0;
        towardsPlayer = towardsPlayer.normalized;
        Vector3 awayFromPlayer = -towardsPlayer;
        Debug.DrawRay(player_pos, awayFromPlayer * 10, new Color(1, 0.5f, 0), 8);
        Quaternion situation_rotation = Quaternion.LookRotation(awayFromPlayer);

        Vector3[] flank_right = new[]
        {
            new Vector3(0.125f, 0f, 0.750f),
            new Vector3(1.000f, 0f, 1.000f),
            new Vector3(0.750f, 0f, 0.125f)
        };
        
        Vector3[] flank_left = new[]
        {
            new Vector3(-0.125f, 0f, 0.750f),
            new Vector3(-1.000f, 0f, 1.000f),
            new Vector3(-0.750f, 0f, 0.125f)
        };
        
        Vector3[] double_back = new[]
        {
            new Vector3(0.000f, 0f, 1.414f),
            new Vector3(0.000f, 0f, 1.414f),
            new Vector3(0.000f, 0f, 0.750f)
        };
        
        Vector3[] circle_right = new[]
        {
            new Vector3(Mathf.Sin(1), 0f, Mathf.Cos(1)),
            new Vector3(Mathf.Sin(2), 0f, Mathf.Cos(2)),
            new Vector3(Mathf.Sin(3), 0f, Mathf.Cos(3)),
            new Vector3(Mathf.Sin(4), 0f, Mathf.Cos(4)),
            new Vector3(Mathf.Sin(5), 0f, Mathf.Cos(5))
        };
        
        Vector3[] circle_left = new[]
        {
            new Vector3(-Mathf.Sin(1), 0f, Mathf.Cos(1)),
            new Vector3(-Mathf.Sin(2), 0f, Mathf.Cos(2)),
            new Vector3(-Mathf.Sin(3), 0f, Mathf.Cos(3)),
            new Vector3(-Mathf.Sin(4), 0f, Mathf.Cos(4)),
            new Vector3(-Mathf.Sin(5), 0f, Mathf.Cos(5))
        };
        

        transformPath(ref flank_right, repel_scale * Vector3.one, situation_rotation, player_pos);
        transformPath(ref flank_left, repel_scale * Vector3.one, situation_rotation, player_pos);
        transformPath(ref double_back, repel_scale * Vector3.one, situation_rotation, player_pos);
        transformPath(ref circle_right, repel_scale * Vector3.one, situation_rotation, player_pos);
        transformPath(ref circle_left, repel_scale * Vector3.one, situation_rotation, player_pos);

        bool can_flank_right = proposePath(ref flank_right);
        bool can_flank_left = proposePath(ref flank_left);
        bool can_double_back = proposePath(ref double_back);
        bool can_circle_right = proposePath(ref double_back);
        bool can_circle_left = proposePath(ref circle_left);

        float repel_duration = repel_time;

        List<Vector3[]> viable_paths = new List<Vector3[]>();
        List<string> viable_path_names = new List<string>();
        if (can_flank_right) {
            viable_paths.Add(flank_right);
            viable_path_names.Add("Flank Right");
        }
        if (can_flank_left) {
            viable_paths.Add(flank_left);
            viable_path_names.Add("Flank Left");
        }
        if (can_double_back) {
            viable_paths.Add(double_back);
            viable_path_names.Add("Double Back");
        }
        if (can_circle_right) {
            viable_paths.Add(circle_right);
            viable_path_names.Add("Circle Right");
        }
        if (can_circle_left) {
            viable_paths.Add(circle_left);
            viable_path_names.Add("Circle Left");
        }

        
        Vector3[] path = {transform.position}; // default path if all else fails
        if (viable_paths.Count > 0) {
            int selection = Random.Range(0, viable_paths.Count);
            path = viable_paths[selection];
            repel_pattern = viable_path_names[selection];
            // these are longer paths
            if (path == circle_right || path == circle_left) {
                repel_duration *= 1.5f;
            }
        }

        for (int i = 0; i < path.Length - 1; i++) {
            Debug.DrawLine(path[i], path[i + 1], Color.yellow, 8);
        }

        _coyoteController.StartCoroutine(_coyoteController.SwapStateTimer(repel_duration));
        
        foreach (var point in path) {
            agent.destination = point;
            yield return new WaitForSeconds (repel_duration/path.Length);
        }

        /*NavMeshHit hit;
        int scatterPoints = 3; 
        for(int scatter = 0; scatter < scatterPoints; scatter++) {


            Debug.DrawRay(transform.position, opposeDirection - transform.position, Color.yellow);
            GetComponent<NavMeshAgent>().destination = opposeDirection;
            if(scatter != 0)
            {
                opposeDirection = Random.insideUnitSphere * 10;
                opposeDirection += transform.position;
            }
            
            NavMesh.SamplePosition(opposeDirection, out hit, 10, 1);
            Vector3 finalPosition = hit.position;
            Debug.Log(opposeDirection);
            GetComponent<NavMeshAgent>().destination = finalPosition;
            yield return new WaitForSeconds (8.0f/scatterPoints); // 6 seconds of repelling, divided out by 3 different points of scatter
        }
        */

        //GetComponent<NavMeshAgent>().destination = transform.position;
    }

    private bool proposePath(ref Vector3[] points)
    {
        for (int i = 0; i < points.Length; i++){
            if (!proposePoint(ref points[i])) {
                return false;
            }
        }
        return true;
    }

    private bool proposePoint(ref Vector3 point)
    {
        NavMesh.SamplePosition(point, out NavMeshHit hit, 20, NavMesh.AllAreas);
        if (!hit.hit) {
            return false;
        }

        point = hit.position;
        return IsReachable(point);
    }

    private void transformPath(ref Vector3[] path, Vector3 scale, Quaternion rotation, Vector3 translation)
    {
        scalePath(ref path, scale);
        rotatePath(ref path, rotation);
        translatePath(ref path, translation);
    }
    
    private void rotatePath(ref Vector3[] path, Quaternion rotation)
    {
        for (int i = 0; i < path.Length; i++) {
            path[i] = rotation * path[i];
        }
    }

    private void translatePath(ref Vector3[] path, Vector3 translation)
    {
        for (int i = 0; i < path.Length; i++) {
            path[i] += translation;
        }
    }

    private void scalePath(ref Vector3[] path, Vector3 scale)
    {
        for (int i = 0; i < path.Length; i++) {
            path[i].Scale(scale);
        }
    }

    private bool IsReachable(Vector3 position)
    {
        NavMeshPath path = new NavMeshPath();
        agent.CalculatePath(position, path);
        bool reachable = (path.status != NavMeshPathStatus.PathPartial);
        return reachable;
    }
    
    private void OnDrawGizmos()
    {
        if (!_coyoteController) {return;}
        if (_coyoteController.currentState != CoyoteState.Repel) {return;}
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireCube(GetComponent<NavMeshAgent>().destination, Vector3.one);
#if UNITY_EDITOR
        Handles.Label(transform.position + Vector3.up * 1.75f + Vector3.down * 2, repel_pattern);
#endif
    }
    

}
