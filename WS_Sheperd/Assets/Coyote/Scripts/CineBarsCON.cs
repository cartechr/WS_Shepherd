using System.Collections;
using UnityEngine;

public class CineBarsCON : MonoBehaviour
{
   public static CineBarsCON Instance { get; private set; }

    [SerializeField] private GameObject CineBarContainerGO;
    [SerializeField] private Animator cinematicBarsAnimator;

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else if (Instance != this)
            Destroy(gameObject);
    }

    public void ShowBars()
    {
        CineBarContainerGO.SetActive(true);
    }

    public void HideBars()
    {
        if (CineBarContainerGO.activeSelf)
            StartCoroutine(HideBarsAndDisableGO());
    }

    private IEnumerator HideBarsAndDisableGO()
    {
        cinematicBarsAnimator.SetTrigger("HideBars");
        yield return new WaitForSeconds(0.5f);
        CineBarContainerGO.SetActive(false);
    }

    public void HideBarsImmediate()
    {
        CineBarContainerGO.SetActive(false);
    }



}
