﻿using UnityEngine;
using System.Collections;

public class animationTrigger : MonoBehaviour {
	//remember to go to the animator on the object you are animating and add a parameter in the parameters menu!  It must be the EXACT same name as the trigger you enter.
	public string parameterName;
	public bool currentTriggerState;  

	//Don't touch these variables
	private Animator anim;
	private GameObject player;
	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator> ();
		}
	
	// Update is called once per frame
	void Update () {
		currentTriggerState = anim.GetBool (parameterName);
	}

	void OnTriggerEnter (Collider other){
		//this will animate when you ENTER the collider
		if (!anim.GetBool (parameterName))
			anim.SetBool (parameterName, true);
		}
	void OnTriggerExit (Collider other){
		//this will stop animating when you EXIT the collider.  If you want this disabled, comment out this whole section or delete it.
		if (anim.GetBool (parameterName))
			anim.SetBool (parameterName, false);
	}
}
