using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoyoteHowl : MonoBehaviour
{
    private bool hasHowled = false;

    FMOD.Studio.EventInstance coyoteHowl;

    private void Awake()
    {
        coyoteHowl = FMODUnity.RuntimeManager.CreateInstance("event:/Howl");
    }




    private void Update()
    {

    }


    public void Howl()
    {
        if (!hasHowled)
        {
            coyoteHowl.start();
            hasHowled = true;
            FMODUnity.RuntimeManager.AttachInstanceToGameObject(coyoteHowl, this.transform, this.GetComponent<Rigidbody>());


        }
    }
}

