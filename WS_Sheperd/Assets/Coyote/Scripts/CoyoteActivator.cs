using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoyoteActivator : MonoBehaviour
{
    [SerializeField] private Coyote_Controller myCoyote;
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player")) {
            myCoyote.Activate();
        }
    }
}
