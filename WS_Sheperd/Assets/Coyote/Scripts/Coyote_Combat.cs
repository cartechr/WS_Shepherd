using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;

public class Coyote_Combat : Coyote_State
{
    [SerializeField] private float targetrange;
    [SerializeField] private float killrange;
    [SerializeField] public int CoyoteDamage;
    [SerializeField] public int speed;

    public GameController gamecontroller;
    //public Player player;

    bool is_attacking = false;
    SheepHealth target;
    private TimeSphereArgs lastRepelTimeSphere = null;

    protected override void Start()
    {
        base.Start();
        //player = GetComponent<Player>();
        GameEvents.PlayerRepelEvent += OnPlayerRepel;

        if (!gamecontroller) {
            gamecontroller = GameController.Instance();
        }
    }

    private void OnDestroy()
    {
        GameEvents.PlayerRepelEvent -= OnPlayerRepel;
    }

    // Update is called once per frame
    public void Combat_Update()
    {

        if (lastRepelTimeSphere != null) {
            if (Vector3.Distance(lastRepelTimeSphere.center, transform.position) < lastRepelTimeSphere.radius &&
                Time.time < lastRepelTimeSphere.time) {
                _coyoteController.RepelByPlayer();
            }
        }

        // target nearest
        float nearest_distance = float.PositiveInfinity;
        foreach (SheepHealth sheep in SheepHealth.GetSheepList()) {
            float distance = Vector3.Distance(transform.position, sheep.transform.position);
            if (distance < targetrange && distance < nearest_distance) {
                nearest_distance = distance;
                //Sheep within range
                //this.transform.LookAt(sheep.transform.position);
                target = sheep;
                GetComponent<NavMeshAgent>().destination = sheep.transform.position;
                //Debug.Log("Sheep Found: " + target);
            }
        }
        // still null, no sheep
        if (target == null) {
            _coyoteController.EnterRepelState();
            return;
        }
        
        // chase and attack
        //this.transform.LookAt(target.transform.position);
        GetComponent<NavMeshAgent>().destination = target.transform.position;
        Debug.Log("Walk towards sheep");

        if (Vector3.Distance(transform.position, target.transform.position) < killrange)
        {
            Debug.Log("Attacked Sheep");
            //player.followplayer = false;
            _coyoteController.EnterRepelState();
            FMODUnity.RuntimeManager.PlayOneShot("event:/Audio/CoyoteBite");
            target.friendshipStats.DecreaseFriendship(15);
            GameEvents.AnySheepAttackedEvent?.Invoke(this, EventArgs.Empty);
            if (target.TakeDamage(CoyoteDamage)) //killed sheep
            {
                // the sheep is dead!
                target = null;
                _coyoteController.animator.ResetTrigger("Kill");
                _coyoteController.animator.SetTrigger("Kill");
                StartCoroutine(_coyoteController.PauseMoving(2));
                _coyoteController.EnterDieState();
            } else {
                // sheep not dead
                string[] attacks = {"Attack1", "Attack2", "Attack3", "Attack4"};
                int currentAttack;
                if (_coyoteController.myAgent.velocity.magnitude > 6f) {
                    currentAttack = Random.Range(0, 3);
                } else {
                    currentAttack = 3;
                }

                _coyoteController.animator.ResetTrigger(attacks[currentAttack]);
                _coyoteController.animator.SetTrigger(attacks[currentAttack]);
                if (currentAttack != 4) {
                    StartCoroutine(_coyoteController.PauseMoving(1.5f));
                }
            }
        }

    }

    void OnPlayerRepel(object sender, TimeSphereArgs args)
    {
        lastRepelTimeSphere = args;
    }
    
}


