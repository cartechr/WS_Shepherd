using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Howl_Trigger : MonoBehaviour
{
    private GameObject coyote;
    private bool hasHowled = false;

    FMOD.Studio.EventInstance coyoteHowl;

    // Start is called before the first frame update
    void Awake()
    {
        coyote = GameObject.Find("Coyote Scene Parent");
        coyoteHowl = FMODUnity.RuntimeManager.CreateInstance("event:/Audio/Howl");
    }
    void Start()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player") && hasHowled == false)
        {
            //coyote.GetComponent<CoyoteHowl>().Howl();
            coyoteHowl.start();
            hasHowled = true;
            FMODUnity.RuntimeManager.AttachInstanceToGameObject(coyoteHowl, coyote.transform, coyote.GetComponent<Rigidbody>());
        }
        Debug.Log("Howl");
    }
}
