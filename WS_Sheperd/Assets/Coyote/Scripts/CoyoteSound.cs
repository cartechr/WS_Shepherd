using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// This script is a copy of the CoyoteStep and CoyoteBite functions from Coyote_Controller
/// in order to not cause errors with the Cinematic Coyote (since he relies on those functions)
/// lmk if this causes issues
/// </summary>

public class CoyoteSound : MonoBehaviour
{
    public void CoyoteStep(string path)
    {
        FMODUnity.RuntimeManager.PlayOneShot(path, GetComponent<Transform>().position);
    }

    public void CoyoteBite(string path)
    {
        FMODUnity.RuntimeManager.PlayOneShot(path, GetComponent<Transform>().position);
    }
}
