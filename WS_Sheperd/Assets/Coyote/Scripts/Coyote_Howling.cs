using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Coyote_Howling : MonoBehaviour
{
    protected Agent agent;
    Coyote_Walking coyote_walking;
    Cinematic_AI cinematic_ai;

    //FMOD STUFF
    FMOD.Studio.EventInstance coyoteHowl;
    private bool hasHowled = false;


    void Start()
    {
        coyote_walking = GetComponent<Coyote_Walking>();
        cinematic_ai = GetComponent<Cinematic_AI>();
        agent = GetComponent<Agent>();
        //FMOD-ben stuff
        coyoteHowl = FMODUnity.RuntimeManager.CreateInstance("event:/Audio/Howl");
        
        //coyote_rigidbody = GetComponent<Rigidbody>();
       // coyote_transform = GetComponent<Transform>();
    }
    
    //StartCoroutine(WaitToHowl());

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Howl_WP") && hasHowled == false)
        {
            agent.SetStop(true);
            Howling_Update();
            cinematic_ai.SwapStateCineReversed();
            coyoteHowl.start();
            FMODUnity.RuntimeManager.AttachInstanceToGameObject(coyoteHowl, GetComponent<Transform>(), GetComponent<Rigidbody>());
            hasHowled = true;
        }
    }


    public void Howling_Update()
    {
        StartCoroutine(WaitToHowl());
        Debug.Log("Ready to Howl");
    }


    private IEnumerator WaitToHowl()
    {
        //wait 6 seconds
        yield return new WaitForSeconds(10);
        cinematic_ai.SwapStateCine();
    }
}
