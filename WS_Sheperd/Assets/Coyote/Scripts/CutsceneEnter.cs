using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CutsceneEnter : MonoBehaviour
{

    // This script is not currently being used but only serves as a template.

    // setting the speed to not be zero is the dumbest activation mechanism
    public Coyote_Walking my_cinematic_coyote;

    public GameObject CM_vcam1;
    public GameObject CM_vcam3;

    private void Start()
    {
        GameObject playerObject = GameObject.FindGameObjectWithTag("Player");
        CM_vcam1 = playerObject.transform.Find("CM vcam1").gameObject;
        CM_vcam3 = playerObject.transform.Find("CM vcam3").gameObject;
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Move"))
        {
            //this.gameObject.GetComponent<BoxCollider>().enabled = false;
            CM_vcam3.SetActive(true);
            CM_vcam1.SetActive(false);

            StartCoroutine(FinishCut());
        }
        if (other.gameObject.CompareTag("Player"))
        {
            my_cinematic_coyote.speed = 3f;
            Debug.Log("Coyote is moving to Howl");
            gameObject.SetActive(false);
        }
        
    }

    IEnumerator FinishCut()
    {
        yield return new WaitForSeconds(9);
        CM_vcam1.SetActive(true);
        CM_vcam3.SetActive(false);
    }

}
