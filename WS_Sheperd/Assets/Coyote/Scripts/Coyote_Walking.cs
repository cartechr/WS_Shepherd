using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coyote_Walking : MonoBehaviour
{
    //public Transform[] waypoints;
    //public int speed;

    //public int waypointIndex;
    // private float dist;
    public GameObject Null_Wall;
    public GameObject Move;

    public GameObject[] waypoints;
    Animator anim;
    [SerializeField] float rotSpeed = 0.8f;
    [SerializeField]public float speed = 0.0f;
    float accuracyWP = 2.0f;
    int currentWP = 0;

    List<Transform> path = new List<Transform>();

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        foreach (GameObject go in waypoints)
        {
            if (go) { // null check
                path.Add(go.transform);
            }
        }
        currentWP = FindClosestWP();
        //anim.SetBool("isWalking", true); this line is for walking animation
    }

    int FindClosestWP()
    {
        if (path.Count == 0) return -1;
        int closest = 0;
        float lastDist = Vector3.Distance(this.transform.position, path[0].position);
        for(int i = 1; i < path.Count; i++)
        {
            float thisDist = Vector3.Distance(this.transform.position, path[i].position);
            if(lastDist > thisDist && i != currentWP)
            {
                closest = i;
            }
        }
        return closest;
    }

    // Update is called once per frame
    public void Walking_Update()
    {
        Vector3 direction = path[currentWP].position - transform.position;
        this.transform.rotation = Quaternion.Slerp(transform.rotation,
                             Quaternion.LookRotation(direction), rotSpeed * Time.deltaTime);
        this.transform.Translate(0, 0, Time.deltaTime * speed);
        if(direction.magnitude < accuracyWP)
        {
            path.Remove(path[currentWP]);
            currentWP = FindClosestWP();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Move"))
        {
            speed = 6f;
            Debug.Log("Cinematic Coyote moving");
        }

        if (other.gameObject.CompareTag("Null_Wall"))
        {
            Destroy(this.gameObject);
            Debug.Log("touched null wall");
        }
    }
}
