using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coyote_State : MonoBehaviour
{
    protected Coyote_Controller _coyoteController;
    
    // Start is called before the first frame update
    protected virtual void Start()
    {
        _coyoteController = GetComponent<Coyote_Controller>();
    }

    public virtual void EnterState()
    {
        //
    }
    
    public virtual void ExitState()
    {
        //
    }

}
