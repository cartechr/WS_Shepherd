using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindChange : MonoBehaviour
{
    public WindZone windZone;

    void Start()
    {
        StartCoroutine(ChangeWind());
    }

    IEnumerator ChangeWind()
    {
        while (true)
        {
            float startAngle = windZone.transform.rotation.eulerAngles.y;
            float endAngle = Random.Range(0, 360);
            float startSpeed = windZone.windMain;
            float endSpeed = Random.Range(1, 3);

            float t = 0f;
            while (t < 1f)
            {
                t += Time.deltaTime / 7f;
                windZone.transform.rotation = Quaternion.Euler(0, Mathf.Lerp(startAngle, endAngle, t), 0);
                windZone.windMain = Mathf.Lerp(startSpeed, endSpeed, t);
                yield return null;
            }

            yield return new WaitForSeconds(8);
        }
    }
}
