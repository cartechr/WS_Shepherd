using UnityEngine;

public class WindBoneReactor : MonoBehaviour
{
    public WindZone windZone;
    public Transform[] bones;
    public float swayAmount = 5.0f;

    private Quaternion[] originalRotations;

    void Start()
    {
        originalRotations = new Quaternion[bones.Length];
        for (int i = 0; i < bones.Length; i++)
        {
            originalRotations[i] = bones[i].localRotation;
        }
    }

    void Update()
    {
        for (int i = 0; i < bones.Length; i++)
        {
            Transform bone = bones[i];
            Vector3 windForce = windZone.transform.forward * windZone.windMain * swayAmount;
            Quaternion rotation = Quaternion.LookRotation(windForce, Vector3.up);
            bone.localRotation = Quaternion.Lerp(originalRotations[i], rotation, Time.deltaTime);
        }
    }
}




